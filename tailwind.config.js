/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
        "node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}",
    ],

    theme: {
        extend: {
            spacing: {
                18: "4.5rem",
                128: "134rem",
                82: "22rem",
                100: "30rem",
                136: "34rem",
                30: "120px",
            },
            colors: {
                "primary-blue": "#1A56DB",
                "primary-black": "#2d2f31",
                "primary-gray": "#6a6f73",
                "primary-hover-gray": "#1739531f",
            },
            boxShadow: {
                st: "0 -2px 4px rgb(0 0 0 / 8%), 0 -4px 12px rgb(0 0 0 / 8%)",
            },
            flexGrow: {
                2: "2",
            },
            keyframes: {
                fadeIn: {
                    "0%": { opacity: 0 },
                    "100%": { opacity: 1 },
                },
            },
            animation: {
                "fade-in": "fadeIn 250ms linear 250ms forwards",
            },
            transitionDuration: {
                400: "400ms",
            },
            backgroundImage: {
                "hero-pattern":
                    "url('https://kaidemy.b-cdn.net/avatar/h-high-resolution-logo-transparent.png')",
                // "footer-texture": "url('/img/footer-texture.png')",
            },
        },
    },
    variants: {
        extend: {
            // fontWeight: ["responsive", "hover", "focus"],
            // opacity: ["hover"],
            // borderColor: ["hover", "focus"],
            // margin: ["first", "last"],
            // backgroundColor: ["odd", "even"],
            // scale: ["hover", "active", "group-hover"],
            display: ["group-hover", "group-focus"],
        },
    },
    plugins: [require("flowbite/plugin"), require("daisyui")],
};
