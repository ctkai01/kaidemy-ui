interface Config {
    apiUrl: string;
}

const config: Config = {
    apiUrl: import.meta.env.VITE_REACT_APP_API_URL || "http://localhost:3000",
   
};

export default config;
