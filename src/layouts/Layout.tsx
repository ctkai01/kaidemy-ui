import { Outlet } from "react-router-dom";
import Footer from "../components/ui/core/Footer";
import Header from "../components/ui/core/Header/Header";


function Layout() {
    return (
        <>
            <Header />
            <Outlet />
            <Footer />
            {/* <Banner />
            <div className="mb-64 mt-16 px-56">
                <Show />
                <TopCategories />
                <TeachIntro />
            </div> */}
        </>
    );
}

export default Layout;
