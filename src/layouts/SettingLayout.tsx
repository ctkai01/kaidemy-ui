import { Avatar, CustomFlowbiteTheme, Flowbite, Sidebar } from "flowbite-react";
import { Link, Outlet, useLocation } from "react-router-dom";
import {
    ACCOUNT_NORMAL,
    EDIT_ACCOUNT_PATH,
    EDIT_PHOTO_PATH,
    EDIT_PROFILE_PATH,
} from "../constants";
import { useAppSelector } from "../hooks/redux-hook";
import { selectUserAuth } from "../services/state/redux/authSlide";
import { akaName } from "../utils";

const customTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            initials: {
                base: "bg-primary-black flex justify-center items-center",
                text: "text-white font-bold text-3xl",
            },
            size: {
                xl: "w-30 h-30",
            },
        },
    },
};

const customSidebarTheme: CustomFlowbiteTheme = {
    sidebar: {
        root: {
            inner: "bg-white",
        },
        item: {
            base: "px-4 py-1 block hover:bg-primary-gray hover:text-white",
            active: "bg-primary-gray text-white",
        },
        items: "py-6 ",
        itemGroup: "",
    },
};

export default function SettingLayout() {
    const location = useLocation();
    const user = useAppSelector(selectUserAuth);

    return (
        <div className="flex-1">
            <div className="mx-auto  flex max-w-[1164px] px-6 pb-12 pt-6">
                <div>
                    <Flowbite theme={{ theme: customSidebarTheme }}>
                        <Sidebar
                            className="border border-r-0 border-[#d1d7dc]"
                            aria-label="Sidebar with logo branding example"
                        >
                            <div className="p-4">
                                <Flowbite theme={{ theme: customTheme }}>
                                    {user?.avatar ? (
                                        <Avatar
                                            img={user.avatar}
                                            alt="avatar"
                                            rounded
                                            size="xl"
                                        />
                                    ) : (
                                        <Avatar
                                            placeholderInitials={akaName(
                                                user ? user.name : "",
                                            )}
                                            rounded
                                            size="xl"
                                        />
                                    )}
                                </Flowbite>
                            </div>
                            {/* max-width: 100%; text-overflow: ellipsis;
                            white-space: nowrap; overflow: hidden; padding: 0
                            16px; */}
                            <div className="max-w-full overflow-hidden text-ellipsis whitespace-nowrap px-4 text-center text-base font-bold text-primary-black">
                                {user?.name}
                            </div>
                            <Sidebar.Items>
                                <Sidebar.ItemGroup>
                                    <Link to={`${user?.id}`}>
                                        <Sidebar.Item>
                                        View public profile
                                        </Sidebar.Item>
                                    </Link>

                                    <Link to={"edit-profile"}>
                                        <Sidebar.Item
                                            active={
                                                location.pathname ===
                                                EDIT_PROFILE_PATH
                                            }
                                        >
                                          Profile
                                        </Sidebar.Item>
                                    </Link>
                                    <Link to={"edit-photo"}>
                                        <Sidebar.Item
                                            active={
                                                location.pathname ===
                                                EDIT_PHOTO_PATH
                                            }
                                        >
                                            Photo
                                        </Sidebar.Item>
                                    </Link>

                                    {user?.typeAccount === ACCOUNT_NORMAL && (
                                        <Link to={"edit-account"}>
                                            <Sidebar.Item
                                                active={
                                                    location.pathname ===
                                                    EDIT_ACCOUNT_PATH
                                                }
                                            >
                                               Account Security
                                            </Sidebar.Item>
                                        </Link>
                                    )}
                                </Sidebar.ItemGroup>
                            </Sidebar.Items>
                        </Sidebar>
                    </Flowbite>
                </div>
                <div className="flex-1">
                    <Outlet />
                </div>
            </div>
        </div>
    );
}
