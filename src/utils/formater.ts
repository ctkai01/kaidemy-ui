// import { formatDistanceToNow } from 'date-fns';
// import viLocale from 'date-fns/locale/vi';
import dayjs from "dayjs";
import "dayjs/locale/vi";
import relativeTime from "dayjs/plugin/relativeTime";
import utc from "dayjs/plugin/utc";
import timezone from "dayjs/plugin/timezone";
import localizedFormat from "dayjs/plugin/localizedFormat";
//    dayjs.extend(utc);
dayjs.extend(relativeTime);

// dayjs.extend(timezone);
//    dayjs.extend(localizedFormat);
dayjs.extend(utc);
dayjs.extend(timezone);

// Set the default time zone to Asia/Ho_Chi_Minh
//    dayjs.tz.setDefault("Asia/Ho_Chi_Minh");
export function formatNumberWithCommas(price: number) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function formatMonthLongYear(date: string): string {
    const inputDate = new Date(date);
    const formattedDate = inputDate.toLocaleDateString("vi", {
        month: "long",
        year: "numeric",
    });

    // Capitalize "Tháng" and "Năm"
    const capitalizedDate = formattedDate.replace(/(^\w|\s\w)/g, (match) =>
        match.toLocaleUpperCase(),
    );

    return capitalizedDate;
}

export function formatDateFull(date: string): string {
    const inputDate = new Date(date);
    const formattedDate = inputDate.toLocaleDateString("vi", {
        month: "long",
        year: "numeric",
        hour: "2-digit",
        day: "numeric",
        minute: "numeric",
    });

    // Capitalize "Tháng" and "Năm"
    const capitalizedDate = formattedDate.replace(/(^\w|\s\w)/g, (match) =>
        match.toLocaleUpperCase(),
    );

    return capitalizedDate;
}

export function formatBetweenNumberPaginate(
    currentPage: number,
    totalPerPage: number,
    total: number,
): string {
    const remainItem = total - (currentPage - 1) * totalPerPage;
    const toIndex =
        remainItem >= totalPerPage
            ? currentPage * totalPerPage
            : (currentPage - 1) * totalPerPage + remainItem;
    return `${(currentPage - 1) * totalPerPage + 1}-${toIndex}`;
}

export function akaName(name: string): string {
    const splitName = name.trim().split(/\s+/); // Trim whitespace and split by any whitespace
    if (splitName.length === 0) {
        return ""; // Return empty string if input is empty or only whitespace
    } else if (splitName.length === 1) {
        return splitName[0].charAt(0).toUpperCase();
    } else {
        const firstInitial = splitName[0].charAt(0).toUpperCase();
        const lastInitial = splitName[splitName.length - 1]
            .charAt(0)
            .toUpperCase();
        return `${firstInitial}${lastInitial}`;
    }
}

export function formatNumber(number: number): string {
    let formattedNumber = number.toLocaleString();
    return formattedNumber;
}

export function formatDistanceToNowTime(time: string, isAdd = false): string {
    // return dayjs.tz(time).locale("vi").fromNow();
    // return dayjs(time).tz("Asia/Ho_Chi_Minh").fromNow();

    return isAdd
        ? dayjs.utc(time).add(7, "hour").tz("Asia/Ho_Chi_Minh").fromNow()
        : dayjs.utc(time).tz("Asia/Ho_Chi_Minh").fromNow();
    // return dayjs.tz(time, "Asia/Ho_Chi_Minh").fromNow();

    // return dayjs(time).tz("Asia/Ho_Chi_Minh").fromNow();
    // return dayjs.utc(time).tz("Asia/Ho_Chi_Minh").fromNow();
    // return dayjs.tz(time, "Asia/Ho_Chi_Minh").fromNow();
}
