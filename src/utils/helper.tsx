import { toast } from "react-toastify";
import {
    LECTURE_RESOURCE_ASSET_TYPE,
    LECTURE_TYPE,
    LECTURE_WATCH_ASSET_TYPE,
    QUIZ_TYPE,
} from "../constants";
import {
    Asset,
    ContentArticle,
    ContentCourse,
    ContentQuiz,
    ContentVideo,
    Course,
    CourseShow,
    Curriculum,
    Learning,
    Lecture,
    NavigateLecture,
    Notification,
    OverallCategoryCourse,
} from "../models";

interface NumberStar {
    number: number;
    isHalf: boolean;
}
export function numberStar(count: number): NumberStar {
    const number = Math.floor(count);
    let result: NumberStar = {
        number: number,
        isHalf: false,
    };
    if (count % 1 === 0) {
        return result;
    } else {
        result.isHalf = true;
        return result;
    }
}

export function setAuthLocalStorage(token: string) {
    localStorage.setItem("auth-token", token);
}

export function getAuthLocalStorage(): string | null {
    return localStorage.getItem("auth-token");
}

export function clearAuthLocalStorage() {
    localStorage.removeItem("auth-token");
}

export function setUserLocalStorage(user: string) {
    localStorage.setItem("auth-user", user);
}

export function getUserLocalStorage(): string | null {
    return localStorage.getItem("auth-user");
}

export function customToast(text: string) {
    const notify = () =>
        toast(<div className="font-bold">{text}</div>, {
            draggable: false,
            position: "top-center",
            type: "success",
        });
    return notify;
}

export function convertToHHMM(minutes: number): string {
    const hours = Math.floor(minutes / 60);
    const remainingMinutes = minutes % 60;

    const hoursString = String(hours).padStart(2, "0");
    const minutesString = String(remainingMinutes).padStart(2, "0");

    return `${hoursString}:${minutesString}`;
}

export function convertSecondsToHoursMinutes(totalSeconds: number): string {
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const seconds = totalSeconds % 60;

    return `${hours}h ${minutes}m ${seconds}s`;
}

export function formatBytes(bytes: number, decimals = 2) {
    if (!+bytes) return "0 b";

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
}

export function convertPercentComplete(course: Course): number {
    let totalPercent = 0;
    //Percent Intended learner
    if (course.outComes?.length) {
        totalPercent += 25 / 3;
    }

    if (course.intendedFor?.length) {
        totalPercent += 25 / 3;
    }

    if (course.requirements?.length) {
        totalPercent += 25 / 3;
    }

    //curriculum
    if (course.curriculums.length) {
        totalPercent += 25;
    }

    //landing page
    if (course.title) {
        totalPercent += 5;
    }

    if (course.subtitle) {
        totalPercent += 5;
    }

    if (course.description) {
        totalPercent += 5;
    }

    if (course.languageId) {
        totalPercent += 5;
    }

    if (course.levelId) {
        totalPercent += 5;
    }

    if (course.categoryId) {
        totalPercent += 5;
    }

    if (course.subCategoryId) {
        totalPercent += 5;
    }

    if (course.primarilyTeach) {
        totalPercent += 5;
    }

    if (course.image) {
        totalPercent += 5;
    }

    if (course.priceId) {
        totalPercent += 5;
    }

    return Math.floor(totalPercent);
}

// function pe
export function isTopicComplete(
    course: Course,
    topicTitle: string,
    curriculums: Curriculum[],
): boolean {
    if (topicTitle === "Intended learners") {
        let isComplete = true;
        if (
            !course.outComes?.length ||
            !course.intendedFor?.length ||
            !course.requirements?.length
        ) {
            isComplete = false;
        }
        return isComplete;
    }

    if (topicTitle === "Curriculum") {
        let isComplete = true;
        if (!curriculums.length) {
            isComplete = false;
        }
        return isComplete;
    }

    if (topicTitle === "Course landing page") {
        let isComplete = true;
        if (
            !course.title ||
            !course.subtitle ||
            !course.description ||
            !course.languageId ||
            !course.levelId ||
            !course.categoryId ||
            !course.subCategoryId ||
            !course.primarilyTeach ||
            !course.image
        ) {
            isComplete = false;
        }
        return isComplete;
    }

    if (topicTitle === "Pricing") {
        let isComplete = true;
        if (!course.priceId) {
            isComplete = false;
        }
        return isComplete;
    }

    return false;
}

export function formatDate(dateString: string): string {
    const date = new Date(dateString);
    const formattedDate = `${date.getMonth() + 1}/${date.getFullYear()}`;

    return formattedDate;
}

export function getCoursePurchaseByID(
    coursePurchase: Learning[],
    courseID: number,
): Learning | undefined {
    return coursePurchase.find((item) => item.courseID === courseID);
}

export function formatDayMonthYear(dateString: string): string {
    const date = new Date(dateString);

    // Options for formatting the date to Vietnamese
    const options = {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour12: false,
        timeZone: "Asia/Ho_Chi_Minh", // Assuming Vietnam time zone
    };
    const vietnameseDate = date.toLocaleDateString("en-US", options);

    return vietnameseDate;
}
export function generateTextEllipsis(text: string): string {
    if (text.length > 50) {
        return text.slice(0, 50) + "…";
    } else {
        return text;
    }
}

export function generatePrefixNavigateLecture(
    lectureNavigate: NavigateLecture,
): string {
    if (lectureNavigate.lecture.type === QUIZ_TYPE) {
        return `Quiz ${lectureNavigate.indexNumber}: ${lectureNavigate.lecture.title}`;
    } else {
        return `${lectureNavigate.indexNumber}. ${lectureNavigate.lecture.title}`;
    }
}
export function initContentCourse(
    curriculums: Curriculum[],
): ContentCourse | null {
    if (curriculums.length && curriculums[0].lectures) {
        if (curriculums[0].lectures.length) {
            if (curriculums[0].lectures[0].type === QUIZ_TYPE) {
                const contentQuiz: ContentQuiz = {
                    // article: curriculums[0].lectures[0].article,
                    indexNumber: "Quiz 1:",
                    title: curriculums[0].lectures[0].title,
                    questions: curriculums[0].lectures[0].questions,
                };
                return {
                    id: curriculums[0].lectures[0].id,
                    type: "quiz",
                    content: contentQuiz,
                    index: 0,
                    curriculumID: curriculums[0].id,
                };
            } else {
                if (curriculums[0].lectures[0].article) {
                    const contentArticle: ContentArticle = {
                        article: curriculums[0].lectures[0].article,
                        title: curriculums[0].lectures[0].title,
                    };
                    return {
                        id: curriculums[0].lectures[0].id,
                        type: "article",
                        content: contentArticle,
                        index: 0,
                        curriculumID: curriculums[0].id,
                    };
                }

                const assetWatch = curriculums[0].lectures[0].assets.find(
                    (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
                );

                if (assetWatch) {
                    const contentVideo: ContentVideo = {
                        url: assetWatch.url,
                    };
                    return {
                        id: curriculums[0].lectures[0].id,
                        type: "video",
                        content: contentVideo,
                        index: 0,
                        curriculumID: curriculums[0].id,
                    };
                } else {
                    return null;
                }
            }
            // const assetWatch = curriculums[0].lectures[0].assets.find(
            //     (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
            // );
        } else {
            return null;
        }
    } else {
        return null;
    }

    // course.curriculums ?  course.curriculums[0].lectures ? course.curriculums[0].lectures[0].assets   : []
}
const MINUTE = 60;
export const totalTimeLecture = (lectures: Lecture[]): string => {
    const totalSeconds = lectures.reduce((total, lecture, _) => {
        const assetWatch: Asset | undefined = lecture.assets.find(
            (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
        );
        if (assetWatch) {
            return total + assetWatch.duration;
        }

        if (lecture.type === LECTURE_TYPE && lecture.article) {
            return total + MINUTE;
        }
        return total;
    }, 0);

    return convertSecondsToHoursMinutes(totalSeconds);
};

export const totalTimeAsset = (assets: Asset[]): number => {
    const totalSeconds = assets.reduce((total, asset, _) => {
        if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
            return total + asset.duration;
        } else {
            return total;
        }
    }, 0);
    return totalSeconds;
};

export function getResources(assets: Asset[]): Asset[] {
    const resources = assets.filter(
        (asset) => asset.type === LECTURE_RESOURCE_ASSET_TYPE,
    );

    return resources;
}

export interface InfoIndexLecture {
    index: number;
    type: "quiz" | "lecture";
}

export function getTitleLectureByID(
    course: CourseShow,
    id: number,
): InfoIndexLecture {
    const indexLecture = {
        quiz: 1,
        article: 1,
        general: 0,
    };

    let result: InfoIndexLecture;

    for (let i = 0; i < course.curriculums.length; i++) {
        const curriculum = course.curriculums[i];
        if (curriculum.lectures) {
            for (let j = 0; j < curriculum.lectures.length; j++) {
                const lecture = curriculum.lectures[j];

                const indexNumber =
                    lecture.type === QUIZ_TYPE
                        ? indexLecture.quiz++
                        : indexLecture.article++;
                if (lecture.id === id) {
                    result = {
                        index: indexNumber,
                        type: lecture.type === QUIZ_TYPE ? "quiz" : "lecture",
                    };

                    return result;
                }
            }
        }
    }

    return {
        index: -1,
        type: "quiz",
    };
}

export function getCountLevel(
    name: string,
    overallCategoryCourse: OverallCategoryCourse,
): number {
    if (name === "Beginner") {
        return overallCategoryCourse?.beginnerLevelCount
            ? overallCategoryCourse.beginnerLevelCount
            : 0;
    }

    if (name === "Intermediate") {
        return overallCategoryCourse?.intermediateLevelCount
            ? overallCategoryCourse.intermediateLevelCount
            : 0;
    }

    if (name === "Expert") {
        return overallCategoryCourse?.expertLevelCount
            ? overallCategoryCourse.expertLevelCount
            : 0;
    }

    if (name === "All") {
        return overallCategoryCourse?.allLevelCount
            ? overallCategoryCourse.allLevelCount
            : 0;
    }
    return 0;
}

export const isAnyUnreadNotification = (
    notification: Notification[],
): boolean => {
    return notification.some((notification) => !notification.isRead);
};
