import { NavigateFunction, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { Learning, User } from "../models";
import { Cart } from "../models/cart";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { PaymentServiceApi } from "../services/api/paymentServiceApi";
import { selectUserAuth } from "../services/state/redux/authSlide";
import { addCartItem, removeCartItem } from "../services/state/redux/cartSlide";
import {
    addWishList,
    removeWishList,
    selectWishList,
} from "../services/state/redux/wishListSlide";
import { useAppDispatch, useAppSelector } from "./redux-hook";

export default function useAddWishList(
    courseID: number,
    user: User | undefined,
    navigate: NavigateFunction,
    dispatch: any,
    cart: Cart | null,
): [() => Promise<void>] {
    const handleAddWishList = async () => {
        if (!user) {
            navigate("/login");
        } else {
            try {
                //Add wishlist
                const dataCreate = await CourseServiceApi.addWishList(courseID);
                dispatch(addWishList(dataCreate));

                //Check exist Cart
                if (cart) {
                    const cartExist = cart.cartItems.find(
                        (item) => item.course.id === courseID,
                    );
                    if (cartExist) {
                        await PaymentServiceApi.removeCartItem(cartExist.course.id);
                        dispatch(removeCartItem(cartExist.course.id));
                    }
                }

                toast(
                    <div className="font-bold">
                        Add to Wishlist successfully!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            } catch (e) {
                toast(
                    <div className="font-bold">
                        Add to Wishlist failed!

                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "error",
                        theme: "colored",
                    },
                );
            }
        }
    };
    return [handleAddWishList];
}
