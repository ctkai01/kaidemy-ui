import { NavigateFunction, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { Learning, User } from "../models";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { PaymentServiceApi } from "../services/api/paymentServiceApi";
import { selectUserAuth } from "../services/state/redux/authSlide";
import { addCartItem } from "../services/state/redux/cartSlide";
import {
    removeWishList,
    selectWishList,
} from "../services/state/redux/wishListSlide";
import { useAppDispatch, useAppSelector } from "./redux-hook";

export default function useAddCart(
    courseID: number,
    user: User | undefined,
    navigate: NavigateFunction,
    dispatch: any,
    wishList: Learning[],
): [() => Promise<void>] {
    const addCart = async () => {
        if (!user) {
            navigate("/login");
        } else {
            try {
                const dataAddCartItem =
                    await PaymentServiceApi.addCartItem(courseID);
                dispatch(addCartItem(dataAddCartItem));

                //Check whistlist
                const isWishListExist = wishList.find(
                    (item) => item.courseID === courseID,
                );
                if (isWishListExist) {
                    await CourseServiceApi.removeWishList(isWishListExist.id);
                    dispatch(removeWishList(isWishListExist.id));
                }

                toast(
                    <div className="font-bold">
                        Added to cart successfully!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            } catch (e) {
                console.log(e);
                toast(<div className="font-bold">Added to cart failed!</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                });
            }
        }
    };
    return [addCart];
}
