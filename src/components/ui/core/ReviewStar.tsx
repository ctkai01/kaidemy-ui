import { numberStar } from "../../../utils";
import { BsStarFill, BsStarHalf, BsStar } from "react-icons/bs";
import React from "react";
interface IReviewStar {
    number: number;
    size?: number;
    color?: string;
}
const MAX_STAR = 5;
const ReviewStar: React.FC<IReviewStar> = (props: IReviewStar) => {
    const { number, color, size } = props;
    const star = numberStar(number);
    const colorSet = color
    return (
        <div className="flex items-center">
            {number ? (
                <>
                    {Array.from({ length: star.number }).map((_, i) => (
                        <BsStarFill
                            key={i}
                            className={`mr-[2px] h-${size} w-${size} text-[#c99920]`}
                        />
                    ))}
                    {star.isHalf && (
                        <BsStarHalf
                            className={`mr-[2px] h-${size} w-${size} text-[#c99920]`}
                        />
                    )}
                    {Array.from({
                        length: MAX_STAR - star.number - (star.isHalf ? 1 : 0),
                    }).map((_, index) => (
                        <BsStar
                            key={index}
                            className={`mr-[2px] h-${size} w-${size} text-[#c99920]`}
                        />
                    ))}
                </>
            ) : (
                <>
                    {Array.from({ length: MAX_STAR }).map((_, index) => (
                        <BsStar
                            key={index}
                            className={`mr-[2px] h-${size} w-${size} text-[#c99920]`}
                        />
                    ))}
                </>
            )}
        </div>
    );
};

ReviewStar.defaultProps = {
    color: "#c99920",
    size: 4
};

export default ReviewStar;
