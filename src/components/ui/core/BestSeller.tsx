interface IBestSeller {
    className?: string;
}

const BestSeller = (props: IBestSeller) => {
    const { className } = props;
    return (
        <div
            className={`${className} inline-block bg-[#ECEB98] px-2 py-1 text-[12px] font-bold text-[#3D3C0A]`}
        >
            Best seller
        </div>
    );
};

export default BestSeller;
