import { menuClass } from "../../../../data/common";
import MenuItem, { ItemMenu } from "./MenuItem";
interface IDropdown {
    subMenus: ItemMenu[];
    depthLevel: number;
    group: string;
    // menuClass: MenuClass;
}

const Dropdown = (props: IDropdown) => {
    let { depthLevel, subMenus, group } = props;
    depthLevel += 1;
    return (
        <>
            {depthLevel == 1 ? (
                <ul
                    className={`${
                        // menuClass[`group-hover/developments:block`]
                        menuClass[`group-hover/${group}:block`]
                        // menuClass["group/developments"]
                    } absolute  left-0 mr-1 hidden rounded-md border bg-white shadow-md`}
                >
                    {subMenus.map((subMenu, index) => (
                        <MenuItem
                            // menuClass={menuClass}
                            key={index}
                            item={subMenu}
                            depthLevel={depthLevel}
                            groupName={subMenu.groupName}
                        />
                    ))}
                </ul>
            ) : (
                <ul
                    className={`${
                        menuClass[`group-hover/${group}:block`]
                    } absolute -right-2 top-0   hidden translate-x-full rounded-md border bg-white shadow-md`}
                    // className="absolute right-0  top-0 translate-x-full rounded-md border bg-white shadow-md group-hover/item:block"
                >
                    {subMenus.map((subMenu, index) => (
                        <MenuItem
                            // menuClass={menuClass}
                            key={index}
                            item={subMenu}
                            depthLevel={depthLevel}
                            groupName={subMenu.groupName}
                        />
                    ))}
                </ul>
            )}
        </>
    );
};

export default Dropdown;
