import { Progress } from "flowbite-react";
import { Link } from "react-router-dom";
import { Learning } from "../../../../models";

interface IMyLearningItem {
    courseLearning: Learning;
}

const MyLearningItem = (props: IMyLearningItem) => {
    const { courseLearning } = props;
    return (
        <div className="relative flex border-b border-[#d1d7dc] p-4 last:border-b-0">
            <div className="relative h-16 w-16 overflow-hidden">
                <img
                    className="absolute left-1/2 top-0 h-full w-auto max-w-none -translate-x-1/2"
                    src={
                        courseLearning.course.image
                            ? courseLearning.course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
            </div>
            <div className="w-[192px] px-2 text-[#2D2F31]">
                <Link
                    className="mb-1 line-clamp-2 font-bold after:absolute after:left-0 after:top-0  after:h-full after:w-full after:content-['']"
                    to={`/course/${courseLearning.course.id}/learn`}
                >
                    {courseLearning.course.title}
                </Link>
                {0 ? (
                    <Progress progress={0} />
                ) : (
                    <div className="font-bold text-primary-blue">
                        Start learning
                    </div>
                )}
            </div>
        </div>
    );
};

export default MyLearningItem;
