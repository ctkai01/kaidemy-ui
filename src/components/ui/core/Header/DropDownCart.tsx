import { Button } from "flowbite-react";
import { useMemo } from "react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import { Cart } from "../../../../models/cart";
import CartItemShow from "./CartItemShow";

interface IDropDownCart {
    cart: Cart | null;
}

const DropDownCart = (props: IDropDownCart) => {
    const { cart } = props;

    const totalMoney = useMemo(() => {
        if (cart) {
            const total = cart.cartItems.reduce((total, cartItem) => {
                const price = cartItem.course.price
                    ? cartItem.course.price.value
                    : 0;
                return price + total;
            }, 0);
            return total;
        }
    }, [cart]);

    console.log("cart.cartItems: ", cart);
    return (
        <div className="absolute -bottom-3 right-0 hidden  min-w-[288px] translate-y-full  border border-[#d1d7dc]  bg-white after:absolute  after:-top-[14px] after:h-[20px] after:w-full  after:content-[''] group-hover:block">
            {cart && cart.cartItems.length ? (
                <>
                    <div className="max-h-100 overflow-auto">
                        {cart.cartItems.map((carItem) => (
                            <CartItemShow
                                key={carItem.cartID}
                                cartItem={carItem}
                            />
                        ))}
                    </div>
                    <div className="border-t border-[#d1d7dc] bg-white p-4 shadow-st">
                        <div className="mb-2 flex text-[19px] font-bold">
                            <div className="mr-1">Total: </div>
                            <div className="flex items-center">
                                <BsCurrencyDollar />
                                <span>{totalMoney}</span>
                            </div>
                        </div>
                        <Link to={"/cart"}>
                            <Button color="dark" className="w-full font-bold">
                                Go to cart
                            </Button>
                        </Link>
                    </div>
                </>
            ) : (
                <div className="p-4 text-center">
                    <div className="mb-4 text-center text-base text-[#6a6f73]">
                        Your cart is empty
                    </div>
                    <Link
                        to={"/"}
                        className="text-center text-sm font-bold text-primary-blue"
                    >
                        Keep shopping
                    </Link>
                </div>
            )}
        </div>
    );
};

export default DropDownCart;
