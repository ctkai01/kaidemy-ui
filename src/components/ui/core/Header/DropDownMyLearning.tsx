import { Button } from "flowbite-react";
import { Link } from "react-router-dom";

import { useAppSelector } from "../../../../hooks/redux-hook";
import { selectCoursePurchase } from "../../../../services/state/redux/coursePurchaseSlide";
import MyLearningItem from "./MyLearningItem";

const MAX_SHOW = 4;
const DropDownMyLearning = () => {
    let coursePurchase = useAppSelector(selectCoursePurchase);
    coursePurchase = coursePurchase.slice(0, MAX_SHOW);

    return (
        <div className="absolute -bottom-3 right-0 hidden  translate-y-full border  border-[#d1d7dc] bg-white after:absolute  after:-top-[14px] after:h-[20px] after:w-full  after:content-[''] group-hover:block">
            {coursePurchase.map((courseLearning, index) => (
                <MyLearningItem key={index} courseLearning={courseLearning} />
            ))}
            {coursePurchase.length ? (
                <div className="p-4">
                    <Link to={"/home/my-courses/learning"}>
                        <Button color="dark" className="w-full font-bold">
                            Go to my learning
                        </Button>
                    </Link>
                </div>
            ) : (
                <div className="w-[192px] p-4 text-center font-bold  text-primary-black">
                    You don’t have any courses!
                </div>
            )}
        </div>
    );
};

export default DropDownMyLearning;
