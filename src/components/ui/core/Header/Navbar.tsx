import MenuItem, { ItemMenu } from "./MenuItem";
import MenuSkeleton from "./MenuSkeleton";

interface INavbarProp {
    menuItems: ItemMenu[];
    loading: boolean
}


// const menuClass: MenuClass = {}
// const menuClass: MenuClass = {
//     "": "",
//     "group/development": "group/development",
//     "group-hover/development": "group-hover/development",
//     "group/categories": "group/categories",
//     "group-hover/categories:block": "group-hover/categories:block",
//     "group-hover/development:block": "group-hover/development:block",
//     "group/business": "group/business",
//     "group-hover/business:block": "group-hover/business:block",
//     "group/finance_&_accounting": "group/finance_&_accounting",
//     "group-hover/finance_&_accounting:block":
//         "group-hover/finance_&_accounting:block",
//     "group/it_&_software": "group/it_&_software",
//     "group-hover/it_&_software:block": "group-hover/it_&_software:block",
//     "group/office_productivity": "group/office_productivity",
//     "group-hover/office_productivity:block":
//         "group-hover/office_productivity:block",
//     "group/personal_development": "group/personal_development",
//     "group-hover/personal_development:block":
//         "group-hover/personal_development:block",
//     "group/design": "group/design",
//     "group-hover/design:block": "group-hover/design:block",
//     "group/marketing": "group/marketing",
//     "group-hover/marketing:block": "group-hover/marketing:block",
//     "group/lifestyte": "group/lifestyte",
//     "group-hover/lifestyte:block": "group-hover/lifestyte:block",
//     "group/photography_&_video": "group/photography_&_video",
//     "group-hover/photography_&_video:block":
//         "group-hover/photography_&_video:block",
//     "group/heath_&_fitness": "group/heath_&_fitness",
//     "group-hover/heath_&_fitness:block": "group-hover/heath_&_fitness:block",
//     "group/music": "group/music",
//     "group-hover/music:block": "group-hover/music:block",
//     "group/teaching_&_academics": "group/teaching_&_academics",
//     "group-hover/teaching_&_academics:block":
//     "group-hover/teaching_&_academics:block",
// };
// menuClass[""] = ""
// menuClass["group/development"] = "group/development";
// menuClass["group-hover/development"] = "group-hover/development";
const Navbar = (props: INavbarProp) => {
    const { menuItems, loading } = props;
    return (
        <nav className="relative">
            <ul className="flex">
                <div className="relative">
                    <li className="group/categories min false  block cursor-pointer px-3 text-sm font-bold text-gray-700 hover:text-primary-blue  dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white">
                        <div>
                            <button className="my-3 h-12 max-h-18">
                                Categories
                            </button>
                            <ul className="absolute left-0  mr-1 hidden rounded-md border bg-white shadow-md group-hover/categories:block">
                                {loading && (
                                    <MenuSkeleton/>
                                )}
                                {!loading &&
                                    menuItems.map((menu, index) => {
                                        const depthLevel = 1;
                                        // console.log("Menu: ", menu);
                                        return (
                                            <MenuItem
                                                key={index}
                                                item={menu}
                                                // menuClass={classMenus}
                                                depthLevel={depthLevel}
                                                groupName={menu.groupName}
                                            />
                                        );
                                    })}
                            </ul>
                        </div>
                    </li>
                </div>
            </ul>
        </nav>
    );
};

export default Navbar;
