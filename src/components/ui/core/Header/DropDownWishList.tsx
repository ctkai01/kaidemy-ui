import { Button } from "flowbite-react";
import { Link } from "react-router-dom";
import { Learning } from "../../../../models";
import WishListCardItem from "./WishListCardItem";

interface IDropDownWishList {
    wishList: Learning[];
}

const DropDownWishList = (props: IDropDownWishList) => {
    const { wishList } = props;
    return (
        <div className="absolute -bottom-3 right-0 hidden  min-w-[288px] translate-y-full  border border-[#d1d7dc]  bg-white after:absolute  after:-top-[14px] after:h-[20px] after:w-full  after:content-[''] group-hover:block">
            {wishList.length ? (
                <>
                    {wishList.map((wishCourse, _) => (
                        <WishListCardItem
                            key={wishCourse.id}
                            wishCourse={wishCourse}
                        />
                    ))}

                    <div className="p-4">
                        <Link to={"/home/my-courses/wishlist"}>
                            <Button color="dark" className="w-full font-bold">
                                Go to wishlist
                            </Button>
                        </Link>
                    </div>
                </>
            ) : (
                <div className="p-4 text-center">
                    <div className="mb-4 text-center text-base text-[#6a6f73]">
                        Your wishlist is empty
                    </div>
                    <Link
                        to={"/"}
                        className="text-center text-sm font-bold text-primary-blue"
                    >
                        Explore courses
                    </Link>
                </div>
            )}
        </div>
    );
};

export default DropDownWishList;
