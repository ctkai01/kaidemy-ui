import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Tabs,
    TabsComponent,
    TabsRef,
} from "flowbite-react";
import React, { useEffect, useRef, useState } from "react";
import { TEACHER_ROLE } from "../../../../constants";
import {
    NotificationType,
    NOTIFICATION_STUDENT,
    NOTIFICATION_TEACHER,
} from "../../../../constants/notification";
import { useAppDispatch, useAppSelector } from "../../../../hooks/redux-hook";
import { Notification, PaginationInfo } from "../../../../models";
import { NotificationServiceApi } from "../../../../services/api/notificationServiceApi";
import { selectUserAuth } from "../../../../services/state/redux/authSlide";
import {
    readAllNotificationStudent,
    readAllNotificationTeacher,
    selectNotificationStudent,
    selectNotificationTeacher,
} from "../../../../services/state/redux/notificationSlide";
import { isAnyUnreadNotification } from "../../../../utils";
import NotificationItem from "./Notification/NotificationItem";

const customTheme: CustomFlowbiteTheme = {
    tab: {
        base: "flex flex-col",
        tablist: {
            styles: {
                underline:
                    "flex-wrap -mb-px border-b border-gray-200 dark:border-gray-700",
            },
            tabitem: {
                base: "flex text-primary-gray items-center flex-1 justify-center p-4 rounded-t-lg text-sm font-medium first:ml-0 disabled:cursor-not-allowed disabled:text-gray-400 disabled:dark:text-gray-500 font-bold",
            },
        },
        tabpanel: "",
    },
};
// const NOTIFICATION_TABS = {
//     TEACHER: 0,
//     STUDENT: 1,
// };
const NOTIFICATION_SIZE = 100;
const DropDownNotification = () => {
    const tabsRef = useRef<TabsRef>(null);
    // const [currentPageNotificationTeacher, setCurrentPageNotificationTeacher] =
    //     useState(1);
    // const [currentPageNotificationStudent, setCurrentPageNotificationStudent] =
    //     useState(1);
    // const containerRef = useRef<HTMLDivElement>(null);
    const [_, setActiveTab] = useState(0);
    const user = useAppSelector(selectUserAuth);
    // const notificationsTeacher = useAppSelector(selectNotificationTeacher);

    // console.log(setCurrentPageNotificationTeacher);
    // console.log(setCurrentPageNotificationStudent);

    // const notificationsStudent = useAppSelector(selectNotificationStudent);
    // const [notificationsTeacher, setNotificationsTeacher] = useState<
    //     Notification[]
    // >([]);
    // const [notificationsStudent, setNotificationsStudent] = useState<
    //     Notification[]
    // >([]);
    // const [
    //     infoNotificationTeacherPagination,
    //     setInfoNotificationTeacherPagination,
    // ] = React.useState<PaginationInfo>({
    //     totalItem: 0,
    //     totalPage: 0,
    //     size: 0,
    // });
    // console.log(infoNotificationTeacherPagination);

    const [
        infoNotificationStudentPagination,
        setInfoNotificationStudentPagination,
    ] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    // const dispatch = useAppDispatch();
    // const [isLoadingTeacher, setIsLoadingTeacher] = useState(false);
    // const [isLoadingStudent, setIsLoadingStudent] = useState(false);

    // console.log("notificationsStudent: ", notificationsStudent);
    // console.log("notificationsTEacher: ", notificationsTeacher);
    // console.log("isLoadingStudent: ", isLoadingStudent);
    // console.log("isLoadingTeacher: ", isLoadingTeacher);
    // useEffect(() => {
    //     const fetchNotificationTeacher = async () => {
    //         try {
    //             setIsLoadingTeacher(true);
    //             // if (currentPageNotificationTeacher === 1) {
    //             //     dispatch(addListNotificationTeacher([]));
    //             // }
    //             const dataNotificationTeacher =
    //                 await NotificationServiceApi.getNotifications(
    //                     currentPageNotificationTeacher,
    //                     NOTIFICATION_SIZE,
    //                     NOTIFICATION_TEACHER,
    //                 );

    //             setInfoNotificationTeacherPagination({
    //                 totalPage: dataNotificationTeacher.meta.pageCount,
    //                 totalItem: dataNotificationTeacher.meta.itemCount,
    //                 size: dataNotificationTeacher.meta.size,
    //             });

    //             if (!dataNotificationTeacher?.item) {
    //             } else {
    //                 setNotificationsTeacher((prev) => [
    //                     ...prev,
    //                     ...dataNotificationTeacher.item,
    //                 ]);
    //                 // dispatch(
    //                 //     addListNotificationTeacher(
    //                 //         dataNotificationTeacher.notifications.items,
    //                 //     ),
    //                 // );
    //             }
    //             setIsLoadingTeacher(false);
    //             console.log("Teacher");
    //             console.log(
    //                 "dataNotificationTeacher: ",
    //                 dataNotificationTeacher,
    //             );
    //         } catch (e) {
    //             console.log(e);
    //         }
    //     };

    //     if (user) {
    //         fetchNotificationTeacher();
    //     }
    // }, [currentPageNotificationTeacher, user]);

    // useEffect(() => {
    //     const fetchNotificationStudent = async () => {
    //         try {
    //             // if (currentPageNotificationStudent === 1) {
    //             //     dispatch(addListNotificationStudent([]));
    //             // }

    //             const dataNotificationStudent =
    //                 await NotificationServiceApi.getNotifications(
    //                     currentPageNotificationStudent,
    //                     NOTIFICATION_SIZE,
    //                     NOTIFICATION_STUDENT,
    //                 );
    //             console.log(
    //                 "dataNotificationStudent: ",
    //                 dataNotificationStudent,
    //             );

    //             setInfoNotificationStudentPagination({
    //                 totalPage: dataNotificationStudent.meta.pageCount,
    //                 totalItem: dataNotificationStudent.meta.itemCount,
    //                 size: dataNotificationStudent.meta.size,
    //             });

    //             if (!dataNotificationStudent.item) {
    //             } else {
    //                 console.log(
    //                     "Hey wtf: ",
    //                     dataNotificationStudent.item,
    //                 );
    //                 // dispatch(
    //                 //     addListNotificationStudent(
    //                 //         dataNotificationStudent.notifications.items,
    //                 //     ),
    //                 // );
    //                 setNotificationsStudent((prev) => [
    //                     ...prev,
    //                     ...dataNotificationStudent.item,
    //                 ]);
    //             }
    //             setIsLoadingStudent(false);
    //         } catch (e) {
    //             console.log(e);
    //         }
    //     };
    //     if (user) {
    //         fetchNotificationStudent();
    //     }
    // }, [currentPageNotificationStudent, user]);
    const notificationsTeacher = useAppSelector(selectNotificationTeacher);
    const notificationsStudent = useAppSelector(selectNotificationStudent);
    const dispatch = useAppDispatch();

    const handleReadAll = async (type: NotificationType) => {
        try {
            const body = {
                type,
            };

            await NotificationServiceApi.readAllNotification(body);

            //Handle state redux
            if (type === NotificationType.INSTRUCTOR) {
                dispatch(readAllNotificationTeacher());
            } else {
                dispatch(readAllNotificationStudent());
            }
        } catch (err) {
            console.log("Error read all notification: ", err);
        }
    };

    return (
        <div className="absolute -bottom-3 right-0 hidden min-w-[360px]  translate-y-full border  border-[#d1d7dc] bg-white  shadow-xl after:absolute  after:-top-[14px] after:h-[20px] after:w-full  after:content-[''] group-hover:block">
            <div className="pl-4 pr-4 pt-4">
                <div className="text-[19px] font-bold text-[#2d2f31]">
                    Notifications
                </div>
            </div>
            <Flowbite theme={{ theme: customTheme }}>
                <TabsComponent
                    aria-label="Default tabs"
                    style="underline"
                    ref={tabsRef}
                    onActiveTabChange={(tab) => setActiveTab(tab)}
                >
                    {user?.role === TEACHER_ROLE && (
                        <Tabs.Item active title="Instructor">
                            <div className="max-h-[200px] overflow-y-scroll">
                                {notificationsTeacher.map((notification) => (
                                    <NotificationItem
                                        key={notification.id}
                                        notification={notification}
                                    />
                                ))}
                            </div>
                            {notificationsTeacher.length === 0 && (
                                <div className="p-4 text-center text-base text-[#6a6f73]">
                                    No notifications
                                </div>
                            )}
                            {notificationsTeacher.length &&
                            isAnyUnreadNotification(notificationsTeacher) ? (
                                <div className="flex justify-center border-t border-primary-hover-gray p-4">
                                    <Button
                                        className="font-bold"
                                        onClick={() =>
                                            handleReadAll(
                                                NotificationType.INSTRUCTOR,
                                            )
                                        }
                                    >
                                        Mark all as read
                                    </Button>
                                </div>
                            ) : (
                                <div className="flex justify-center border-t border-primary-hover-gray p-4"></div>
                            )}
                        </Tabs.Item>
                    )}

                    <Tabs.Item title="Student">
                        <div className="max-h-[200px] overflow-y-scroll">
                            {notificationsStudent.map((notification) => (
                                <NotificationItem
                                    key={notification.id}
                                    notification={notification}
                                />
                            ))}
                        </div>
                        {notificationsStudent.length === 0 && (
                            <div className="p-4 text-center text-base text-[#6a6f73]">
                                No notifications
                            </div>
                        )}

                        {notificationsStudent.length !== 0 &&
                        isAnyUnreadNotification(notificationsStudent) ? (
                            <div className="flex justify-center border-t border-primary-hover-gray p-4">
                                <Button
                                    className="font-bold"
                                    onClick={() =>
                                        handleReadAll(NotificationType.STUDENT)
                                    }
                                >
                                    Mark all as read
                                </Button>
                            </div>
                        ) : (
                            <div className="flex justify-center border-t border-primary-hover-gray p-4"></div>
                        )}

                        {/* <div className="flex justify-center border-t border-primary-hover-gray p-4">
                            <Button className="font-bold">
                                Đánh dấu đọc tất cả
                            </Button>
                        </div> */}
                    </Tabs.Item>
                </TabsComponent>
            </Flowbite>
        </div>
    );
};

export default DropDownNotification;
