import { Link } from "react-router-dom";
import { menuClass } from "../../../../data/common";
import Dropdown from "./Dropdown";
// import { MenuClass } from "./Header";


interface IMenuItemProp {
    // menu: ItemMenu;
    item: ItemMenu;
    depthLevel: number;
    groupName?: string;
    // menuClass: MenuClass;
}

export interface ItemMenu {
    name: string;
    id: number;
    // submenu?: ItemMenu[];
    groupName?: string;
    parentID: number | null;
    children: ItemMenu[] | null;
    // root: boolean;
}
// interface Menu {
//   name: string;
//   submenu?: Menu[];
//   root: boolean;
// }

const MenuItem = (props: IMenuItemProp) => {
    const { depthLevel, groupName, item } = props;
    return (
        // {props.depthLevel == 0 ? <button className="my-3 h-14">Categories</button> :
        // {}
        <div className="relative">
            {/* {props.depthLevel == 0 && (
        <button className="my-3 px-3 h-14">{props.item.name}</button>
      // )} */}
            {/* <ul className="my-3 h-14 pt-1 text-gray-700"> */}
            {/* <li className="h block cursor-pointer px-6 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white"> */}
            <li
                className={`${
                    // groupName ? `group/${groupName}` : ""
                    menuClass[`group/${groupName}`]
                    // menuClass[groupName ? `group/${item.groupName}` : ""]
                } min block  ${
                    depthLevel != 0 &&
                    "w-64 after:absolute after:-right-2 after:top-0 after:h-full after:w-2 after:content-[''] hover:bg-gray-100"
                }        
         cursor-pointer px-3 text-sm font-bold text-gray-700 hover:text-primary-blue  dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white`}
            >
                {/* <li className="group/main block cursor-pointer px-3 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-600 dark:hover:text-white"> */}
                {item.children && groupName ? (
                    <>
                        <Link
                            to={`/courses/categories/${item.id}`}
                            className={`${
                                depthLevel == 0
                                    ? ""
                                    : "flex items-center justify-between"
                            }`}
                        >
                            <button
                                className={`${
                                    depthLevel == 0
                                        ? "my-3 h-12 max-h-18"
                                        : `h-10`
                                }`}
                            >
                                {item.name}
                            </button>
                            {depthLevel != 0 && (
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="h-4 w-4"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M8.25 4.5l7.5 7.5-7.5 7.5"
                                    />
                                </svg>
                            )}
                        </Link>
                        <Dropdown
                            depthLevel={depthLevel}
                            subMenus={item.children}
                            group={groupName}
                        />
                    </>
                ) : (
                    <Link to={`/courses/categories/${item.id}`}>
                        <button
                            className={`${
                                depthLevel == 0 ? "my-3 h-12 max-h-18" : "h-10"
                            }`}
                        >
                            {item.name}
                        </button>
                    </Link>
                )}
            </li>
            {/* </ul> */}
        </div>
    );
};

export default MenuItem;
