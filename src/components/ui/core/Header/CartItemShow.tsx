// import { Button } from "flowbite-react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";

import { CartItem } from "../../../../models/cart";

interface ICartItemShow {
    cartItem: CartItem;
}

const CartItemShow = (props: ICartItemShow) => {
    const { cartItem } = props;
    return (
        <div className="relative border-b border-[#d1d7dc] p-4 last:border-b-0">
            <div className="flex">
                <div className="relative h-16 w-16 overflow-hidden">
                    <img
                        className="absolute left-1/2 top-0 h-full w-auto max-w-none -translate-x-1/2"
                        src={
                            cartItem.course.image
                                ? cartItem.course.image
                                : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                        }
                    />
                </div>
                <div className="w-[192px] px-2 text-[#2D2F31]">
                    <Link
                        className="mb-1 line-clamp-2 text-sm font-bold after:absolute after:left-0 after:top-0  after:h-full after:w-full after:content-['']"
                        to={`/course/${cartItem.course.id}`}
                    >
                        {cartItem.course.title}
                    </Link>
                    <div className="line-clamp-1 text-xs text-[#6a6f73]">
                        {cartItem.course.user.name}
                    </div>
                    <div className="flex items-center text-sm font-bold">
                        <BsCurrencyDollar />
                        {cartItem.course.price
                            ? cartItem.course.price.value
                            : 0}
                    </div>
                </div>
            </div>
            {/* <div className="mt-2">
                <Button
                    className="w-full font-bold"
                    outline
                    gradientDuoTone="cyanToBlue"
                >
                    Add to cart
                </Button>
            </div> */}
        </div>
    );
};

export default CartItemShow;
