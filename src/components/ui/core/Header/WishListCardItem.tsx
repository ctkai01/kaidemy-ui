import { Button } from "flowbite-react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { WishListCourse } from "../../../../data/common";
import { useAppDispatch } from "../../../../hooks/redux-hook";
import { Learning } from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import { PaymentServiceApi } from "../../../../services/api/paymentServiceApi";
import { addCartItem } from "../../../../services/state/redux/cartSlide";
import { removeWishList } from "../../../../services/state/redux/wishListSlide";

interface IWishListCardItem {
    wishCourse: Learning;
}

const WishListCardItem = (props: IWishListCardItem) => {
    const { wishCourse } = props;
    const dispatch = useAppDispatch();

    const handleAddCourse = async () => {
        try {
            const dataAddCartItem = await PaymentServiceApi.addCartItem(
                wishCourse.course.id,
            );
            dispatch(addCartItem(dataAddCartItem));

            //Remove
            await CourseServiceApi.removeWishList(wishCourse.id);
            dispatch(removeWishList(wishCourse.id));

            toast(<div className="font-bold">Added to cart succesfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Added to cart failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="relative border-b border-[#d1d7dc] p-4 last:border-b-0">
            <div className="flex">
                <div className="relative h-16 w-16 overflow-hidden">
                    <img
                        className="absolute left-1/2 top-0 h-full w-auto max-w-none -translate-x-1/2"
                        src={
                            wishCourse.course.image
                                ? wishCourse.course.image
                                : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                        }
                    />
                </div>
                <div className="w-[192px] px-2 text-[#2D2F31]">
                    <Link
                        className="mb-1 line-clamp-2 text-sm font-bold after:absolute after:left-0 after:top-0  after:h-full after:w-full after:content-['']"
                        to={`course/${wishCourse.courseID}`}
                    >
                        {wishCourse.course.title}
                    </Link>
                    <div className="line-clamp-1 text-xs text-[#6a6f73]">
                        {wishCourse.course.author.name}
                    </div>
                    <div className="flex items-center text-sm font-bold">
                        <BsCurrencyDollar />
                        {wishCourse.course.price?.value}
                    </div>
                </div>
            </div>
            <div className="mt-2" onClick={() => handleAddCourse()}>
                <Button
                    className="w-full font-bold"
                    outline
                    gradientDuoTone="cyanToBlue"
                >
                    Add to cart
                </Button>
            </div>
        </div>
    );
};

export default WishListCardItem;
