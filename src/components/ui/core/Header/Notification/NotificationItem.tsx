// import { CustomFlowbiteTheme } from 'flowbite-react';
import { Notification } from '../../../../../models';
import { formatDistanceToNowTime } from '../../../../../utils';

export interface INotificationItemProps {
    notification: Notification
}
// const customAvatarTheme: CustomFlowbiteTheme = {
//     avatar: {
//         root: {
//             size: {
//                 md: "w-14 h-14",
//             },
//             initials: {
//                 text: "text-white",
//                 base: "inline-flex overflow-hidden relative justify-center items-center bg-black dark:bg-gray-600",
//             },
//         },
//     },
// };
export default function NotificationItem (props: INotificationItemProps) {
    const {notification} = props
    return (
        <div className="relative flex gap-2 p-4">
            {/* {false ? (
              <Flowbite theme={{ theme: customAvatarTheme }}>
                  <Avatar img={""} alt="avatar" rounded size="md" />
              </Flowbite>
          ) : (
              <Flowbite theme={{ theme: customAvatarTheme }}>
                  <Avatar
                      placeholderInitials={akaName("nam lq")}
                      rounded
                      size="md"
                  />
              </Flowbite>
          )} */}
            <div>
                <div className="mb-1 mr-2 line-clamp-2 overflow-hidden text-sm font-bold text-primary-black">
                    {notification.title}
                </div>
                <div className="mb-1 mr-2 line-clamp-2 overflow-hidden text-sm  text-primary-black">
                    {notification.body}
                </div>
                <div className="text-sm text-primary-gray">
                    {formatDistanceToNowTime(notification.created_at)}
                </div>
            </div>
            {!notification.isRead && (
                <div className="absolute right-4  h-3 w-3 rounded-full bg-cyan-400"></div>
            )}
        </div>
    );
}
