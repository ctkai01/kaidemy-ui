import { Avatar } from "flowbite-react";
import { FaSearch } from "react-icons/fa";
import { Link } from "react-router-dom";
import { SearchDataItem } from "../../../../models";
import { akaName } from "../../../../utils";

export interface ISearchItemProps {
    type: "course" | "author" | "search";
    searchData?: SearchDataItem;
    search?: string;
    handleResetSearch: () => void;
}

export default function SearchItem(props: ISearchItemProps) {
    const { type, searchData, search, handleResetSearch } = props;
    return (
        <div
            onClick={() => handleResetSearch()}
            className="bg-white py-2 pl-4 pr-2 hover:bg-[#f7f9fa]"
        >
            {type == "course" && searchData && (
                <Link className="" to={`/course/${searchData.course?.id}`}>
                    <div className="flex items-center">
                        <div className="mr-4">
                            <img
                                className="h-[32px] w-[32px] border border-[#d1d7dc]"
                                src={
                                    searchData.course?.image
                                        ? searchData.course?.image
                                        : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                                }
                            />
                        </div>
                        <div className="flex-1">
                            <div className="w-[650px] overflow-hidden text-ellipsis whitespace-nowrap font-bold text-primary-black">
                                {searchData.course?.title}
                            </div>
                            <div className="mt-1 flex text-[12px] text-[#6a6f73] ">
                                <div className="font-bold">Course</div>
                                <div className="pl-2">
                                    {searchData.course?.name_author}
                                </div>
                            </div>
                        </div>
                    </div>
                </Link>
            )}
            {type == "author" && searchData  && (
                <Link className="" to={`/user/${searchData.teacher?.id}`}>
                    <div className="flex items-center">
                        <div className="mr-4">
                            {/* <Avatar size="sm" rounded placeholder="" /> */}
                            {searchData.teacher?.avatar ? (
                                <Avatar
                                    img={searchData.teacher?.avatar}
                                    alt="avatar"
                                    rounded
                                    size="sm"
                                />
                            ) : (
                                <Avatar
                                    placeholderInitials={akaName(
                                        searchData.teacher?.name
                                            ? searchData.teacher?.name
                                            : "",
                                    )}
                                    rounded
                                    size="sm"
                                />
                            )}
                            {/* <img
                              className="h-[32px] w-[32px] rounded-full border border-[#d1d7dc]"
                              src="https://img-c.udemycdn.com/user/50x50/7799204_2091_5.jpg"
                          /> */}
                        </div>
                        <div>
                            <div className="w-[650px] overflow-hidden text-ellipsis whitespace-nowrap font-bold text-primary-black">
                                {searchData.teacher?.name}
                            </div>
                            <div className="mt-1 flex text-[12px] text-[#6a6f73] ">
                                <div className="font-bold">Instructor</div>
                            </div>
                        </div>
                    </div>
                </Link>
            )}

            {type == "search" && (
                <Link className="" to={`/courses/search?q=${search}`}>
                    <div className="flex items-center">
                        <div className="mr-6 ">
                            <FaSearch className="h-5 w-5" />
                        </div>
                        <div>
                            <div className="w-[650px] overflow-hidden text-ellipsis whitespace-nowrap font-bold text-primary-black">
                                {search}
                            </div>
                            {/* <div className="mt-1 flex text-[12px] text-[#6a6f73] ">
                              <div className="font-bold">Giảng viên</div>
                          </div> */}
                        </div>
                    </div>
                </Link>
            )}
        </div>
    );
}
