import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import { Link, useNavigate } from "react-router-dom";
import { ACCOUNT_NORMAL, STUDENT_ROLE } from "../../../../constants";
import { useAppDispatch, useAppSelector } from "../../../../hooks/redux-hook";
import { NotificationServiceApi } from "../../../../services/api/notificationServiceApi";
import {
    logout,
    selectUserAuth,
} from "../../../../services/state/redux/authSlide";
import { selectCart } from "../../../../services/state/redux/cartSlide";
import { selectFcmTokenNotificationStudent } from "../../../../services/state/redux/notificationSlide";
import { akaName, clearAuthLocalStorage } from "../../../../utils";
const customTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-16 h-16",
            },
        },
    },
};
const DropDownHeader = () => {
    const dispatch = useAppDispatch();
    const user = useAppSelector(selectUserAuth);
    const cart = useAppSelector(selectCart);
    const fcmToken = useAppSelector(selectFcmTokenNotificationStudent);
    const navigate = useNavigate();
    const handleLogout = async () => {
        try {
            await NotificationServiceApi.removeFcmToken(fcmToken);
            dispatch(logout());
            clearAuthLocalStorage();

            navigate("/login");
        } catch (err) {
            console.log("Error logout: ", err);
        }
    };

    return (
        <div className="absolute -bottom-3  right-0 hidden  translate-y-full  border border-[#d1d7dc] bg-white after:absolute after:-top-[14px] after:h-[20px] after:w-full after:content-[''] group-hover:block">
            <div className="min-w-[260px] bg-white ">
                <div className="">
                    <div className="flex items-center border-b border-[#d1d7dc] p-4">
                        <div className="h-16 w-16">
                            <Flowbite theme={{ theme: customTheme }}>
                                {user?.avatar ? (
                                    <Avatar
                                        img={user.avatar}
                                        alt="avatar"
                                        rounded
                                        size="md"
                                    />
                                ) : (
                                    <Avatar
                                        placeholderInitials={akaName(
                                            user ? user.name : "",
                                        )}
                                        rounded
                                        size="md"
                                    />
                                )}
                            </Flowbite>
                        </div>

                        <div className="ml-2 flex-1 text-[#2D2F31]">
                            <div className="max-w-[154px] overflow-hidden text-ellipsis whitespace-nowrap text-base font-bold">
                                {user?.name}
                            </div>
                            <div className="max-w-[154px] overflow-hidden  text-ellipsis whitespace-nowrap text-xs text-[#6a6f73]">
                                {user?.email}
                            </div>
                        </div>
                    </div>
                    <ul className="border-b border-[#d1d7dc] py-2 text-[#2D2F31]">
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link
                                to="/home/my-courses/learning"
                                className="block h-full w-full"
                            >
                                My learning
                            </Link>
                        </li>
                        <li className="relative px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link to="/cart" className="block h-full w-full">
                                My cart
                            </Link>
                            {cart && cart?.cartItems.length !== 0 && (
                                <div className="absolute right-4 top-0 rounded-full bg-primary-blue px-2 py-1 text-xs text-white ">
                                    {cart?.cartItems.length > 9
                                        ? "9+"
                                        : cart.cartItems.length}
                                </div>
                            )}
                        </li>
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link
                                to="/home/my-courses/wishlist"
                                className="block h-full w-full"
                            >
                                Wishlist
                            </Link>
                        </li>
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            {user?.role === STUDENT_ROLE ? (
                                <Link
                                    to="/register-teacher"
                                    className="block h-full w-full"
                                >
                                    Teach on Hustdemy
                                </Link>
                            ) : (
                                <Link
                                    to="/instructor/course"
                                    className="block h-full w-full"
                                >
                                    Dashboard for instructor
                                </Link>
                            )}
                        </li>
                    </ul>
                    {/* <ul className="border-b border-[#d1d7dc] py-2 text-[#2D2F31]">
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link to="/messages" className="block h-full w-full">
                                Message
                            </Link>
                        </li>
                    </ul> */}
                    <ul className="border-b border-[#d1d7dc] py-2 text-[#2D2F31]">
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link
                                to={
                                    user?.typeAccount === ACCOUNT_NORMAL
                                        ? "/user/edit-account"
                                        : "/user/edit-photo"
                                }
                                className="block h-full w-full"
                            >
                                Account settings
                            </Link>
                        </li>
                        {/* <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link to="/" className="block h-full w-full">
                                Lịch sử mua hàng
                            </Link>
                        </li> */}
                    </ul>
                    <ul className="border-b border-[#d1d7dc] py-2 text-[#2D2F31]">
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link
                                to={`/user/${user?.id}`}
                                className="block h-full w-full"
                            >
                                Public profile
                            </Link>
                        </li>
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <Link
                                to="/user/edit-profile"
                                className="block h-full w-full"
                            >
                                Edit profile
                            </Link>
                        </li>
                    </ul>
                    <ul
                        onClick={() => handleLogout()}
                        className="cursor-pointer py-2 text-[#2D2F31]"
                    >
                        <li className="px-4 py-2 hover:bg-[#1739531f] hover:text-primary-blue">
                            <div className="block h-full w-full">Logout</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default DropDownHeader;
