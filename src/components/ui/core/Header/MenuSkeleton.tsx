
export interface IMenuMenuSkeletonProps {
}

export default function MenuSkeleton () {
  return (
      <div className="w-[256px] animate-pulse px-3 py-2">
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
          <div className="mb-1 h-10  bg-slate-300"></div>
      </div>
  );
}
