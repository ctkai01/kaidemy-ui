import { Avatar, Spinner } from "flowbite-react";
import { debounce } from "lodash";
import React, {
    FormEvent,
    useEffect,
    useLayoutEffect,
    useMemo,
    useRef,
    useState,
} from "react";
import { AiOutlineHeart, AiOutlineShoppingCart } from "react-icons/ai";
import { BsBell } from "react-icons/bs";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { STUDENT_ROLE } from "../../../../constants";
import { useAppSelector } from "../../../../hooks/redux-hook";
import { PaginationInfo, SearchDataItem } from "../../../../models";
import { CategoryServiceApi } from "../../../../services/api/categoryServiceApi";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import {
    selectIsLoggedIn,
    selectUserAuth,
} from "../../../../services/state/redux/authSlide";
import { selectCart } from "../../../../services/state/redux/cartSlide";
import { selectNotificationStudent, selectNotificationTeacher } from "../../../../services/state/redux/notificationSlide";
import { selectWishList } from "../../../../services/state/redux/wishListSlide";
import { akaName } from "../../../../utils";
import DropDownCart from "./DropDownCart";
import DropDownHeader from "./DropDownHeader";
import DropDownMyLearning from "./DropDownMyLearning";
import DropDownNotification from "./DropDownNotification";
import DropDownWishList from "./DropDownWishList";
import { ItemMenu } from "./MenuItem";
import Navbar from "./Navbar";
import SearchItem from "./SearchItem";
export interface MenuClass {
    [key: string]: string;
}

const SIZE = 6;
const Header = () => {
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const location = useLocation();
    const user = useAppSelector(selectUserAuth);
    const cart = useAppSelector(selectCart);
    const wishList = useAppSelector(selectWishList);
    const notificationTeacher = useAppSelector(selectNotificationTeacher);
    const notificationStudent = useAppSelector(selectNotificationStudent);
    const [search, setSearch] = useState("");
    const searchRef = useRef<HTMLDivElement>(null);
    const [searchData, setSearchData] = useState<SearchDataItem[]>([]);
    const [menus, setMenus] = useState<ItemMenu[]>([]);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [currPage, setCurrPage] = useState(1);
    // const [classMenus, setClassMenus] = useState<MenuClass | null>(null);
    const [loading, setLoading] = useState(false);
    const [isShowSearch, setIsShowSearch] = useState(false);
    const [loadingSearch, setLoadingSearch] = useState(false);
    let isStickyHeader = true;
    const navigate = useNavigate();

    if (location.pathname.startsWith("/course")) {
        isStickyHeader = false;
    }

    const numberNotificationNoRead = useMemo(() => {
        return notificationTeacher.concat(notificationStudent).filter(notification => !notification.isRead).length
        
    }, [notificationTeacher, notificationStudent]);
    const fetchSearchData = async (value: string, currentPage: number) => {
        //   setIsLoading(true);
        try {
            if (value === "") {
                setInfoPagination({
                    totalItem: 0,
                    totalPage: 0,
                    size: 0,
                });
                setSearchData([]);
                setCurrPage(1);
                setLoadingSearch(false);
            } else {
                const searchDataResult = await CourseServiceApi.searchGlobal(
                    value,
                    SIZE,
                    currentPage,
                );
                setInfoPagination({
                    totalPage: searchDataResult.meta.pageCount,
                    totalItem: searchDataResult.meta.itemCount,
                    size: searchDataResult.meta.size,
                });

                if (!searchDataResult.item) {
                    setSearchData([]);
                } else {
                    setSearchData((prev) => [
                        ...prev,
                        ...searchDataResult.item,
                    ]);
                }
                setLoadingSearch(false);
            }
        } catch (e) {
            console.log(e);
        }
    };
    const debounceSearch = React.useCallback(
        debounce(fetchSearchData, 1000),
        [],
    );
    useEffect(() => {
        // setCurrPage(1);

        debounceSearch(search, currPage);
    }, [search, currPage]);
    const childrenGroupName = (childrenMenu: any) => {
        return childrenMenu.map((item: any) => {
            const newData: ItemMenu = {
                name: item.name,
                parentID: item.parentID,
                children: item.children && [],
                id: item.id,
                groupName: item.name.toLowerCase().split(" ").join("_"),
            };
            return newData;
        });
    };
    useLayoutEffect(() => {
        const fetchCategoryMenu = async () => {
            try {
                setLoading(true);

                const data = await CategoryServiceApi.getCategoryMenu();
                const menus = data.item.map((item: any) => {
                    const newData: ItemMenu = {
                        name: item.name,
                        parentID: item.parentID,
                        children: childrenGroupName(item.children),
                        id: item.id,
                        groupName: item.name.toLowerCase().split(" ").join("_"),
                    };
                    return newData;
                });

                setMenus(menus);
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCategoryMenu();
    }, []);


    const onScroll = () => {
        if (searchRef.current) {
            const { scrollTop, scrollHeight, clientHeight } = searchRef.current;
            if (scrollTop + clientHeight === scrollHeight) {
                if (currPage < infoPagination.totalPage) {
                    setCurrPage((current) => current + 1);
                }
            }
        }
    };

    const handleResetSearch = () => {
        setSearch("");
    };

    const handleSubmitSearch = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (search != "") {
            setIsShowSearch(false);
            navigate(`/courses/search?q=${search}`);
            console.log("Serchsss: ", search);
        }
    };
    return (
        <div
            className={`${
                isStickyHeader ? "sticky" : ""
            } top-0 z-[100] flex  h-18 items-center bg-white px-6 py-3 shadow-md backdrop-blur`}
        >
            <Link to="/" className="pr-2">
                <img
                    loading="lazy"
                    src={
                        "https://kaidemy.b-cdn.net/avatar/hustdemy-high-logo-transparent-black.png"
                    }
                    className="w-24"
                />
            </Link>
            <Navbar menuItems={menus} loading={loading} />
            {/* {loading && (
                <div>
                    <div className="mr-3 rounded  h-10 w-[72px] animate-pulse bg-slate-500"></div>
                </div>
            )} */}

            <form className="flex-1" onSubmit={handleSubmitSearch}>
                <label className="sr-only mb-2 text-sm font-medium text-gray-900 dark:text-white">
                    Search
                </label>
                <div className="relative">
                    <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <svg
                            className="h-4 w-4 text-gray-500 dark:text-gray-400"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 20 20"
                        >
                            <path
                                stroke="currentColor"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                            />
                        </svg>
                    </div>
                    <input
                        value={search}
                        type="search"
                        id="default-search"
                        className="block w-full rounded-xl border-2 border-gray-300 bg-gray-50 p-4 pl-10 text-sm text-gray-900 outline-none focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                        placeholder="Search for anything"
                        required
                        onChange={(e) => {
                            setSearch(e.target.value);
                            setLoadingSearch(true);
                            setIsShowSearch(true);
                            setInfoPagination({
                                totalItem: 0,
                                totalPage: 0,
                                size: 0,
                            });
                            setSearchData([]);
                            setCurrPage(1);
                        }}
                    />
                    <button
                        type="submit"
                        className="absolute bottom-2.5 right-2.5 w-28 rounded-lg bg-blue-700 px-4 py-2 text-sm font-medium text-white hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                    >
                        Search
                    </button>
                    {search && isShowSearch && (
                        <div
                            ref={searchRef}
                            onScroll={onScroll}
                            className="absolute -bottom-1 left-0 max-h-[240px] min-h-[80px] w-full translate-y-full overflow-y-scroll border border-[#d1d7dc] bg-white py-2"
                        >
                            <SearchItem
                                search={search}
                                type="search"
                                handleResetSearch={handleResetSearch}
                            />
                            {searchData.map((searchItem) => {
                                if (searchItem.course) {
                                    return (
                                        <SearchItem
                                            searchData={searchItem}
                                            type="course"
                                            handleResetSearch={
                                                handleResetSearch
                                            }
                                        />
                                    );
                                } else {
                                    return (
                                        <SearchItem
                                            searchData={searchItem}
                                            type="author"
                                            handleResetSearch={
                                                handleResetSearch
                                            }
                                        />
                                    );
                                }
                            })}
                            {/* <div className="text-center">
                                <Spinner />
                            </div> */}
                            {loadingSearch && (
                                <div className="text-center">
                                    <Spinner />
                                </div>
                            )}
                        </div>
                    )}
                </div>
            </form>
            {isLoggedIn && (
                <>
                    <div className="my-3 h-12 max-h-18">
                        {user?.role === STUDENT_ROLE ? (
                            <Link
                                to="/register-teacher"
                                className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-primary-blue"
                            >
                                Teach on Hustdemy
                            </Link>
                        ) : (
                            <Link
                                to="/instructor/course"
                                className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-primary-blue"
                            >
                                Instructor
                            </Link>
                        )}
                    </div>
                    <div className="after:top group  relative z-10 my-3 h-12 max-h-18">
                        <Link
                            to="/home/my-courses/learning"
                            className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-primary-blue"
                        >
                            My learning
                        </Link>
                        <DropDownMyLearning />
                    </div>
                    <div className="group relative my-3 h-12 max-h-18">
                        <Link
                            to="/home/my-courses/wishlist"
                            className="relative flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-primary-blue"
                        >
                            <AiOutlineHeart className="h-6 w-6" />
                        </Link>
                        <DropDownWishList wishList={wishList} />
                    </div>
                </>
            )}
            {isLoggedIn && (
                <div className="group relative my-3 h-12 max-h-18">
                    <Link
                        to="/cart"
                        className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-primary-blue"
                    >
                        <AiOutlineShoppingCart className="h-6 w-6" />
                    </Link>
                    <DropDownCart cart={cart} />

                    {cart && cart?.cartItems.length !== 0 && (
                        <div className="absolute left-1/2 top-0 rounded-full bg-primary-blue px-2 py-1 text-xs text-white ">
                            {cart?.cartItems.length > 9
                                ? "9+"
                                : cart.cartItems.length}
                        </div>
                    )}
                </div>
            )}

            {isLoggedIn && (
                <div className="group relative my-3 h-12 max-h-18">
                    <Link
                        to="/"
                        className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-primary-blue"
                    >
                        <BsBell className="h-6 w-6" />
                    </Link>
                    <DropDownNotification />
                    {numberNotificationNoRead !== 0 && (
                        <div className="font absolute left-1/2 top-0 flex h-4 w-4 items-center justify-center rounded-full bg-primary-blue text-xs font-bold text-white ">
                            {numberNotificationNoRead > 9
                                ? "9+"
                                : numberNotificationNoRead}
                        </div>
                    )}
                </div>
            )}
            {isLoggedIn && (
                <div className="group relative my-3 h-12 max-h-18">
                    <Link
                        to="/"
                        className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31]"
                    >
                        {user?.avatar ? (
                            <Avatar
                                img={user.avatar}
                                alt="avatar"
                                rounded
                                size="sm"
                            />
                        ) : (
                            <Avatar
                                placeholderInitials={akaName(
                                    user ? user.name : "",
                                )}
                                rounded
                                size="sm"
                            />
                        )}
                    </Link>
                    <DropDownHeader />
                </div>
            )}
            {!isLoggedIn && (
                <>
                    <button
                        className="relative ml-4 mr-2 h-10 rounded-md border border-black px-5 font-bold text-black
hover:bg-[#1739531f]"
                    >
                        <Link
                            className="after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                            to="/login"
                        >
                            Login
                        </Link>
                    </button>
                    <button className="relative h-10 rounded-md border border-black bg-black px-5 font-bold text-white hover:bg-[#3e4143]">
                        <Link
                            className="after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                            to="/sign-up"
                        >
                            Sign up
                        </Link>
                    </button>
                </>
            )}
        </div>
    );
};

export default Header;
