
import { Footer as FooterFlowBite }   from "flowbite-react";
import kaidemyIconBlack from "../../../assets/svg/kaidemy-icon-black.svg";

const Footer = () => {
    const currentYear = new Date().getFullYear()
    return (
        <FooterFlowBite
            container
            className="mt-20  rounded-none bg-primary-black text-white"
        >
            <div className="w-full text-center">
                <div className="w-full justify-between sm:flex sm:items-center sm:justify-between">
                    <FooterFlowBite.Brand
                        href="https://flowbite.com"
                        src={kaidemyIconBlack}
                        alt="Flowbite Logo"
                        // name="Flowbite"
                    />
                    <FooterFlowBite.LinkGroup className="text-white">
                        {/* <FooterFlowBite.LinkGroup className="text-cyan-700"> */}
                        {/* <FooterFlowBite.Link href="#">
                            About
                        </FooterFlowBite.Link>
                        <FooterFlowBite.Link href="#">
                            Privacy Policy
                        </FooterFlowBite.Link>
                        <FooterFlowBite.Link href="#">
                            Licensing
                        </FooterFlowBite.Link>
                        <FooterFlowBite.Link href="#">
                            Contact
                        </FooterFlowBite.Link> */}
                    </FooterFlowBite.LinkGroup>
                </div>
                <FooterFlowBite.Divider className="h-[2px] text-white" />
                <FooterFlowBite.Copyright
                    href="#"
                    by="Hustdemy"
                    year={currentYear}
                    className="font-bold text-white"
                />
            </div>
        </FooterFlowBite>
    );
}

export default Footer