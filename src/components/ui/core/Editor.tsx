// import BlotFormatter from "quill-blot-formatter";

// import "quill/dist/quill.snow.css";
import { useEffect, useState } from "react";
// import { useQuill } from "react-quilljs";

import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

// import { ImageResize } from 'quill-image-resize-module';
// Quill.register("modules/imageResize", ImageResize);
// import { ImageResize } from "quill-image-resize-module";
// Quill.register("modules/imageResize", ImageResize);
interface IEditor {
    defaultValue?: string;
    handleSetValue?: (value: string) => void;
    handleSetHTML: (value: string) => void;
    isImage: boolean;
    placeholder?: string;
}
const toolBarStandard = [
    [{ header: [1, 2, 3, 4, false] }],
    ["bold", "italic", "underline"],
    [{ list: "ordered" }, { list: "bullet" }],
];

const toolBarImage = [
    [{ header: [1, 2, 3, 4, false] }],
    ["bold", "italic", "underline"],
    [{ list: "ordered" }, { list: "bullet" }],
    ["link", "image"],
];
const Editor = (props: IEditor) => {
    const {
        isImage,
        defaultValue,
        placeholder,
        handleSetValue,
        handleSetHTML,
    } = props;
    // const { quill, quillRef, Quill } = useQuill({
    //     modules: {
    //         // blotFormatter: {},
    //         toolbar: isImage ? toolBarImage : toolBarStandard,
    //     },
    //     placeholder: placeholder,
    // });
    const [value, setValue] = useState("");
    // if (Quill && !quill) {
    //     // const BlotFormatter = require('quill-blot-formatter');
    //     // Quill.register("modules/blotFormatter", BlotFormatter);
    // }
    //  const [value, setValue] = useState("");
    // useEffect(() => {
    //     if (quill) {
    //         const delta = quill.clipboard.convert({html: defaultValue});
    //         quill.setContents(delta);
    //         quill.on("text-change", (delta: any, oldContents: any) => {
    //             // console.log("Text change!");
    //             const html = quill.root.innerHTML;
    //             console.log("currentContents HTML: ", html);
    //             console.log("currentContents delta: ", delta);

    //             let currentContents = quill.getContents();
    //             console.log("currentContents: ", currentContents);
    //             if (handleSetValue) {
    //                 handleSetValue(currentContents.ops?.[0].insert?.toString() ? currentContents.ops?.[0].insert?.toString() : "");
    //             }

    //             if (handleSetHTML) {
    //                 handleSetHTML(html);
    //             }

    //         });
    //     }
    // }, [quill, Quill, defaultValue]);
    // const customToolbar = [
    //     [{ header: [1, 2, 3, 4, false] }],
    //     ["bold", "italic", "underline", "strike"],
    //     [{ list: "ordered" }, { list: "bullet" }],
    //     ["link", "image", "video"],
    //     ["clean"],
    // ];
    return (
        <div>
            {/* <div ref={quillRef} /> */}
            <ReactQuill
                defaultValue={defaultValue}
                placeholder={placeholder}
                theme="snow"
                onChange={(data) => {
                    handleSetHTML(data);
                }}
            />
        </div>
    );
};

export default Editor;
