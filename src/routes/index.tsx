import { createBrowserRouter, redirect, useNavigate } from "react-router-dom";
import { useAppSelector } from "../hooks/redux-hook";
import Layout from "../layouts/Layout";
import ForgotPassword from "../pages/ForgotPassword";
import HomeNotAuth from "../pages/Home";
import Login from "../pages/Login";
import NotFound from "../pages/NotFound";
import ResetPassword from "../pages/ResetPassword";
import SignUp from "../pages/SignUp";
import { selectIsLoggedIn } from "../services/state/redux/authSlide";
// const Test = () => {
//     return (
//         <div>
//             What
//             <Outlet />
//         </div>
//     );
// };

// const PublicPage = () => {
//     return <div>Publics</div>;
// };
const router = createBrowserRouter([
    {
        id: "root",
        path: "/",
        // errorElement: <NotFound />,
        Component: Layout,
        // element
        children: [
            {
                index: true,

                Component: HomeNotAuth,
            },
            {
                path: "login",
                // action: loginAction,
                loader: loginLoader,
                Component: Login,
            },
            {
                path: "sign-up",
                // loader: signUpLoader,
                Component: SignUp,
            },
            {
                path: "forgot-password",
                // loader: signUpLoader,
                Component: ForgotPassword,
            },
            {
                path: "email-password-change",
                // loader: signUpLoader,
                Component: ResetPassword,
            },
        ],
    },
]);
function loginLoader() {
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const navigate = useNavigate();
    if (isLoggedIn) {
        navigate;
        ("/");
        // return redirect("/");
    }
    return null;
}

// async function signUpLoader() {
//     const isLoggedIn = useAppSelector(selectIsLoggedIn);
//     if (isLoggedIn) {
//         return redirect("/");
//     }
//     return null;
// }
export default router;

// function protectedLoader({ request }: LoaderFunctionArgs) {
//     // If the user is not logged in and tries to access `/protected`, we redirect
//     // them to `/login` with a `from` parameter that allows login to redirect back
//     // to this page upon successful authentication
//     if (!fakeAuthProvider.isAuthenticated) {
//         let params = new URLSearchParams();
//         params.set("from", new URL(request.url).pathname);
//         return redirect("/login?" + params.toString());
//     }
//     return null;
// }

// function ProtectedPage() {
//     return <h3>Protected</h3>;
// }
