import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import App from './App';
import './index.css';
import { persistor, store } from './services/state/redux/store';
import { GoogleOAuthProvider } from "@react-oauth/google";
import "react-loading-skeleton/dist/skeleton.css";

createRoot(document.getElementById("root")!).render(
    // <BrowserRouter >
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <GoogleOAuthProvider clientId="148930819635-guifu4v3u1kgsuk3rantuk6d93mp7in0.apps.googleusercontent.com">
                {/* <GoogleOAuthProvider clientId="77171024969-6mgl5svemgh3nq7mepssp4cns488q5sg.apps.googleusercontent.com"> */}
                <App />
            </GoogleOAuthProvider>
        </PersistGate>
    </Provider>,
    // </BrowserRouter>,
);
