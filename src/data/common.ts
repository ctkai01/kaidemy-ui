import { MenuClass } from "../components/ui/core/Header/Header";
import { CourseItem } from "../models";
import { CategoryItem } from "../models/category";

export const universities = [
    "hust.png",
    "ptit.png",
    "utt.jpg",
    "neu.png",
    "ba.png",
    "huce.png",
    "hv.png",
    "tl.png",
    "tmu.jpg",
    "mit.png",
];

export const banners = [
    {
        image: "/images/banner1.jpg",
        title: "Code for your future",
        desc: "Take control of your career. Learn the latest skills in web development.",
    },

    {
        image: "/images/banner2.jpg",
        title: "Pro tip:",
        desc: "Watch courses while you exercise or cook.",
    },
    // {
    //     image: "/images/banner3.jpg",
    //     title: "Special offer — just hours to save!",
    //     desc: "Your exclusive longtime-learner deal: courses from just 10$ for a very limited time.",
    // },
];

export const courses: CourseItem[] = [
    {
        image: "https://img-c.udemycdn.com/course/240x135/567828_67d0.jpg",
        author: "Jose Portilla",
        isBestseller: true,
        price: 500,
        reviews: 4,
        title: "The Complete Python Bootcamp From Zero to Hero in Python",
        countReviews: 484437,
        intendedFor: [
            "You will learn how to leverage the power of Python to solve tasks.",
            "You will build games and programs that use Python libraries.",
            "You will be able to use Python for your own work problems or personal projects.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/543600_64d1_4.jpg",
        author: "Jose Portilla",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "Automate the Boring Stuff with Python Programming",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/2776760_f176_10.jpg",
        author: "Dr. Angela Yu",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "100 Days of Code: The Complete Python Pro Bootcamp for 2023",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/950390_270f_3.jpg",
        author: "Dr. Angela Yu",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "Machine Learning A-Z™: AI, Python & R + ChatGPT Bonus [2023]",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/903744_8eb2.jpg",
        author: "Dr. Angela Yu",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "Python for Data Science and Machine Learning Bootcamp",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/629302_8a2d_2.jpg",
        author: "Tim Buchalka, Jean-Paul Roberts, Tim Buchalka's Learn Programming Academy",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "Learn Python Programming Masterclass",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/3047216_4888_2.jpg",
        author: "Tim Buchalka, Jean-Paul Roberts, Tim Buchalka's Learn Programming Academy",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "Python for beginners - Learn all the basics of pythons",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/1495788_1aaa_2.jpg",
        author: "Tim Buchalka, Jean-Paul Roberts, Tim Buchalka's Learn Programming Academy",
        isBestseller: false,
        price: 500,
        reviews: 3.6,
        title: "Learn to Code in Python 3: Programming beginner to advanced",
        countReviews: 484437,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/3105814_7810.jpg",
        author: "Tim Buchalka, Jean-Paul Roberts, Tim Buchalka's Learn Programming Academy",
        isBestseller: false,
        price: 500,
        reviews: 4.2,
        title: "Python-Introduction to Data Science and Machine learning A-Z",
        countReviews: 3109,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/3434032_8b2b_2.jpg",
        author: "Tim Buchalka, Jean-Paul Roberts, Tim Buchalka's Learn Programming Academy",
        isBestseller: false,
        price: 500,
        reviews: 4.2,
        title: "Python Complete Course For Python Beginners",
        countReviews: 3109,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/1554180_150a_2.jpg",
        author: "Python Network Programming for Network Engineers (Python 3)",
        isBestseller: false,
        price: 500,
        reviews: 4.2,
        title: "Python Complete Course For Python Beginners",
        countReviews: 3109,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
    {
        image: "https://img-c.udemycdn.com/course/240x135/692188_9da7_30.jpg",
        author: "Python Mega Course: Learn Python in 60 Days, Build 20 Apps",
        isBestseller: true,
        price: 500,
        reviews: 4.2,
        title: "Python Complete Course For Python Beginners",
        countReviews: 3109,
        intendedFor: [
            "Automate tasks on their computer by writing simple Python programs.",
            'Write programs that can do text pattern recognition with "regular expressions".',
            "Programmatically generate and update Excel spreadsheets.",
        ],
        level: "All Levels",
        updateAt: "2023-10-27T08:22:13.775Z",
        totalTime: "22 total hours",
        subTitle:
            "A practical programming course for office workers, academics, and administrators who want to improve their productivity.",
    },
];

export const categoriesList: CategoryItem[] = [
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-design-v2.jpg",
        title: "Design",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-development-v2.jpg",
        title: "Development",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-marketing-v2.jpg",
        title: "Marketing",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-it-and-software-v2.jpg",
        title: "IT and Software",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-personal-development-v2.jpg",
        title: "Personal Development",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-business-v2.jpg",
        title: "Business",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-photography-v2.jpg",
        title: "Photography",
        url: "/test",
    },
    {
        image: "https://s.udemycdn.com/home/top-categories/lohp-category-music-v2.jpg",
        title: "Music",
        url: "/test",
    },
];

export interface ContinueCourse {
    thumbnail: string;
    title: string;
    titleLecture: string;
    time: string;
}
export const continueCourses: ContinueCourse[] = [
    {
        thumbnail:
            "https://mp4-b.udemycdn.com/2020-10-24_09-19-17-4f0965584cd9021127e7b84e12d57fc8/1/thumb-1.jpg?secure=1w_Zgk9K3nv1oOrTygSQEA%3D%3D%2C1699631972",
        time: "5m",
        title: "ReactJS cho người mới bắt đầu 2020",
        titleLecture: "1. React là gì?",
    },
    {
        thumbnail:
            "https://mp4-b.udemycdn.com/2022-12-03_16-46-36-568aa0a6253da934e2672f2a27ccd29b/2/thumb-1.jpg?secure=gXOaKfx0fws4UQ0AnzO2QQ%3D%3D%2C1699631973",
        time: "5m",
        title: "Laravel 9 A-Z For Beginner With Complete News Portal Website",
        titleLecture: "1. News Portal Introduction - What you will build",
    },
    {
        thumbnail:
            "https://mp4-b.udemycdn.com/2017-08-07_12-29-22-44dec34a4d4361fee41fcdbf2baa60a1/thumb-1.jpg?secure=6MlIwSFwLbn3iWhfoPWCDg%3D%3D%2C1699631973",
        time: "5m",
        title: "Web Development Course : laravel ORM  and Raw PHP!",
        titleLecture: "22. Introduction to Laravel ORM and CakePHP ORm",
    },
    {
        thumbnail:
            "https://mp4-b.udemycdn.com/2020-11-02_14-58-41-1a7ffc6bfa2b3f4b29a0a88b664d5421/1/thumb-1.jpg?secure=RurgIjDcocSox6PhuDZviw%3D%3D%2C1699631973",
        time: "5m",
        title: "Laravel 7 Tutorial for Beginners",
        titleLecture: "1. Important Information",
    },
    {
        thumbnail:
            "https://mp4-b.udemycdn.com/2019-06-19_15-51-29-1202553b6ca21d75ece9d724eca2bc93/thumb-1.jpg?secure=MbNTIN6_wqTWzU7kEnrW1Q%3D%3D%2C1699631973",
        time: "5m",
        title: "Laravel Query Builder",
        titleLecture: "1. Introduction",
    },
    {
        thumbnail:
            "https://mp4-b.udemycdn.com/2020-01-27_06-10-22-7df0bad1dc54dc70d7afca8c7ad3517c/thumb-1.jpg?secure=vB_AXzQddQC8o1PIsgCmaw%3D%3D%2C1699631973",
        time: "5m",
        title: "Create a CRUD API with Laravel and Postman",
        titleLecture: "1. Installing Vagrant",
    },
];

// export interface CourseLearning {
//     title: string
//     image: string
//     process: number
//     author: string
//     rating: number
// }

// export const courseLearnings: CourseLearning[] = [
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/3257380_c043_3.jpg",
//         title: "ReactJS cho người mới bắt đầu 2020",
//         process: 35,
//         author: "Angle AU",
//         rating: 1,
//     },
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/2578056_b170_2.jpg",
//         title: "Laravel 9 A-Z For Beginner With Complete News Portal Website",
//         process: 0,
//         author: "Angle AU",
//         rating: 1,
//     },
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/1304674_2c57.jpg",
//         title: "Web Development Course : laravel ORM  and Raw PHP!",
//         process: 0,
//         author: "Angle AU",
//         rating: 1,
//     },
// ];

// export const allCourseLearnings: CourseLearning[] = [
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/2578056_b170_2.jpg",
//         title: "Laravel 9 A-Z For Beginner With Complete News Portal Website",
//         process: 0,
//         rating: 1,
//         author: "Kazi Ariyan",
//     },
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/3257380_c043_3.jpg",
//         title: "ReactJS cho người mới bắt đầu 2020",
//         process: 35,
//         rating: 3.5,
//         author: "Hau Nguyen",
//     },
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/1304674_2c57.jpg",
//         title: "Web Development Course : laravel ORM  and Raw PHP!",
//         process: 0,
//         rating: 1,
//         author: "Dave Partner",
//     },
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/3580711_74a7_2.jpg",
//         title: "Laravel 7 Tutorial for Beginners!",
//         process: 0,
//         rating: 1,
//         author: "Dave Partner",
//     },
//     {
//         image: "https://img-b.udemycdn.com/course/240x135/3580711_74a7_2.jpg",
//         title: "Laravel Query Builder",
//         process: 0,
//         rating: 1,
//         author: "Dave Partner",
//     },

//     //
// ];

export interface WishListCourse {
    title: string;
    image: string;
    author: string;
    price: number;
}

export const wishListCourse: WishListCourse[] = [
    {
        image: "https://img-c.udemycdn.com/course/100x100/1565838_e54e_18.jpg",
        title: "The Complete 2023 Web Development Bootcamp",
        author: "Dr. Angela Yu",
        price: 10,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/3653758_2d1e_8.jpg",
        title: "Full Android Development Masterclass | 14 Real Apps-46 Hours",
        author: "Oak Academy, OAK Academy Team, Mehmet ÖNGEL",
        price: 30,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/2235576_b9d9_3.jpg",
        title: "The Comprehensive Android App Development Masterclass",
        author: "Paulo Dichone | Software Engineer, AWS Cloud Practitioner & Instructor",
        price: 15,
    },
];

export interface CartCourse {
    title: string;
    image: string;
    author: string;
    price: number;
}

export const cartList: CartCourse[] = [
    {
        image: "https://img-c.udemycdn.com/course/100x100/1565838_e54e_18.jpg",
        title: "The Complete 2023 Web Development Bootcamp",
        author: "Dr. Angela Yu",
        price: 10,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/3653758_2d1e_8.jpg",
        title: "Full Android Development Masterclass | 14 Real Apps-46 Hours",
        author: "Oak Academy, OAK Academy Team, Mehmet ÖNGEL",
        price: 30,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/2235576_b9d9_3.jpg",
        title: "The Comprehensive Android App Development Masterclass",
        author: "Paulo Dichone | Software Engineer, AWS Cloud Practitioner & Instructor",
        price: 15,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/2235576_b9d9_3.jpg",
        title: "The Comprehensive Android App Development Masterclass",
        author: "Paulo Dichone | Software Engineer, AWS Cloud Practitioner & Instructor",
        price: 15,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/2235576_b9d9_3.jpg",
        title: "The Comprehensive Android App Development Masterclass",
        author: "Paulo Dichone | Software Engineer, AWS Cloud Practitioner & Instructor",
        price: 15,
    },
    {
        image: "https://img-b.udemycdn.com/course/100x100/2235576_b9d9_3.jpg",
        title: "The Comprehensive Android App Development Masterclass",
        author: "Paulo Dichone | Software Engineer, AWS Cloud Practitioner & Instructor",
        price: 15,
    },
];

export interface VideoPreview {
    video: string;
    thumbnail: string;
    name: string;
    time: string;
}

export const dataVideoPreview: VideoPreview[] = [
    {
        name: "What is K8s?",
        time: "04:30",
        thumbnail: "thumbnail.jpg",
        video: "40b85003-f455-4956-aeb7-52614eb0cff9",
    },
    {
        name: "What is Docker?",
        time: "05:10",
        thumbnail: "thumbnail.jpg",
        video: "d4d3836c-c404-454a-8d79-a6e9cf3fa5ec",
    },
];

export const menuClass: MenuClass = {
    "": "",
    "group/categories": "group/categories",
    "group-hover/categories:block": "group-hover/categories:block",
    "group/development": "group/development",
    "group-hover/development:block": "group-hover/development:block",
    "group/business": "group/business",
    "group-hover/business:block": "group-hover/business:block",
    "group/design": "group/design",
    "group-hover/design:block": "group-hover/design:block",
    "group/teaching_&_academy": "group/teaching_&_academy",
    "group-hover/teaching_&_academy:block":
        "group-hover/teaching_&_academy:block",
    // "group-hover/business:block": "group-hover/business:block",
    // "group-hover/categories:block": "group-hover/categories:block",
    // "group-hover/design:block": "group-hover/design:block",
    // "group-hover/development:block": "group-hover/development:block",
    // "group-hover/finance_&_accounting:block":
    //     "group-hover/finance_&_accounting:block",
    // "group-hover/heath_&_fitness:block": "group-hover/heath_&_fitness:block",
    // "group-hover/it_&_software:block": "group-hover/it_&_software:block",
    // "group-hover/lifestyte:block": "group-hover/lifestyte:block",
    // "group-hover/marketing:block": "group-hover/marketing:block",
    // "group-hover/music:block": "group-hover/music:block",
    // "group-hover/office_productivity:block":
    //     "group-hover/office_productivity:block",
    // "group-hover/personal_development:block":
    //     "group-hover/personal_development:block",
    // "group-hover/photography_&_video:block":
    //     "group-hover/photography_&_video:block",
    // "group-hover/teaching_&_academics:block":
    //     "group-hover/teaching_&_academics:block",
    // "group/business": "group/business",
    // "group/categories": "group/categories",
    // "group/design": "group/design",
    // "group/development": "group/development",
    // "group/finance_&_accounting": "group/finance_&_accounting",
    // "group/heath_&_fitness": "group/heath_&_fitness",
    // "group/it_&_software": "group/it_&_software",
    // "group/lifestyte": "group/lifestyte",
    // "group/marketing": "group/marketing",
    // "group/music": "group/music",
    // "group/office_productivity": "group/office_productivity",
    // "group/personal_development": "group/personal_development",
    // "group/photography_&_video": "group/photography_&_video",
    // "group/teaching_&_academics": "group/teaching_&_academics",
};
