// import { lsRefreshTokenAuth, lsTokenAuth } from "@utils/storage";
import axios from "axios";
import { clearAuthLocalStorage, getAuthLocalStorage } from "../../utils";
import config from "../../configs/config";
// import { AuthApi } from "./authApi";
// import { store } from "@redux/store";
// import { authActions } from "@features/Auth/authSlice";

export const axiosClientService = axios.create({
    baseURL: `${config.apiUrl}/api`,
    headers: {
        // "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest",
    },
});

axiosClientService.interceptors.request.use(
    (config) => {
        // Do something before request is sent
        // config.headers!.authorization = `Bearer `;
        // console.log("Request interceptor:", config);
        // if (config.url === "/auth/refreshToken") {
        //   //   const refreshToken = lsRefreshTokenAuth.getItem();
        //   //   config.headers!.authorization = `Bearer ${refreshToken}`;

        //   return config;
        // }
        const token = getAuthLocalStorage();

        config.headers!.authorization = `Bearer ${token}`;

        return config;
    },
    (error) => {
        // Do something with request error
        return Promise.reject(error);
    },
);

// Interceptor
axiosClientService.interceptors.response.use(
    function (response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        // if (response.config.url === "/api/posts") {
        //     return response.data.data;
        // }

        return response.data.data;
    },
    async function (error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        // const originalRequest = error.config;
        // console.log("Error: ", error)
        if (error.response?.status === 401 && error.config.url !== "/auth/login") {
            console.log("Fix 401");
            clearAuthLocalStorage();
            localStorage.clear();

            window.location.href = "/login";
        }
        return Promise.reject(error);
    },
);
