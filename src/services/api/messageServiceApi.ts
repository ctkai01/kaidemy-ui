import { axiosClientService } from "./axiosClientService";

export const MessageServiceApi = {
    getChannels: (search?: string): Promise<any> => {
        let url = `/chats/channel`;
        if (search) {
            url += `?search=${search}`
        }
        return axiosClientService.get(url);
    },

    getMessages: (
        to: number,
        size: number,
        page: number,
    ): Promise<any> => {
        const url = `/chats?size=${size}&page=${page}&to=${to}`;
        return axiosClientService.get(url);
    },

    createMessage: (data: any): Promise<any> => {
        const url = `/chats/message`;
        return axiosClientService.post(url, data);
    },

    // createChannel: (data: any): Promise<any> => {
    //     const url = `/channels`;
    //     return axiosClientService.post(url, data);
    // },
};
