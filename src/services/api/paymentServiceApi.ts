import { axiosClientService } from "./axiosClientService";

export const PaymentServiceApi = {
    createCart: (): Promise<any> => {
        const url = `/carts`;
        return axiosClientService.post(url);
    },
    getCart: (): Promise<any> => {
        const url = `/carts`;
        return axiosClientService.get(url);
    },

    addCartItem: (courseID: number): Promise<any> => {
        const url = `/carts/item/${courseID}`;
        return axiosClientService.post(url);
    },

    removeCartItem: (id: number): Promise<any> => {
        const url = `/carts/item/${id}`;
        return axiosClientService.delete(url);
    },

    requestPayment: (): Promise<any> => {
        const url = `/carts/claims`;
        return axiosClientService.post(url);
    },

    createInvoice: (data: any): Promise<any> => {
        const url = `/invoices`;
        return axiosClientService.post(url, data);
    },

    // getLanguages: (): Promise<any> => {
    //     const url = `/languages?size=10&page=1`;
    //     return axiosClientCategoryService.get(url);
    // },

    // getCategoryParent: (): Promise<any> => {
    //     const url = `/categories?filters={"field":"parent_id","comparison":"equals"}`;
    //     return axiosClientCategoryService.get(url);
    // },

    // getCategoryByParentID: (id: number): Promise<any> => {
    //     const url = `/categories?filters={"field":"parent_id","value":"${id}","comparison":"equals"}`;
    //     return axiosClientCategoryService.get(url);
    // },

    // getPrices: (): Promise<any> => {
    //     const url = `/prices?size=10&page=1`;
    //     return axiosClientCategoryService.get(url);
    // },

    // getCategoryMenu: (): Promise<any> => {
    //     const url = `/categories?page=1&size=100&relation=1`;
    //     return axiosClientCategoryService.get(url);
    // },

    // createCourse: (data: any): Promise<any> => {
    //     const url = "/courses";
    //     return axiosClientCourseService.post(url, data);
    // },
    // createCurriculum: (data: any): Promise<any> => {
    //     const url = "/curriculums";
    //     return axiosClientCourseService.post(url, data);
    // },
    // getCurriCulumsByCourseByID: (id: number): Promise<any> => {
    //     const url = `/courses/${id}/curriculums`;
    //     return axiosClientCourseService.get(url);
    // },
    // getCourseByID: (id: number): Promise<any> => {
    //     const url = `/courses/${id}`;
    //     return axiosClientCourseService.get(url);
    // },
    // createLecture: (data: any): Promise<any> => {
    //     const url = `/lectures`;
    //     return axiosClientCourseService.post(url, data);
    // },
    // createQuiz: (data: any): Promise<any> => {
    //     const url = `/quizs`;
    //     return axiosClientCourseService.post(url, data);
    // },
    // createQuestion: (data: any): Promise<any> => {
    //     const url = `/questions`;
    //     return axiosClientCourseService.post(url, data);
    // },
    // createAnswer: (data: any): Promise<any> => {
    //     const url = `/answers`;
    //     return axiosClientCourseService.post(url, data);
    // },
    // updateQuestion: (data: any, id: number): Promise<any> => {
    //     const url = `/questions/${id}`;
    //     return axiosClientCourseService.put(url, data);
    // },
    // updateAnswer: (data: any, id: number): Promise<any> => {
    //     const url = `/answers/${id}`;
    //     return axiosClientCourseService.put(url, data);
    // },
    // deleteQuestion: (id: number): Promise<any> => {
    //     const url = `/questions/${id}`;
    //     return axiosClientCourseService.delete(url);
    // },
    // updateQuiz: (data: any, id: number): Promise<any> => {
    //     const url = `/quizs/${id}`;
    //     return axiosClientCourseService.put(url, data);
    // },
    // deleteQuiz: (id: number): Promise<any> => {
    //     const url = `/quizs/${id}`;
    //     return axiosClientCourseService.delete(url);
    // },
    // updateLecture: (data: any, id: number): Promise<any> => {
    //     const url = `/lectures/${id}`;
    //     return axiosClientCourseService.put(url, data);
    // },
    // deleteLecture: (id: number): Promise<any> => {
    //     const url = `/lectures/${id}`;
    //     return axiosClientCourseService.delete(url);
    // },
    // updateCourse: (data: any, id: number): Promise<any> => {
    //     const url = `/courses/${id}`;
    //     return axiosClientCourseService.put(url, data);
    // },
    // loginByGoogle: (data: SignInByGoogle): Promise<any> => {
    //     const url = "/auth/login-google";
    //     return axiosClientUserService.post(url, data);
    // },
    // forgotPassword: (data: ForgotPassword): Promise<any> => {
    //     const url = "/auth/forgot-password";
    //     return axiosClientUserService.post(url, data);
    // },
    // resetPassword: (data: ResetPasswordRequest): Promise<any> => {
    //     const url = "/auth/reset-password";
    //     return axiosClientUserService.post(url, data);
    // },
    // logout: () => {
    //     const url = "api/auth/logout";
    //     return axiosClient.post(url);
    // },
    // refreshToken: (): Promise<Tokens> => {
    //     const url = "api/auth/refreshToken";
    //     return axiosClient.post(url);
    // },
};
