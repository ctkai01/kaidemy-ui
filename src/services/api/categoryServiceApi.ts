import { axiosClientService } from "./axiosClientService";

export const CategoryServiceApi = {
    getLevels: (): Promise<any> => {
        const url = `/levels?size=10&page=1`;
        return axiosClientService.get(url);
    },

    getLanguages: (): Promise<any> => {
        const url = `/languages?size=10&page=1`;
        return axiosClientService.get(url);
    },

    getCategoryParent: (): Promise<any> => {
        const url = `/categories?page=1&size=1000&parentID=-1`;
        return axiosClientService.get(url);
    },

    getCategoryByParentID: (id: number): Promise<any> => {
        const url = `/categories?page=1&size=1000&parentID=${id}`;
        return axiosClientService.get(url);
    },

    getPrices: (): Promise<any> => {
        const url = `/prices?size=100&page=1`;
        return axiosClientService.get(url);
    },

    getCategoryMenu: (): Promise<any> => {
        const url = `/categories?menu=1`;
        return axiosClientService.get(url);
    },

    getIssueTypes: (): Promise<any> => {
        const url = `/issue-types?page=1&size=100`;
        return axiosClientService.get(url);
    },
};
