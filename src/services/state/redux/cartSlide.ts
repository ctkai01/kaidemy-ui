import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ResponseLogin, ResponseSignUp, ResponseUpdateProfile, User } from "../../../models/auth";
import { Cart, CartItem } from "../../../models/cart";
import { clearAuthLocalStorage } from "../../../utils";
import { RootState } from "./store";

export interface CartState {
    cart: Cart | null;
    isLoading: boolean
}

const initialState: CartState = {
    cart: null,
    isLoading: false,
};

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        connectCart(state, action: PayloadAction<Cart>) {
            state.cart = action.payload;
        },
        addCartItem(state, action: PayloadAction<CartItem>) {
            if (state.cart) {
                state.cart = {
                    ...state.cart,
                    cartItems: [action.payload, ...state.cart.cartItems], // Update cart_items with the provided payload (an array of CartItem)
                };
            }
        },
        removeCartItem(state, action: PayloadAction<number>) {
            if (state.cart) {
                state.cart = {
                    ...state.cart,
                    cartItems: state.cart.cartItems.filter(
                        (cartItem) => cartItem.course.id !== action.payload,
                    ), // Update cart_items with the provided payload (an array of CartItem)
                };
            }
        },
        removeCart(state) {
            state.cart = null;
        },
        // updateProfile(state, action: PayloadAction<ResponseUpdateProfile>) {
        //     state.currentUser = action.payload.user;
        // },
        // logout(state) {
        //     state.currentUser = undefined;
        //     state.token = "";
        //     state.isLoggedIn = false;
        // },
    },
});

export const { connectCart, addCartItem, removeCartItem, removeCart } =
    cartSlice.actions;

// export const selectIsLoading = (state: RootState): boolean => state.auth.isLoading;
export const selectCart = (state: RootState): Cart | null => state.cart.cart;
// export const selectToken = (state: RootState): string => state.auth.token;
// export const selectUserAuth = (state: RootState): User | undefined =>
//     state.auth.currentUser;


export default cartSlice.reducer;
