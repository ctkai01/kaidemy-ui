import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Learning } from "../../../models";
import { ResponseLogin, ResponseSignUp, ResponseUpdateProfile, User } from "../../../models/auth";
import { Cart, CartItem } from "../../../models/cart";
import { clearAuthLocalStorage } from "../../../utils";
import { RootState } from "./store";

export interface WishlistState {
    wishList: Learning[];
    // isLoading: boolean
}

const initialState: WishlistState = {
    wishList: [],
    // isLoading: false,
};

export const wishListSlice = createSlice({
    name: "wishlist",
    initialState,
    reducers: {
        connectWishList(state, action: PayloadAction<Learning[]>) {
            state.wishList = action.payload;
        },
        addWishList(state, action: PayloadAction<Learning>) {
            state.wishList = [action.payload, ...state.wishList];
           
        },
        removeWishList(state, action: PayloadAction<number>) {
            state.wishList = state.wishList.filter(
                (item) => item.id !== action.payload,
            );
        },
        // removeCart(state) {
        //     state.cart = null;
        // },
        // updateProfile(state, action: PayloadAction<ResponseUpdateProfile>) {
        //     state.currentUser = action.payload.user;
        // },
        // logout(state) {
        //     state.currentUser = undefined;
        //     state.token = "";
        //     state.isLoggedIn = false;
        // },
    },
});

export const { connectWishList, addWishList, removeWishList } = wishListSlice.actions;

// export const selectIsLoading = (state: RootState): boolean => state.auth.isLoading;
export const selectWishList = (state: RootState): Learning[] => state.wishlist.wishList;
// export const selectToken = (state: RootState): string => state.auth.token;
// export const selectUserAuth = (state: RootState): User | undefined =>
//     state.auth.currentUser;


export default wishListSlice.reducer;
