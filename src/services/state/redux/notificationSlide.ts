import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Notification } from "../../../models";
import { ResponseLogin, ResponseSignUp, ResponseUpdateProfile, User } from "../../../models/auth";
import { Cart, CartItem } from "../../../models/cart";
import { clearAuthLocalStorage } from "../../../utils";
import { RootState } from "./store";

export interface NotificationState {
    teacher: Notification[];
    student: Notification[];
    fcmToken: string;
}

const initialState: NotificationState = {
    teacher: [],
    student: [],
    fcmToken: ""
};

export const notificationSlice = createSlice({
    name: "notification",
    initialState,
    reducers: {
        connectNotificationTeacher(
            state,
            action: PayloadAction<Notification[]>,
        ) {
            state.teacher = action.payload;
        },
        connectFcmToken(
            state,
            action: PayloadAction<string>,
        ) {
            state.fcmToken = action.payload;
        },
        connectNotificationStudent(
            state,
            action: PayloadAction<Notification[]>,
        ) {
            state.student = action.payload;
        },
        addNotificationTeacher(state, action: PayloadAction<Notification>) {
            state.teacher = [action.payload, ...state.teacher];
        },
        addListNotificationTeacher(
            state,
            action: PayloadAction<Notification[]>,
        ) {
            state.teacher = [...state.teacher, ...action.payload];
        },

        addNotificationStudent(state, action: PayloadAction<Notification>) {
            state.student = [action.payload, ...state.student];
        },
        addListNotificationStudent(
            state,
            action: PayloadAction<Notification[]>,
        ) {
            state.student = [...state.student, ...action.payload];
        },
        readAllNotificationStudent(state) {
            // state.student = [...state.student].map(item => {
            //     return {...item, isRead: true}
            // })
            state.student.forEach((notification) => {
                notification.isRead = true;
            });
        },

        readAllNotificationTeacher(state) {
            // state.teacher = [...state.teacher].map(item => {
            //     return {...item, isRead: true}
            // })
            state.teacher.forEach((notification) => {
                notification.isRead = true;
            });
        },
        // updateProfile(state, action: PayloadAction<ResponseUpdateProfile>) {
        //     state.currentUser = action.payload.user;
        // },
        // logout(state) {
        //     state.currentUser = undefined;
        //     state.token = "";
        //     state.isLoggedIn = false;
        // },
    },
});

export const {
    connectNotificationTeacher,
    connectNotificationStudent,
    connectFcmToken,
    addNotificationTeacher,
    addNotificationStudent,
    addListNotificationTeacher,
    addListNotificationStudent,
    readAllNotificationStudent,
    readAllNotificationTeacher,
} = notificationSlice.actions;

// export const selectIsLoading = (state: RootState): boolean => state.auth.isLoading;
export const selectNotificationTeacher = (state: RootState): Notification[] =>
    state.notification.teacher;
export const selectNotificationStudent = (state: RootState): Notification[] =>
    state.notification.student;
export const selectFcmTokenNotificationStudent = (state: RootState): string =>
    state.notification.fcmToken;
// export const selectToken = (state: RootState): string => state.auth.token;
// export const selectUserAuth = (state: RootState): User | undefined =>
//     state.auth.currentUser;


export default notificationSlice.reducer;
