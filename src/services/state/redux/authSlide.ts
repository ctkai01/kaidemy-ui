import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ResponseLogin, ResponseSignUp, ResponseUpdateProfile, User } from "../../../models/auth";
import { clearAuthLocalStorage } from "../../../utils";
import { RootState } from "./store";

export interface AuthState {
    isLoggedIn: boolean;
    currentUser?: User;
    token: string;
    isLoading: boolean
}

const initialState: AuthState = {
    isLoggedIn: false,
    currentUser: undefined,
    token: "",
    isLoading: false
};

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        startLogin(state) {
            state.isLoading = true;
        },
        finishLogin(state, action: PayloadAction<ResponseLogin>) {
            state.currentUser = action.payload.user;
            state.token = action.payload.token;
            state.isLoggedIn = true;
            state.isLoading = false;
        },
        signUp(state, action: PayloadAction<ResponseSignUp>) {
            state.currentUser = action.payload.user;
            state.token = action.payload.token;
            state.isLoggedIn = true;
        },
        updateProfile(state, action: PayloadAction<ResponseUpdateProfile>) {
            state.currentUser = action.payload.user;
        },
        logout(state) {
            state.currentUser = undefined;
            state.token = "";
            state.isLoggedIn = false;
        },
    },
});

export const { startLogin, finishLogin, logout, signUp, updateProfile } =
    authSlice.actions;

export const selectIsLoading = (state: RootState): boolean => state.auth.isLoading;
export const selectIsLoggedIn = (state: RootState): boolean => state.auth.isLoggedIn;
export const selectToken = (state: RootState): string => state.auth.token;
export const selectUserAuth = (state: RootState): User | undefined =>
    state.auth.currentUser;


export default authSlice.reducer;
