import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Learning } from "../../../models";
import {
    ResponseLogin,
    ResponseSignUp,
    ResponseUpdateProfile,
    User,
} from "../../../models/auth";
import { Cart, CartItem } from "../../../models/cart";
import { clearAuthLocalStorage } from "../../../utils";
import { RootState } from "./store";
import { wishListSlice } from "./wishListSlide";

export interface CoursePurchaseState {
    coursePurchase: Learning[];
    // isLoading: boolean
}

const initialState: CoursePurchaseState = {
    coursePurchase: [],
    // isLoading: false,
};

export const coursePurchaseSlice = createSlice({
    name: "coursePurchase",
    initialState,
    reducers: {
        connectCoursePurchase(state, action: PayloadAction<Learning[]>) {
            state.coursePurchase = action.payload;
        },
        addCoursePurchase(state, action: PayloadAction<Learning>) {
            state.coursePurchase = [action.payload, ...state.coursePurchase];
        },
        // removeWishList(state, action: PayloadAction<number>) {
        //     state.wishList = state.wishList.filter(
        //         (item) => item.id !== action.payload,
        //     );
        // },
        // removeCart(state) {
        //     state.cart = null;
        // },
        // updateProfile(state, action: PayloadAction<ResponseUpdateProfile>) {
        //     state.currentUser = action.payload.user;
        // },
        // logout(state) {
        //     state.currentUser = undefined;
        //     state.token = "";
        //     state.isLoggedIn = false;
        // },
    },
});

export const { connectCoursePurchase, addCoursePurchase } =
    coursePurchaseSlice.actions;

// export const selectIsLoading = (state: RootState): boolean => state.auth.isLoading;
export const selectCoursePurchase = (state: RootState): Learning[] =>
    state.coursePurchase.coursePurchase;
// export const selectToken = (state: RootState): string => state.auth.token;
// export const selectUserAuth = (state: RootState): User | undefined =>
//     state.auth.currentUser;

export default coursePurchaseSlice.reducer;
