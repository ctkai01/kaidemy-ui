import { onMessage } from "@firebase/messaging";
import { Alert } from "flowbite-react";
import React, { useEffect, useState } from "react";
import {
    BrowserRouter,
    Navigate,
    Route,
    Routes,
    useNavigate,
} from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import config from "./configs/config";
import { ACCOUNT_NORMAL, TEACHER_ROLE, STUDENT_ROLE } from "./constants";
import {
    NOTIFICATION_PURCHASE_COURSE_INSTRUCTOR,
    NOTIFICATION_PURCHASE_COURSE_STUDENT,
} from "./constants/notification";
import CommunicationGeneral from "./features/Instructor/Communication/CommunicationGeneral";
import CommunicationMessage from "./features/Instructor/Communication/CommunicationMessage";
import CommunicationQA from "./features/Instructor/Communication/CommunicationQA";
import CourseGeneral from "./features/Instructor/CourseGeneral";
import PerformanceGeneral from "./features/Instructor/Performance/PerformanceGeneral";
import PerformanceOverview from "./features/Instructor/Performance/PerformanceOverview";
import PerformanceReviews from "./features/Instructor/Performance/PerformanceReviews";
import PerformanceStudents from "./features/Instructor/Performance/PerformanceStudents";
import { useAppDispatch, useAppSelector } from "./hooks/redux-hook";
import Layout from "./layouts/Layout";
import SettingLayout from "./layouts/SettingLayout";
import Cart from "./pages/Cart";
import ChangePassword from "./pages/ChangePassword";
import CheckOut from "./pages/CheckOut";
import CourseCategory from "./pages/CourseCategory";
import CourseDetail from "./pages/CourseDetail";
import CourseManger from "./pages/CourseManager";
import CourseSearch from "./pages/CourseSearch";
import ForgotPassword from "./pages/ForgotPassword";
import Home from "./pages/Home";
import HomeAuth from "./pages/HomeAuth";
import InstructorManager from "./pages/InstructorManager";
import LearningLecture from "./pages/LearningLecture";
import Login from "./pages/Login";
import Message from "./pages/Messages";
import MyCourse from "./pages/MyCoure";
import NotFound from "./pages/NotFound";
import PublicProfile from "./pages/PublicProfile";
import RegisterTeacher from "./pages/RegisterTeacher";
import ResetPassword from "./pages/ResetPassword";
import SignUp from "./pages/SignUp";
import UpdateAvatar from "./pages/UpdateAvatar";
import UpdateProfile from "./pages/UpdateProfile";
import { NotificationServiceApi } from "./services/api/notificationServiceApi";
import { PaymentServiceApi } from "./services/api/paymentServiceApi";
import {
    getFirebaseToken,
    messaging,
    onForegroundMessage,
} from "./services/firebase";
import {
    finishLogin,
    logout,
    selectIsLoading,
    selectIsLoggedIn,
    selectUserAuth,
    startLogin,
} from "./services/state/redux/authSlide";
import { removeCartItem, selectCart } from "./services/state/redux/cartSlide";
import {
    addNotificationStudent,
    addNotificationTeacher,
    connectFcmToken,
} from "./services/state/redux/notificationSlide";
import { clearAuthLocalStorage, setAuthLocalStorage } from "./utils";
import { Socket, io } from "socket.io-client";
import { ResponseLogin, Notification as MyNotification } from "./models";

function App() {
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const isLoading = useAppSelector(selectIsLoading);
    const user = useAppSelector(selectUserAuth);
    const cart = useAppSelector(selectCart);
    const socket = React.useRef<Socket>();

    const [token, setToken] = useState("");
    const dispatch = useAppDispatch();
    const [showNotificationBanner, setShowNotificationBanner] = useState(
        Notification.permission === "default",
    );
    const handleGetFirebaseToken = async () => {
        if (isLoggedIn) {
            await getFirebaseToken()
                .then((firebaseToken) => {
                    if (firebaseToken) {
                        const handleSendFcmToken = async () => {
                            try {
                                if (token) {
                                    const body = {
                                        fcmToken: token,
                                    };

                                    await NotificationServiceApi.registerFCMToken(
                                        body,
                                    );
                                }
                            } catch (e) {
                                console.log("Err: ", e);
                            }
                        };
                        dispatch(connectFcmToken(firebaseToken));
                        handleSendFcmToken();
                        setToken(firebaseToken);
                        setShowNotificationBanner(false);
                    }
                })
                .catch((err: any) =>
                    console.error(
                        "An error occured while retrieving firebase token. ",
                        err,
                    ),
                );
        }
    };

    const checkAdmin = () => {
        if (user) {
            if (user?.role !== TEACHER_ROLE && user?.role !== STUDENT_ROLE) {
                dispatch(logout());
                clearAuthLocalStorage();
                window.location.href = "/login";
            }
        }
    };
    useEffect(() => {
        handleGetFirebaseToken();
        //    onForegroundMessage().then((payload) => {
        //        console.log("Message received in foreground: ", payload);
        //        // Handle the message payload here
        //    });
        const unsubscribe = onMessage(messaging, async (payload: any) => {
            const {
                notification: { title, body },
            } = payload;
            if (payload.data) {
                if (
                    payload.data.type === NOTIFICATION_PURCHASE_COURSE_STUDENT
                ) {
                    try {
                        // await PaymentServiceApi.removeCartItem(
                        const data: MyNotification = JSON.parse(
                            payload.data.data,
                        );
                        //     payload.data.cart_item_id,
                        // );

                        dispatch(removeCartItem(data.resourceID));
                        dispatch(addNotificationStudent(data));
                    } catch (e) {
                        console.log(eval);
                    }
                }

                if (
                    payload.data.type ===
                    NOTIFICATION_PURCHASE_COURSE_INSTRUCTOR
                ) {
                    try {
                        // await PaymentServiceApi.removeCartItem(
                        const data: MyNotification = JSON.parse(
                            payload.data.data,
                        );
                        //     payload.data.cart_item_id,
                        // );

                        //  dispatch(removeCartItem(data.resourceID));
                        dispatch(addNotificationTeacher(data));
                    } catch (e) {
                        console.log(eval);
                    }
                }
            }

            toast(
                <div className="push-notification">
                    <h2 className="my-1 font-bold">{title}</h2>
                    <p className="text-sm">{body}</p>
                </div>,
                {
                    type: "info",
                    theme: "colored",
                    hideProgressBar: true,
                    autoClose: 2000,
                },
            );
        });
        return () => {
            unsubscribe();
        };
    }, [token, user]);

    useEffect(() => {
        checkAdmin();
    }, []);

    React.useEffect(() => {
        const handleRegisterInstructor = (data: any) => {
            dispatch(startLogin());
            const payloadLogin: ResponseLogin = {
                token: data.token,
                user: data.user,
            };
            setAuthLocalStorage(data.token);
            dispatch(finishLogin(payloadLogin));

            toast(
                <div className="font-bold">
                    Register instructor successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        };
        const connectSocket = () => {
            if (user) {
                socket.current = io(`${config.apiUrl}`);
                socket.current.connect();
                socket.current.emit("join", { userID: user.id });

                socket.current.on(
                    "register-instructor",
                    handleRegisterInstructor,
                );
            }
        };
        connectSocket();
        return () => {
            // if (socket.current) {
            //     socket.current.disconnect();
            // }
        };
        // handleFocusChannel;
    }, []);

    return (
        <>
            {" "}
            <ToastContainer />
            {/* <div className="relative h-full w-full"> */}
            {showNotificationBanner && (
                // <div className=" h-full w-full ">
                <div className="z- fixed left-0 right-0 top-[72px] z-[100] font-bold text-white">
                    {/* <span>Ứng dụng cần có quyền để</span>
                    <a
                        href="#"
                        className="ml-1 text-white"
                        // onClick={handleGetFirebaseToken}
                    >
                        đẩy thông báo.
                    </a> */}
                    <Alert
                        color="info"
                        rounded
                        // onClick={handleGetFirebaseToken}
                    >
                        {/* <span className="font-medium">Info alert!</span> */}
                        Ứng dụng cần có quyền để đẩy thông báo.
                    </Alert>
                </div>
                // </div>
            )}
            {/* </div> */}
            <BrowserRouter>
                <Routes>
                    <Route path="/" Component={Layout}>
                        <Route
                            path="/checkout-success"
                            element={<CheckOut />}
                        />
                        <Route
                            index={true}
                            element={
                                isLoggedIn ? (
                                    <HomeAuth isLoading={isLoading} />
                                ) : (
                                    <Home />
                                )
                            }
                        />
                        <Route
                            path="login"
                            element={
                                isLoggedIn ? <Navigate to="/" /> : <Login />
                            }
                        />
                        <Route
                            path="sign-up"
                            element={
                                isLoggedIn ? <Navigate to="/" /> : <SignUp />
                            }
                        />
                        <Route
                            path="forgot-password"
                            element={<ForgotPassword />}
                        />
                        <Route
                            path="register-teacher"
                            element={<RegisterTeacher />}
                        />
                        {/* {user?.type_account === ACCOUNT_NORMAL && ( */}
                        <Route
                            path="email-password-change"
                            element={<ResetPassword />}
                        />
                        {/* )} */}
                        <Route
                            path="home/my-courses/:typeLearning"
                            element={<MyCourse />}
                        />
                        <Route
                            path="courses/categories/:categoryID"
                            element={<CourseCategory />}
                        />
                        <Route
                            path="courses/search"
                            element={<CourseSearch />}
                        />
                        <Route path="user" Component={SettingLayout}>
                            <Route
                                path="edit-account"
                                element={<ChangePassword />}
                            />

                            <Route
                                path="edit-photo"
                                element={<UpdateAvatar avatar={user?.avatar} />}
                            />

                            <Route
                                path="edit-profile"
                                element={<UpdateProfile user={user} />}
                            />
                        </Route>
                        <Route path="user/:id" element={<PublicProfile />} />
                        <Route path="course/:id" element={<CourseDetail />} />
                        <Route path="cart" element={<Cart cart={cart} />} />

                        {/* <Route path="/messages" element={<Message />} /> */}
                    </Route>
                    <Route
                        path="/course/:id/learn"
                        // path="/course/:id/learn/lecture/:idLecture"
                        element={<LearningLecture />}
                    />

                    <Route
                        path="/instructor/course/:id/manage/:content"
                        element={<CourseManger />}
                    />

                    <Route path="/instructor" Component={InstructorManager}>
                        <Route path="course" element={<CourseGeneral />} />
                        <Route
                            path="performance"
                            Component={PerformanceGeneral}
                        >
                            <Route
                                path="reviews"
                                element={<PerformanceReviews />}
                            />
                            <Route
                                path="overview"
                                element={<PerformanceOverview />}
                            />
                            <Route
                                path="students"
                                element={<PerformanceStudents />}
                            />
                        </Route>
                        <Route
                            path="communication"
                            Component={CommunicationGeneral}
                        >
                            <Route path="qa" element={<CommunicationQA />} />
                            {/* <Route
                                path="messages"
                                element={<CommunicationMessage />}
                            /> */}
                        </Route>
                    </Route>

                    {/* <Route
                        path="/instructor/:content"
                        element={<InstructorManager />}
                    /> */}

                    {/* <Route path="/test" element={<InstructorManagerCopy />} /> */}
                    <Route path="*" element={<NotFound />} />
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;
