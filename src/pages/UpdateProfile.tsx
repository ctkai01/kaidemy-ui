import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Label, TextInput } from "flowbite-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { ResponseUpdateProfile, UpdateProfile, User } from "../models/auth";
import { schemeUpdateProfile } from "../validators";
import Editor from "../components/ui/core/Editor";
import { toast } from "react-toastify";
import { UserServiceApi } from "../services/api/userServiceApi";
import { useAppDispatch } from "../hooks/redux-hook";
import { updateProfile } from "../services/state/redux/authSlide";

interface IUpdateProfileProps {
    user?: User;
}

export default function UpdateProfile(props: IUpdateProfileProps) {
    const { user } = props;
    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        getValues,
        watch,
        reset,
    } = useForm<UpdateProfile>({
        mode: "onChange",
        defaultValues: {
            biography: user?.biography ? user?.biography : "",
            facebook_url: user?.facebookURL ? user?.facebookURL : "",
            headline: user?.headline ? user?.headline : "",
            linkedin_url: user?.linkedInURL ? user?.linkedInURL : "",
            twitter_url: user?.twitterURL ? user?.twitterURL : "",
            website_url: user?.websiteURL ? user?.websiteURL : "",
            youtube_url: user?.youtubeURL ? user?.youtubeURL : "",
            name: user?.name ? user?.name : "",
        },
        resolver: yupResolver(schemeUpdateProfile),
    });
    const dispatch = useAppDispatch();

    const [biographyHTML, setBiographyHTML] = useState("");
    const onSubmit = async (data: UpdateProfile) => {
        try {
            const formData = new FormData();
            formData.append("name", data.name);
            formData.append("biography", data.biography ? data.biography : "");
            formData.append("headline", data.headline ? data.headline : "");
            formData.append(
                "twitterURL",
                data.twitter_url ? data.twitter_url : "",
            );
            formData.append(
                "facebookURL",
                data.facebook_url ? data.facebook_url : "",
            );
            formData.append(
                "linkedInURL",
                data.linkedin_url ? data.linkedin_url : "",
            );
            formData.append(
                "youtubeURL",
                data.youtube_url ? data.youtube_url : "",
            );

            formData.append(
                "websiteURL",
                data.website_url ? data.website_url : "",
            );

            const result = await UserServiceApi.updateProfile(formData);

            const payloadUpdateProfile: ResponseUpdateProfile = {
                user: result,
            };
            dispatch(updateProfile(payloadUpdateProfile));

            toast(<div className="font-bold">Update successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Update failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    // const handleSetDescription = (value: string) => {
    //     setValue("biography", value, {
    //         shouldValidate: true,
    //     });
    // };

    const handleSetBiographyHTML = (value: string) => {
        // setBiographyHTML(value);
        setValue("biography", value);
    };

    return (
        <div className="border border-[#d1d7dc]">
            <div className="border-b py-4">
                <div className="mx-auto my-0 max-w-[648px] px-6 text-center text-primary-black">
                    <h1 className="text-2xl font-bold">Public profile</h1>
                    <p className="font-base mt-2">
                        Add information about yourself
                    </p>
                </div>
            </div>

            <div className="py-6">
                <form
                    onSubmit={handleSubmit(onSubmit)}
                    className="flex flex-col gap-4"
                >
                    <div className="mx-auto my-0 max-w-[648px] px-6">
                        <div className="w-[600px] min-w-[180px]">
                            <div>
                                <div className="mb-2 block">
                                    <Label value="Basic:" />
                                </div>
                                <TextInput
                                    className="mt-4 first:mt-0"
                                    type="text"
                                    placeholder="Name"
                                    color={errors.name ? "failure" : ""}
                                    {...register("name")}
                                    helperText={
                                        <>
                                            {errors.name ? (
                                                <>
                                                    <span className="font-medium">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {errors.name.message}
                                                    </span>
                                                </>
                                            ) : (
                                                <></>
                                            )}
                                        </>
                                    }
                                />
                                <TextInput
                                    className="mb-4 mt-4 first:mt-0"
                                    type="text"
                                    placeholder="Headline"
                                    color={errors.headline ? "failure" : ""}
                                    {...register("headline")}
                                    helperText={
                                        <>
                                            {errors.headline ? (
                                                <>
                                                    <span className="font-medium">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {
                                                            errors.headline
                                                                ?.message
                                                        }
                                                    </span>
                                                </>
                                            ) : (
                                                <></>
                                            )}
                                        </>
                                    }
                                />
                                <Editor
                                    // handleSetValue={handleSetDescription}
                                    handleSetHTML={handleSetBiographyHTML}
                                    isImage={false}
                                    defaultValue={getValues("biography")}
                                    placeholder="Biography"
                                    // defaultValue={""}
                                />
                                {errors.biography ? (
                                    <div className="mt-2 text-sm text-red-700">
                                        <span className="font-medium">
                                            Oops!{" "}
                                        </span>
                                        <span>{errors.biography?.message}</span>
                                    </div>
                                ) : (
                                    <></>
                                )}
                            </div>
                        </div>
                        <div className="mt-6 w-[600px] min-w-[180px] border-t pt-6">
                            <div>
                                <div className="mb-2 block">
                                    <Label value="Basic:" />
                                </div>
                                <TextInput
                                    className="mt-4 first:mt-0"
                                    type="text"
                                    placeholder="Website (http(s)://)"
                                    color={errors.website_url ? "failure" : ""}
                                    {...register("website_url")}
                                    helperText={
                                        <>
                                            {errors.website_url ? (
                                                <>
                                                    <span className="font-medium">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {
                                                            errors.website_url
                                                                ?.message
                                                        }
                                                    </span>
                                                </>
                                            ) : (
                                                <></>
                                            )}
                                        </>
                                    }
                                />
                                <div className="mt-4 flex ">
                                    <div className="flex w-2/5 min-w-min items-center justify-center rounded-lg bg-primary-hover-gray">
                                        http://twitter.com/
                                    </div>
                                    <TextInput
                                        className="w-3/5"
                                        type="text"
                                        placeholder="Twitter Profile"
                                        color={
                                            errors.twitter_url ? "failure" : ""
                                        }
                                        {...register("twitter_url")}
                                        helperText={
                                            <>
                                                {errors.twitter_url ? (
                                                    <>
                                                        <span className="font-medium">
                                                            Oops!{" "}
                                                        </span>
                                                        <span>
                                                            {
                                                                errors
                                                                    .twitter_url
                                                                    ?.message
                                                            }
                                                        </span>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </>
                                        }
                                    />
                                </div>
                                <div className="mt-4 flex ">
                                    <div className="flex w-2/5 min-w-min items-center justify-center rounded-lg bg-primary-hover-gray">
                                        http://www.facebook.com/
                                    </div>
                                    <TextInput
                                        className="w-3/5"
                                        type="text"
                                        placeholder="Facebook Profile"
                                        color={
                                            errors.facebook_url ? "failure" : ""
                                        }
                                        {...register("facebook_url")}
                                        helperText={
                                            <>
                                                {errors.facebook_url ? (
                                                    <>
                                                        <span className="font-medium">
                                                            Oops!{" "}
                                                        </span>
                                                        <span>
                                                            {
                                                                errors
                                                                    .facebook_url
                                                                    ?.message
                                                            }
                                                        </span>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </>
                                        }
                                    />
                                </div>
                                <div className="mt-4 flex ">
                                    <div className="flex w-2/5 min-w-min items-center justify-center rounded-lg bg-primary-hover-gray">
                                        http://www.linkedin.com/
                                    </div>
                                    <TextInput
                                        className="w-3/5"
                                        type="text"
                                        placeholder="Linkedin Profile"
                                        color={
                                            errors.linkedin_url ? "failure" : ""
                                        }
                                        {...register("linkedin_url")}
                                        helperText={
                                            <>
                                                {errors.linkedin_url ? (
                                                    <>
                                                        <span className="font-medium">
                                                            Oops!{" "}
                                                        </span>
                                                        <span>
                                                            {
                                                                errors
                                                                    .linkedin_url
                                                                    ?.message
                                                            }
                                                        </span>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </>
                                        }
                                    />
                                </div>
                                <div className="mt-4 flex ">
                                    <div className="flex w-2/5 min-w-min items-center justify-center rounded-lg bg-primary-hover-gray">
                                        http://www.youtube.com/
                                    </div>
                                    <TextInput
                                        className="w-3/5"
                                        type="text"
                                        placeholder="Youtube Profile"
                                        color={
                                            errors.youtube_url ? "failure" : ""
                                        }
                                        {...register("youtube_url")}
                                        helperText={
                                            <>
                                                {errors.youtube_url ? (
                                                    <>
                                                        <span className="font-medium">
                                                            Oops!{" "}
                                                        </span>
                                                        <span>
                                                            {
                                                                errors
                                                                    .youtube_url
                                                                    ?.message
                                                            }
                                                        </span>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </>
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="flex justify-center">
                            <Button
                                className="mt-8 font-bold"
                                color="success"
                                type="submit"
                            >
                                Save
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}
