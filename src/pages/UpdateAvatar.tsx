import { Avatar, Button, FileInput, Label } from "flowbite-react";
import { ChangeEvent, FormEvent, useRef, useState } from "react";
import { Crop, PixelCrop, ReactCrop } from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { toast } from "react-toastify";
import { ValidationError } from "yup";
import { canvasPreview } from "../features/User/CanvasPreview";
import { useAppDispatch } from "../hooks/redux-hook";
import { useDebounceEffect } from "../hooks/useDebounce";
import { ResponseUpdateProfile, UpdateAvatar } from "../models/auth";
import { UserServiceApi } from "../services/api/userServiceApi";
import { updateProfile } from "../services/state/redux/authSlide";
import { schemeUpdateImage } from "../validators";

interface IUpdateAvatarProps {
    avatar: string | null | undefined;
}

export default function UpdateAvatar(props: IUpdateAvatarProps) {
    const { avatar } = props;
    const [crop, setCrop] = useState<Crop>();
    const [completedCrop, setCompletedCrop] = useState<PixelCrop>();
    const [isCrop, setIsCrop] = useState(false);
    const [isCropMode, setIsCropMode] = useState(false);
    const previewCanvasRef = useRef<HTMLCanvasElement>(null);
    const imgRef = useRef<HTMLImageElement>(null);
    const [error, setError] = useState("");
    const [imageFile, setImageFile] = useState<File | undefined>(undefined);
    const [imagePreview, setImagePreview] = useState(avatar ? avatar : "");

    const dispatch = useAppDispatch();

    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formData = new FormData();
        if (imageFile) {
            formData.append("avatar", imageFile);
        }

        try {
            const result = await UserServiceApi.updateProfile(formData);

            const payloadUpdateProfile: ResponseUpdateProfile = {
                user: result,
            };
            dispatch(updateProfile(payloadUpdateProfile));
            setImageFile(undefined);
            setImagePreview(result.avatar);
            toast(
                <div className="font-bold">Updated avatar successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated avatar failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
        try {
            const data: UpdateAvatar = {
                image: e.target.files ? e.target.files[0] : undefined,
            };
            const dataValidate = await schemeUpdateImage.validate(data);
            const newFile = dataValidate.image;

            setImageFile(newFile);
            setError("");
            setIsCropMode(true);
            setIsCrop(false);

            if (newFile) {
                setImagePreview(URL.createObjectURL(newFile));
            }
        } catch (e) {
            if (e instanceof ValidationError) {
                // setValue("image", undefined);

                setError(e.message);
            }
        }
    };

    useDebounceEffect(
        async () => {
            if (
                completedCrop?.width &&
                completedCrop?.height &&
                imgRef.current &&
                previewCanvasRef.current
            ) {
                // We use canvasPreview as it's much faster than imgPreview.
                canvasPreview(
                    imgRef.current,
                    previewCanvasRef.current,
                    completedCrop,
                    1,
                    0,
                );
            }
        },
        100,
        [completedCrop],
    );

    return (
        <div className="border border-[#d1d7dc]">
            <div className="border-b border-[#d1d7dc] py-4">
                <div className="mx-auto my-0 max-w-[648px] px-6 text-center text-primary-black">
                    <h1 className="text-2xl font-bold">Photo</h1>
                    <p className="font-base mt-2">
                        Add a nice photo of yourself for your profile.
                    </p>
                </div>
            </div>
            <div className="py-6">
                <form
                    onSubmit={handleSubmit}
                    className="mt-4 flex flex-col gap-4"
                >
                    <div className="mx-auto my-0 max-w-[648px] px-6">
                        <div className="min-w-[180px] max-w-[600px]">
                            <div>
                                <div className="mb-2 block">
                                    <Label value="Image preview" />
                                </div>

                                <div className="relative mb-4 flex justify-center border border-primary-black p-4">
                                    {imagePreview ? (
                                        (isCrop && isCropMode) ||
                                        (!isCropMode && isCrop) ||
                                        (!isCropMode && !isCrop) ? (
                                            <img
                                                ref={imgRef}
                                                src={imagePreview}
                                                className="max-w-[200px]"
                                            />
                                        ) : (
                                            <ReactCrop
                                                crop={crop}
                                                onChange={(_, percentCrop) =>
                                                    setCrop(percentCrop)
                                                }
                                                onComplete={(c) =>
                                                    setCompletedCrop(c)
                                                }
                                            >
                                                <img
                                                    loading="lazy"
                                                    ref={imgRef}
                                                    src={imagePreview}
                                                    className="max-w-[200px]"
                                                />
                                            </ReactCrop>
                                        )
                                    ) : (
                                        <Avatar
                                            className="h-[200px] w-[200px]"
                                            rounded
                                            size="xl"
                                            img={
                                                "https://img-b.udemycdn.com/user/200_H/anonymous_3.png"
                                            }
                                        />
                                    )}
                                    {!!completedCrop && (
                                        <div className="invisible absolute left-0 top-0">
                                            <canvas
                                                ref={previewCanvasRef}
                                                style={{
                                                    // border: "1px solid black",
                                                    objectFit: "contain",
                                                    width: completedCrop.width,
                                                    height: completedCrop.height,
                                                }}
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="w-[600px]">
                                <div className="mb-2 block">
                                    <Label
                                        htmlFor="file"
                                        value="Add / Change Image"
                                    />
                                </div>
                                {isCropMode ? (
                                    isCrop ? (
                                        <Button
                                            onClick={() => {
                                                setIsCropMode(false);
                                            }}
                                        >
                                            Change
                                        </Button>
                                    ) : (
                                        <Button
                                            onClick={() => {
                                                if (imageFile) {
                                                    previewCanvasRef.current?.toBlob(
                                                        (blob) => {
                                                            if (blob) {
                                                                // You can use the blob as needed, for example, create an object URL
                                                                const imageURL =
                                                                    URL.createObjectURL(
                                                                        blob,
                                                                    );

                                                                // Perform further actions with the imageURL or the blob

                                                                setImagePreview(
                                                                    imageURL,
                                                                );

                                                                if (imageFile) {
                                                                    const newFile =
                                                                        new File(
                                                                            [
                                                                                blob,
                                                                            ],
                                                                            imageFile.name,
                                                                            {
                                                                                type: imageFile.type,
                                                                            },
                                                                        );
                                                                    setImageFile(
                                                                        newFile,
                                                                    );
                                                                }
                                                                setIsCrop(true);
                                                            }
                                                        },
                                                        imageFile.type,
                                                    );
                                                }
                                            }}
                                        >
                                            Crop image
                                        </Button>
                                    )
                                ) : (
                                    <FileInput
                                        id="file"
                                        color={error ? "failure" : ""}
                                        onChange={handleFileChange}
                                        helperText={error ? error : ""}
                                    />
                                )}
                            </div>
                        </div>

                        <div className="flex justify-center">
                            <Button
                                className="mt-8 font-bold"
                                color="success"
                                type="submit"
                            >
                                Save
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}
