import { Alert, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import { useState } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import ForgotPasswordForm from "../features/Auth/ForgotPassword/ForgotPasswordForm";
import { ForgotPassword } from "../models/auth";
import { UserServiceApi } from "../services/api/userServiceApi";

const customAlertTheme: CustomFlowbiteTheme = {
    alert: {
        icon: "h-10 w-10 mr-4",
    },
};

const ForgotPassword = () => {
    const [isSendEmail, setIsSendEmail] = useState(false);
    const [errResponse, setErrResponse] = useState("");

    const handleSendRequestForgotPassword = async (data: ForgotPassword) => {
        try {
            const result = await UserServiceApi.forgotPassword(data);
            setIsSendEmail(true);
        } catch (err: any) {
            setErrResponse("Something error server");
        }
    };
    return (
        <div className="bg-white h-screen">
            <div className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                {isSendEmail ? (
                    <Flowbite theme={{ theme: customAlertTheme }}>
                        <Alert
                            color="success"
                            icon={AiFillCheckCircle}
                            rounded
                            withBorderAccent
                        >
                            <h2 className="text-base font-bold text-[#2D2F31]">
                                Reset password email sent
                            </h2>
                            <p className="text-[#2D2F31]">
                                You should soon receive an email allowing you to
                                reset your password. Please make sure to check
                                your spam and trash if you can't find the email.
                            </p>
                        </Alert>
                    </Flowbite>
                ) : (
                    <ForgotPasswordForm
                        className="shadow-2xl"
                        handleSendRequestForgotPassword={
                            handleSendRequestForgotPassword
                        }
                        errResponse={errResponse}
                    />
                )}
            </div>
        </div>
    );
};

export default ForgotPassword;
