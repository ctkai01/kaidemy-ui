import { useLayoutEffect, useState } from "react";
import ActionLectureTabs from "../features/LeaningLecture/ActionLectureTabs";
import LearningHeader from "../features/LeaningLecture/LearningHeader";
import SectionLectureSidebar from "../features/LeaningLecture/SectionLectureSibar";
import VideoLearning from "../features/LeaningLecture/VideoLearning";
import LeaveRatingModal from "../features/LeaningLecture/LeaveRatingModal";
import { useParams } from "react-router-dom";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { CourseShow } from "../models";
import { Spinner } from "flowbite-react";
import LearningLectureDetail from "../features/LeaningLecture/LearningLectureDetail";
import { toast } from "react-toastify";

export default function LearningLecture() {
    const [openLeaveRatingModal, setOpenLeaveRatingModal] = useState(false);
    const [loading, setLoading] = useState(false);

    const handleSetOpenLeaveRatingModal = (command: boolean) => {
        setOpenLeaveRatingModal(command);
    };
    const [course, setCourse] = useState<CourseShow | null>(null);

    const [courseNotFound, setCourseNotFound] = useState(false);

    const { id: courseID } = useParams();

    useLayoutEffect(() => {
        const fetchCourse = async () => {
            try {
                setLoading(true);

                if (courseID && !isNaN(+courseID)) {
                    const courseResult =
                        await CourseServiceApi.getDetailCurriCulumsByCourseByID(
                            +courseID,
                        );
                    setCourse(courseResult);
                    setLoading(false);
                } else {
                    setCourseNotFound(true);
                    setLoading(false);
                }
            } catch (e: any) {
                console.log(e);

                if (e.response.data.Message.includes("course not found")) {
                    setCourseNotFound(true);
                    setLoading(false);
                }
            }
        };
        fetchCourse();
    }, []);

    const handleMarkLecture = async (
        curriculumID: number,
        lectureID: number,
    ) => {
        if (course) {
            try {
                const body = {
                    courseID: course.id,
                };
                const learningLecture = await CourseServiceApi.markLecture(
                    lectureID,
                    body,
                );

                setCourse((course) => {
                    if (course) {
                        const courseUpdate = { ...course };
                        const curriculumsUpdate = [...courseUpdate.curriculums];

                        const findIndexCurriculum = curriculumsUpdate.findIndex(
                            (curriculum) => curriculum.id === curriculumID,
                        );

                        if (findIndexCurriculum !== -1) {
                            const curriculumUpdate = {
                                ...curriculumsUpdate[findIndexCurriculum],
                            };

                            const lecturesUpdate = [
                                ...curriculumUpdate.lectures,
                            ];
                            const findIndexLecture = lecturesUpdate.findIndex(
                                (lecture) => lecture.id === lectureID,
                            );

                            if (findIndexLecture !== -1) {
                                const lectureUpdate = {
                                    ...lecturesUpdate[findIndexLecture],
                                };
                                lectureUpdate.learningLectures[0] = {
                                    ...learningLecture,
                                };

                                lecturesUpdate[findIndexLecture] =
                                    lectureUpdate;

                                curriculumUpdate.lectures = lecturesUpdate;

                                curriculumsUpdate[findIndexCurriculum] =
                                    curriculumUpdate;

                                courseUpdate.curriculums = curriculumsUpdate;

                                return courseUpdate;
                            } else {
                                return course;
                            }
                        } else {
                            return course;
                        }
                    }

                    return course;
                });
                toast(
                    <div className="font-bold">
                        Marked as complete successfully!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            } catch (e) {
                console.log(e);
                toast(
                    <div className="font-bold">
                        {" "}
                        Marked as complete failed!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "error",
                        theme: "colored",
                    },
                );
            }
        }
    };
    return (
        <>
            {courseNotFound ? (
                <div className="flex-1">
                    <div className="mx-auto  max-w-[1340px]  px-6 pb-16 pt-12 text-center">
                        <div className="flex justify-center">
                            <img src="https://s.udemycdn.com/error_page/error-desktop-v1.jpg" />
                        </div>

                        <h1 className="text-[32px] font-bold text-primary-black">
                            We can’t find the page you’re looking for
                        </h1>
                    </div>
                </div>
            ) : loading ? (
                <div className="flex-1 text-center">
                    <Spinner size="xl" className="mt-10" />
                </div>
            ) : (
                course && (
                    <LearningLectureDetail
                        handleSetOpenLeaveRatingModal={
                            handleSetOpenLeaveRatingModal
                        }
                        handleMarkLecture={handleMarkLecture}
                        course={course}
                    />
                )
            )}
        </>
    );
}
