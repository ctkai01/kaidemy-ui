import {
    Breadcrumb,
    CustomFlowbiteTheme,
    Flowbite,
    Spinner,
} from "flowbite-react";
import { useEffect, useLayoutEffect, useMemo, useState } from "react";
import { PiSealWarningFill } from "react-icons/pi";
import { TbWorldCode } from "react-icons/tb";
import { Link, useParams } from "react-router-dom";
import BestSeller from "../components/ui/core/BestSeller";
import ReviewStar from "../components/ui/core/ReviewStar";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../constants";
import { dataVideoPreview } from "../data/common";
import CourseDetailBody from "../features/Course/CourseDetailBody";
import HeaderCourse from "../features/Course/HeaderCourse";
import ReportModal from "../features/Course/ReportModal";
import ReviewListModal from "../features/Course/ReviewListModal";
import SidebarCourse from "../features/Course/SidebarCourse";
import VideoPreviewList from "../features/Course/VideoPreviewList";
import { Asset, Course, CourseShow, StatAuthor } from "../models";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { UserServiceApi } from "../services/api/userServiceApi";
import { formatDate, formatNumberWithCommas } from "../utils";
export interface ICourseDetailProps {}

const customTheme: CustomFlowbiteTheme = {
    breadcrumb: {
        item: {
            base: "group flex items-center font-bold",
            chevron: "mx-1 h-4 w-4 text-white group-first:hidden md:mx-2",
            href: {
                // off: "flex items-center text-sm font-medium text-gray-500 dark:text-gray-400",
                on: "flex !text-[#07baf0f] font-bold items-center text-sm dark:text-gray-400",
            },
        },
    },
};
export interface VideoPreview {
    title: string;
    asset: Asset;
}

export default function CourseDetail(props: ICourseDetailProps) {
    const [isStickySidebar, setIsStickySidebar] = useState(false);
    const [prevScrollPos, setPrevScrollPos] = useState(0);
    const [course, setCourse] = useState<CourseShow | null>(null);
    const [statAuthor, setStatAuthor] = useState<StatAuthor | null>(null);
    let { id: courseID } = useParams();
    const [courseNotFound, setCourseNotFound] = useState(false);
    const [loading, setLoading] = useState(false);
    const [videoPreviewID, setVideoPreviewID] = useState("");

    const [videosPreviewID, setVideosPreviewID] = useState<VideoPreview[]>([]);

    const [openPreviewVideoModal, setOpenPreviewVideoModal] = useState(false);
    const [openReviewListModal, setOpenReviewListModal] = useState(false);
    const [openReportModal, setOpenReportModal] = useState(false);
    useEffect(() => {
        const handleScroll = () => {
            const currentScrollPos = window.scrollY;
            // If scrolling down and past a certain distance (e.g., 100 pixels), hide the header
            if (currentScrollPos > 200) {
                setIsStickySidebar(true);
            } else {
                setIsStickySidebar(false);
            }

            setPrevScrollPos(currentScrollPos);
        };

        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, [prevScrollPos]);

    useLayoutEffect(() => {
        const fetchCourse = async () => {
            try {
                setLoading(true);

                if (courseID && !isNaN(+courseID)) {
                    const data =
                        await CourseServiceApi.getCurriCulumsByCourseByID(
                            +courseID,
                        );
                    // data.course.user.id
                    const statAuthor = await UserServiceApi.getStatAuthor(
                        data.user.id,
                    );
                    setStatAuthor(statAuthor);

                    const courseData: CourseShow = data;
                    const dataVideo: VideoPreview[] = [];
                    courseData.curriculums.forEach((curriculum) => {
                        curriculum.lectures.forEach((lecture) => {
                            if (lecture.isPromotional) {
                                const videoAsset = lecture.assets.find(
                                    (asset) =>
                                        asset.type === LECTURE_WATCH_ASSET_TYPE,
                                );

                                if (videoAsset) {
                                    dataVideo.push({
                                        title: lecture.title,
                                        asset: videoAsset,
                                    });
                                }
                            }
                        });
                    });
                    setVideosPreviewID(dataVideo);
                    if (dataVideo.length) {
                        setVideoPreviewID(dataVideo[0].asset.url);
                    }
                    setCourse(data);
                    setLoading(false);
                } else {
                    setCourseNotFound(true);
                    setLoading(false);
                }
            } catch (e: any) {
                console.log(e);

                if (e.response.status === 404) {
                    setCourseNotFound(true);
                    setLoading(false);
                }
            }
        };

        fetchCourse();
    }, [courseID]);

    const handleSetPreviewVideoModal = (command: boolean, init: boolean) => {
        if (init && videoPreviewID) {
            setVideoPreviewID(videosPreviewID[0].asset.url);
        }
        setOpenPreviewVideoModal(command);
    };

    const handleSetReviewListModal = (command: boolean) => {
        setOpenReviewListModal(command);
    };

    const handleSetReportModal = (command: boolean) => {
        setOpenReportModal(command);
    };

    const handleChangeVideoPreviewID = (idVideo: string) => {
        setVideoPreviewID(idVideo);
    };

    const handleShowModalPreviewWithVideoID = (videoID: string) => {
        setOpenPreviewVideoModal(true);
        setVideoPreviewID(videoID);
    };

    const { totalDuration, totalLecture, totalSection } = useMemo(() => {
        let totalLecture = 0;
        let totalDuration = 0;
        let totalSection = 0;
        if (course) {
            totalSection = course.curriculums.length;
            let totalLecture = 0;
            let totalDuration = 0;
            course.curriculums.forEach((curriculum) => {
                if (curriculum.lectures) {
                    totalLecture += curriculum.lectures.length;
                    curriculum.lectures.forEach((lecture) => {
                        if (lecture.type === LECTURE_TYPE) {
                            lecture.assets.forEach((asset) => {
                                if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
                                    totalDuration += asset.duration;
                                }
                            });
                        }
                    });
                }
            });

            return { totalSection, totalLecture, totalDuration };
        } else {
            return { totalSection, totalLecture, totalDuration };
        }
    }, [course]);

    return (
        <>
            {courseNotFound ? (
                <div className="flex-1">
                    <div className="mx-auto  max-w-[1340px]  px-6 pb-16 pt-12 text-center">
                        <div className="flex justify-center">
                            <img src="https://s.udemycdn.com/error_page/error-desktop-v1.jpg" />
                        </div>

                        <h1 className="text-[32px] font-bold text-primary-black">
                            We can’t find the page you’re looking for
                        </h1>
                    </div>
                </div>
            ) : loading ? (
                <div className="flex-1 text-center">
                    <Spinner size="xl" className="mt-10" />
                </div>
            ) : (
                course && (
                    <div className="relative flex-1">
                        {isStickySidebar && (
                            <div className="fixed left-0 top-0 z-10 w-full bg-primary-black">
                                <HeaderCourse course={course} />
                            </div>
                        )}
                        <VideoPreviewList
                            course={course}
                            videosPreviewID={videosPreviewID}
                            openPreviewVideoModal={openPreviewVideoModal}
                            videoPreviewID={videoPreviewID}
                            handleChangeVideoPreviewID={
                                handleChangeVideoPreviewID
                            }
                            handleSetPreviewVideoModal={
                                handleSetPreviewVideoModal
                            }
                        />

                        <ReportModal
                            idCourse={course.id}
                            openReportModal={openReportModal}
                            handleSetReportModal={handleSetReportModal}
                        />
                        <div className="bg-primary-black py-8">
                            <div className="mx-auto max-w-[1184px]">
                                <div className="mx-12 font-bold text-[#8dfdff]">
                                    <Flowbite theme={{ theme: customTheme }}>
                                        <Breadcrumb aria-label="Default breadcrumb example text-[#07baf0f]">
                                            <Breadcrumb.Item href="#">
                                                {course.category?.name}
                                            </Breadcrumb.Item>
                                            <Breadcrumb.Item href="#">
                                                {course.subCategory?.name}
                                            </Breadcrumb.Item>
                                            {/* <Breadcrumb.Item href="#">
                                                Rust (programming language)
                                            </Breadcrumb.Item> */}
                                        </Breadcrumb>
                                    </Flowbite>
                                </div>
                                <div className="mx-12 max-w-[730px]">
                                    <div>
                                        <h1 className="text-[32px] font-bold text-white">
                                            {course.title}
                                        </h1>
                                        <div className="mb-4 text-lg text-white">
                                            {course.subtitle}
                                        </div>
                                        <div className="mb-2 flex items-center">
                                            {/* <BestSeller className="mr-2" /> */}
                                            <span className="mr-2 text-sm font-bold text-[#f69c08]">
                                                {course.averageReview}
                                            </span>
                                            <ReviewStar
                                                color="#f69c08"
                                                number={course.averageReview}
                                            />
                                            <span className="mr-2 text-sm text-[#8dfdff] underline">
                                                (
                                                {formatNumberWithCommas(
                                                    course.countReview,
                                                )}{" "}
                                                ratings)
                                            </span>
                                            <span className="text-sm text-white">
                                                {course.countStudent} students
                                            </span>
                                        </div>
                                        <div className="mb-2 flex items-center text-sm">
                                            <span className="mr-1 text-white">
                                                Created by{" "}
                                            </span>
                                            <Link
                                                className="text-[#8dfdff] underline"
                                                to={`/user/${course.user.id}`}
                                            >
                                                {course.user.name}
                                            </Link>
                                        </div>
                                        <div className="flex text-sm text-white">
                                            {/* <div className="mb-2 mr-4 flex items-center">
                                                <PiSealWarningFill className="mr-2" />
                                                <span>
                                                    Cập nhật mới nhất{" "}
                                                    {formatDate(
                                                        course.updated_at,
                                                    )}
                                                </span>
                                            </div> */}
                                            <div className="mb-2 flex items-center">
                                                <TbWorldCode className="mr-2 " />
                                                <span>
                                                    {course.language?.name}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* Sidebar course */}
                        <div className="mx-auto max-w-[1184px]">
                            <div
                                className={`${
                                    isStickySidebar
                                        ? "fixed top-4 z-20 animate-fade-in opacity-0"
                                        : "absolute top-8"
                                }  ml-[796px]`}
                            >
                                <div className="relative z-[11]">
                                    <SidebarCourse
                                        handleSetPreviewVideoModal={
                                            handleSetPreviewVideoModal
                                        }
                                        totalDuration={totalDuration}
                                        course={course}
                                        videoPreviewID={videoPreviewID}
                                    />
                                </div>
                            </div>
                        </div>

                        {/* Detail */}
                        <div className="mx-auto max-w-[1184px] pt-8">
                            <div className="mx-12 max-w-[700px]">
                                <CourseDetailBody
                                    handleSetReviewListModal={
                                        handleSetReviewListModal
                                    }
                                    openReviewListModal={openReviewListModal}
                                    statAuthor={statAuthor}
                                    handleShowModalPreviewWithVideoID={
                                        handleShowModalPreviewWithVideoID
                                    }
                                    totalDuration={totalDuration}
                                    totalLecture={totalLecture}
                                    totalSection={totalSection}
                                    course={course}
                                    handleSetReportModal={handleSetReportModal}
                                />
                            </div>
                        </div>
                    </div>
                )
            )}
        </>
    );
}
