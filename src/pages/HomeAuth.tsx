// import Banner from "../components/ui/core/Banner/Banner";
import { Spinner } from "flowbite-react";
import { useEffect, useState } from "react";
import { WISH_LIST_TYPE } from "../constants";
import { NOTIFICATION_STUDENT, NOTIFICATION_TEACHER } from "../constants/notification";
import { continueCourses } from "../data/common";
import Banner from "../features/Home/Banner/Banner";
import ContinueCourseList from "../features/Home/ContinueCourseList/ContinueCourseList";
import TopCourse from "../features/Home/TopCourse/TopCourse";
import { useAppDispatch, useAppSelector } from "../hooks/redux-hook";
import { Learning, Notification, NotificationType, TopCategory } from "../models";
import { Cart } from "../models/cart";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { NotificationServiceApi } from "../services/api/notificationServiceApi";
import { PaymentServiceApi } from "../services/api/paymentServiceApi";
import { selectUserAuth } from "../services/state/redux/authSlide";
import { connectCart } from "../services/state/redux/cartSlide";
import { connectCoursePurchase } from "../services/state/redux/coursePurchaseSlide";
import { connectNotificationStudent, connectNotificationTeacher } from "../services/state/redux/notificationSlide";
import { connectWishList } from "../services/state/redux/wishListSlide";

export interface IHomeAuthProps {
    isLoading: boolean;
}

const HomeAuth = (props: IHomeAuthProps) => {
    // const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const [isLoadingHome, setIsLoadingHome] = useState(true);
    const [isLoadingTopCategories, setIsLoadingTopCategories] = useState(false);
    const [topCategories, setTopCategories] = useState<TopCategory[]>([]);
    const dispatch = useAppDispatch();
    const user = useAppSelector(selectUserAuth);
    const { isLoading } = props;

    useEffect(() => {
        const fetchTopCategory = async () => {
            try {
                setIsLoadingTopCategories(true);
                const topCategoriesData =
                    await CourseServiceApi.getTopCategories();
                setTopCategories(topCategoriesData);
                setIsLoadingTopCategories(false);
            } catch (e) {
                console.log(e);
            }
        };
        const fetchCart = async () => {
            try {
                const cartData = await PaymentServiceApi.getCart();
                const payloadConnectCart: Cart = cartData;

                if (!cartData) {
                    const createCartData = await PaymentServiceApi.createCart();
                    dispatch(connectCart(createCartData));
                } else {
                    dispatch(connectCart(payloadConnectCart));
                }
            } catch (e) {
                console.log(e);
            }
        };

        const fetchWishList = async () => {
            try {
                const wishListData = await CourseServiceApi.getLearnings(
                    [WISH_LIST_TYPE],
                    1,
                    100,
                    user ? user.id : 0,
                );
                const payloadConnectWishList: Learning[] = wishListData.item;
                dispatch(connectWishList(payloadConnectWishList));
                // if (!cartData.cart) {
                //     const createCartData = await PaymentServiceApi.createCart();
                //     console.log("createCartData: ", createCartData);
                //     dispatch(connectCart(createCartData.cart));
                // } else {
                //     dispatch(connectCart(payloadConnectCart));
                // }
            } catch (e) {
                console.log(e);
            }
        };

        const fetchCoursePurchase = async () => {
            try {
                const wishListData = await CourseServiceApi.getCoursePurchase(
                    1,
                    100,
                    user ? user.id : 0,
                );
                console.log("cartData: ", wishListData);
                const payloadConnectCoursePurchase: Learning[] =
                    wishListData.item;
                dispatch(connectCoursePurchase(payloadConnectCoursePurchase));
                // if (!cartData.cart) {
                //     const createCartData = await PaymentServiceApi.createCart();
                //     console.log("createCartData: ", createCartData);
                //     dispatch(connectCart(createCartData.cart));
                // } else {
                //     dispatch(connectCart(payloadConnectCart));
                // }
            } catch (e) {
                console.log(e);
            }
        };


        const fetchNotificationTeacher = async () => {
            try {
                const notificationTeacher =
                    await NotificationServiceApi.getNotifications(
                        1,
                        100,
                        NOTIFICATION_TEACHER,
                    );
                const payloadConnectNotificationTeacher: Notification[] =
                    notificationTeacher.item;
                dispatch(
                    connectNotificationTeacher(
                        payloadConnectNotificationTeacher,
                    ),
                );
            } catch (e) {
                console.log(e);
            }
        };

         const fetchNotificationStudent = async () => {
             try {
                 const notificationStudent =
                     await NotificationServiceApi.getNotifications(
                         1,
                         100,
                         NOTIFICATION_STUDENT,
                     );
                 const payloadConnectNotificationStudent: Notification[] =
                     notificationStudent.item;
                 dispatch(
                     connectNotificationStudent(
                         payloadConnectNotificationStudent,
                     ),
                 );
             } catch (e) {
                 console.log(e);
             }
         };


        const fetchData = async () => {
            await Promise.all([
                fetchTopCategory(),
                fetchCart(),
                fetchWishList(),
                fetchCoursePurchase(),
                fetchNotificationTeacher(),
                fetchNotificationStudent()
            ]);
        };
        fetchData();
        setIsLoadingHome(false);
    }, []);
    return (
        // <>
        <div className="flex-1 px-56">
            {isLoading || isLoadingHome ? (
                <div className="mt-6 text-center">
                    <Spinner size="xl" />
                </div>
            ) : (
                <div className="overflow-hidden">
                    <Banner />
                    <div className="px-6">
                        {/* {continueCourses.length && (
                            <ContinueCourseList
                                continueCourseList={continueCourses}
                            />
                        )} */}
                        <TopCourse
                            isLoadingTopCategories={isLoadingTopCategories}
                            topCategories={topCategories}
                        />
                    </div>
                </div>
            )}
        </div>
    );
};

export default HomeAuth;
