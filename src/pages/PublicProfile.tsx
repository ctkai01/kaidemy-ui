import { Spinner } from "flowbite-react";
import { useEffect, useLayoutEffect, useState } from "react";
import { useParams } from "react-router-dom";
import UserInstructor from "../features/User/PublicProfile/UserInstructor";
import UserStudent from "../features/User/PublicProfile/UserStudent";
import { User } from "../models";
import { UserServiceApi } from "../services/api/userServiceApi";
import { STUDENT_ROLE } from "../constants";
import { CourseServiceApi } from "../services/api/courseServiceApi";
export default function PublicProfile() {
    // const isTeacher = Math.floor(Math.random() * 2);

    let { id: userID } = useParams();
    const [user, setUser] = useState<User | null>(null);
    const [loading, setLoading] = useState(false);
    const [userNotFound, setUserNotFound] = useState(false);

    useLayoutEffect(() => {
        const fetchUser = async () => {
            try {
                setLoading(true);

                if (userID && !isNaN(+userID)) {
                    const data = await UserServiceApi.getUserByID(+userID);

                    setUser(data);
                } else {
                    setUserNotFound(true);
                }
                setLoading(false);
            } catch (e: any) {
                console.log(e);
                if (e.response.data.message.includes("user not found")) {
                    setUserNotFound(true);
                    setLoading(false);
                }
            }
        };
        fetchUser();
    }, []);

    const isTeacher = false;
    // return <>{isTeacher ? <UserInstructor /> : <UserStudent />}</>;
    return (
        <>
            {userNotFound ? (
                <div className="flex-1">
                    <div className="mx-auto  max-w-[1340px]  px-6 pb-16 pt-12 text-center">
                        <div className="flex justify-center">
                            <img src="https://s.udemycdn.com/error_page/error-desktop-v1.jpg" />
                        </div>

                        <h1 className="text-[32px] font-bold text-primary-black">
                            We can’t find the page you’re looking for
                        </h1>
                    </div>
                </div>
            ) : loading ? (
                <div className="flex-1 text-center">
                    <Spinner size="xl" className="mt-10" />
                </div>
            ) : user && user.role === STUDENT_ROLE ? (
                <UserStudent user={user} />
            ) : (
                user && <UserInstructor user={user} />
                // <div>dsds</div>
            )}
        </>
    );
}
