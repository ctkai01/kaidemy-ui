import React, { useState } from "react";
import CreateCourseModal from "../features/Instructor/CreateCourseModal";
import InstructorHeader from "../features/Instructor/InstructorHeader";
import InstructorSidebar from "../features/Instructor/InstructorSidebar";
import { Category } from "../models";
import { CategoryServiceApi } from "../services/api/categoryServiceApi";

export default function InstructorManager() {
    // const [openCreateCourseModal, setOpenCreateCourseModal] = useState(false);
    // const [categories, setCategories] = React.useState<Category[]>([]);

    // const handleCommandCourseModal = (command: boolean) => {
    //     setOpenCreateCourseModal(command);
    // };
    // React.useEffect(() => {
    //     const getParentCategories = async () => {
    //         try {
    //             const categoryParentResult =
    //                 await CategoryServiceApi.getCategoryMenu();
    //             console.log(categoryParentResult.Categories.items);
    //             setCategories(categoryParentResult.Categories.items);
    //         } catch (e) {
    //             console.log(e);
    //         }
    //     };

    //     getParentCategories();
    // }, []);

    // console.log("categories: ", categories);
    return (
        <>
            <div className="relative flex-1">
                {/* <CreateCourseModal
                    categories={categories}
                    openCreateCourseModal={openCreateCourseModal}
                    handleCommandCourseModal={handleCommandCourseModal}
                /> */}
                <div className="w-286-calc absolute left-[286px] z-[100]">
                    <InstructorHeader />
                </div>
                {/* <div className="absolute h-full"> */}
                <InstructorSidebar />

                {/* </div> */}
            </div>
            {/* <div className="h-[200px] bg-primary-blue">dsds</div> */}
        </>
    );
}
