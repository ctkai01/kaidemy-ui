// import Banner from "../components/ui/core/Banner/Banner";
import { useEffect, useState } from "react";
import Banner from "../features/Home/Banner/Banner";
import TrustList from "../features/Home/Banner/TrustList";
import Explore from "../features/Home/Explore/Explore";
import TeachIntro from "../features/Home/TeachIntro/TeachIntro";
import TopCategories from "../features/Home/TopCategories/TopCategories";
import { TopCategory } from "../models";
import { CourseServiceApi } from "../services/api/courseServiceApi";

const Home = () => {
    const [isLoadingTopCategories, setIsLoadingTopCategories] = useState(false);
    const [topCategories, setTopCategories] = useState<TopCategory[]>([]);

    useEffect(() => {
        const fetchTopCategory = async () => {
            try {
                setIsLoadingTopCategories(true);
                const topCategoriesData =
                    await CourseServiceApi.getTopCategories();
                setTopCategories(topCategoriesData);
                setIsLoadingTopCategories(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchTopCategory();
    }, []);

    return (
        <div className="flex-1">
            <div className="bg-white px-56">
                <Banner />
            </div>
            <TrustList />
            <div className="bg-white px-56">
                <Explore topCategories={topCategories} isLoadingTopCategories={isLoadingTopCategories}/>
                {/* <TopCategories />
                <TeachIntro /> */}
            </div>
        </div>
    );
};

export default Home;
