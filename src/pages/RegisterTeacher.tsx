import { Alert, Button, Card } from "flowbite-react";
import * as React from "react";
import { AiOutlineLoading } from "react-icons/ai";
import { FaExternalLinkAlt } from "react-icons/fa";
import { Link, useSearchParams } from "react-router-dom";
import { STRIPE_ACCOUNT_VERIFY_STATUS, TEACHER_ROLE } from "../constants";
import { useAppDispatch, useAppSelector } from "../hooks/redux-hook";
import { ResponseLogin } from "../models";
import { UserServiceApi } from "../services/api/userServiceApi";
import {
    finishLogin,
    selectUserAuth,
    startLogin,
} from "../services/state/redux/authSlide";
import { setAuthLocalStorage } from "../utils";

export interface IRegisterTeacherProps {}

export default function RegisterTeacher(props: IRegisterTeacherProps) {
    const [searchParams] = useSearchParams();
    const [urlRegister, setUrlRegister] = React.useState("");
    const [registerSuccess, setRegisterSuccess] = React.useState(false);
    const [isTeacher, setIsTeacher] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const dispatch = useAppDispatch();
    const user = useAppSelector(selectUserAuth);

    React.useEffect(() => {
        const requestTeacher = async () => {
            try {
                setLoading(true);

                const result = await UserServiceApi.requestTeacher();
                setUrlRegister(result.link);

                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };

        if (
            user?.role === TEACHER_ROLE &&
            user?.accountStripeStatus === STRIPE_ACCOUNT_VERIFY_STATUS
        ) {
            setIsTeacher(true);
        } else {
            // if (!searchParams.get("key") && !searchParams.get("return_url")) {
            //     requestTeacher();
            // }
            requestTeacher();
            // if (searchParams.get("key") && searchParams.get("return_url")) {
            //     verifyTeacher();
            // }
            // verifyTeacher();
        }
    }, []);
    return (
        <div className="flex-1">
            {/* <div className="mx-auto w-[450px]">
              You need to link your card with us to become a teacher and to
              receive fees from the courses you create
          </div> */}
            {isTeacher ? (
                <div className="mx-auto mt-16  w-[450px]">
                    <Alert
                        color="success"
                        // onDismiss={() => alert("Alert dismissed!")}
                    >
                        <span className="mr-1 text-lg font-bold">
                            Info alert!
                        </span>{" "}
                        <span className="text-lg">
                            You are already a teacher
                        </span>
                    </Alert>
                    <Link to="/instructor/course" className="mt-4 block w-full">
                        <Button className="w-full">
                            <span className="font-bold">
                                Go to Course manage
                            </span>
                        </Button>
                    </Link>
                </div>
            ) : (
                <>
                    {registerSuccess && (
                        <Card className="mx-auto mt-12 max-w-sm bg-red-700">
                            <h5 className="text-3xl font-bold tracking-tight text-yellow-400  dark:text-white">
                                Congratulations!
                            </h5>
                            <p className="font-normal text-white dark:text-gray-400">
                                You have become a teacher. You can now create
                                your own course.
                            </p>

                            <Link to="/instructor/course" className="w-full">
                                <Button className="w-full">
                                    <span className="font-bold">
                                        Go to Course manage
                                    </span>
                                </Button>
                            </Link>
                        </Card>
                    )}

                    {!registerSuccess && (
                        <Card className="mx-auto mt-12 max-w-sm">
                            <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                Become to teacher ?
                            </h5>
                            <p className="font-normal text-gray-700 dark:text-gray-400">
                                You need to link your card with us to become a
                                teacher and to receive fees from the courses you
                                create.
                            </p>
                            {loading ? (
                                <Button isProcessing>
                                    <span className="font-bold">Link</span>
                                    <FaExternalLinkAlt className="ml-4" />
                                </Button>
                            ) : (
                                <Link
                                    target={"_blank"}
                                    to={urlRegister}
                                    className="w-full"
                                >
                                    <Button className="w-full">
                                        <span className="font-bold">Link</span>
                                        <FaExternalLinkAlt className="ml-4" />
                                    </Button>
                                </Link>
                            )}
                        </Card>
                    )}
                </>
            )}
        </div>
    );
}
