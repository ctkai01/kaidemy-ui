import {
    CustomFlowbiteTheme,
    Flowbite,
    Tabs,
    TabsComponent,
    TabsRef,
} from "flowbite-react";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { MY_LEARNING_TABS } from "../constants";
import CourseLearningList from "../features/MyLearning/AllCourse/CourseLearningList";
import ArchivedListList from "../features/MyLearning/Archived/ArchivedList";
import MyLists from "../features/MyLearning/MyLists/MyLists";
import WishList from "../features/MyLearning/WishList/WishList";
import { Learning } from "../models";
import { CourseServiceApi } from "../services/api/courseServiceApi";
const customTheme: CustomFlowbiteTheme = {
    tab: {
        tablist: {
            base: "grid !gap-4 grid-flow-col auto-cols-max w-full ml-auto mr-auto max-w-5xl bg-[#2d2f31] px-6 focus:bg-white",
            tabitem: {
                base: "!text-[#d1d7dc] font-bold px-1 py-2 flex justify-center items-center !border-b-4",
                styles: {
                    underline: {
                        active: {
                            on: "border-b-white bg-[#2d2f31] !text-white",
                            // off: "text-[#d1d7dc]",
                        },
                    },
                },
            },
        },
        tabitemcontainer: {
            base: "bg-white pt-4",
        },
        tabpanel: "ml-auto mr-auto max-w-5xl px-6 pt-4 pb-12",
    },
};

const MyCourse = () => {
    let { typeLearning } = useParams();
    const tabsRef = useRef<TabsRef>(null);
    const [activeTab, setActiveTab] = useState(0);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();

    useLayoutEffect(() => {
        if (typeLearning === "learning") {
            tabsRef.current?.setActiveTab(MY_LEARNING_TABS.ALL_COURSE);
            setActiveTab(0);
        } else if (typeLearning === "lists") {
            tabsRef.current?.setActiveTab(MY_LEARNING_TABS.MY_LISTS);
            setActiveTab(1);
        } else if (typeLearning === "wishlist") {
            tabsRef.current?.setActiveTab(MY_LEARNING_TABS.WISH_LIST);
            setActiveTab(2);
        } else if (typeLearning === "archived") {
            tabsRef.current?.setActiveTab(MY_LEARNING_TABS.ARCHIVED);
            setActiveTab(3);
        } else {
            tabsRef.current?.setActiveTab(MY_LEARNING_TABS.ALL_COURSE);
            setActiveTab(0);
        }
        setLoading(true);
    }, [typeLearning]);

    useEffect(() => {
        setLoading(false);
    }, [typeLearning]);

    const handleChangeTab = (tab: number) => {
        setActiveTab(tab);

        if (tab === MY_LEARNING_TABS.ALL_COURSE) {
            navigate("/home/my-courses/learning");
        } else if (tab === MY_LEARNING_TABS.MY_LISTS) {
            navigate("/home/my-courses/lists");
        } else if (tab === MY_LEARNING_TABS.WISH_LIST) {
            navigate("/home/my-courses/wishlist");
        } else if (tab === MY_LEARNING_TABS.ARCHIVED) {
            navigate("/home/my-courses/archived");
        }
    };

    const handleChangeTabCurrent = (tab: number) => {
        tabsRef.current?.setActiveTab(tab);
    };

    return (
        <div className="flex-auto flex-shrink-0">
            <div className=" bg-[#2d2f31] pt-12">
                <div className="mb-6 ml-auto mr-auto max-w-5xl bg-[#2d2f31] px-6 text-[40px] font-bold text-white">
                    My Learning
                </div>
                <div className="gap-1"></div>
                {/* <div className=""> */}
                <Flowbite theme={{ theme: customTheme }}>
                    <TabsComponent
                        className="gap-0"
                        aria-label="Tabs with underline"
                        style="underline"
                        ref={tabsRef}
                        onActiveTabChange={handleChangeTab}
                    >
                        <Tabs.Item
                            // active
                            title="All Courses"
                            className=""
                        >
                            <CourseLearningList
                                activeTab={activeTab}
                                loading={loading}
                            />
                        </Tabs.Item>
                        <Tabs.Item title="My Lists">
                            <MyLists
                                activeTab={activeTab}
                                loading={loading}
                                handleChangeTab={handleChangeTabCurrent}
                            />
                        </Tabs.Item>
                        <Tabs.Item title="Wishlist">
                            <WishList loading={loading} />
                        </Tabs.Item>

                        {/* <Tabs.Item title="My Lists">
                            <MyLists loading={loading} />
                        </Tabs.Item>
                        <Tabs.Item title="Wishlist">
                            <WishList loading={loading} />
                        </Tabs.Item>
                        <Tabs.Item title="Archived">
                            <ArchivedListList loading={loading} />
                        </Tabs.Item> */}
                        <Tabs.Item title="Archived">
                            <ArchivedListList
                                activeTab={activeTab}
                                handleChangeTab={handleChangeTabCurrent}
                                loading={loading}
                            />
                        </Tabs.Item>
                    </TabsComponent>
                </Flowbite>
            </div>
        </div>
    );
};

export default MyCourse;
