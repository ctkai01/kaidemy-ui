import { Button } from "flowbite-react";
import * as React from "react";
import { io, Socket } from "socket.io-client";
import { MESS_MODE } from "../features/Instructor/Communication/CommunicationMessage";
import MessageBox from "../features/Instructor/Communication/Message/MessageBox";
import { useAppSelector } from "../hooks/redux-hook";
import { Channel, Message, PaginationInfo, User } from "../models";
import { MessageServiceApi } from "../services/api/messageServiceApi";
import { selectUserAuth } from "../services/state/redux/authSlide";
import config from "../configs/config";

export interface IMessageProps {}
const MESSAGE_SIZE = 12;
export default function Message(props: IMessageProps) {
    const [modeMessage, setModeMessage] = React.useState(MESS_MODE.NORMAL);
    const [channels, setChannels] = React.useState<Channel[]>([]);
    const user = useAppSelector(selectUserAuth);
    const handleSetNothingMode = () => {
        setCurrentChannel(null);
        setModeMessage(MESS_MODE.NORMAL);
    };
    const [messages, setMessages] = React.useState<Message[]>([]);
    const [searchChannel, setSearchChannel] = React.useState("");

    const [currentChannel, setCurrentChannel] = React.useState<Channel | null>(
        null,
    );
    const [loadingMessage, setLoadingMessage] = React.useState(false);
    const [loadingChannel, setLoadingChannel] = React.useState(false);
    const [currMessagePage, setCurrMessagePage] = React.useState(1);
    const scrollContainerRef = React.useRef<HTMLDivElement>(null);
    const [isInitScroll, setIsInitScroll] = React.useState(false);

    const observer = React.useRef<IntersectionObserver | null>(null);

    const [infoMessagePagination, setInfoMessagePagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const socket = React.useRef<Socket>();
    React.useEffect(() => {
        const connectSocket = () => {
            if (user) {
                socket.current = io(`${config.apiUrl}`);
                socket.current.connect();
                socket.current.emit("add-user", user.id);

            }
        };
        connectSocket();
        return () => {
            if (socket.current) {
                socket.current.disconnect();
            }
        };
    }, []);

    const handleFocusChannel = (channel: Channel) => {
        setCurrentChannel(channel);
        setCurrMessagePage(1);
        setIsInitScroll(false);
        // setMessages([]);
        setModeMessage(MESS_MODE.NORMAL);
    };

    const handleSearchChannel = (value: string) => {
        setSearchChannel(value);
    };
    const setReadyIsInitScroll = () => {
        setIsInitScroll(true);
    };
    React.useLayoutEffect(() => {
        const fetchChannels = async () => {
            if (user) {
                // if (currentUser.isAvatarImageSet) {
                setLoadingChannel(true);
                const dataChannels =
                    await MessageServiceApi.getChannels(searchChannel);
                // const formatData: Channel[] = dataChannels.data.map(
                //     (channel: any) => {
                //         return {
                //             _id: channel._id,
                //             createdAt: channel.createdAt,
                //             contact: {
                //                 ID: channel.contact.ID,
                //                 Avatar: channel.contact.Avatar,
                //                 Name: channel.contact.Name,
                //             },
                //             latestMessage: {
                //                 _id: channel.latestMessage._id,
                //                 channelId: channel.latestMessage.channelId,
                //                 senderId: channel.latestMessage.senderId,
                //                 message: {
                //                     text: channel.latestMessage.message.text,
                //                 },
                //                 create_at: channel.latestMessage.create_at,
                //             },
                //         };
                //     },
                // );

                setChannels(dataChannels.item);
                setLoadingChannel(false);
            }
        };

        fetchChannels();
    }, [searchChannel]);

    React.useEffect(() => {
        const fetchChannels = async () => {
            if (user) {
                // if (currentUser.isAvatarImageSet) {
                const dataChannels = await MessageServiceApi.getChannels();
                // const formatData: Channel[] = dataChannels.data.map(
                //     (channel: any) => {
                //         return {
                //             _id: channel._id,
                //             createdAt: channel.createdAt,
                //             contact: {
                //                 ID: channel.contact.ID,
                //                 Avatar: channel.contact.Avatar,
                //                 Name: channel.contact.Name,
                //             },
                //             latestMessage: {
                //                 _id: channel.latestMessage._id,
                //                 channelId: channel.latestMessage.channelId,
                //                 senderId: channel.latestMessage.senderId,
                //                 message: {
                //                     text: channel.latestMessage.message.text,
                //                 },
                //                 create_at: channel.latestMessage.create_at,
                //             },
                //         };
                //     },
                // );

                setChannels(dataChannels.item);
                //  setContacts(data.data.data);
                // } else {
                //   navigate("/setAvatar");
                // }
            }
        };

        fetchChannels();
    }, []);
    // MessageServiceApi;

    React.useEffect(() => {
        const fetchMessage = async () => {
            try {
                if (currentChannel && user) {
                    setLoadingMessage(true);
                    const messageData = await MessageServiceApi.getMessages(
                        currentChannel.user.id,
                        MESSAGE_SIZE,
                        currMessagePage,
                    );
                    setInfoMessagePagination({
                        totalPage: messageData.meta.pageCount,
                        totalItem: messageData.meta.itemCount,
                        size: messageData.meta.size,
                    });

                    // const dataMessages = messageData.ite.map(
                    //     (message: any) => {
                    //         console.log("Fet: ", message);
                    //         if (message.message.user.id == user.id) {
                    //             return {
                    //                 ...message,
                    //                 message: {
                    //                     ...message.message,
                    //                     fromSelf: true,
                    //                 },
                    //             };
                    //         } else {
                    //             return {
                    //                 ...message,
                    //                 message: {
                    //                     ...message.message,
                    //                     fromSelf: false,
                    //                 },
                    //             };
                    //         }
                    //     },
                    // );
                    // if current
                    if (currMessagePage > 1) {
                        setMessages((prevMessage) => [
                            ...prevMessage,
                            ...messageData.item,
                        ]);
                    } else {
                        setMessages([...messageData.item]);
                    }

                    setLoadingMessage(false);
                }
            } catch (e) {
                console.log(e);
            }
        };

        fetchMessage();
    }, [currentChannel, currMessagePage]);

    const handleSendMsg = async (msg: string) => {
        try {
            if (currentChannel && socket.current && user) {
                // const formData = new FormData();
                // formData.append("channelId", currentChannel._id);
                // formData.append("message", msg);
                const body = {
                    to: currentChannel.user.id,
                    text: msg,
                };
                const messageData = await MessageServiceApi.createMessage(body);
                socket.current.emit("chat", {
                    to: currentChannel.user.id,
                    from: user.id,
                    text: msg,
                    id: messageData.id,
                    // msg: {
                    //     _id: messageData.data._id,
                    //     channelId: messageData.data.channelId,
                    //     created_at: messageData.data.create_at,
                    //     message: {
                    //         fromSelf: false,
                    //         text: messageData.data.message.text,
                    //         user: {
                    //             id: user.id,
                    //             name: user.name,
                    //             avatar: user.avatar ? user.avatar : undefined,
                    //         },
                    //     },
                    //     senderId: user.id,
                    // },
                });
                setMessages((messages) => {
                    return [
                        {
                            id: messageData.id,
                            fromUser: messageData.fromUser,
                            toUser: messageData.toUser,
                            createdAt: messageData.createdAt,
                            text: messageData.text,
                        },
                        ...messages,
                    ];
                });
                setChannels((channels) => {
                    const channelsUpdate = [...channels];
                    const findIndex = channelsUpdate.findIndex(
                        (channel) => channel.user.id === currentChannel.user.id,
                    );
                    if (findIndex != -1) {
                        const channelUpdate = { ...channelsUpdate[findIndex] };

                        // const latestMessage: LatestMessage = {

                        channelUpdate.latestMessage = {
                            user: {
                                id: messageData.fromUser.id,
                                name: messageData.fromUser.name,
                                avatar: messageData.fromUser.avatar,
                            },
                            text: messageData.text,
                            createdAt: messageData.createdAt,
                            // ...channelUpdate.latestMessage,
                            // senderId: user.id,
                            // message: {
                            //     text: messageData.data.message.text,
                            // },
                            // _id: messageData.data._id,
                            // create_at: messageData.data.create_at,
                            // channelId: currentChannel._id,
                        };
                        channelsUpdate[findIndex] = channelUpdate;
                        return channelsUpdate;
                    }

                    return channels;
                });
            }
        } catch (e) {
            console.log(e);
        }
    };

    const handleSendMsgCustom = async (msg: string, userData: User) => {
        try {
            if (socket.current && user) {
                const indexExistChannel = channels.findIndex(
                    (channel) => channel.user.id === userData.id,
                );

                if (indexExistChannel != -1) {
                    // Exist, only send message to
                    const body = {
                        to: userData.id,
                        text: msg,
                    };
                    const messageData =
                        await MessageServiceApi.createMessage(body);
                    socket.current.emit("chat", {
                        to: userData.id,
                        from: user.id,
                        text: msg,
                    });
                    setMessages((messages) => {
                        return [
                            {
                                id: messageData.id,
                                fromUser: messageData.fromUser,
                                toUser: messageData.toUser,
                                createdAt: messageData.createdAt,
                                text: messageData.text,
                            },
                            ...messages,
                        ];
                    });
                    setChannels((channels) => {
                        const channelsUpdate = [...channels];
                        const findIndex = channelsUpdate.findIndex(
                            (channel) => channel.user.id === userData.id,
                        );
                        if (findIndex != -1) {
                            const channelUpdate = {
                                ...channelsUpdate[findIndex],
                            };

                            // const latestMessage: LatestMessage = {

                            channelUpdate.latestMessage = {
                                user: {
                                    id: messageData.fromUser.id,
                                    name: messageData.fromUser.name,
                                    avatar: messageData.fromUser.avatar,
                                },
                                text: messageData.text,
                                createdAt: messageData.createdAt,
                                // ...channelUpdate.latestMessage,
                                // senderId: user.id,
                                // message: {
                                //     text: messageData.data.message.text,
                                // },
                                // _id: messageData.data._id,
                                // create_at: messageData.data.create_at,
                                // channelId: currentChannel._id,
                            };
                            channelsUpdate[findIndex] = channelUpdate;
                            return channelsUpdate;
                        }

                        return channels;
                    });
                } else {
                    //Not exist, create channel and send message to
                    // const formData = new FormData();
                    // formData.append("receiveId", `${userData.id}`);
                    // const dataChannel =
                    //     await MessageServiceApi.createChannel(formData);
                    // //Send message
                    // const formMessageData = new FormData();
                    // formMessageData.append("channelId", dataChannel.data._id);
                    // formMessageData.append("message", msg);
                    // const messageData =
                    //     await MessageServiceApi.createMessage(formMessageData);
                    // socket.current.emit("send-msg", {
                    //     to: userData.id,
                    //     from: user.id,
                    //     msg: {
                    //         _id: messageData.data._id,
                    //         channelId: messageData.data.channelId,
                    //         created_at: messageData.data.create_at,
                    //         message: {
                    //             fromSelf: false,
                    //             text: messageData.data.message.text,
                    //             user: {
                    //                 id: user.id,
                    //                 name: user.name,
                    //                 avatar: user.avatar
                    //                     ? user.avatar
                    //                     : undefined,
                    //             },
                    //         },
                    //         senderId: user.id,
                    //     },
                    // });
                    // setMessages((messages) => {
                    //     return [
                    //         ...messages,
                    //         {
                    //             _id: messageData.data._id,
                    //             channelId: messageData.data.channelId,
                    //             created_at: messageData.data.create_at,
                    //             message: {
                    //                 fromSelf: true,
                    //                 text: messageData.data.message.text,
                    //                 user: {
                    //                     id: user.id,
                    //                     name: user.name,
                    //                     avatar: user.avatar
                    //                         ? user.avatar
                    //                         : undefined,
                    //                 },
                    //             },
                    //             senderId: user.id,
                    //         },
                    //     ];
                    // });
                    // setChannels((channels) => {
                    //     const newChannel: Channel = {
                    //         _id: dataChannel.data._id,
                    //         contact: {
                    //             ID: userData.id,
                    //             Name: userData.name,
                    //             Avatar: userData.avatar
                    //                 ? userData.avatar
                    //                 : undefined,
                    //         },
                    //         createdAt: dataChannel.data.createdAt,
                    //         latestMessage: {
                    //             _id: messageData.data._id,
                    //             channelId: messageData.data.channelId,
                    //             create_at: messageData.data.create_at,
                    //             message: {
                    //                 text: messageData.data.message.text,
                    //             },
                    //             senderId: user.id,
                    //         },
                    //     };
                    //     return [newChannel, ...channels];
                    // });
                }
            }
        } catch (e) {
            console.log(e);
        }
    };

    React.useEffect(() => {
        const handleReceive = (data: any) => {
            console.log("Data receive: ", data);
            console.log("channels: ", channels);

            setChannels((channels) => {
                const channelsUpdate = [...channels];
                const findIndex = channelsUpdate.findIndex(
                    (channel) =>
                        channel.user.id === data.fromUser.id ||
                        channel.user.id === data.to,
                );

                console.log("findIndex: ", findIndex);
                if (findIndex != -1) {
                    const channelUpdate = {
                        ...channelsUpdate[findIndex],
                    };
                    channelUpdate.latestMessage = {
                        user: {
                            id: data.fromUser.id,
                            name: data.fromUser.name,
                            avatar: data.fromUser.avatar,
                        },
                        text: data.text,
                        createdAt: new Date().toISOString(),
                    };
                    channelsUpdate[findIndex] = channelUpdate;
                    setMessages((prev) => [
                        {
                            ...data,
                            toUser: {
                                id: user?.id,
                                name: user?.name,
                                avatar: user?.avatar,
                            },
                            createdAt: new Date().toISOString(),
                        },
                        ...prev,
                    ]);
                    return channelsUpdate;
                } else {
                    return channelsUpdate;
                }
            });
        };
        if (socket.current) {
            socket.current.on("receive", handleReceive);
        }
        return () => {
            if (socket.current) {
                socket.current.off("receive", handleReceive);
            }
        };
    }, [channels]);

    const handleIncreaseMessagePage = () => {
        setCurrMessagePage((curr) => curr + 1);
    };
    // React.useEffect(() => {
    //     arrivalMessage && setMessages((prev) => [...prev, arrivalMessage]);
    // }, [arrivalMessage]);
    const lastMessageElementRef = React.useCallback(
        async (node: any) => {
            await new Promise((r) => setTimeout(r, 1000));
            if (loadingMessage) return;
            if (observer.current) observer.current.disconnect();

            observer.current = new IntersectionObserver((entries) => {
                if (
                    entries[0].isIntersecting &&
                    currMessagePage < infoMessagePagination.totalPage &&
                    isInitScroll
                ) {
                    setCurrMessagePage((curr) => curr + 1);
                }
            });
            if (node) observer.current.observe(node);
        },
        [loadingMessage, currMessagePage, isInitScroll],
    );
    // console.log("modeMessage: ", modeMessage);
    console.log("channel watch:: ", channels);
    console.log("message watch:: ", messages);
    return (
        <div className="mt-10 flex-1">
            <div className="px-16">
                <div className="mb-8 flex items-center justify-between gap-6">
                    <h2 className="text-[32px] font-bold text-primary-black">
                        Tin nhắn
                    </h2>
                    <Button
                        onClick={() => {
                            setModeMessage(MESS_MODE.CREATE_NEW);
                            setCurrentChannel(null);
                        }}
                        color="dark"
                        className="font-bold"
                    >
                        Tạo tin nhắn mới
                    </Button>
                </div>

                <MessageBox
                    modeMessage={modeMessage}
                    messages={messages}
                    channels={channels}
                    user={user}
                    loadingChannel={loadingChannel}
                    handleFocusChannel={handleFocusChannel}
                    handleIncreaseMessagePage={handleIncreaseMessagePage}
                    currentChannel={currentChannel}
                    handleSendMsg={handleSendMsg}
                    loadingMessage={loadingMessage}
                    infoMessagePagination={infoMessagePagination}
                    handleSetNothingMode={handleSetNothingMode}
                    handleSendMsgCustom={handleSendMsgCustom}
                    handleSearchChannel={handleSearchChannel}
                    currMessagePage={currMessagePage}
                    setReadyIsInitScroll={setReadyIsInitScroll}
                    lastMessageElementRef={lastMessageElementRef}
                    scrollContainerRef={scrollContainerRef}
                />
            </div>
        </div>
    );
}
