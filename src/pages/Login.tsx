import LoginForm from "../features/Auth/Login/LoginForm";
import learningIcon from "../assets/images/onlinelearning.png";
import learningIcon2 from "../assets/images/learning-2.png";
import { useAppSelector } from "../hooks/redux-hook";
import { selectIsLoggedIn } from "../services/state/redux/authSlide";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const Login = () => {
    // const isLoggedIn = useAppSelector(selectIsLoggedIn);
    // const navigate = useNavigate();
    // useEffect(() => {
    //     console.log("isLoggedIn: ", isLoggedIn);
    //     if (isLoggedIn) {
    //         navigate("/");
    //     }
    // }, [])
    return (
        <div className="h-screen bg-white">
            <div className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                <LoginForm className="shadow-2xl" />
            </div>
            {/* <img className="fixed bottom-5 left-1" src={learningIcon} />
            <img className="fixed right-10 top-36" src={learningIcon2} /> */}
        </div>
    );
};

export default Login;
