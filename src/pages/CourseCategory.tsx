import { Button, Select, Spinner } from "flowbite-react";
import * as React from "react";
import { useForm } from "react-hook-form";
import { IoFilterSharp } from "react-icons/io5";
import { useParams } from "react-router-dom";
import CoursesSection from "../features/Category/CoursesSection";
import FilterSection from "../features/Category/FilterSection";
import {
    Category,
    CategoryCourse,
    FilterCoursesCategory,
    Level,
    OverallCategoryCourse,
    PaginationInfo,
} from "../models";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { CategoryServiceApi } from "../services/api/categoryServiceApi";
import { MdDeleteSweep } from "react-icons/md";
import { useAppSelector } from "../hooks/redux-hook";
import {
    selectIsLoggedIn,
    selectUserAuth,
} from "../services/state/redux/authSlide";

export interface ICourseCategoryProps {}

export const NEWEST_SORT = "newest";
export const MOST_REVIEW_SORT = "most_review";
export const HIGHEST_SORT = "highest_rated";

const SIZE = 6;

export interface FilterCategory {
    rate: string;
    sort: string;
    duration: string[] | false;
    level: string[] | false;
}

export default function CourseCategory(props: ICourseCategoryProps) {
    let { categoryID } = useParams();

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [currentPage, setCurrentPage] = React.useState(1);
    const [categoryName, setCategoryName] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const [categoryNotFound, setCategoryNotFound] = React.useState(false);
    const [levels, setLevels] = React.useState<Level[]>([]);
    const [categoryCourses, setCategoryCourses] = React.useState<
        CategoryCourse[]
    >([]);
    const [overallCategoryCourse, setOverallCategoryCourse] =
        React.useState<OverallCategoryCourse>({
            tierOneRatingCount: 0,
            tierTwoRatingCount: 0,
            tierThreeRatingCount: 0,
            tierFourRatingCount: 0,
            tierOneDurationCount: 0,
            tierTwoDurationCount: 0,
            allLevelCount: 0,
            beginnerLevelCount: 0,
            intermediateLevelCount: 0,
            expertLevelCount: 0,
        });
    const {
        register,
        reset,
        watch,
        formState: { isDirty },
    } = useForm<FilterCategory>({
        mode: "onChange",
    });

    const isLoggedIn = useAppSelector(selectIsLoggedIn);

    const onPageChange = (page: number) => setCurrentPage(page);

    React.useLayoutEffect(() => {
        const fetchCoursesCategory = async (filter: FilterCoursesCategory) => {
            try {
                setLoading(true);

                if (categoryID && !isNaN(+categoryID)) {
                    //
                    const courseCategoryData = isLoggedIn
                        ? await CourseServiceApi.getCoursesCategory(
                              +categoryID,
                              SIZE,
                              currentPage,
                              filter,
                          )
                        : await CourseServiceApi.getNotAuthCoursesCategory(
                              +categoryID,
                              SIZE,
                              currentPage,
                              filter,
                          );

                    setCategoryCourses(courseCategoryData.item);

                    setCategoryNotFound(false);
                    setOverallCategoryCourse(courseCategoryData.overall);
                    setCategoryName(courseCategoryData.category.name);
                    setInfoPagination({
                        totalPage: courseCategoryData.meta.pageCount,
                        totalItem: courseCategoryData.meta.itemCount,
                        size: courseCategoryData.meta.size,
                    });
                    setLoading(false);
                } else {
                    setCategoryNotFound(true);
                    setLoading(false);
                }
            } catch (e: any) {
                console.log(e);

                if (e.response.status === 404) {
                    setCategoryNotFound(true);
                    setLoading(false);
                }
            }
        };

        const subscription = watch((value) => {
            let filterDuration: string[];
            if (value.duration) {
                const onlyUndefined = value.duration.every(
                    (item) => item === undefined,
                );

                if (onlyUndefined) {
                    filterDuration = [];
                } else {
                    filterDuration = value.duration as string[];
                }
            } else {
                filterDuration = [];
            }

            let filterLevel: string[];
            if (value.level) {
                const onlyUndefined = value.level.every(
                    (item) => item === undefined,
                );

                if (onlyUndefined) {
                    filterLevel = [];
                } else {
                    filterLevel = value.level as string[];
                }
            } else {
                filterLevel = [];
            }

            const filter: FilterCoursesCategory = {
                duration: filterDuration,
                level: filterLevel,
                rating: value.rate ? value.rate : null,
                sort: value.sort ? value.sort : null,
            };

            fetchCoursesCategory(filter);
        });

        const filter: FilterCoursesCategory = {
            duration: watch("duration") ? watch("duration") : [],
            level: watch("level") ? watch("level") : [],
            rating: watch("rate") ? watch("rate") : null,
            sort: watch("sort") ? watch("sort") : null,
        };

        fetchCoursesCategory(filter);
        return () => subscription.unsubscribe();
    }, [currentPage, categoryID, watch]);

    React.useEffect(() => {
        const fetchLevel = async () => {
            try {
                const levelsResult = await CategoryServiceApi.getLevels();

                setLevels(levelsResult.item);
            } catch (e) {
                console.log(e);
            }
        };
        fetchLevel();
    }, []);
    return (
        <div className="mx-auto w-full max-w-[1340px] flex-1 px-6 py-12">
            {categoryNotFound ? (
                <div className="flex-1">
                    <div className="mx-auto  max-w-[1340px]  px-6 pb-16 pt-12 text-center">
                        <div className="flex justify-center">
                            <img src="https://s.udemycdn.com/error_page/error-desktop-v1.jpg" />
                        </div>

                        <h1 className="text-[32px] font-bold text-primary-black">
                            We can’t find the page you’re looking for
                        </h1>
                    </div>
                </div>
            ) : loading ? (
                <div className="text-center">
                    <Spinner />
                </div>
            ) : (
                <>
                    <h1 className="mb-12 text-[32px] font-bold text-primary-black">
                        Khóa học {categoryName}
                    </h1>

                    <div className="mb-6 flex justify-between">
                        <div className="flex gap-2">
                            <Button color="dark" className="font-bold">
                                <IoFilterSharp className="mr-2 h-5 w-5" />
                                Filter
                            </Button>
                            <Select required {...register("sort")}>
                                <option value={NEWEST_SORT}>Newest</option>
                                <option value={HIGHEST_SORT}>
                                    Highest Rated
                                </option>
                                <option value={MOST_REVIEW_SORT}>
                                    Most Popular
                                </option>
                            </Select>
                            {isDirty && (
                                <Button
                                    onClick={() => {
                                        reset({
                                            rate: "",
                                            sort: NEWEST_SORT,
                                            duration: [],
                                            level: [],
                                        });
                                    }}
                                    className="font-bold"
                                >
                                    <MdDeleteSweep className="mr-2 h-5 w-5" />
                                    Remove Filter
                                </Button>
                            )}
                        </div>
                        <div className="tex-base font-bold text-primary-gray">
                            {infoPagination.totalItem} results
                        </div>
                    </div>

                    <div className="flex gap-4">
                        <div className="flex-1">
                            <FilterSection
                                overallCategoryCourse={overallCategoryCourse}
                                levels={levels}
                                register={register}
                            />
                        </div>
                        <div className="shrink grow-[3] basis-0">
                            <CoursesSection
                                categoryCourses={categoryCourses}
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                            />
                        </div>
                    </div>
                </>
            )}
        </div>
    );
}
