import learningIcon from "../assets/images/onlinelearning.png";
import learningIcon2 from "../assets/images/learning-2.png";
import SignUpForm from "../features/Auth/SignUp/SignUpForm";
import { useNavigate } from "react-router-dom";
import { useAppSelector } from "../hooks/redux-hook";
import { selectIsLoggedIn } from "../services/state/redux/authSlide";

const SignUp = () => {
    const isLoggedIn = useAppSelector(selectIsLoggedIn);
    const navigate = useNavigate();

    if (isLoggedIn) {
        navigate("/");
    }
    return (
        <div className="h-screen bg-white">
            <div className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                <SignUpForm className="shadow-2xl" />
            </div>
            {/* <img className="fixed bottom-5 left-4" src={learningIcon} />
            <img className="fixed right-10 top-36" src={learningIcon2} /> */}
        </div>
    );
};

export default SignUp;
