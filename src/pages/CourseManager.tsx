import * as React from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
    topicPaths,
    LECTURE_TYPE,
    LECTURE_WATCH_ASSET_TYPE,
} from "../constants";
import CurriculumSection from "../features/Instructor/CourseManage/Curriculum/Curriculum";
import HeaderCourseManage from "../features/Instructor/CourseManage/HeaderCourseManage";
import IntendedLearner from "../features/Instructor/CourseManage/IntendedLearner/IntendedLearner";
import LandingPage from "../features/Instructor/CourseManage/LandingPage/LandingPage";
import Pricing from "../features/Instructor/CourseManage/Pricing/Pricing";
import SidebarCourseManager from "../features/Instructor/CourseManage/SidebarCourseManager";
import { Course, Curriculum } from "../models";
import { CourseServiceApi } from "../services/api/courseServiceApi";
import { convertSecondsToHoursMinutes, convertToHHMM } from "../utils";
import NotFound from "./NotFound";

export interface ICourseMangerProps {}

export default function CourseManger(props: ICourseMangerProps) {
    const { id, content } = useParams();
    const [topic, setTopic] = React.useState("");
    const [curriculums, setCurriculums] = React.useState<Curriculum[]>([]);
    // const [title, setTitle] = React.useState("")
    const [loading, setLoading] = React.useState(false);

    const [course, setCourse] = React.useState<Course>();
    const [isCourseNotFound, setIsCourseNotFound] = React.useState(false);
    const navigate = useNavigate();

    const handleChangeTab = (path: string) => {
        setTopic(path);
        navigate(`/instructor/course/${id}/manage/${path}`);
    };
    React.useLayoutEffect(() => {
        if (!topicPaths.includes(content || "")) {
            navigate(`/instructor/course/${id}/manage/goals`);
            setTopic("goals");
        } else {
            setTopic(content || "");
        }

        const getCourse = async () => {
            try {
                const result = await CourseServiceApi.getCourseByID(
                    id ? +id : 0,
                );
                // setTitle(result.course.title)
                setCourse(result);
            } catch (e: any) {
                console.log(e);
                if (e.response.status === 404) {
                    handleCourseNotFound();
                }
            }
        };

        const getCurriculums = async () => {
            try {
                const result =
                    await CourseServiceApi.getCurriCulumsByCourseByID(
                        id ? +id : 0,
                    );

                setCurriculums(result.curriculums);
                // setIsLoading(false);
            } catch (e: any) {
                console.log(e);
                if (e.response.status === 404) {
                    handleCourseNotFound();
                }
            }
        };

        const fetchData = async () => {
            setLoading(true);
            await Promise.all([getCourse(), getCurriculums()]);
            setLoading(false);
        };
        fetchData();
    }, []);

    const handleCourseNotFound = () => {
        setIsCourseNotFound(true);
    };

    // const handleSetTitle = (value: string) => {
    //     setTitle(value);
    // };

    const totalDuration = React.useMemo(() => {
        let totalDuration = 0;
        curriculums.forEach((curriculum) => {
            if (curriculum.lectures) {
                curriculum.lectures.forEach((lecture) => {
                    if (lecture.type === LECTURE_TYPE) {
                        lecture.assets.forEach((asset) => {
                            if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
                                totalDuration += asset.duration;
                            }
                        });
                    }
                });
            }
        });

        return totalDuration;
    }, [curriculums]);

    // const handleSetCourse = (course: Course) => {
    //     setCourse(course)
    // }

    return (
        <>
            {isCourseNotFound ? (
                <NotFound />
            ) : (
                <div>
                    <HeaderCourseManage
                        title={course ? course?.title : ""}
                        totalDuration={totalDuration}
                        course={course}
                    />
                    <div className="mx-auto flex max-w-[1340px] px-6 pb-16 pt-8">
                        {course && (
                            <SidebarCourseManager
                                currentPath={topic}
                                curriculums={curriculums}
                                course={course}
                                handleSetCourse={setCourse}
                                handleChangeTab={handleChangeTab}
                            />
                        )}

                        <div className="shadow-left-custom flex-1">
                            <div className="border-b border-primary-hover-gray px-12 py-6">
                                <h2 className="text-2xl font-bold text-primary-black">
                                    {topic == "goals" && (
                                        <div>Intended leanrners</div>
                                    )}
                                    {topic == "curriculum" && (
                                        <div>Curriculum</div>
                                    )}
                                    {topic == "basics" && (
                                        <div>Course landing page</div>
                                    )}
                                    {topic == "pricing" && <div>Pricing</div>}
                                </h2>
                            </div>
                            <div className="p-12">
                                {topic == "goals" && (
                                    <IntendedLearner
                                        handleCourseNotFound={
                                            handleCourseNotFound
                                        }
                                        loading={loading}
                                        course={course}
                                        handleSetCourse={setCourse}
                                        courseID={id ? +id : 0}
                                    />
                                )}
                                {topic == "curriculum" && (
                                    <CurriculumSection
                                        handleCourseNotFound={
                                            handleCourseNotFound
                                        }
                                        loading={loading}
                                        handleSetCourse={setCourse}
                                        curriculums={curriculums}
                                        setCurriculums={setCurriculums}
                                        courseID={id ? +id : 0}
                                    />
                                )}
                                {topic == "basics" && (
                                    <LandingPage
                                        loading={loading}
                                        course={course}
                                        handleSetCourse={setCourse}
                                        // handleSetTitle={handleSetTitle}
                                        courseID={id ? +id : 0}
                                    />
                                )}
                                {topic == "pricing" && (
                                    <Pricing
                                        handleCourseNotFound={
                                            handleCourseNotFound
                                        }
                                        loading={loading}
                                        course={course}
                                        handleSetCourse={setCourse}
                                        courseID={id ? +id : 0}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}
