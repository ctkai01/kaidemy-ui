import { Button } from "flowbite-react";
import { Link } from "react-router-dom";
import notFoundGif from "../assets/images/404.gif";
const NotFound = () => {
    return (
        <div className="bg-white h-screen">
            <div className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                <div className="flex flex-col items-center">
                    <img src={notFoundGif} loading="lazy"/>
                    <Button
                        className="mt-5 w-fit font-bold"
                        outline
                        gradientDuoTone="cyanToBlue"
                    >
                        <Link to="/">Go Home</Link>
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default NotFound;
