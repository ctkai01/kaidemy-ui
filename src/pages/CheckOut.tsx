import { Button, Card, Spinner } from "flowbite-react";

import * as React from "react";
import { Link, useSearchParams } from "react-router-dom";
import { useAppDispatch } from "../hooks/redux-hook";
import { PaymentServiceApi } from "../services/api/paymentServiceApi";
import { removeCart } from "../services/state/redux/cartSlide";

export interface ICheckOutProps {}

export default function CheckOut(props: ICheckOutProps) {
    // React.useEffect()
    const [loading, setLoading] = React.useState(false);
    const [isErr, setIsErr] = React.useState(false);
    const [searchParams] = useSearchParams();
    const dispatch = useAppDispatch()

    // React.useLayoutEffect(() => {
    //     const createInvoice = async () => {
    //         try {
    //             if (searchParams.get("key")) {
    //                 setLoading(true);
    //                 const formData = new FormData();

    //                 formData.append(
    //                     "key",
    //                     searchParams.get("key") ? searchParams.get("key") : "",
    //                 );
    //                await PaymentServiceApi.createInvoice(formData);
    //                dispatch(removeCart());
    //                setLoading(false);

    //             } else {
    //                 setIsErr(true);
    //             }
    //         } catch (e) {
    //             console.log(e);
    //             setIsErr(true);

    //         }
    //     };
    //     createInvoice();
    // }, []);

    return (
        <div className="flex-1">
            <Card className="mx-auto mt-12 max-w-sm bg-red-700">
                {/* {isErr ? (
                    <div className="text-center font-bold text-white">
                        Đã xảy ra lỗi
                    </div>
                ) : loading ? (
                    <div className="text-center">
                        <Spinner />
                    </div>
                ) : ( */}
                    <>
                        <h5 className="text-3xl font-bold tracking-tight text-yellow-400  dark:text-white">
                        Congratulations!
                        </h5>
                        <p className="font-normal text-white dark:text-gray-400">
                        You have successfully paid, please wait for our
                            system to process
                        </p>

                        <Link to="/home/my-courses/learning" className="w-full">
                            <Button className="w-full">
                                <span className="font-bold">
                                Go to My learning
                                </span>
                            </Button>
                        </Link>
                    </>
                {/* )} */}
            </Card>
        </div>
    );
}
