import { Alert, Button, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import { useState } from "react";
import { RiErrorWarningFill } from "react-icons/ri";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import ResetPasswordForm from "../features/Auth/ResetPassword/ResetPasswordForm";
import { ResetPasswordRequest } from "../models/auth";
import { UserServiceApi } from "../services/api/userServiceApi";

const customAlertTheme: CustomFlowbiteTheme = {
    alert: {
        icon: "h-10 w-10 mr-4",
    },
};
const ResetPassword = () => {
    const [isTokenInValid, setIsTokenInValid] = useState(false);
    const [errResponse, setErrResponse] = useState("");
    const navigate = useNavigate();

    let [searchParams, _] = useSearchParams();
    const handleSendRequestResetPassword = async (password: string) => {
        try {
            const data: ResetPasswordRequest = {
                password,
                emailToken: searchParams.get("email") || "",
            };
            await UserServiceApi.resetPassword(data);
            navigate("/login")
        } catch (err: any) {
            if (err.response.data.Message.includes("email token invalid")) {
                setIsTokenInValid(true)
            } else {
                setErrResponse("Something error server");
            }
            setErrResponse("Something error server");
        }
    };
    return (
        <div className="h-screen">
            <div className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                {isTokenInValid ? (
                    <div >
                        <Flowbite theme={{ theme: customAlertTheme }}>
                            <Alert
                                color="failure"
                                icon={RiErrorWarningFill}
                                rounded
                                withBorderAccent
                            >
                                <div>Token invalid or expired</div>
                            </Alert>
                        </Flowbite>
                        <div className="flex justify-between min-w-[300px]">
                            <Button
                                className="mt-8"
                                outline
                                gradientDuoTone="purpleToPink"
                            >
                                <Link to="/forgot-password">
                                    Go Forgot password
                                </Link>
                            </Button>

                            <Button
                                className="mt-8"
                                outline
                                gradientDuoTone="cyanToBlue"
                            >
                                <Link to="/">Go Home</Link>
                            </Button>
                        </div>
                    </div>
                ) : (
                    <ResetPasswordForm
                        className="shadow-2xl"
                        handleSendRequestResetPassword={
                            handleSendRequestResetPassword
                        }
                        errResponse={errResponse}
                    />
                )}
            </div>
        </div>
    );
};

export default ResetPassword;
