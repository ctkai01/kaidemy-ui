import CartList from "../features/Cart/CartList";
import Checkout from "../features/Cart/Checkout";
import RecentWishList from "../features/Cart/RecentWishList";
import { useAppSelector } from "../hooks/redux-hook";
import { Cart } from "../models/cart";
import { selectWishList } from "../services/state/redux/wishListSlide";

export interface ICartProps {
    cart: Cart | null;
}

export default function Cart(props: ICartProps) {
    const { cart } = props;
    const wishList = useAppSelector(selectWishList);

    return (
        <div className="mx-auto mb-16 min-w-[320px] max-w-[1340px] flex-auto px-12">
            <div className="mt-8">
                <h1 className="text-[40px] font-bold text-primary-black">
                    Cart
                </h1>
            </div>
            <div className="mt-8 flex">
                <div>
                    <CartList cart={cart} />
                    {wishList.length !== 0 && (
                        <RecentWishList wishList={wishList}/>
                    )}
                </div>
                {cart && cart.cartItems.length !== 0 && (
                    <Checkout cart={cart} />
                )}
            </div>
        </div>
    );
}
