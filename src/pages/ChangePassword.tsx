import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Label, TextInput } from "flowbite-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { toast } from "react-toastify";
import { ChangePassword } from "../models/auth";
import { UserServiceApi } from "../services/api/userServiceApi";
import { schemeChangePassword } from "../validators";

export interface IChangePasswordProps {}

export default function ChangePassword(props: IChangePasswordProps) {
    const [isCurrentPasswordVisible, setIsCurrentPasswordVisible] =
        useState(false);

    const [isNewPasswordVisible, setIsNewPasswordVisible] = useState(false);
    const [isRePasswordVisible, setIsRePasswordVisible] = useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<ChangePassword>({
        mode: "onChange",
        resolver: yupResolver(schemeChangePassword),
    });

    const handleToggleShowCurrentPassword = () => {
        setIsCurrentPasswordVisible(
            (isCurrentPasswordVisible) => !isCurrentPasswordVisible,
        );
    };

    const handleToggleShowNewPassword = () => {
        setIsNewPasswordVisible(
            (isNewPasswordVisible) => !isNewPasswordVisible,
        );
    };

    const handleToggleShowRePassword = () => {
        setIsRePasswordVisible((isRePasswordVisible) => !isRePasswordVisible);
    };

    const onSubmit = async (data: ChangePassword) => {
        try {
            const body = {
                old_password: data.currentPassword,
                new_password: data.newPassword,
            };
            await UserServiceApi.changePassword(body);

            toast(<div className="font-bold">Changed password successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">Changed password failed!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
        reset();
    };
    return (
        <div className="border border-[#d1d7dc]">
            <div className="py-4">
                <div className="mx-auto my-0 max-w-[648px] px-6 text-center text-primary-black">
                    <h1 className="text-2xl font-bold">Tài khoản</h1>
                    <p className="font-base mt-2">
                    Edit your account settings and change your password
                        here.

                    </p>
                </div>
            </div>
            <div className="py-6">
                <form
                    onSubmit={handleSubmit(onSubmit)}
                    className="mt-4 flex flex-col gap-4"
                >
                    <div className="mx-auto my-0 max-w-[648px] px-6">
                        <div className="min-w-[180px] max-w-[600px]">
                            <div className="mb-2 block">
                                <Label value="Password:" />
                            </div>
                            <div className="relative text-[#2D2F31]">
                                <TextInput
                                    id="password"
                                    className="w-[600px]"
                                    placeholder="Enter current password"
                                    type={
                                        isCurrentPasswordVisible
                                            ? "text"
                                            : "password"
                                    }
                                    color={
                                        errors.currentPassword ? "failure" : ""
                                    }
                                    {...register("currentPassword")}
                                />
                                <div
                                    onClick={handleToggleShowCurrentPassword}
                                    className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                                >
                                    {!isCurrentPasswordVisible ? (
                                        <AiFillEye />
                                    ) : (
                                        <AiFillEyeInvisible />
                                    )}
                                </div>
                            </div>
                            {errors.currentPassword ? (
                                <div className="mt-1 text-sm text-[#e02424]">
                                    <span className="font-medium">Oops! </span>
                                    <span>
                                        {errors.currentPassword.message}
                                    </span>
                                </div>
                            ) : (
                                <></>
                            )}
                            <div className="relative mt-4 text-[#2D2F31]">
                                <TextInput
                                    className="w-[600px]"
                                    placeholder="Enter new password"
                                    type={
                                        isNewPasswordVisible
                                            ? "text"
                                            : "password"
                                    }
                                    color={errors.newPassword ? "failure" : ""}
                                    {...register("newPassword")}
                                />
                                <div
                                    onClick={handleToggleShowNewPassword}
                                    className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                                >
                                    {!isNewPasswordVisible ? (
                                        <AiFillEye />
                                    ) : (
                                        <AiFillEyeInvisible />
                                    )}
                                </div>
                            </div>
                            {errors.newPassword ? (
                                <div className="text-sm text-[#e02424]">
                                    <span className="font-medium">Oops! </span>
                                    <span>{errors.newPassword.message}</span>
                                </div>
                            ) : (
                                <></>
                            )}
                            <div className="relative mt-4 text-[#2D2F31]">
                                <TextInput
                                    className="w-[600px]"
                                    placeholder="Re-type new password"
                                    type={
                                        isRePasswordVisible
                                            ? "text"
                                            : "password"
                                    }
                                    color={
                                        errors.confirmPassword ? "failure" : ""
                                    }
                                    {...register("confirmPassword")}
                                />
                                <div
                                    onClick={handleToggleShowRePassword}
                                    className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                                >
                                    {!isRePasswordVisible ? (
                                        <AiFillEye />
                                    ) : (
                                        <AiFillEyeInvisible />
                                    )}
                                </div>
                            </div>
                            {errors.confirmPassword ? (
                                <div className="text-sm text-[#e02424]">
                                    <span className="font-medium">Oops! </span>
                                    <span>
                                        {errors.confirmPassword.message}
                                    </span>
                                </div>
                            ) : (
                                <></>
                            )}
                        </div>

                        <div className="flex justify-center">
                            <Button
                                className="mt-8 font-bold"
                                color="success"
                                type="submit"
                            >
                                Change password
                            </Button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}
