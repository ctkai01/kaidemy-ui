import {
    CustomFlowbiteTheme,
    Flowbite,
    Sidebar,
    Tabs,
    TabsComponent,
} from "flowbite-react";
import { useState } from "react";
import { MdBarChart, MdOutlineMessage } from "react-icons/md";
import { PiVideoBold } from "react-icons/pi";
import { Link, Outlet, useLocation } from "react-router-dom";
import { INSTRUCTOR_COMMUNICATION_PATH, INSTRUCTOR_COMMUNICATION_QA_PATH, INSTRUCTOR_COURSE_PATH, INSTRUCTOR_PERFORMANCE_OVERVIEW_PATH, INSTRUCTOR_PERFORMANCE_PATH } from "../../constants";
import CourseGeneral from "./CourseGeneral";
import PerformanceGeneral from "./Performance/PerformanceGeneral";

const customTheme: CustomFlowbiteTheme = {
    // tab: {
    //     base: "flex h-full",
    //     tablist: {
    //         base: "w-[56px] fixed top-0 left-0 h-full  transition-[width] duration-400 ease-in-out  hover:w-[250px] bg-primary-black border-none group",
    //         tabitem: {
    //             base: "w-full text-base relative flex justify-start hover:bg-[#3e4143] hover:text-white rounded-none items-center p-4  font-bold first:ml-0 disabled:cursor-pointer disabled:text-gray-400 disabled:dark:text-gray-500",
    //             styles: {
    //                 underline: {
    //                     active: {
    //                         on: "text-white  rounded-none border-l-4 border-cyan-600 active dark:text-cyan-500 dark:border-cyan-500",
    //                         off: "border-l-2  rounded-none border-transparent text-gray-500  dark:text-gray-400 dark:hover:text-gray-300",
    //                     },
    //                 },
    //             },
    //             icon: "shrink-0 mr-2 h-5 w-5",
    //         },
    //     },
    //     tabitemcontainer: {
    //         base: "w-full",
    //     },
    //     tabpanel: "",
    // },
    // border-l-4 border-cyan-600
    sidebar: {
        root: {
            base: "h-full",
            collapsed: {
                on: "w-16",
                off: "w-[56px] group transition-[width] duration-400 ease-in-out  hover:w-[250px]",
            },
            inner: "h-full w-full bg-primary-black overflow-y-auto overflow-x-hidden ",
        },
        item: {
            base: " p-4 flex hover:bg-[#3e4143] hover:text-white text-base font-bold  ml-1 text-gray-500",
            active: "border-l-4 border-cyan-600 text-white  ml-0",
            content: {
                base: ""
            }
        },
        // items: "w-16",
    },
};

export interface IInstructorSidebarProps {}

interface SidebarCollapse {
    community: boolean;
    performance: boolean;
}

export default function InstructorSidebar(props: IInstructorSidebarProps) {
    const [sidebarCollapse, setSidebarCollapse] = useState<SidebarCollapse>({
        community: false,
        performance: false,
    });
       const location = useLocation();
    // INSTRUCTOR_PERFORMANCE_PATH;
    const {} = props;
    return (
        <div className="flex h-full w-full">
            {/* <div className="h-full"> */}
            <div className="lef-0 fixed top-0 z-[100] flex h-screen ">
                <Flowbite theme={{ theme: customTheme }}>
                    <Sidebar
                        className=""
                        aria-label="Sidebar with logo branding example"
                    >
                        <div className="p-4">
                            <Link
                                // className="after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                                to="/"
                            >
                                <img
                                    loading="lazy"
                                    src={
                                        "https://kaidemy.b-cdn.net/avatar/h-high-resolution-logo-transparent.png"
                                    }
                                    className="h-[34px] group-hover:h-[34px]"
                                />
                            </Link>
                        </div>
                        <Sidebar.Items>
                            <Sidebar.ItemGroup>
                                <Link to={INSTRUCTOR_COURSE_PATH} className="">
                                    <Sidebar.Item
                                        active={location.pathname.startsWith(
                                            INSTRUCTOR_COURSE_PATH,
                                        )}
                                    >
                                        <div className="flex items-center">
                                            <PiVideoBold className="mr-2 h-5 w-5 shrink-0" />
                                            <div className="opacity-0  transition-[opacity] duration-400 ease-linear group-hover:opacity-100">
                                                Courses
                                            </div>
                                        </div>
                                    </Sidebar.Item>
                                </Link>
                                <Link
                                    to={INSTRUCTOR_COMMUNICATION_QA_PATH}
                                    className=""
                                >
                                    <Sidebar.Item
                                        active={location.pathname.startsWith(
                                            INSTRUCTOR_COMMUNICATION_PATH,
                                        )}
                                    >
                                        <div className="flex items-center">
                                            <MdOutlineMessage className="mr-2 h-5 w-5 shrink-0" />
                                            <div className="opacity-0  transition-[opacity] duration-400 ease-linear group-hover:opacity-100">
                                                Communication
                                            </div>
                                        </div>
                                    </Sidebar.Item>
                                </Link>
                                <Link
                                    to={INSTRUCTOR_PERFORMANCE_OVERVIEW_PATH}
                                    className=""
                                >
                                    <Sidebar.Item
                                        active={location.pathname.startsWith(
                                            INSTRUCTOR_PERFORMANCE_PATH,
                                        )}
                                    >
                                        <div className="flex items-center">
                                            <MdBarChart className="mr-2 h-5 w-5 shrink-0" />
                                            <div className="opacity-0  transition-[opacity] duration-400 ease-linear group-hover:opacity-100">
                                                Performance
                                            </div>
                                        </div>
                                    </Sidebar.Item>
                                </Link>
                            </Sidebar.ItemGroup>
                        </Sidebar.Items>
                    </Sidebar>
                </Flowbite>
                {/* </div> */}
            </div>
            <div className="relative z-50 ml-14 flex-1 ">
                <Outlet />
            </div>
        </div>
    );
}
