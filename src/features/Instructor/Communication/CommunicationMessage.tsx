import { Button } from "flowbite-react";
import * as React from "react";
import { io, Socket } from "socket.io-client";
import { useAppSelector } from "../../../hooks/redux-hook";
import { Channel, Message, PaginationInfo, User } from "../../../models";
import { MessageServiceApi } from "../../../services/api/messageServiceApi";
import { selectUserAuth } from "../../../services/state/redux/authSlide";
import MessageBox from "./Message/MessageBox";
import config from "../../../configs/config";
export interface ICommunicationMessageProps {}
export const MESS_MODE = {
    NORMAL: 1,
    CREATE_NEW: 2,
};
const MESSAGE_SIZE = 12;
export default function CommunicationMessage(
    props: ICommunicationMessageProps,
) {
    const [modeMessage, setModeMessage] = React.useState(MESS_MODE.NORMAL);
    const [channels, setChannels] = React.useState<Channel[]>([]);
    const user = useAppSelector(selectUserAuth);
    const handleSetNothingMode = () => {
        setCurrentChannel(null);
        setModeMessage(MESS_MODE.NORMAL);
    };
    const [messages, setMessages] = React.useState<Message[]>([]);
    const [searchChannel, setSearchChannel] = React.useState("");

    const [currentChannel, setCurrentChannel] = React.useState<Channel | null>(
        null,
    );
    const [isInitScroll, setIsInitScroll] = React.useState(false);
    const [loadingMessage, setLoadingMessage] = React.useState(false);
    const [loadingChannel, setLoadingChannel] = React.useState(false);
    const [currMessagePage, setCurrMessagePage] = React.useState(1);
    const observer = React.useRef<IntersectionObserver | null>(null);
    const [infoMessagePagination, setInfoMessagePagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const socket = React.useRef<Socket>();

    const scrollContainerRef = React.useRef<HTMLDivElement>(null);

    React.useEffect(() => {
        const connectSocket = () => {
            if (user) {
                socket.current = io(`${config.apiUrl}`);
                socket.current.connect();
                lastMessageElementRef;
                socket.current.emit("join", { userID: user.id });

            }
        };
        connectSocket();
        return () => {
            if (socket.current) {
                socket.current.disconnect();
            }
        };
        // handleFocusChannel;
    }, []);

    const handleFocusChannel = (channel: Channel) => {
        setCurrentChannel(channel);
        setCurrMessagePage(1);
        setIsInitScroll(false);
        // setMessages([]);
        setModeMessage(MESS_MODE.NORMAL);
    };

    const handleSearchChannel = (value: string) => {
        setSearchChannel(value);
    };
    React.useLayoutEffect(() => {
        const fetchChannels = async () => {
            if (user) {
                // if (currentUser.isAvatarImageSet) {
                setLoadingChannel(true);
                const dataChannels =
                    await MessageServiceApi.getChannels(searchChannel);
                // const formatData: Channel[] = dataChannels.data.map(
                //     (channel: any) => {
                //         return {
                //             _id: channel._id,
                //             createdAt: channel.createdAt,
                //             contact: {
                //                 ID: channel.contact.ID,
                //                 Avatar: channel.contact.Avatar,
                //                 Name: channel.contact.Name,
                //             },
                //             latestMessage: {
                //                 _id: channel.latestMessage._id,
                //                 channelId: channel.latestMessage.channelId,
                //                 senderId: channel.latestMessage.senderId,
                //                 message: {
                //                     text: channel.latestMessage.message.text,
                //                 },
                //                 create_at: channel.latestMessage.create_at,
                //             },
                //         };
                //     },
                // );

                setChannels(dataChannels.item);
                setLoadingChannel(false);
            }
        };

        fetchChannels();
    }, [searchChannel]);

    React.useEffect(() => {
        const fetchChannels = async () => {
            if (user) {
                // if (currentUser.isAvatarImageSet) {
                const dataChannels = await MessageServiceApi.getChannels();

                setChannels(dataChannels.item);
                //  setContacts(data.data.data);
                // } else {
                //   navigate("/setAvatar");
                // }
            }
        };

        fetchChannels();
    }, []);
    // MessageServiceApi;

    React.useEffect(() => {
        const fetchMessage = async () => {
            try {
                if (currentChannel && user) {
                    setLoadingMessage(true);
                    const messageData = await MessageServiceApi.getMessages(
                        currentChannel.user.id,
                        MESSAGE_SIZE,
                        currMessagePage,
                    );
                    setInfoMessagePagination({
                        totalPage: messageData.meta.pageCount,
                        totalItem: messageData.meta.itemCount,
                        size: messageData.meta.size,
                    });

                    if (currMessagePage > 1) {
                        setMessages((prevMessage) => [
                            ...prevMessage,
                            ...messageData.item,
                        ]);
                    } else {
                        setMessages([...messageData.item]);
                    }

                    //  if (scrollContainerRef.current) {
                    //      const scrollAmount = 300; // Adjust this value as needed
                    //      console.log("SCROLL ACK");
                    //      scrollContainerRef.current.scrollTop += scrollAmount;
                    //  }

                    setLoadingMessage(false);
                }
            } catch (e) {
                console.log(e);
            }
        };

        fetchMessage();
    }, [currentChannel, currMessagePage]);
    console.log("messageData: ", messages);

    const handleSendMsg = async (msg: string) => {
        try {
            if (currentChannel && socket.current && user) {
                const body = {
                    to: currentChannel.user.id,
                    text: msg,
                };
                const messageData = await MessageServiceApi.createMessage(body);
                socket.current.emit("chat", {
                    to: currentChannel.user.id,
                    from: user.id,
                    text: msg,
                    id: messageData.id,
                });
                setMessages((messages) => {
                    return [
                        {
                            id: messageData.id,
                            fromUser: messageData.fromUser,
                            toUser: messageData.toUser,
                            createdAt: messageData.createdAt,
                            text: messageData.text,
                        },
                        ...messages,
                    ];
                });
                setChannels((channels) => {
                    const channelsUpdate = [...channels];
                    const findIndex = channelsUpdate.findIndex(
                        (channel) => channel.user.id === currentChannel.user.id,
                    );
                    if (findIndex != -1) {
                        const channelUpdate = { ...channelsUpdate[findIndex] };

                        channelUpdate.latestMessage = {
                            user: {
                                id: messageData.fromUser.id,
                                name: messageData.fromUser.name,
                                avatar: messageData.fromUser.avatar,
                            },
                            text: messageData.text,
                            createdAt: messageData.createdAt,
                        };
                        channelsUpdate[findIndex] = channelUpdate;
                        return channelsUpdate;
                    }

                    return channels;
                });
            }
        } catch (e) {
            console.log(e);
        }
    };

    const handleSendMsgCustom = async (msg: string, userData: User) => {
        try {
            console.log("socker: ", socket.current);
            if (socket.current && user) {
                const indexExistChannel = channels.findIndex(
                    (channel) => channel.user.id === userData.id,
                );

                if (indexExistChannel != -1) {
                    // Exist, only send message to
                    // const formData = new FormData();
                    // formData.append(
                    //     "channelId",
                    //     channels[indexExistChannel].id,
                    // );
                    // formData.append("message", msg);
                    const body = {
                        to: userData.id,
                        text: msg,
                    };
                    const messageData =
                        await MessageServiceApi.createMessage(body);

                    console.log("messageData", messageData);
                    socket.current.emit("chat", {
                        to: userData.id,
                        from: user.id,
                        text: msg,
                        id: messageData.id,
                    });
                    setMessages((messages) => {
                        return [
                            {
                                id: messageData.id,
                                fromUser: messageData.fromUser,
                                toUser: messageData.toUser,
                                createdAt: messageData.createdAt,
                                text: messageData.text,
                            },
                            ...messages,
                        ];
                    });
                    setChannels((channels) => {
                        const channelsUpdate = [...channels];
                        const findIndex = channelsUpdate.findIndex(
                            (channel) => channel.user.id === userData.id,
                        );
                        if (findIndex != -1) {
                            const channelUpdate = {
                                ...channelsUpdate[findIndex],
                            };

                            // const latestMessage: LatestMessage = {

                            channelUpdate.latestMessage = {
                                user: {
                                    id: messageData.fromUser.id,
                                    name: messageData.fromUser.name,
                                    avatar: messageData.fromUser.avatar,
                                },
                                text: messageData.text,
                                createdAt: messageData.createdAt,
                                // ...channelUpdate.latestMessage,
                                // senderId: user.id,
                                // message: {
                                //     text: messageData.data.message.text,
                                // },
                                // _id: messageData.data._id,
                                // create_at: messageData.data.create_at,
                                // channelId: currentChannel._id,
                            };
                            channelsUpdate[findIndex] = channelUpdate;
                            return channelsUpdate;
                        }

                        return channels;
                    });
                } else {
                    const body = {
                        to: userData.id,
                        text: msg,
                    };
                    const messageData =
                        await MessageServiceApi.createMessage(body);

                    console.log("messageData", messageData);
                    socket.current.emit("chat", {
                        to: userData.id,
                        from: user.id,
                        text: msg,
                        id: messageData.id,
                    });
                    setMessages((messages) => {
                        return [
                            {
                                id: messageData.id,
                                fromUser: messageData.fromUser,
                                toUser: messageData.toUser,
                                createdAt: messageData.createdAt,
                                text: messageData.text,
                            },
                            ...messages,
                        ];
                    });
                    setChannels((channels) => {
                        const channelsUpdate = [...channels];

                        // const latestMessage: LatestMessage = {
                        const newChannel: Channel = {
                            id: messageData.id,
                            user: {
                                id: userData.id,
                                name: userData.name,
                                avatar: userData.avatar
                                    ? userData.avatar
                                    : null,
                            },
                            latestMessage: {
                                user: {
                                    id: messageData.fromUser.id,
                                    name: messageData.fromUser.name,
                                    avatar: messageData.fromUser.avatar,
                                },
                                text: messageData.text,
                                createdAt: messageData.createdAt,
                            },
                        };

                        return [newChannel, ...channelsUpdate];
                        // channelUpdate.latestMessage = {
                        //     user: {
                        //         id: messageData.fromUser.id,
                        //         name: messageData.fromUser.name,
                        //         avatar: messageData.fromUser.avatar,
                        //     },
                        //     text: messageData.text,
                        //     createdAt: messageData.createdAt,
                        //     // ...channelUpdate.latestMessage,
                        //     // senderId: user.id,
                        //     // message: {
                        //     //     text: messageData.data.message.text,
                        //     // },
                        //     // _id: messageData.data._id,
                        //     // create_at: messageData.data.create_at,
                        //     // channelId: currentChannel._id,
                        // };
                        // channelsUpdate[findIndex] = channelUpdate;
                        // return channelsUpdate;
                    });
                }
            }
        } catch (e) {
            console.log(e);
        }
    };
    console.log("Message OK:", messages);
    React.useEffect(() => {
        const handleReceive = (data: any) => {
            console.log("Data receive: ", data);
            console.log("channels: ", channels);

            setChannels((channels) => {
                const channelsUpdate = [...channels];
                const findIndex = channelsUpdate.findIndex(
                    (channel) =>
                        channel.user.id === data.fromUser.id ||
                        channel.user.id === data.to,
                );

                console.log("findIndex: ", findIndex);
                if (findIndex != -1) {
                    const channelUpdate = {
                        ...channelsUpdate[findIndex],
                    };
                    channelUpdate.latestMessage = {
                        user: {
                            id: data.fromUser.id,
                            name: data.fromUser.name,
                            avatar: data.fromUser.avatar,
                        },
                        text: data.text,
                        createdAt: new Date().toISOString(),
                    };
                    channelsUpdate[findIndex] = channelUpdate;

                    return channelsUpdate;
                } else {
                    return channelsUpdate;
                }
            });

            if (user) {
                setMessages((prev) => [
                    {
                        toUser: {
                            id: user.id,
                            name: user.name,
                            avatar: user.avatar,
                        },
                        fromUser: {
                            id: data.fromUser.id,
                            name: data.fromUser.name,
                            avatar: data.fromUser.avatar,
                        },
                        id: data.id,
                        text: data.text,
                        createdAt: new Date().toISOString(),
                    },
                    ...prev,
                ]);
            }
        };
        if (socket.current) {
            socket.current.on("receive", handleReceive);
        }
        return () => {
            if (socket.current) {
                socket.current.off("receive", handleReceive);
            }
        };
    }, [channels]);

    const handleIncreaseMessagePage = () => {
        setCurrMessagePage((curr) => curr + 1);
    };
    const setReadyIsInitScroll = () => {
        setIsInitScroll(true);
    };
  
    const lastMessageElementRef = React.useCallback(
        async (node: any) => {
            await new Promise((r) => setTimeout(r, 1000));
            if (loadingMessage) return;
            if (observer.current) observer.current.disconnect();

            observer.current = new IntersectionObserver((entries) => {
                if (
                    entries[0].isIntersecting &&
                    currMessagePage < infoMessagePagination.totalPage &&
                        isInitScroll
                ) {
                   
                    setCurrMessagePage((curr) => curr + 1);
                }
            });
            if (node) observer.current.observe(node);
        },
        [loadingMessage, currMessagePage, isInitScroll],
    );
    return (
        <div className="px-16">
            <div className="mb-8 flex items-center justify-between gap-6">
                <h2 className="text-[32px] font-bold text-primary-black">
                    Tin nhắn
                </h2>
                <Button
                    onClick={() => {
                        setModeMessage(MESS_MODE.CREATE_NEW);
                        setCurrentChannel(null);
                    }}
                    color="dark"
                    className="font-bold"
                >
                    Tạo tin nhắn mới
                </Button>
            </div>

            <MessageBox
                modeMessage={modeMessage}
                messages={messages}
                channels={channels}
                user={user}
                scrollContainerRef={scrollContainerRef}
                lastMessageElementRef={lastMessageElementRef}
                loadingChannel={loadingChannel}
                handleFocusChannel={handleFocusChannel}
                handleIncreaseMessagePage={handleIncreaseMessagePage}
                currentChannel={currentChannel}
                handleSendMsg={handleSendMsg}
                loadingMessage={loadingMessage}
                infoMessagePagination={infoMessagePagination}
                handleSetNothingMode={handleSetNothingMode}
                handleSendMsgCustom={handleSendMsgCustom}
                handleSearchChannel={handleSearchChannel}
                setReadyIsInitScroll={setReadyIsInitScroll}
                currMessagePage={currMessagePage}
            />
        </div>
    );
}
