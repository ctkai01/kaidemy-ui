import { Avatar, Flowbite } from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import Editor from "../../../../components/ui/core/Editor";
import {
    AnswerLecture,
    PaginationInfo,
    QuestionLectureAuthor,
} from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import { akaName, formatDistanceToNowTime } from "../../../../utils";
import AnswerList from "./AnswerList";
import { customAvatarTheme } from "./QuestionItem";

export interface IAnswerSectionProps {
    currentQuestion: QuestionLectureAuthor;
    answerLectures: AnswerLecture[];
    handlePaginateAnswer: () => void;
}

export default function AnswerSection(props: IAnswerSectionProps) {
    const { currentQuestion, answerLectures, handlePaginateAnswer } = props;

    // const [infoAnswerPagination, setAnswerInfoPagination] =
    //     React.useState<PaginationInfo>({
    //         totalItem: 0,
    //         totalPage: 0,
    //         size: 0,
    //     });
    // const [currAnswerPage, setCurrAnswerPage] = React.useState(1);

    // const [answerLectures, setAnswerLectures] = React.useState<AnswerLecture[]>(
    //     [],
    // );
    const refAnswer = React.useRef(null);

    // React.useLayoutEffect(() => {
    //     const fetchAnswerLectures = async () => {
    //         try {
    //             if (currentQuestion) {
    //                 const dataAnswerLectures: any =
    //                     await CourseServiceApi.getAnswerLectures(
    //                         currentQuestion.id,
    //                         currAnswerPage,
    //                         SIZE_PAGE_ANSWER,
    //                     );

    //                 setAnswerInfoPagination({
    //                     totalPage: dataAnswerLectures.answer_lectures.total_page
    //                         ? dataAnswerLectures.answer_lectures.total_page
    //                         : 0,
    //                     totalItem: dataAnswerLectures.answer_lectures
    //                         .total_items
    //                         ? dataAnswerLectures.answer_lectures.total_items
    //                         : 0,
    //                     size: dataAnswerLectures.answer_lectures.size,
    //                 });

    //                 if (!dataAnswerLectures.answer_lectures?.items) {
    //                     setAnswerLectures([]);
    //                 } else {
    //                     if (currAnswerPage > 1) {
    //                         const newQuestionLectures = [
    //                             ...answerLectures,
    //                             ...dataAnswerLectures.answer_lectures.items,
    //                         ];

    //                         setAnswerLectures(newQuestionLectures);
    //                     } else {
    //                         const newQuestionLectures = [
    //                             ...dataAnswerLectures.answer_lectures.items,
    //                         ];

    //                         // const uniqueQuestions = _.uniqBy(newQuestionLectures, "id");
    //                         setAnswerLectures(newQuestionLectures);
    //                     }
    //                 }
    //             }
    //         } catch (e) {
    //             console.log(e);
    //         }
    //     };
    //     fetchAnswerLectures();
    // }, [currAnswerPage, currentQuestion]);

    const onScroll = () => {
        if (refAnswer.current) {
            const { scrollTop, scrollHeight, clientHeight } = refAnswer.current;
            if (scrollTop + clientHeight === scrollHeight) {
                handlePaginateAnswer();
            }
        }
    };

    // console.log("answerLectures: ", answerLectures);
    return (
        <div
            onScroll={onScroll}
            ref={refAnswer}
            className="max-h-[600px] overflow-y-scroll"
        >
            <div className="flex gap-2 px-5 py-8">
                <div>
                    <Flowbite theme={{ theme: customAvatarTheme }}>
                        {false ? (
                            <Avatar img="" alt="avatar" rounded size="md" />
                        ) : (
                            <Avatar
                                placeholderInitials={akaName(
                                    currentQuestion.user.name,
                                )}
                                rounded
                                size="md"
                            />
                        )}
                    </Flowbite>
                </div>
                <div>
                    <div className="mb-6 flex gap-4 text-sm">
                        <Link to={`/user/${currentQuestion.user.id}`}>
                            <h3 className="text-cyan-600">
                                {currentQuestion.user.name}
                            </h3>
                        </Link>
                        <div className="text-primary-gray">
                            {formatDistanceToNowTime(currentQuestion.createdAt)}
                        </div>
                    </div>
                    <div>
                        <div className="mb-2 font-bold text-primary-black">
                            {currentQuestion.title}
                        </div>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: currentQuestion.description
                                    ? currentQuestion.description
                                    : "",
                            }}
                        ></div>
                    </div>
                </div>
            </div>
            <div className="flex items-center">
                <div className="flex-1 border-t border-primary-hover-gray"></div>
                <div className="mx-6 font-bold">
                    {currentQuestion.totalAnswer} Answers
                </div>
                <div className="flex-1 border-t border-primary-hover-gray"></div>
            </div>

            <AnswerList answerLectures={answerLectures} />
        </div>
    );
}
