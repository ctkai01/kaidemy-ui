import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import * as React from "react";
import { QuestionLecture, QuestionLectureAuthor } from "../../../../models";
import { akaName, formatDistanceToNowTime } from "../../../../utils";

export interface IQuestionItemProps {
    question: QuestionLectureAuthor;
    isActive: boolean
    handleFocusQuestion: (question: QuestionLectureAuthor) => void;
}

export const customAvatarTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-14 h-14",
            },
        },
    },
};

export default function QuestionItem(props: IQuestionItemProps) {
    const { question, isActive, handleFocusQuestion } = props;
    return (
        <div
            onClick={() => {
                handleFocusQuestion(question);
            }}
            className={`${
                isActive ? "bg-primary-hover-gray" : ""
            } flex cursor-pointer gap-4 border-b p-4 first:border-t  hover:bg-primary-hover-gray`}
        >
            <div>
                <Flowbite theme={{ theme: customAvatarTheme }}>
                    {question.user.avatar ? (
                        <Avatar
                            img={question.user.avatar}
                            alt="avatar"
                            rounded
                            size="md"
                        />
                    ) : (
                        <Avatar
                            placeholderInitials={akaName(question.user.name)}
                            rounded
                            size="md"
                        />
                    )}
                </Flowbite>
            </div>
            <div className="flex-1">
                <div>
                    <div className="w-full max-w-[350px] truncate font-bold text-cyan-600">
                        {question.title}
                    </div>
                    {/* <div className="line-clamp-2 w-full max-w-[350px] overflow-hidden text-ellipsis">
                        Lorem ipsum dolor sit amet consectetur adipicing
                        elit.dsds Perspiciatis natus quis accusantium nulla ex
                        cum sapiente aliquid, sequi nesciunt? Iure aliquam
                        mollitia cupiditate odit asperiores fuga quo quasi
                        adipisci hic.
                    </div> */}
                </div>
                <div className="mt-1 flex justify-between text-sm font-bold text-primary-gray">
                    <h2 className="max-w-[200px] truncate">
                        {question.user.name}
                    </h2>
                    <div>{formatDistanceToNowTime(question.createdAt)}</div>
                </div>
            </div>
        </div>
    );
}
