import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Spinner,
    TextInput,
} from "flowbite-react";
import * as React from "react";
import { UseFormRegister } from "react-hook-form";
import { FaSearch } from "react-icons/fa";
import { Link } from "react-router-dom";
import Editor from "../../../../components/ui/core/Editor";
import {
    AnswerLecture,
    QuestionLecture,
    QuestionLectureAuthor,
} from "../../../../models";
import { FilterQA } from "../CommunicationQA";
import AnswerList from "./AnswerList";
import AnswerSection from "./AnswerSection";
import QuestionList from "./QuestionList";

export interface IQABoxProps {
    isShowReply: boolean;
    questionLectures: QuestionLectureAuthor[];
    currentQuestion: QuestionLectureAuthor | null;
    loadingQuestion: boolean;
    answerLectures: AnswerLecture[];
    handlePaginateQuestion: () => void;
    handlePaginateAnswer: () => void;
    handleSetReplyHTML: (value: string) => void;
    handleFocusQuestion: (question: QuestionLectureAuthor) => void;
    registerForm: UseFormRegister<FilterQA>;
}

const customInputTheme: CustomFlowbiteTheme = {
    textInput: {
        field: {
            input: {
                base: "overflow-hidden block w-full border disabled:cursor-not-allowed disabled:opacity-50",
                withAddon: {
                    on: "",
                    off: "",
                },
                // colors: {
                //     // info: "placeholder-cyan-700",
                //     gray: ""
                // },
            },
        },
    },
};

export default function QABox(props: IQABoxProps) {
    const {
        isShowReply,
        questionLectures,
        currentQuestion,
        loadingQuestion,
        answerLectures,
        handlePaginateAnswer,
        handleFocusQuestion,
        registerForm,
        handlePaginateQuestion,
        handleSetReplyHTML,
    } = props;

    return (
        <div className="flex border border-primary-hover-gray">
            <div className="flex-1 overflow-hidden  border-r border-primary-hover-gray">
                <Flowbite theme={{ theme: customInputTheme }}>
                    <TextInput
                        type="text"
                        rightIcon={FaSearch}
                        placeholder="Search"
                        required
                        className="m-1"
                        {...registerForm("search")}
                        //   onChange={handleChangeSearch}
                        //   value={search}
                    />
                </Flowbite>
                {loadingQuestion ? (
                    <div className="flex h-[600px] items-center justify-center">
                        <Spinner />
                    </div>
                ) : currentQuestion ? (
                    <QuestionList
                        currentQuestion={currentQuestion}
                        handlePaginateQuestion={handlePaginateQuestion}
                        handleFocusQuestion={handleFocusQuestion}
                        questionLectures={questionLectures}
                    />
                ) : (
                    <div className="mt-10 text-center font-bold">
                        Have no questions.
                    </div>
                )}
            </div>
            <div className="relative shrink grow-2 basis-0 overflow-hidden border-primary-hover-gray">
                {loadingQuestion ? (
                    <div className="flex h-[600px] items-center justify-center">
                        <Spinner />
                    </div>
                ) : currentQuestion ? (
                    <>
                        <div className="flex gap-2 border-b border-primary-hover-gray p-2">
                            <div className="w-[100px]">
                                <img
                                    className="w-full"
                                    src={
                                        currentQuestion.course.image
                                            ? currentQuestion.course.image
                                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                                    }
                                />
                            </div>
                            <div className="flex-1 overflow-hidden">
                                <Link
                                    to={`/course/${currentQuestion.course.id}`}
                                    className="max-w-[800px] overflow-hidden text-ellipsis whitespace-nowrap font-bold text-cyan-600"
                                >
                                    {currentQuestion.course.title}
                                </Link>
                            </div>
                        </div>
                        <AnswerSection
                            handlePaginateAnswer={handlePaginateAnswer}
                            currentQuestion={currentQuestion}
                            answerLectures={answerLectures}
                        />
                        {isShowReply && (
                            <div className="bot absolute bottom-0 left-0 w-full bg-white">
                                <Editor
                                    handleSetHTML={handleSetReplyHTML}
                                    isImage={false}
                                />
                            </div>
                        )}
                    </>
                ) : (
                    <div className="flex h-[600px] items-center justify-center font-bold">
                        Không có câu trả lời nào
                    </div>
                )}
            </div>
        </div>
    );
}
