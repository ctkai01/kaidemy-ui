import * as React from "react";
import { useAppSelector } from "../../../../hooks/redux-hook";
import { AnswerLecture } from "../../../../models";
import { selectUserAuth } from "../../../../services/state/redux/authSlide";
import AnswerQAItem from "./AnswerQAItem";

export interface IAnswerListProps {
    answerLectures: AnswerLecture[];
}

export default function AnswerList(props: IAnswerListProps) {
    const { answerLectures } = props;
    const user = useAppSelector(selectUserAuth);
    return (
        <div className="">
            {user &&
                answerLectures.map((answer) => (
                    <AnswerQAItem
                        isAuthor={user.id === answer.user.id}
                        key={answer.id}
                        answer={answer}
                    />
                ))}
        </div>
    );
}
