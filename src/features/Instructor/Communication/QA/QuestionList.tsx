import * as React from 'react';
import { QuestionLecture, QuestionLectureAuthor } from '../../../../models';
import QuestionItem from './QuestionItem';

export interface IQuestionListProps {
    questionLectures: QuestionLectureAuthor[];
    currentQuestion: QuestionLectureAuthor
    handlePaginateQuestion: () => void;
    handleFocusQuestion: (question: QuestionLectureAuthor) => void;
}

export default function QuestionList (props: IQuestionListProps) {
    const {
        questionLectures,
        currentQuestion,
        handlePaginateQuestion,
        handleFocusQuestion,
    } = props;
    const refQuestion = React.useRef(null)

    const onScroll = () => {
        if (refQuestion.current) {
            const { scrollTop, scrollHeight, clientHeight } = refQuestion.current;
            if (scrollTop + clientHeight === scrollHeight) {
                handlePaginateQuestion()
            }
        }
    };
  return (
      <div
          onScroll={onScroll}
          ref={refQuestion}
          className="h-[623px] overflow-y-scroll"
      >
          {questionLectures.map((question) => (
              <QuestionItem
                  isActive={currentQuestion.id === question.id}
                  handleFocusQuestion={handleFocusQuestion}
                  key={question.id}
                  question={question}
              />
          ))}
      </div>
  );
}
