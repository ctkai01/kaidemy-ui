import { Avatar, Flowbite } from 'flowbite-react';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { AnswerLecture } from '../../../../models';
import { akaName, formatDistanceToNowTime } from '../../../../utils';
import { customAvatarTheme } from './QuestionItem';

export interface IAnswerQAItemProps {
    answer: AnswerLecture
    isAuthor: boolean
}

export default function AnswerQAItem (props: IAnswerQAItemProps) {
  const { answer, isAuthor } = props;
  return (
      <div className="border-b px-5 py-8">
          <div className={`mb-4 flex ${isAuthor && "flex-row-reverse"} gap-2`}>
              <div>
                  <Flowbite theme={{ theme: customAvatarTheme }}>
                      {answer.user.avatar ? (
                          <Avatar
                              img={answer.user.avatar}
                              alt="avatar"
                              rounded
                              size="md"
                          />
                      ) : (
                          <Avatar
                              placeholderInitials={akaName(answer.user.name)}
                              rounded
                              size="md"
                          />
                      )}
                  </Flowbite>
              </div>
              <div>
                  <div
                      className={`mb-6 flex ${
                          isAuthor && "flex-row-reverse"
                      } gap-4  text-sm`}
                  >
                      <Link to={`/user/${answer.user.id}`}>
                          <h3 className="text-cyan-600">{answer.user.name}</h3>
                      </Link>
                      <div className="text-primary-gray">
                          {formatDistanceToNowTime(answer.createdAt)}
                      </div>
                  </div>
                  <div>
                      {/* <div className="mb-2 font-bold text-primary-black">
                      What did Caesar have top say about the constitution?
                  </div> */}
                  </div>
              </div>
          </div>
          <div className={`${isAuthor && "text-right"}`}>
              <div
                  dangerouslySetInnerHTML={{
                      __html: answer.answer,
                  }}
              ></div>
          </div>
      </div>
  );
}
