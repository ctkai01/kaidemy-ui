import * as React from "react";
import { Controller, useForm } from "react-hook-form";
import {
    AnswerLecture,
    CreateAnswerLecture,
    FilterQuestionAuthor,
    PaginationInfo,
    QuestionLecture,
    QuestionLectureAuthor,
} from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { SelectData } from "../Performance/PerformanceReviews";
import Select from "react-select";
import {
    Button,
    Checkbox,
    Label,
    Pagination,
    Select as SelectFlowbite,
    Spinner,
} from "flowbite-react";
import QABox from "./QA/QABox";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemeCreateAnswerLecture } from "../../../validators/course";
import { toast } from "react-toastify";

export interface ICommunicationQAProps {}
export interface FilterQA {
    sort: string;
    course_id: SelectData;
    search: string;
}

const COURSE_SIZE = 10;
const QUESTION_SIZE = 8;
const ANSWER_PAGE = 5;
export default function CommunicationQA(props: ICommunicationQAProps) {
    const [selectCoursesData, setSelectCoursesData] = React.useState<
        SelectData[]
    >([
        {
            label: "Tất cả khóa học",
            value: "-1",
        },
    ]);
    const [currCoursePage, setCurrCoursePage] = React.useState(1);
    const [infoCoursePagination, setCourseInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [loadingCourse, setLoadingCourse] = React.useState(false);
    const [isShowReply, setIsShowReply] = React.useState(false);
    const [currentQuestionPage, setCurrentQuestionPage] = React.useState(1);
    const [questionLectures, setQuestionLectures] = React.useState<
        QuestionLectureAuthor[]
    >([]);

    const [infoAnswerPagination, setAnswerInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [currAnswerPage, setCurrAnswerPage] = React.useState(1);

    const [answerLectures, setAnswerLectures] = React.useState<AnswerLecture[]>(
        [],
    );
    const [loadingQuestion, setLoadingQuestion] = React.useState(false);

    const [currentQuestion, setCurrentQuestion] =
        React.useState<QuestionLectureAuthor | null>(null);

    const [infoQuestionPagination, setQuestionInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });

    const {
        formState: { errors },
        reset: resetCreateAnswer,
        setValue: setValueCreateAnswer,
        watch: watchCreateAnswer,
    } = useForm<CreateAnswerLecture>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateAnswerLecture),
    });
    const { register, reset, control, watch } = useForm<FilterQA>({
        mode: "onChange",
        defaultValues: {
            sort: "DESC",
        },
    });

    const handleCommandShowReply = (command: boolean) => {
        setIsShowReply(command);
    };

    const handleSetReplyHTML = (value: string) => {
        if (value === "<p><br></p>") {
            setValueCreateAnswer("answer", "");
        } else {
            setValueCreateAnswer("answer", value);
        }
    };

    React.useLayoutEffect(() => {
        const fetchAnswerLectures = async () => {
            try {
                if (currentQuestion) {
                    const dataAnswerLectures: any =
                        await CourseServiceApi.getAnswerLectures(
                            currentQuestion.id,
                            currAnswerPage,
                            ANSWER_PAGE,
                        );

                    setAnswerInfoPagination({
                        totalPage: dataAnswerLectures.meta.pageCount,
                        totalItem: dataAnswerLectures.meta.itemCount,
                        size: dataAnswerLectures.meta.size,
                    });

                    if (!dataAnswerLectures.item) {
                        setAnswerLectures([]);
                    } else {
                        if (currAnswerPage > 1) {
                            const newQuestionLectures = [
                                ...answerLectures,
                                ...dataAnswerLectures.item,
                            ];

                            setAnswerLectures(newQuestionLectures);
                        } else {
                            const newQuestionLectures = [
                                ...dataAnswerLectures.item,
                            ];

                            // const uniqueQuestions = _.uniqBy(newQuestionLectures, "id");
                            setAnswerLectures(newQuestionLectures);
                        }
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchAnswerLectures();
    }, [currAnswerPage, currentQuestion]);

    React.useLayoutEffect(() => {
        const fetchCourseData = async () => {
            try {
                setLoadingCourse(true);
                const data = await CourseServiceApi.getCoursesAuthor(
                    COURSE_SIZE,
                    currCoursePage,
                );
                setCourseInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data?.item) {
                    setSelectCoursesData([]);
                } else {
                    const newData: SelectData[] = data.item.map((item: any) => {
                        return {
                            label: item.title,
                            value: `${item.id}`,
                        };
                    });
                    setSelectCoursesData((data) => [...data, ...newData]);
                }
                setLoadingCourse(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseData();
    }, [currCoursePage]);
    React.useLayoutEffect(() => {
        const fetchQuestionByCourseID = async (
            filter: FilterQuestionAuthor,
        ) => {
            setLoadingQuestion(true);
            const dataQuestionLectures =
                await CourseServiceApi.getQuestionLecturesAuthor(
                    currentQuestionPage,
                    QUESTION_SIZE,
                    filter,
                );
            setCurrentQuestion(null);
            setQuestionInfoPagination({
                totalPage: dataQuestionLectures.meta.pageCount,
                totalItem: dataQuestionLectures.meta.itemCount,
                size: dataQuestionLectures.meta.size,
            });

            if (!dataQuestionLectures?.item) {
                setQuestionLectures([]);
                setCurrentQuestion(null);
            } else {
                if (currentQuestionPage > 1) {
                    const newQuestionLectures = [
                        ...questionLectures,
                        ...dataQuestionLectures.item,
                    ];
                    if (newQuestionLectures.length > 0) {
                        setCurrentQuestion(newQuestionLectures[0]);
                    } else {
                        setCurrentQuestion(null);
                    }
                    setQuestionLectures(newQuestionLectures);
                } else {
                    const newQuestionLectures = [...dataQuestionLectures.item];
                    if (newQuestionLectures.length > 0) {
                        setCurrentQuestion(newQuestionLectures[0]);
                    } else {
                        setCurrentQuestion(null);
                    }
                    // const uniqueQuestions = _.uniqBy(newQuestionLectures, "id");
                    setQuestionLectures(newQuestionLectures);
                }
            }
            setLoadingQuestion(false);
        };

        const subscription = watch((value) => {
            const filter: FilterQuestionAuthor = {
                search: value.search ? value.search : null,
                course_id:
                    value.course_id && value.course_id.value
                        ? value.course_id.value
                        : null,
                sort: value.sort ? value.sort : null,
            };
            setCurrentQuestionPage(1);
            fetchQuestionByCourseID(filter);
        });
        const filter: FilterQuestionAuthor = {
            course_id: watch("course_id") ? watch("course_id").value : null,
            sort: watch("sort") ? watch("sort") : null,
            search: watch("search") ? watch("search") : null,
        };

        fetchQuestionByCourseID(filter);
        return () => subscription.unsubscribe();
    }, [watch, currentQuestionPage]);
    // FilterQuestionAuthor

    const handlePaginateQuestion = () => {
        if (currentQuestionPage < infoQuestionPagination.totalPage) {
            setCurrentQuestionPage((current) => current + 1);
        }
    };

    const handlePaginateAnswer = () => {
        if (currAnswerPage < infoAnswerPagination.totalPage) {
            setCurrAnswerPage((current) => current + 1);
        }
    };

    const handleFocusQuestion = (question: QuestionLectureAuthor) => {
        setCurrentQuestion(question);
    };
    // {
    //     errors.answer ? (
    //         <div className="mt-2 text-sm text-red-700">
    //             <span className="font-medium ">Oops! </span>
    //             <span>{errors.answer.message}</span>
    //         </div>
    //     ) : (
    //         <></>
    //     );
    // }
    const handleCreateAnswer = async () => {
        if (currentQuestion) {
            if (errors.answer && errors.answer.message) {
                toast(<div className="font-bold"> errors.answer.message</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                });
                return;
            }
            try {
                // const formData = new FormData();
                // formData.append("question_lecture_id", `${currentQuestion.id}`);
                const body = {
                    questionLectureID: currentQuestion.id,
                    answer: watchCreateAnswer("answer"),
                };
                // formData.append("answer", watchCreateAnswer("answer"));
                const dataCreateAnswer =
                    await CourseServiceApi.createAnswerLecture(body);

                setAnswerLectures((answerLectures) => {
                    const answerLecturesUpdate = [...answerLectures];
                    let newData = [dataCreateAnswer, ...answerLecturesUpdate];

                    if (
                        infoAnswerPagination.totalItem + 1 >
                        ANSWER_PAGE * infoAnswerPagination.totalPage
                    ) {
                        // newData = newData.splice(0, SIZE_PAGE_ANSWER);
                        setAnswerInfoPagination((data) => {
                            return {
                                ...data,
                                totalPage: data.totalPage + 1,
                                totalItem: data.totalItem + 1,
                            };
                        });
                    } else {
                        setAnswerInfoPagination((data) => {
                            return {
                                ...data,
                                totalItem: data.totalItem + 1,
                            };
                        });
                    }
                    return newData;
                });

                setQuestionLectures((questionLectures) => {
                    const questionLecturesUpdate = [...questionLectures];

                    const findIndex = questionLecturesUpdate.findIndex(
                        (questionLecture) =>
                            questionLecture.id === currentQuestion.id,
                    );

                    if (findIndex != -1) {
                        const questionLectureUpdate = {
                            ...questionLecturesUpdate[findIndex],
                        };

                        questionLectureUpdate.totalAnswer =
                            questionLectureUpdate.totalAnswer + 1;

                        questionLecturesUpdate[findIndex] =
                            questionLectureUpdate;

                        return questionLecturesUpdate;
                    }

                    return questionLectures;
                });

                resetCreateAnswer();
                toast(
                    <div className="font-bold">
                        {" "}
                        Answered question successfully!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );

                reset();
            } catch (e) {
                console.log("");
                toast(
                    <div className="font-bold"> Answered question failed</div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "error",
                        theme: "colored",
                    },
                );
            }
        }
    };

    return (
        <div className="px-16">
            <div className="mb-8 flex items-center gap-6">
                <h2 className="text-[32px] font-bold text-primary-black">QA</h2>

                {loadingCourse && selectCoursesData.length === 0 ? (
                    <div className="animate-pulse">
                        <div className="bg-slate-300">
                            <Controller
                                name="course_id"
                                control={control}
                                render={({ field }) => (
                                    <Select
                                        {...field}
                                        defaultValue={selectCoursesData[0]}
                                        isClearable={true}
                                        isSearchable={true}
                                        maxMenuHeight={200}
                                        options={selectCoursesData}
                                        onMenuScrollToBottom={() => {}}
                                    />
                                )}
                            />
                        </div>
                    </div>
                ) : (
                    <div className="w-[400px]">
                        <Controller
                            name="course_id"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    className="w-full"
                                    defaultValue={selectCoursesData[0]}
                                    isClearable={true}
                                    isSearchable={true}
                                    maxMenuHeight={200}
                                    options={selectCoursesData}
                                    onMenuScrollToBottom={() => {
                                        if (
                                            currCoursePage <
                                            infoCoursePagination.totalPage
                                        ) {
                                            setCurrCoursePage(
                                                currCoursePage + 1,
                                            );
                                        }
                                    }}
                                />
                            )}
                        />
                    </div>
                )}
            </div>
            <div className="flex items-center gap-3  pb-6 pt-4">
                <div className="mb-2 block">
                    <Label htmlFor="sort" value="Sắp xếp theo:" />
                </div>
                <SelectFlowbite {...register("sort")} id="sort" required>
                    <option value="DESC">Newest to oldest</option>
                    <option value="ASC">Oldest to newest</option>
                </SelectFlowbite>

                <div className="flex flex-1 justify-end gap-4">
                    <Button
                        className="font-bold"
                        onClick={() => handleCommandShowReply(!isShowReply)}
                    >
                        {isShowReply ? "Hide Reply form" : "Show Reply form"}
                    </Button>
                    {isShowReply && (
                        <Button
                            color="dark"
                            className="font-bold"
                            disabled={
                                !watchCreateAnswer("answer") ? true : false
                            }
                            onClick={() => {
                                handleCreateAnswer();
                            }}
                        >
                            Reply
                        </Button>
                    )}
                </div>
            </div>
            <QABox
                isShowReply={isShowReply}
                registerForm={register}
                handlePaginateQuestion={handlePaginateQuestion}
                handleFocusQuestion={handleFocusQuestion}
                currentQuestion={currentQuestion}
                questionLectures={questionLectures}
                loadingQuestion={loadingQuestion}
                handleSetReplyHTML={handleSetReplyHTML}
                handlePaginateAnswer={handlePaginateAnswer}
                answerLectures={answerLectures}
            />
        </div>
    );
}
