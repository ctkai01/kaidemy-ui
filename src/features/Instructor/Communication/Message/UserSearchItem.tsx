import { Avatar, CustomFlowbiteTheme, Flowbite } from 'flowbite-react';
import * as React from 'react';
import { User } from '../../../../models';
import { akaName } from '../../../../utils';

export interface IUserSearchItemProps {
    user: User;
    handlePickUser: (user: User) => void;
}

const customAvatarChannelMessageTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-8 h-8",
            },
            initials: {
                text: "text-white",
                base: "inline-flex overflow-hidden relative justify-center items-center bg-black dark:bg-gray-600",
            },
        },
    },
};
export default function UserSearchItem (props: IUserSearchItemProps) {
  const { user, handlePickUser } = props;
    return (
        <div
            onClick={() => handlePickUser(user)}
            className="flex cursor-pointer items-center gap-2 p-3"
        >
            <Flowbite theme={{ theme: customAvatarChannelMessageTheme }}>
                {user.avatar ? (
                    <Avatar img={user.avatar} alt="avatar" rounded size="md" />
                ) : (
                    <Avatar
                        placeholderInitials={akaName(user.name)}
                        rounded
                        size="md"
                    />
                )}
            </Flowbite>
            <div className="text-primary-black">{user.name}</div>
        </div>
    );
}
