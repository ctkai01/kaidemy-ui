import _ from 'lodash';
import * as React from 'react';
import { Channel } from '../../../../models';
import StudentItem from './StudentItem';

export interface IStudentListProps {
    channels: Channel[];
    handleFocusChannel: (channel: Channel) => void;
    currentChannel: Channel | null;
}

export default function StudentList (props: IStudentListProps) {
    const { channels, currentChannel, handleFocusChannel } = props;
    const sortedChannels = _.orderBy(
        channels,
        [(channel) => new Date(channel.latestMessage.createdAt).getTime()],
        ["desc"],
    );
    return (
        <div
            //   onScroll={onScroll}
            //   ref={refQuestion}
            className="h-[623px] overflow-y-scroll"
        >
            {sortedChannels.map((channel) => (
                <StudentItem
                    isActive={
                        !currentChannel || channel.user.id !== currentChannel.user.id
                            ? false
                            : true
                    }
                    handleFocusChannel={handleFocusChannel}
                    key={channel.id}
                    channel={channel}
                />
            ))}
            {/* <StudentItem />
            <StudentItem />
            <StudentItem />
            <StudentItem />
            <StudentItem />
            <StudentItem />
            <StudentItem />
            <StudentItem /> */}
            {/* {questionLectures.map((question) => (
              <QuestionItem
                  isActive={currentQuestion.id === question.id}
                  handleFocusQuestion={handleFocusQuestion}
                  key={question.id}
                  question={question}
              />
          ))} */}
        </div>
    );
}
