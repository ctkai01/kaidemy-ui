import * as React from "react";
import { useAppSelector } from "../../../../hooks/redux-hook";
import { Message, PaginationInfo } from "../../../../models";
import { selectUserAuth } from "../../../../services/state/redux/authSlide";
import MessageItem from "./MessageItem";

export interface IMessageListProps {
    messages: Message[];
    loadingMessage: boolean;
    currMessagePage: number;
    infoMessagePagination: PaginationInfo;
    handleIncreaseMessagePage: () => void;
    lastMessageElementRef: (node: any) => void;
    setReadyIsInitScroll: () => void;
    scrollContainerRef: React.RefObject<HTMLDivElement>;

}

export default function MessageList(props: IMessageListProps) {
    const {
        messages,
        loadingMessage,
        currMessagePage,
        infoMessagePagination,
        scrollContainerRef,
        handleIncreaseMessagePage,
        lastMessageElementRef,
        setReadyIsInitScroll,
    } = props;
    const scrollMessageItemRef = React.useRef<HTMLDivElement>(null);
    // const scrollContainerRef = React.useRef<HTMLDivElement>(null);
     const user = useAppSelector(selectUserAuth);

    React.useEffect(() => {
        if (scrollMessageItemRef.current && currMessagePage === 1) {
            scrollMessageItemRef.current.scrollIntoView({ behavior: "smooth" });
            setReadyIsInitScroll()
        }
    }, []);

    // React.useEffect(() => {
    //     const handleScroll = () => {
    //         if (scrollContainerRef.current) {
    //             const { scrollTop, clientHeight, scrollHeight } =
    //                 scrollContainerRef.current;

    //          console.log("scrollTop: ", scrollTop);
    //         //  console.log("!loadingMessage: ", !loadingMessage);
    //         //  console.log(
    //         //      "currMessagePage",
    //         //      currMessagePage ,
    //         //  );

    //         //  console.log("infoMessagePagination", infoMessagePagination);

    //             if (
    //                 scrollTop === 0 &&
    //                 !loadingMessage &&
    //                 currMessagePage < infoMessagePagination.totalPage
    //             ) {
    //                 // Scrolled to the top; load older messages
    //                 //  loadOlderMessages();
    //                 handleIncreaseMessagePage();
    //                 // console.log("Scroll");
    //             }
    //         }
    //     };
    //     if (scrollContainerRef.current) {
    //         // console.log("What: ", scrollContainerRef.current);
    //         scrollContainerRef.current.addEventListener("scroll", handleScroll);

    //         return () => {
    //             // Clean up the event listener when the component unmounts
    //             if (scrollContainerRef.current) {
    //                 scrollContainerRef.current.removeEventListener(
    //                     "scroll",
    //                     handleScroll,
    //                 );
    //             }
    //         };
    //     }
    // }, [loadingMessage]);
    return (
        <div
            ref={scrollContainerRef}
            className="mb-[64px] max-h-[600px] overflow-y-scroll"
        >
            {user && messages.map((message, i) => {
                if (i == 0) {
                    return  <div ref={lastMessageElementRef}>
                    <MessageItem
                        user={user}
                        key={message.id}
                        message={message}
                    />
                </div>
                } else if (i === messages.length - 1) {
                    return (
                        <div ref={scrollMessageItemRef}>
                            <MessageItem
                                user={user}
                                key={message.id}
                                message={message}
                            />
                        </div>
                    ); 
                } else {
                    return <div>
                        <MessageItem
                            user={user}
                            key={message.id}
                            message={message}
                        />
                    </div>;
                }
            })}
        </div>
    );
}
