import * as React from "react";
import { BsEmojiSmileFill } from "react-icons/bs";
import Picker, { EmojiClickData } from "emoji-picker-react";
import { IoMdSend } from "react-icons/io";

export interface IChatInputProps {
    handleSendMsg: (value: string) => void;
}

export default function ChatInput(props: IChatInputProps) {
    const { handleSendMsg } = props;

    const [msg, setMsg] = React.useState("");
    const [showEmojiPicker, setShowEmojiPicker] = React.useState(false);
    const handleEmojiPickerhideShow = () => {
        setShowEmojiPicker(!showEmojiPicker);
    };

    const handleEmojiClick = (emoji: EmojiClickData, event: MouseEvent) => {
        setMsg((message) => {

            return message + emoji.emoji;
        });
    };

    const sendChat = (event: any) => {
        event.preventDefault();
        if (msg.length > 0) {
            handleSendMsg(msg);
            setMsg("");
        }
    };

    return (
        <div className="grip-col-95 grid items-center rounded-[15px] bg-slate-500 px-6 py-3 ">
            <div className="button-container">
                <div className="emoji">
                    <BsEmojiSmileFill onClick={handleEmojiPickerhideShow} />
                    {showEmojiPicker && (
                        <Picker onEmojiClick={handleEmojiClick} />
                    )}
                </div>
            </div>
            <form
                className="input-container"
                onSubmit={(event) => sendChat(event)}
            >
                <input
                    type="text"
                    placeholder="Type message"
                    onChange={(e) => setMsg(e.target.value)}
                    value={msg}
                    maxLength={200}
                    className="focus:ring-0"
                />
                <button type="submit">
                    <IoMdSend />
                </button>
            </form>
        </div>
    );
}
