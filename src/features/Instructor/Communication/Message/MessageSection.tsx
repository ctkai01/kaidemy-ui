import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import { Channel, Message, PaginationInfo } from "../../../../models";
import { akaName } from "../../../../utils";
import { MESS_MODE } from "../CommunicationMessage";
import { customAvatarTheme } from "../QA/QuestionItem";
import ChatInput from "./ChatInput";
import CreateNewMessageSection from "./CreateNewMessageSection";
import MessageList from "./MessageList";
import _ from "lodash";

export interface IMessageSectionProps {
    modeMessage: number;
    messages: Message[];
    loadingMessage: boolean;
    currMessagePage: number;
    currentChannel: Channel;
    infoMessagePagination: PaginationInfo;
    scrollContainerRef: React.RefObject<HTMLDivElement>;

    handleSendMsg: (value: string) => void;
    handleSetNothingMode: () => void;
    handleIncreaseMessagePage: () => void;
    lastMessageElementRef: (node: any) => void;
    setReadyIsInitScroll: () => void;
}

const customAvatarTitleMessageTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-14 h-14",
            },
            initials: {
                text: "text-white",
                base: "inline-flex overflow-hidden relative justify-center items-center bg-black dark:bg-gray-600",
            },
        },
    },
};

export default function MessageSection(props: IMessageSectionProps) {
    const {
        modeMessage,
        loadingMessage,
        messages,
        infoMessagePagination,
        currMessagePage,
        currentChannel,
        scrollContainerRef,
        handleSendMsg,
        handleSetNothingMode,
        handleIncreaseMessagePage,
        lastMessageElementRef,
        setReadyIsInitScroll,
    } = props;

    const sortedMessages = _.orderBy(
        messages,
        [(message) => new Date(message.createdAt).getTime()],
        ["asc"],
    );
    return (
        <>
            {modeMessage == MESS_MODE.NORMAL && (
                <>
                    <div className="relative flex items-center gap-2 border-b border-primary-hover-gray p-2">
                        <div className="w-[100px]">
                            <Flowbite
                                theme={{ theme: customAvatarTitleMessageTheme }}
                            >
                                {currentChannel.user.avatar ? (
                                    <Avatar
                                        img={currentChannel.user.avatar}
                                        alt="avatar"
                                        rounded
                                        size="md"
                                    />
                                ) : (
                                    <Avatar
                                        placeholderInitials={akaName(
                                            currentChannel.user.name,
                                        )}
                                        rounded
                                        size="md"
                                    />
                                )}
                            </Flowbite>
                        </div>
                        <div className="flex-1 overflow-hidden">
                            <Link
                                to={`/user/${currentChannel.user.id}`}
                                className="max-w-[800px] overflow-hidden text-ellipsis whitespace-nowrap font-bold text-cyan-600"
                            >
                                {currentChannel.user.name}
                            </Link>
                        </div>
                    </div>
                    <MessageList
                        scrollContainerRef={scrollContainerRef}
                        setReadyIsInitScroll={setReadyIsInitScroll}
                        loadingMessage={loadingMessage}
                        infoMessagePagination={infoMessagePagination}
                        messages={sortedMessages}
                        currMessagePage={currMessagePage}
                        lastMessageElementRef={lastMessageElementRef}
                        handleIncreaseMessagePage={handleIncreaseMessagePage}
                    />
                    <div className="absolute bottom-0 left-0 w-full">
                        <ChatInput handleSendMsg={handleSendMsg} />
                    </div>
                </>
            )}
            {messages.length === 0 && (
                <div className="flex h-[600px] items-center justify-center font-bold">
                    Không có tin nhắn nào
                </div>
            )}

            {/* {modeMessage == MESS_MODE.NOTHING && (
                <div className="flex h-full w-full items-center justify-center font-bold">
                    Chọn một chuỗi tin nhắn để đọc nó ở đây
                </div>
            )} */}
        </>
    );
}
