import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import { Message, User } from "../../../../models";
import { akaName, formatDateFull, formatDistanceToNowTime } from "../../../../utils";
import { customAvatarTheme } from "../QA/QuestionItem";

export interface IMessageItemProps {
    message: Message;
    user: User
}

const customAvatarMessageTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-7 h-7",
            },
            initials: {
                text: "text-[12px] text-white",
                base: "inline-flex overflow-hidden relative justify-center items-center bg-black dark:bg-gray-600",
            },
        },
    },
};

export default function MessageItem(props: IMessageItemProps) {
    const { message, user } = props;

    return (
        <div className=" px-5 py-4">
            <div className="mb-[10px] px-5 py-4 text-center text-xs">
                {formatDateFull(message.createdAt)}
            </div>
            <div
                className={`mb-4 flex ${
                    message.fromUser.id === user.id && "flex-row-reverse"
                } gap-2`}
            >
                <div className="flex gap-4">
                    <div className="flex items-end">
                        {!(message.fromUser.id === user.id) && (
                            <Flowbite
                                theme={{ theme: customAvatarMessageTheme }}
                            >
                                {message.fromUser.avatar ? (
                                    <Avatar
                                        img={message.fromUser.avatar}
                                        alt="avatar"
                                        rounded
                                        size="md"
                                    />
                                ) : (
                                    <Avatar
                                        placeholderInitials={akaName(
                                            message.fromUser.name,
                                        )}
                                        rounded
                                        size="md"
                                    />
                                )}
                            </Flowbite>
                        )}
                    </div>

                    <div
                        className={`${
                            message.fromUser.id === user.id && "text-right"
                        } flex justify-between`}
                    >
                        <div className="rounded-[18px] bg-[#0084ff] px-3 py-2 text-white">
                            {message.text}
                        </div>

                        {/* <div className="text-sm text-primary-gray">
                            {formatDistanceToNowTime(message.created_at)}
                        </div> */}
                    </div>
                </div>
                {/* <div>
                    <div
                        className={`mb-6 flex ${
                            message.message.fromSelf && "flex-row-reverse"
                        } gap-4  text-sm`}
                    >
                        <div className="text-primary-gray">
                            {formatDistanceToNowTime(message.created_at)}
                        </div>
                    </div>
                    <div></div>
                </div> */}
            </div>
            {/* <div
                className={`${
                    message.message.fromSelf && "text-right"
                } flex justify-between`}
            >
                <div className="rounded-[18px] bg-[#0084ff] py-2 px-3 text-white">
                    {message.message.text}
                </div>

                <div className="text-sm text-primary-gray">
                    {formatDistanceToNowTime(message.created_at)}
                </div>
            </div> */}
        </div>
    );
}
