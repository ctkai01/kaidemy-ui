import {
    Avatar,
    CustomFlowbiteTheme,
    Flowbite,
    Spinner,
    TextInput,
} from "flowbite-react";
import { FaSearch } from "react-icons/fa";
import { Channel, Message, PaginationInfo, User } from "../../../../models";
import { akaName } from "../../../../utils";
import { MESS_MODE } from "../CommunicationMessage";
import { customAvatarTheme } from "../QA/QuestionItem";
import ChatInput from "./ChatInput";
import CreateNewMessageSection from "./CreateNewMessageSection";
import MessageList from "./MessageList";
import MessageSection from "./MessageSection";
import StudentList from "./StudentList";
import WelcomeMessage from "./WelcomeMessage";

export interface IMessageBoxProps {
    modeMessage: number;
    channels: Channel[];
    messages: Message[];
    user?: User;
    loadingChannel: boolean;
    currentChannel: Channel | null;
    loadingMessage: boolean;
    infoMessagePagination: PaginationInfo;
    currMessagePage: number;
    handleSendMsgCustom: (msg: string, user: User) => void;
    handleIncreaseMessagePage: () => void;
    handleSendMsg: (value: string) => void;
    handleSetNothingMode: () => void;
    handleFocusChannel: (channel: Channel) => void;
    handleSearchChannel: (value: string) => void;
    lastMessageElementRef: (node: any) => void;
    setReadyIsInitScroll: () => void;
    scrollContainerRef: React.RefObject<HTMLDivElement>;
}

const customInputTheme: CustomFlowbiteTheme = {
    textInput: {
        field: {
            input: {
                base: "overflow-hidden block w-full border disabled:cursor-not-allowed disabled:opacity-50",
                withAddon: {
                    on: "",
                    off: "",
                },
            },
        },
    },
};

export default function MessageBox(props: IMessageBoxProps) {
    const {
        loadingMessage,
        infoMessagePagination,
        user,
        channels,
        modeMessage,
        currentChannel,
        messages,
        currMessagePage,
        loadingChannel,
        scrollContainerRef,
        handleSetNothingMode,
        handleFocusChannel,
        handleSendMsg,
        handleSendMsgCustom,
        handleIncreaseMessagePage,
        handleSearchChannel,
        lastMessageElementRef,
        setReadyIsInitScroll,
    } = props;
    currentChannel && messages.length !== 0;


    return (
        <div className="flex border border-primary-hover-gray">
            <div className="flex-1  overflow-hidden  border-r border-primary-hover-gray">
                <Flowbite theme={{ theme: customInputTheme }}>
                    <TextInput
                        type="text"
                        rightIcon={FaSearch}
                        placeholder="Search"
                        required
                        className="m-1"
                        onChange={(e) => handleSearchChannel(e.target.value)}
                        // {...registerForm("search")}
                        //   onChange={handleChangeSearch}
                        //   value={search}
                    />
                </Flowbite>
                {loadingChannel ? (
                    <div className="flex h-[600px] items-center justify-center">
                        <Spinner />
                    </div>
                ) : channels.length !== 0 ? (
                    <StudentList
                        handleFocusChannel={handleFocusChannel}
                        channels={channels}
                        currentChannel={currentChannel}
                    />
                ) : (
                    <div className="mt-10 h-[600px] text-center font-bold">
                        Không có tin nhắn nào.
                    </div>
                )}
            </div>
            <div className="relative shrink grow-2 basis-0 overflow-hidden border-primary-hover-gray">
                {!currentChannel && modeMessage === MESS_MODE.NORMAL ? (
                    <WelcomeMessage user={user} />
                ) : loadingMessage ? (
                    <div className="flex h-[600px] items-center justify-center">
                        <Spinner />
                    </div>
                ) : currentChannel && messages.length !== 0 ? (
                    <>
                        <MessageSection
                            scrollContainerRef={scrollContainerRef}
                            messages={messages}
                            currentChannel={currentChannel}
                            currMessagePage={currMessagePage}
                            handleSetNothingMode={handleSetNothingMode}
                            modeMessage={modeMessage}
                            handleSendMsg={handleSendMsg}
                            handleIncreaseMessagePage={
                                handleIncreaseMessagePage
                            }
                            setReadyIsInitScroll={setReadyIsInitScroll}
                            lastMessageElementRef={lastMessageElementRef}
                            loadingMessage={loadingMessage}
                            infoMessagePagination={infoMessagePagination}
                        />
                    </>
                ) : (
                    <>
                        {modeMessage == MESS_MODE.CREATE_NEW && user && (
                            <CreateNewMessageSection
                                user={user}
                                handleSetNothingMode={handleSetNothingMode}
                                handleSendMsgCustom={handleSendMsgCustom}
                            />
                        )}
                    </>
                )}
            </div>
        </div>
    );
}
