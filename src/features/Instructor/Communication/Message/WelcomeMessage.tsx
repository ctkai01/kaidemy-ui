import * as React from 'react';
import Robot from "../../../../assets/images/robot.gif";
import { User } from '../../../../models';
export interface IWelcomeMessageProps {
    user?: User
}

export default function WelcomeMessage (props: IWelcomeMessageProps) {
    const { user  } = props;
    return (
        <div className="flex w-full h-full flex-col items-center justify-center text-primary-black ">
            {/* <img src={Robot} alt="" className="h-[320px]" /> */}
            <h1 className='font-bold text-2xl'>
                Xin chào, <span className="text-[#4e0eff]">{user?.name}!</span>
            </h1>
            <h3>Vui lòng chọn cuộc trò chuyện để bắt đầu nhắn tin.</h3>
        </div>
    );
}
