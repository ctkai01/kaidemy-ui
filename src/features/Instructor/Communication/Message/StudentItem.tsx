import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import * as React from "react";
import { Channel } from "../../../../models";
import { akaName, formatDistanceToNowTime } from "../../../../utils";
import { customAvatarTheme } from "../QA/QuestionItem";

export interface IStudentItemProps {
    channel: Channel;
    isActive: boolean;
    handleFocusChannel: (channel: Channel) => void;
}

const customAvatarChannelMessageTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-14 h-14",
            },
            initials: {
                text: "text-white",
                base: "inline-flex overflow-hidden relative justify-center items-center bg-black dark:bg-gray-600",
            },
        },
    },
};

export default function StudentItem(props: IStudentItemProps) {
    const { channel, isActive, handleFocusChannel } = props;
    return (
        <div
            onClick={() => {
                handleFocusChannel(channel);
            }}
            className={`
          ${
              isActive && "bg-primary-hover-gray"
          } flex cursor-pointer gap-4 border-b p-4 first:border-t  hover:bg-primary-hover-gray`}
        >
            <div>
                <Flowbite theme={{ theme: customAvatarChannelMessageTheme }}>
                    {channel.user.avatar ? (
                        <Avatar
                            img={channel.user.avatar}
                            alt="avatar"
                            rounded
                            size="md"
                        />
                    ) : (
                        <Avatar
                            placeholderInitials={akaName(channel.user.name)}
                            rounded
                            size="md"
                        />
                    )}
                </Flowbite>
            </div>
            <div className="flex-1">
                <div>
                    <div className="w-full max-w-[350px] truncate font-bold text-cyan-600">
                        {channel.user.name}
                    </div>
                </div>
                {channel.latestMessage && (
                    <div className="mt-1 flex justify-between text-sm font-bold text-primary-gray">
                        <h2 className="max-w-[250px] truncate">
                            {channel.latestMessage.text}
                        </h2>
                        <div>
                            {formatDistanceToNowTime(
                                channel.latestMessage.createdAt,
                            )}
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
