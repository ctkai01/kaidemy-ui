import {
    Avatar,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Label,
    Spinner,
    Textarea,
    TextInput,
} from "flowbite-react";
import * as React from "react";
import { IoClose } from "react-icons/io5";
import { PaginationInfo, User } from "../../../../models";
import { UserServiceApi } from "../../../../services/api/userServiceApi";
import { akaName } from "../../../../utils";
import UserSearchItem from "./UserSearchItem";

export interface ICreateNewMessageSectionProps {
    user: User;
    handleSetNothingMode: () => void;
    handleSendMsgCustom: (msg: string, user: User) => void;
}

const customInputAvatarTheme: CustomFlowbiteTheme = {
    textInput: {
        field: {
            input: {
                sizes: {
                    md: "p-2.5 pl-[60px] pr-12 text-sm",
                },
            },
        },
    },
};

const customAvatarTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                md: "w-8 h-8",
            },
            initials: {
                text: "font-bold text-white dark:text-gray-300",
                base: "inline-flex overflow-hidden relative justify-center items-center  bg-primary-black dark:bg-gray-600",
            },
        },
    },
};

const MESSAGE_PICK_MODE = {
    NO_PICK: 1,
    PICKED: 2,
};
const USER_PAGE_SIZE = 4;
export default function CreateNewMessageSection(
    props: ICreateNewMessageSectionProps,
) {
    const { user, handleSetNothingMode, handleSendMsgCustom } = props;

    const [textSearch, setTextSearch] = React.useState("");
    const [textMessage, setTextMessage] = React.useState("");
    const [messagePickMode, setMessagePickMode] = React.useState(
        MESSAGE_PICK_MODE.NO_PICK,
    );
    const searchRef = React.useRef<HTMLDivElement>(null);
    const [infoUserSearchPagination, setInfoUserSearchPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [currUserPage, setCurrUserPage] = React.useState(1);
    const [loadingUsers, setLoadingUsers] = React.useState(false);
    const [usersSearch, setUsersSearch] = React.useState<User[]>([]);
    const [userPick, setUserPick] = React.useState<User | null>(null);
    const textInputRef = React.useRef<HTMLInputElement>(null);

    React.useEffect(() => {
        if (
            messagePickMode === MESSAGE_PICK_MODE.NO_PICK &&
            textSearch !== ""
        ) {
            if (textInputRef.current) {
                textInputRef.current.focus();
            }
        }
    }, [messagePickMode]);

    React.useEffect(() => {
        const fetchUserSearch = async () => {
            if (textSearch) {
                try {
                    setLoadingUsers(true);
                    const userData = await UserServiceApi.searchStudent(
                        textSearch,
                        USER_PAGE_SIZE,
                        currUserPage,
                        user.role,
                    );

                    setInfoUserSearchPagination({
                        totalPage: userData.meta.pageCount,
                        totalItem: userData.meta.itemCount,
                        size: userData.meta.size,
                    });

                    if (!userData.item) {
                        setUsersSearch([]);
                    } else {
                        setUsersSearch((pre) => [...pre, ...userData.item]);
                    }
                    setLoadingUsers(false);

                    // setUsersSearch
                } catch (e) {
                    console.log(e);
                }
            }
        };
        fetchUserSearch();
    }, [textSearch, currUserPage]);
    const handlePickUser = (user: User) => {
        setMessagePickMode(MESSAGE_PICK_MODE.PICKED);
        setTextSearch(user.name);
        setUserPick(user);
    };
    const onScrollUserSearch = () => {
        if (searchRef.current) {
            const { scrollTop, scrollHeight, clientHeight } = searchRef.current;
            if (scrollTop + clientHeight === scrollHeight) {
                if (currUserPage < infoUserSearchPagination.totalPage) {
                    setCurrUserPage((current) => current + 1);
                }
            }
        }
    };

    const handleSubmit = () => {
        if (userPick) {
            handleSendMsgCustom(textMessage, userPick);
            setTextMessage("");
            setTextSearch("");
            setMessagePickMode(MESSAGE_PICK_MODE.NO_PICK);
            setUserPick(null);
        }
    };
    return (
        <div className="max-w-[600px] px-5 py-6">
            <h3 className="mb-10 text-lg font-bold">New message</h3>

            <div className="relative mb-5">
                <div className="mb-2 block">
                    <Label
                        className="font-bold"
                        htmlFor="to"
                        value="Gửi tới:"
                    />
                </div>
                {messagePickMode === MESSAGE_PICK_MODE.NO_PICK && (
                    <>
                        <TextInput
                            // className="w-[400px]"
                            onChange={(e) => {
                                setTextSearch(e.target.value);
                                // if (messagePickMode === MESSAGE_PICK_MODE.PICKED) {
                                //     setMessagePickMode(MESSAGE_PICK_MODE.NO_PICK);
                                // }
                                setUsersSearch([]);
                                // if (e.target.value === "") {
                                setInfoUserSearchPagination({
                                    totalItem: 0,
                                    totalPage: 0,
                                    size: 0,
                                });
                                setCurrUserPage(1);
                                setLoadingUsers(false);
                                // }
                            }}
                            ref={textInputRef}
                            id="to"
                            type="text"
                            value={textSearch}
                            required
                            placeholder="Nhập tên học viên muốn gửi tin nhắn"
                        />
                        {textSearch && (
                            <div
                                ref={searchRef}
                                onScroll={onScrollUserSearch}
                                className="absolute bottom-0 left-0 z-20 max-h-[200px] w-full translate-y-full overflow-y-scroll border bg-white shadow-md"
                            >
                                {usersSearch.map((user) => (
                                    <>
                                        <UserSearchItem
                                            user={user}
                                            key={user.id}
                                            handlePickUser={handlePickUser}
                                        />
                                    </>
                                ))}

                                {loadingUsers && (
                                    <div className="mt-2 text-center">
                                        <Spinner />
                                    </div>
                                )}
                                {usersSearch.length === 0 && (
                                    <div className="flex h-[100px] items-center justify-center font-bold">
                                        Không tìm thấy học viên
                                    </div>
                                )}
                            </div>
                        )}
                    </>
                )}

                {messagePickMode === MESSAGE_PICK_MODE.PICKED && userPick && (
                    <div className="relative flex">
                        <div className="absolute left-[10px] top-1/2 z-10 -translate-y-1/2">
                            <Flowbite theme={{ theme: customAvatarTheme }}>
                                {/* <Avatar
                                    placeholderInitials={akaName("hello sz")}
                                    rounded
                                    size="md"
                                    color="dark"
                                /> */}
                                {userPick.avatar ? (
                                    <Avatar
                                        img={userPick.avatar}
                                        alt="avatar"
                                        rounded
                                        size="md"
                                    />
                                ) : (
                                    <Avatar
                                        placeholderInitials={akaName(
                                            userPick.name,
                                        )}
                                        rounded
                                        color="dark"
                                    />
                                )}
                            </Flowbite>
                        </div>
                        <div
                            onClick={() => {
                                setTextSearch("");
                                setMessagePickMode(MESSAGE_PICK_MODE.NO_PICK);
                                setUserPick(null);
                            }}
                            className="absolute right-0 top-1/2 z-10 flex h-full -translate-y-1/2 cursor-pointer items-center justify-center px-3"
                        >
                            <IoClose />
                        </div>

                        <Flowbite theme={{ theme: customInputAvatarTheme }}>
                            <TextInput
                                // className="w-[400px]"
                                id="to"
                                type="text"
                                className="w-full"
                                onChange={(e) => {
                                    setTextSearch(e.target.value);
                                    if (
                                        messagePickMode ===
                                        MESSAGE_PICK_MODE.PICKED
                                    ) {
                                        setMessagePickMode(
                                            MESSAGE_PICK_MODE.NO_PICK,
                                        );
                                    }
                                }}
                                value={textSearch}
                                required
                                placeholder="Nhập tên học viên muốn gửi tin nhắn"
                            />
                        </Flowbite>
                    </div>
                )}
            </div>
            <div>
                <div className="mb-2 block">
                    <Label
                        className="font-bold"
                        htmlFor="message"
                        value="Tin nhắn:"
                    />
                </div>
                <Textarea
                    // className="w-[400px]"
                    onChange={(e) => setTextMessage(e.target.value)}
                    id="message"
                    value={textMessage}
                    required
                    placeholder="Nhập tin nhắn"
                />
            </div>
            <div className="mt-6 flex justify-end gap-4">
                <Button
                    onClick={() => handleSetNothingMode()}
                    color="dark"
                    className=" font-bold"
                >
                    Cancel
                </Button>
                <Button
                    onClick={() => handleSubmit()}
                    disabled={
                        messagePickMode !== MESSAGE_PICK_MODE.PICKED ||
                        textMessage === ""
                            ? true
                            : false
                    }
                    className=" font-bold"
                >
                    Gửi
                </Button>
            </div>
        </div>
    );
}
