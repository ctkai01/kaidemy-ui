import {
    CustomFlowbiteTheme,
    Flowbite,
    Sidebar,
    Tabs,
    TabsComponent,
    TabsRef,
} from "flowbite-react";
import * as React from "react";
import { MdBarChart, MdOutlineMessage } from "react-icons/md";
import { PiVideoBold } from "react-icons/pi";
import {
    Link,
    Outlet,
    useLocation,
    useNavigate,
    useParams,
} from "react-router-dom";
import {
    INSTRUCTOR_COMMUNICATION_QA_PATH,
    INSTRUCTOR_COMMUNICATION_MESSAGE_PATH,
} from "../../../constants";
import { PERFORMANCE_TABS } from "../../../constants/instructor";

export interface ICommunicationGeneralProps {}

const customTheme: CustomFlowbiteTheme = {
    sidebar: {
        root: {
            base: "h-full",
            collapsed: {
                on: "w-16",
                off: "w-[230px]",
            },
            inner: "h-full overflow-y-auto overflow-x-hidden rounded bg-gray-50 py-8 dark:bg-gray-800",
        },
        item: {
            base: " p-4 flex  text-base font-bold ",
            // base: " p-4 flex hover:bg-[#3e4143] hover:text-white text-base font-bold  ml-1 text-gray-500",
            // active: "border-l-4 border-cyan-600 text-white  ml-0",
            // content: {
            //     base: "ssdsds",
            // },
        },
    },
};

export default function CommunicationGeneral(
    props: ICommunicationGeneralProps,
) {
    // PERFORMANCE_TABS;
    // const tabsRef = React.useRef<TabsRef>(null);
    // let { type } = useParams();
    // const [activeTab, setActiveTab] = React.useState(0);
    // const [loading, setLoading] = React.useState(true);
    const location = useLocation();

    return (
        <div className="flex">
            <div className="lef-0 fixed top-0 z-[100] flex h-screen">
                <Flowbite theme={{ theme: customTheme }}>
                    <Sidebar
                        className=""
                        aria-label="Sidebar with logo branding example"
                    >
                        <Sidebar.Items>
                            <Sidebar.ItemGroup>
                                <Link
                                    to={INSTRUCTOR_COMMUNICATION_QA_PATH}
                                    className=""
                                >
                                    <div
                                        className={`${
                                            location.pathname ===
                                                INSTRUCTOR_COMMUNICATION_QA_PATH &&
                                            "font-bold"
                                        } flex items-center py-2 pl-8 pr-4 hover:bg-primary-hover-gray `}
                                    >
                                        <div className="">Q&A</div>
                                    </div>
                                </Link>
                                {/* <Link
                                    to={INSTRUCTOR_COMMUNICATION_MESSAGE_PATH}
                                    className=""
                                >
                                    <div
                                        className={`${
                                            location.pathname ===
                                                INSTRUCTOR_COMMUNICATION_MESSAGE_PATH &&
                                            "font-bold"
                                        } flex items-center py-2 pl-8 pr-4 hover:bg-primary-hover-gray `}
                                    >
                                        <div className="">Message</div>
                                    </div>
                                </Link> */}
                            </Sidebar.ItemGroup>
                        </Sidebar.Items>
                    </Sidebar>
                </Flowbite>
            </div>
            <div className="ml-[230px] flex-1 pt-18">
                <Outlet />
            </div>
        </div>
    );
}
