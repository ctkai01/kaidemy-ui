import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Label, Modal, Select, TextInput } from "flowbite-react";
import React from "react";
import { useForm } from "react-hook-form";
import { Category, CreateCourse } from "../../models";
import { schemeCreateCourse } from "../../validators/course";

export interface ICreateCourseModalProps {
    openCreateCourseModal: boolean;
    categories: Category[];
    handleCommandCourseModal: (command: boolean) => void;
    handleCreateNewCourse: (data: CreateCourse) => void;
}

export default function CreateCourseModal(props: ICreateCourseModalProps) {
    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
        setError,
        clearErrors,
    } = useForm<CreateCourse>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateCourse),
    });

    // const { categories, handleCreateNewCourse } = props;

    const [subCategories, setSubCategories] = React.useState<Category[]>([]);

    const {
        categories,
        openCreateCourseModal,
        handleCommandCourseModal,
        handleCreateNewCourse,
    } = props;

    const handleCreateCourse = async (data: CreateCourse) => {
        handleCreateNewCourse(data);
    };

    const handleChangeCategory = async (
        e: React.ChangeEvent<HTMLSelectElement>,
    ) => {
        console.log(e.target.value);

        const value = e.target.value;
        setValue("category_id", value);
        if (value === "-1") {
            setError("category_id", {
                message: "Category is require",
            });
        } else {
            try {
                const category = categories.find(
                    (category) => category.id === +value,
                );

                clearErrors("category_id");
                if (category) {
                    if (!category.children) {
                        setSubCategories([]);
                    } else {
                        setSubCategories(category.children);
                    }
                } else {
                    setSubCategories([]);
                }
            } catch (e) {
                console.log(e);
            }
        }
    };

    return (
        <Modal
            show={openCreateCourseModal}
            onClose={() => {
                handleCommandCourseModal(false);
                reset();
            }}
        >
            <Modal.Header>Create course</Modal.Header>
            <Modal.Body>
                <form onSubmit={handleSubmit(handleCreateCourse)}>
                    <div className="">
                        <div className="">
                            <div className="mb-2 block">
                                <Label htmlFor="title" value="Tiêu đề" />
                            </div>
                            <TextInput
                                id="title"
                                type="text"
                                color={errors.title ? "failure" : ""}
                                placeholder="eg. Learn Photoshop CS6 from Scratch"
                                {...register("title")}
                                helperText={
                                    <>
                                        {errors.title ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {errors.title.message}
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            />
                        </div>
                        <div className="mt-6">
                            <div className="mb-2 block">
                                <Label htmlFor="category" value="Danh mục" />
                            </div>
                            <Select
                                id="category"
                                required
                                onChange={handleChangeCategory}
                                color={errors.category_id ? "failure" : ""}
                                // {...register("category_id")}
                                helperText={
                                    <>
                                        {errors.category_id ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {errors.category_id.message}
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            >
                                <option value={-1}>-- Select Category</option>

                                {categories.map((category) => (
                                    <option
                                        key={category.id}
                                        value={category.id}
                                    >
                                        {category.name}
                                    </option>
                                ))}
                            </Select>
                        </div>
                        <div className="mt-6">
                            <div className="mb-2 block">
                                <Label
                                    htmlFor="sub_category"
                                    value="Subcategory"
                                />
                            </div>
                            <Select
                                id="sub_category"
                                required
                                color={errors.sub_category_id ? "failure" : ""}
                                {...register("sub_category_id")}
                                helperText={
                                    <>
                                        {errors.sub_category_id ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {
                                                        errors.sub_category_id
                                                            .message
                                                    }
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            >
                                <option value={-1}>-- Select Subcategory</option>
                                {subCategories.map((category) => (
                                    <option
                                        key={category.id}
                                        value={category.id}
                                    >
                                        {category.name}
                                    </option>
                                ))}
                            </Select>
                        </div>
                        <div className="mt-16 flex w-full justify-end">
                            <Button type="submit">Create course</Button>
                        </div>
                    </div>
                </form>
            </Modal.Body>
            {/* <Modal.Footer> */}
            {/* <Button onClick={() => handleCommandCourseModal(false)}>
                  I accept
              </Button>
              <Button
                  color="gray"
                  onClick={() => handleCommandCourseModal(false)}
              >
                  Decline
              </Button> */}
            {/* </Modal.Footer> */}
        </Modal>
    );
}
