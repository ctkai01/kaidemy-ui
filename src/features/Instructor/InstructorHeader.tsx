import { Avatar } from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import DropDownHeader from "../../components/ui/core/Header/DropDownHeader";
import { useAppSelector } from "../../hooks/redux-hook";
import { selectUserAuth } from "../../services/state/redux/authSlide";
import { akaName } from "../../utils";

export interface IInstructorHeaderProps {}

export default function InstructorHeader(props: IInstructorHeaderProps) {
    const user = useAppSelector(selectUserAuth);

    return (
        <div className="relative flex justify-end h-18  px-6">
            {/* <div className="flex-1"></div> */}
            <div className="group relative z-[100] my-3 h-12 max-h-18 hover:text-cyan-500">
                <Link
                    to="/"
                    className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31] hover:text-cyan-500"
                >
                    Student
                </Link>
            </div>
            <div className="group relative z-[100] my-3 h-12 max-h-18">
                <Link
                    to="/"
                    className="flex h-full items-center justify-center px-3 text-sm text-[#2D2F31]"
                >
                    {user?.avatar ? (
                        <Avatar
                            img={user.avatar}
                            alt="avatar"
                            rounded
                            size="sm"
                        />
                    ) : (
                        <Avatar
                            placeholderInitials={akaName(user ? user.name : "")}
                            rounded
                            size="sm"
                        />
                    )}
                </Link>
                <DropDownHeader />
            </div>
        </div>
    );
}
