import * as React from "react";
import { Controller, useForm } from "react-hook-form";
import {
    FilterStudentAuthor,
    PaginationInfo,
    StudentAuthor,
} from "../../../models";
import { SelectData } from "./PerformanceReviews";
import Select from "react-select";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import StudentItem from "./Students/StudentItem";
import { Pagination, Spinner } from "flowbite-react";
export interface IPerformanceStudentsProps {}
export interface FilterStudent {
    course_id: SelectData;
}

const COURSE_SIZE = 10;
const USER_SIZE = 4;
// FilterStudentAuthor
export default function PerformanceStudents(props: IPerformanceStudentsProps) {
    const [infoCoursePagination, setCourseInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [infoUserPagination, setUserInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });

    const [currCoursePage, setCurrCoursePage] = React.useState(1);
    const [selectCoursesData, setSelectCoursesData] = React.useState<
        SelectData[]
    >([
        {
            label: "All courses",
            value: "-1",
        },
    ]);
    const [currStudentsPage, setCurrStudentsPage] = React.useState(1);
    const [studentsAuthor, setStudentsAuthor] = React.useState<StudentAuthor[]>(
        [],
    );
    const [loading, setLoading] = React.useState(false);
    const [totalStudentUnique, setTotalStudentUnique] = React.useState(0);

    const [loadingCourse, setLoadingCourse] = React.useState(false);
    const {
        register,
        reset,
        control,
        watch,
        formState: { isDirty },
    } = useForm<FilterStudent>({
        mode: "onChange",
    });

    React.useLayoutEffect(() => {
        const fetchCourseData = async () => {
            try {
                setLoadingCourse(true);
                const data = await CourseServiceApi.getCoursesAuthor(
                    COURSE_SIZE,
                    currCoursePage,
                );
                setCourseInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data.item) {
                    setSelectCoursesData([]);
                } else {
                    const newData: SelectData[] = data.item.map(
                        (item: any) => {
                            return {
                                label: item.title,
                                value: `${item.id}`,
                            };
                        },
                    );
                    setSelectCoursesData((data) => [...data, ...newData]);
                }
                setLoadingCourse(false);

            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseData();
    }, [currCoursePage]);

    React.useLayoutEffect(() => {
        const fetchUserData = async (filter: FilterStudentAuthor) => {
            try {
                setLoading(true);
                const data = await CourseServiceApi.getUsersEnrollAuthor(
                    USER_SIZE,
                    currStudentsPage,
                    filter,
                );
                setUserInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data?.item) {
                    setStudentsAuthor([]);
                } else {
                    setStudentsAuthor([...data.item]);
                }
                //  total_unique;
                setLoading(false);
                setTotalStudentUnique(data.uniqueStudent);
            } catch (e) {
                console.log(e);
            }
        };

        const subscription = watch((value) => {
            const filter: FilterStudentAuthor = {
                course_id:
                    value.course_id && value.course_id.value
                        ? value.course_id.value
                        : null,
            };
            fetchUserData(filter);
        });
        const filter: FilterStudentAuthor = {
            course_id: watch("course_id") ? watch("course_id").value : null,
        };
        fetchUserData(filter);
        return () => subscription.unsubscribe();
    }, [watch, currStudentsPage]);

    const onPageChange = (page: number) => {
        setCurrStudentsPage(page);
    };
    return (
        <div className="px-16">
            <div className="mb-8 text-[32px] font-bold text-primary-black">
                Students
            </div>
            <div className="flex gap-6 pb-[100px]">
                <div className="w-[300px]">
                    {loadingCourse && selectCoursesData.length === 0 ? (
                        <div className="animate-pulse">
                            <div className="bg-slate-300">
                                <Controller
                                    name="course_id"
                                    control={control}
                                    render={({ field }) => (
                                        <Select
                                            {...field}
                                            defaultValue={selectCoursesData[0]}
                                            isClearable={true}
                                            isSearchable={true}
                                            maxMenuHeight={200}
                                            options={selectCoursesData}
                                            onMenuScrollToBottom={() => {}}
                                        />
                                    )}
                                />
                            </div>
                        </div>
                    ) : (
                        <Controller
                            name="course_id"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    defaultValue={selectCoursesData[0]}
                                    isClearable={true}
                                    isSearchable={true}
                                    maxMenuHeight={200}
                                    options={selectCoursesData}
                                    onMenuScrollToBottom={() => {
                                        if (
                                            currCoursePage <
                                            infoCoursePagination.totalPage
                                        ) {
                                            setCurrCoursePage(
                                                currCoursePage + 1,
                                            );
                                        }
                                    }}
                                />
                            )}
                        />
                    )}
                    <div className="mt-10 text-center font-bold">
                        {totalStudentUnique} students
                    </div>
                </div>
                <div className="flex-1 border border-primary-hover-gray">
                    <div className="flex flex-col gap-4">
                        {studentsAuthor.map((student) => (
                            <StudentItem key={student.id} student={student} />
                        ))}
                    </div>
                    {loading ? (
                        <div className="mt-10 text-center">
                            {" "}
                            <Spinner />
                        </div>
                    ) : infoUserPagination.totalPage !== 0 ? (
                        <div className="mb-10 mt-10 flex justify-center">
                            <Pagination
                                currentPage={currStudentsPage}
                                totalPages={infoUserPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                    ) : (
                        <div className="mt-10 text-center font-bold">
                           No student found
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
