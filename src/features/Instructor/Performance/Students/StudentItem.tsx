import { Avatar, Button, Tooltip } from 'flowbite-react';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { StudentAuthor } from '../../../../models';
import { akaName, formatMonthLongYear } from '../../../../utils';

export interface IStudentItemProps {
    student: StudentAuthor
}

export default function StudentItem (props: IStudentItemProps) {
  const { student } = props
    return (
        <div className="flex  items-center gap-5 border-t border-primary-hover-gray py-8 pl-8 pr-7 first:border-t-0">
            <div>
                {student.avatar ? (
                    <Avatar
                        img={student.avatar}
                        alt="avatar"
                        rounded
                        size="sm"
                    />
                ) : (
                    <Avatar
                        placeholderInitials={akaName(student.name)}
                        rounded
                        size="lg"
                    />
                )}
            </div>
            <div className="flex flex-col justify-center">
                <h2 className="font-bold text-primary-black">{student.name}</h2>
                <h2 className=" text-cyan-500">
                    {/* Đã đăng ký vào  */}
                    Enrolled on {formatMonthLongYear(student.createdAt)}
                </h2>
            </div>
            <div className="flex flex-1 flex-col items-center  justify-center">
                <div>
                    <img
                        className="w-[200px]"
                        src={
                            student.course.image
                                ? student.course.image
                                : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                        }
                    />
                </div>
                <Tooltip content={student.course.title} placement="bottom">
                    <Link to={`/course/${student.course.id}`}>
                        <h3 className="mt-3 w-[440px] flex-1 overflow-hidden text-ellipsis whitespace-nowrap text-center font-bold text-cyan-600">
                            {student.course.title}
                        </h3>
                    </Link>
                </Tooltip>
            </div>
            <Link to={`/user/${student.id}`}>
                <Button className="h-fit font-bold">
                    Go to personal page
                </Button>
            </Link>
        </div>
    );
}
