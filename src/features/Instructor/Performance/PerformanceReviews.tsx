import {
    Checkbox,
    Label,
    Pagination,
    Select as SelectFlowbite,
    Spinner,
} from "flowbite-react";
import * as React from "react";
import {
    FilterReviewAuthor,
    PaginationInfo,
    ReviewAuthor,
} from "../../../models";
import ReviewReportItem from "./Reviews/ReviewReportItem";
import Select from "react-select";
import { Controller, useForm } from "react-hook-form";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
export interface IPerformanceReviewsProps {}

export interface FilterReviews {
    sort: string;
    course_id: SelectData;
    reply: string[] | false;
    rating: string[] | false;
}

export interface SelectData {
    label: string;
    value: string;
}

const COURSE_SIZE = 10;
const REVIEW_SIZE = 6;
export default function PerformanceReviews(props: IPerformanceReviewsProps) {
    const [infoCoursePagination, setCourseInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [infoReviewPagination, setReviewInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [currCoursePage, setCurrCoursePage] = React.useState(1);
    const [currReviewPage, setCurrReviewPage] = React.useState(1);
    const [selectCoursesData, setSelectCoursesData] = React.useState<
        SelectData[]
    >([
        {
            label: "All courses",
            value: "-1",
        },
    ]);
    const [reviewsAuthor, setReviewsAuthor] = React.useState<ReviewAuthor[]>(
        [],
    );

    const [loadingCourse, setLoadingCourse] = React.useState(false);
    const [loadingReview, setLoadingReview] = React.useState(false);

    const {
        register,
        reset,
        control,
        watch,
        formState: { isDirty },
    } = useForm<FilterReviews>({
        mode: "onChange",
    });
    React.useLayoutEffect(() => {
        const fetchCourseData = async () => {
            try {
                setLoadingCourse(true);
                const data = await CourseServiceApi.getCoursesAuthor(
                    COURSE_SIZE,
                    currCoursePage,
                );
                setCourseInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data.item) {
                    setSelectCoursesData([]);
                } else {
                    const newData: SelectData[] = data.item.map((item: any) => {
                        return {
                            label: item.title,
                            value: `${item.id}`,
                        };
                    });
                    setSelectCoursesData((data) => [...data, ...newData]);
                }
                setLoadingCourse(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseData();
    }, [currCoursePage]);

    React.useLayoutEffect(() => {
        const fetchReviewData = async (filter: FilterReviewAuthor) => {
            try {
                setLoadingReview(true);
                const data = await CourseServiceApi.getReviewsAuthor(
                    REVIEW_SIZE,
                    currReviewPage,
                    filter,
                );
                setReviewInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });

                if (!data.item) {
                    setReviewsAuthor([]);
                } else {
                    setReviewsAuthor((reviewsAuthor) => [
                        // ...reviewsAuthor,
                        ...data.item,
                    ]);
                }
                setLoadingReview(false);
            } catch (e) {
                console.log(e);
            }
        };

        const subscription = watch((value) => {
            let filterReply: string[];
            if (value.reply) {
                const onlyUndefined = value.reply.every(
                    (item) => item === undefined,
                );

                if (onlyUndefined) {
                    filterReply = [];
                } else {
                    filterReply = value.reply as string[];
                }
            } else {
                filterReply = [];
            }

            let filterRating: string[];
            if (value.rating) {
                const onlyUndefined = value.rating.every(
                    (item) => item === undefined,
                );

                if (onlyUndefined) {
                    filterRating = [];
                } else {
                    filterRating = value.rating as string[];
                }
            } else {
                filterRating = [];
            }

            const filter: FilterReviewAuthor = {
                rating: filterRating,
                reply: filterReply,
                course_id:
                    value.course_id && value.course_id.value
                        ? value.course_id.value
                        : null,
                sort: value.sort ? value.sort : null,
            };
            fetchReviewData(filter);
        });
        const filter: FilterReviewAuthor = {
            rating: watch("rating") ? watch("rating") : [],
            reply: watch("reply") ? watch("reply") : [],
            course_id: watch("course_id") ? watch("course_id").value : null,
            sort: watch("sort") ? watch("sort") : null,
        };
        fetchReviewData(filter);
        return () => subscription.unsubscribe();
    }, [watch, currReviewPage]);

    const onPageChange = (page: number) => {
        setCurrReviewPage(page);
    };

    return (
        <div className="px-16">
            <div className="mb-8 text-[32px] font-bold text-primary-black">
                Reviews
            </div>
            <div className="flex gap-6 pb-[100px]">
                <div className="w-[300px]">
                    {loadingCourse && selectCoursesData.length === 0 ? (
                        <div className="animate-pulse">
                            <div className="bg-slate-300">
                                <Controller
                                    name="course_id"
                                    control={control}
                                    render={({ field }) => (
                                        <Select
                                            {...field}
                                            defaultValue={selectCoursesData[0]}
                                            isClearable={true}
                                            isSearchable={true}
                                            maxMenuHeight={200}
                                            options={selectCoursesData}
                                            onMenuScrollToBottom={() => {}}
                                        />
                                    )}
                                />
                            </div>
                        </div>
                    ) : (
                        <Controller
                            name="course_id"
                            control={control}
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    defaultValue={selectCoursesData[0]}
                                    isClearable={true}
                                    isSearchable={true}
                                    maxMenuHeight={200}
                                    options={selectCoursesData}
                                    onMenuScrollToBottom={() => {
                                        if (
                                            currCoursePage <
                                            infoCoursePagination.totalPage
                                        ) {
                                            setCurrCoursePage(
                                                currCoursePage + 1,
                                            );
                                        }
                                    }}
                                />
                            )}
                        />
                    )}

                    <div className="flex flex-col gap-3 border-b border-primary-hover-gray pb-6 pt-4">
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="no_response_reply"
                                id="no-response"
                                {...register("reply")}
                            />
                            <Label htmlFor="no-response">No response</Label>
                        </div>
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="response_reply"
                                id="response"
                                {...register("reply")}
                            />
                            <Label htmlFor="response">Has response</Label>
                        </div>
                    </div>
                    {/* <div className=""></div> */}
                    <div className="flex flex-col gap-3 border-b border-primary-hover-gray pb-6 pt-4">
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="1"
                                {...register("rating")}
                                id="1-star"
                            />
                            <Label htmlFor="1-star">1 star</Label>
                        </div>
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="2"
                                {...register("rating")}
                                id="2-star"
                            />
                            <Label htmlFor="2-star">2 stars</Label>
                        </div>
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="3"
                                {...register("rating")}
                                id="3-star"
                            />
                            <Label htmlFor="3-star">3 stars</Label>
                        </div>
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="4"
                                {...register("rating")}
                                id="4-star"
                            />
                            <Label htmlFor="4-star">4 stars</Label>
                        </div>
                        <div className="flex items-center gap-2">
                            <Checkbox
                                value="5"
                                {...register("rating")}
                                id="5-star"
                            />
                            <Label htmlFor="5-star">5 stars</Label>
                        </div>
                    </div>
                    <div className="flex flex-col gap-3 border-b border-primary-hover-gray pb-6 pt-4">
                        <div className="mb-2 block">
                            <Label htmlFor="sort" value="Sắp xếp theo:" />
                        </div>
                        <SelectFlowbite
                            {...register("sort")}
                            id="sort"
                            required
                        >
                            <option value="newest">Newest to oldest</option>
                        </SelectFlowbite>
                    </div>
                </div>
                <div className="flex-1 border border-primary-hover-gray">
                    <div className="flex flex-col gap-4">
                        {reviewsAuthor.map((review, index) => (
                            <ReviewReportItem key={index} review={review} />
                        ))}
                    </div>
                    {loadingReview ? (
                        <div className="mt-10 text-center">
                            {" "}
                            <Spinner />
                        </div>
                    ) : infoReviewPagination.totalPage > 0 ? (
                        <div className="mb-10 mt-10 flex justify-center">
                            <Pagination
                                currentPage={currReviewPage}
                                totalPages={infoReviewPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                    ) : (
                        <div className="mt-10 text-center font-bold">
                            No reviews found
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
