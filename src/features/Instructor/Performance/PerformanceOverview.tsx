import { Spinner } from "flowbite-react";
import * as React from "react";
import Select from "react-select";
import { OverviewCourse, PaginationInfo } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import OverviewBox from "./Overview/OverviewBox";
import { SelectData } from "./PerformanceReviews";

export interface IPerformanceOverviewProps {}
// ;
const COURSE_SIZE = 10;
export default function PerformanceOverview(props: IPerformanceOverviewProps) {
    const [selectCoursesData, setSelectCoursesData] = React.useState<
        SelectData[]
    >([
        {
            label: "All courses",
            value: "-1",
        },
    ]);
    const [filterCourse, setFilterCourse] = React.useState<SelectData>({
        label: "All courses",
        value: "-1",
    });
    const [currCoursePage, setCurrCoursePage] = React.useState(1);
    const [infoCoursePagination, setCourseInfoPagination] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });

    const [loadingCourse, setLoadingCourse] = React.useState(false);
    const [overviewCourse, setOverviewCourse] =
        React.useState<OverviewCourse | null>(null);
    React.useLayoutEffect(() => {
        const fetchCourseData = async () => {
            try {
                setLoadingCourse(true);
                const data = await CourseServiceApi.getCoursesAuthor(
                    COURSE_SIZE,
                    currCoursePage,
                );
                setCourseInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data?.item) {
                    setSelectCoursesData([]);
                } else {
                    const newData: SelectData[] = data.item.map(
                        (item: any) => {
                            return {
                                label: item.title,
                                value: `${item.id}`,
                            };
                        },
                    );
                    setSelectCoursesData((data) => [...data, ...newData]);
                }
                setLoadingCourse(false);

            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseData();
    }, [currCoursePage]);

    React.useEffect(() => {
        const fetchOverview = async () => {
            try {
                const dataOverview =
                    await CourseServiceApi.getOverviewCourseAuthor(
                        filterCourse.value,
                    );
                setOverviewCourse(dataOverview);
            } catch (e) {
                console.log(e);
            }
        };
        fetchOverview();
    }, [filterCourse]);

    return (
        <div className="px-16">
            <div className="flex">
                <div className="mb-8 text-[32px] font-bold text-primary-black">
                    Overview
                </div>
                {/* <div>s</div> */}
            </div>
            <div className="w-[300px]">
                {loadingCourse && selectCoursesData.length === 0 ? (
                    <div className="animate-pulse">
                        <div className="bg-slate-300">
                            <Select
                                defaultValue={selectCoursesData[0]}
                                isClearable={true}
                                isSearchable={true}
                                maxMenuHeight={200}
                                options={selectCoursesData}
                                onMenuScrollToBottom={() => {}}
                            />
                        </div>
                    </div>
                ) : (
                    <Select
                        defaultValue={selectCoursesData[0]}
                        isClearable={true}
                        isSearchable={true}
                        maxMenuHeight={200}
                        options={selectCoursesData}
                        onChange={(data) =>
                            setFilterCourse({
                                label: data?.label ? data?.label : "",
                                value: data?.value ? data?.value : "",
                            })
                        }
                        onMenuScrollToBottom={() => {
                            if (
                                currCoursePage < infoCoursePagination.totalPage
                            ) {
                                setCurrCoursePage(currCoursePage + 1);
                            }
                        }}
                    />
                )}
            </div>
            <div className="mt-4">
                {overviewCourse ? (
                    <OverviewBox overviewCourse={overviewCourse} />
                ) : (
                    <div className="text-center">
                        <Spinner />
                    </div>
                )}
            </div>
        </div>
    );
}
