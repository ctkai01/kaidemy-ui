import * as React from "react";
import { OverviewCourse } from "../../../../models";
import EnrollmentTab from "./EnrollmentTab";
import RatingTab from "./RatingTab";
import RevenueTab from "./RevenueTab";

export interface IOverviewBoxProps {
    overviewCourse: OverviewCourse
}

const OVERVIEW_TAB = {
    REVENUE: 2,
    ENROLLMENT: 1,
    RATING: 0,
};

export default function OverviewBox(props: IOverviewBoxProps) {
    const [overviewTab, setOverviewTab] = React.useState(OVERVIEW_TAB.REVENUE);

    const { overviewCourse } = props

    // const handleChangeOverview = ()
    return (
        <div>
            <div className="flex border">
                <div
                    className={`cursor-pointer border-b border-t-primary-hover-gray px-5 py-4 hover:bg-primary-hover-gray ${
                        overviewTab === OVERVIEW_TAB.REVENUE &&
                        "border-b-[10px] border-cyan-500 bg-primary-hover-gray"
                    }`}
                    onClick={() => {
                        setOverviewTab(OVERVIEW_TAB.REVENUE);
                    }}
                >
                    <div className=" text-primary-gray">Total revenue</div>
                    <div className="text-4xl font-bold text-primary-gray">
                        {overviewCourse.revenues.total}
                    </div>
                    {/* <div className=" text-primary-gray">
                        {overviewCourse.revenues.net.total_this_month}{" "}
                        {overviewCourse.revenues.currency.toUpperCase()} trong
                        tháng này
                    </div> */}
                </div>
                <div
                    className={`cursor-pointer border-b border-t-primary-hover-gray px-5 py-4 hover:bg-primary-hover-gray ${
                        overviewTab === OVERVIEW_TAB.ENROLLMENT &&
                        "border-b-[10px] border-cyan-500 bg-primary-hover-gray"
                    }`}
                    onClick={() => {
                        setOverviewTab(OVERVIEW_TAB.ENROLLMENT);
                    }}
                >
                    <div className=" text-primary-gray">
                       Total enrollments
                    </div>
                    <div className="text-4xl font-bold text-primary-gray">
                        {overviewCourse.enrollments.total}
                    </div>
                    <div className=" text-primary-gray">
                        {overviewCourse.enrollments.totalThisMonth} this month
                    </div>
                </div>
                <div
                    className={`cursor-pointer border-b border-t border-t-primary-hover-gray px-5 py-4 hover:bg-primary-hover-gray ${
                        overviewTab === OVERVIEW_TAB.RATING &&
                        "border-b-[10px] border-cyan-500 bg-primary-hover-gray"
                    }`}
                    onClick={() => {
                        setOverviewTab(OVERVIEW_TAB.RATING);
                    }}
                >
                    <div className=" text-primary-gray">Instructor rating</div>
                    <div className="text-4xl font-bold text-primary-gray">
                        {overviewCourse.ratings.total}
                    </div>
                    <div className=" text-primary-gray">
                        {overviewCourse.ratings.totalThisMonth} this month
                    </div>
                </div>
            </div>
            <div>
                {overviewTab === OVERVIEW_TAB.ENROLLMENT && (
                    <EnrollmentTab overviewCourse={overviewCourse} />
                )}

                {overviewTab === OVERVIEW_TAB.RATING && (
                    <RatingTab overviewCourse={overviewCourse} />
                )}

                {overviewTab === OVERVIEW_TAB.REVENUE && (
                    <RevenueTab overviewCourse={overviewCourse} />
                )}
            </div>
        </div>
    );
}
