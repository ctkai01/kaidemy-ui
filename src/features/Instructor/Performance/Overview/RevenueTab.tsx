import * as React from "react";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,
} from "chart.js";
import { Line, Pie } from "react-chartjs-2";
import { OverviewCourse } from "../../../../models";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    ArcElement,
);
export interface IRevenueTabProps {
    overviewCourse: OverviewCourse;
}
const options = {
    responsive: true,
    interaction: {
        mode: "index" as const,
        intersect: false,
    },
    stacked: false,
    plugins: {
        title: {
            display: true,
            // text: 'Chart.js Line Chart - Multi Axis',
        },
    },
    scales: {
        y: {
            type: "linear" as const,
            display: true,
            position: "left" as const,
        },
        // y1: {
        //     type: "linear" as const,
        //     display: true,
        //     position: "right" as const,
        //     grid: {
        //         drawOnChartArea: false,
        //     },
        // },
    },
};
const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];
export default function RevenueTab(props: IRevenueTabProps) {
    const { overviewCourse } = props;
    const data = {
        labels,
        datasets: [
            {
                label: "Received revenue",
                data: overviewCourse.revenues.detailStats,
                borderColor: "rgb(255, 99, 132)",
                backgroundColor: "rgba(255, 99, 132, 0.5)",
                yAxisID: "y",
            },
            // {
            //     label: "Dataset 2",
            //     data: overviewCourse.revenues.fee.detail_stats,
            //     borderColor: "rgb(53, 162, 235)",
            //     backgroundColor: "rgba(53, 162, 235, 0.5)",
            //     yAxisID: "y1",
            // },
        ],
    };

    // const dataFee = {
    //     labels,
    //     datasets: [
    //         {
    //             label: "Phí ứng dụng",
    //             data: overviewCourse.revenues.fee.detail_stats,
    //             borderColor: "rgb(53, 162, 235)",
    //             backgroundColor: "rgba(53, 162, 235, 0.5)",
    //             yAxisID: "y",
    //         },
    //         // {
    //         //     label: "Dataset 2",
    //         //     data: overviewCourse.revenues.fee.detail_stats,
    //         //     borderColor: "rgb(53, 162, 235)",
    //         //     backgroundColor: "rgba(53, 162, 235, 0.5)",
    //         //     yAxisID: "y1",
    //         // },
    //     ],
    // };

    // const dataOverview = {
    //     labels: ["Doanh thu nhận được", "Phí ứng dụng"],
    //     datasets: [
    //         {
    //             label: "# of Votes",
    //             data: [
    //                 overviewCourse.revenues.net.total,
    //                 overviewCourse.revenues.fee.total,
    //             ],
    //             backgroundColor: [
    //                 "rgba(255, 99, 132, 0.2)",
    //                 "rgba(54, 162, 235, 0.2)",
    //                 // "rgba(255, 206, 86, 0.2)",
    //                 // "rgba(75, 192, 192, 0.2)",
    //                 // "rgba(153, 102, 255, 0.2)",
    //                 // "rgba(255, 159, 64, 0.2)",
    //             ],
    //             borderColor: [
    //                 "rgba(255, 99, 132, 1)",
    //                 "rgba(54, 162, 235, 1)",
    //                 // "rgba(255, 206, 86, 1)",
    //                 // "rgba(75, 192, 192, 1)",
    //                 // "rgba(153, 102, 255, 1)",
    //                 // "rgba(255, 159, 64, 1)",
    //             ],
    //             borderWidth: 1,
    //         },
    //     ],
    // };
    return (
        <div className="flex items-center mb-20">
            <div className="flex-1">
                <div className="h-[400px]">
                    <Line options={options} data={data} />
                </div>
                {/* <div className="h-[400px]">
                    <Line options={options} data={dataFee} />
                </div> */}
            </div>
            {/* <div className="flex-1 h-[300px] justify-center flex">
                <Pie data={dataOverview} />
            </div> */}
            
            {/* <Line options={options} data={dataFee} /> */}
        </div>
    );
}
