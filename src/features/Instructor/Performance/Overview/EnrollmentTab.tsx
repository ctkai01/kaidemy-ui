import * as React from "react";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { OverviewCourse } from "../../../../models";

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
);
export interface IEnrollmentTabProps {
    overviewCourse: OverviewCourse;
}
const options = {
    responsive: true,
    interaction: {
        mode: "index" as const,
        intersect: false,
    },
    stacked: false,
    plugins: {
        title: {
            display: true,
            // text: 'Chart.js Line Chart - Multi Axis',
        },
    },
    scales: {
        y: {
            type: "linear" as const,
            display: true,
            position: "left" as const,
        },
        // y1: {
        //     type: "linear" as const,
        //     display: true,
        //     position: "right" as const,
        //     grid: {
        //         drawOnChartArea: false,
        //     },
        // },
    },
};
const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];
export default function EnrollmentTab(props: IEnrollmentTabProps) {
    const { overviewCourse } = props
    const data = {
        labels,
        datasets: [
            {
                label: "Enrolled course",
                data: overviewCourse.enrollments.detailStats,
                borderColor: "rgb(255, 99, 132)",
                backgroundColor: "rgba(255, 99, 132, 0.5)",
                yAxisID: "y",
            },
            // {
            //   label: 'Dataset 2',
            //   data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
            //   borderColor: 'rgb(53, 162, 235)',
            //   backgroundColor: 'rgba(53, 162, 235, 0.5)',
            //   yAxisID: 'y1',
            // },
        ],
    };
    return (
        <div className="flex h-[500px] justify-center">
            <Line options={options} data={data} />
        </div>
    );
}
