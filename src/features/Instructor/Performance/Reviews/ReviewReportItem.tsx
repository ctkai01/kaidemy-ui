import { Rating } from "@mui/material";
import { Avatar, Button } from "flowbite-react";
import * as React from "react";
import { akaName, formatDistanceToNowTime } from "../../../../utils";
import { makeStyles } from "@mui/styles";
import { ReviewAuthor } from "../../../../models";
import { Link } from "react-router-dom";

export interface IReviewReportItemProps {
    review: ReviewAuthor
}

const useStyles: any = makeStyles({
    "icon-1": { color: "#b16f03" },
});

export default function ReviewReportItem(props: IReviewReportItemProps) {
    const classes = useStyles();
    const { review } = props 
  return (
      <div className="border-t border-primary-hover-gray first:border-t-0">
          <div className="flex border-b border-primary-hover-gray py-8 pl-8 pr-7 first:border-t-0">
              <div className="mr-5">
                  <img
                      className="w-[200px]"
                      src={
                          review.course.image
                              ? review.course.image
                              : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                      }
                  />
              </div>
              <div className="mr-4 flex-1 overflow-hidden">
                  <h3 className="max-w-[500px] overflow-hidden text-ellipsis whitespace-nowrap font-bold text-cyan-600">
                      {review.course.title}
                  </h3>
                  {/* <div className=" text-primary-gray">
                      {review.average_current} Đánh giá khóa học
                  </div> */}
              </div>
              <Link to={`/course/${review.course.id}`}>
                  <Button className="font-bold">Go to course</Button>
              </Link>
          </div>
          <div className="border-b border-primary-hover-gray py-8 pl-8 pr-7">
              <div className="mb-4 flex items-center gap-2">
                  {review.user.avatar ? (
                      <Avatar
                          img={review.user.avatar}
                          alt="avatar"
                          rounded
                          size="sm"
                      />
                  ) : (
                      <Avatar
                          placeholderInitials={akaName(review.user.name)}
                          rounded
                          size="lg"
                      />
                  )}
                  <div>
                      <h2 className="font-bold text-cyan-600">
                          {review.user.name}
                      </h2>
                      <div className=" text-primary-gray">
                          Updated{" "}
                          {formatDistanceToNowTime(review.updatedStarCount)}
                      </div>
                  </div>
              </div>
              <div>
                  <Rating
                      classes={{
                          iconFilled: classes["icon-1"],
                      }}
                      value={review.starCount}
                      precision={0.5}
                      readOnly
                      // size="small"
                  />
                  <div>{review.comment ? review.comment : ""}</div>
              </div>
          </div>
      </div>
  );
}
