import { Button, Pagination, Select, Spinner, TextInput } from "flowbite-react";
import * as React from "react";
import { FaSearch } from "react-icons/fa";
import { toast } from "react-toastify";
import {
    AZ_FILTER,
    NEWEST_FILTER,
    OLDEST_FILTER,
    ZA_FILTER,
} from "../../constants";
import { useAppSelector } from "../../hooks/redux-hook";
import { Category, Course, CreateCourse, PaginationInfo } from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { selectUserAuth } from "../../services/state/redux/authSlide";
import CourseGeneralItem from "./CourseGeneralItem";
import CreateCourseModal from "./CreateCourseModal";

export interface ICourseGeneralProps {}
// const TOTAL = 38;
// const TOTAL_PER_PAGE = 16;
// const TOTAL_PAGE = 3;
export default function CourseGeneral(props: ICourseGeneralProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const user = useAppSelector(selectUserAuth);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [courses, setCourses] = React.useState<Course[]>([]);
    const [searchCourse, setSearchCourse] = React.useState("");
    const [filter, setFilter] = React.useState(NEWEST_FILTER);
    const [loading, setLoading] = React.useState(false);
    const [openCreateCourseModal, setOpenCreateCourseModal] =
        React.useState(false);
    const [categories, setCategories] = React.useState<Category[]>([]);

    const {} = props;

    const handleCommandCourseModal = (command: boolean) => {
        setOpenCreateCourseModal(command);
    };

    React.useEffect(() => {
        const getParentCategories = async () => {
            try {
                const categoryParentResult =
                    await CategoryServiceApi.getCategoryMenu();
                setCategories(categoryParentResult.item);
            } catch (e) {
                console.log(e);
            }
        };

        getParentCategories();
    }, []);
    const onPageChange = (page: number) => setCurrentPage(page);
    React.useLayoutEffect(() => {
        const fetchCourseGeneral = async () => {
            try {
                // await handle
                if (user) {
                    setLoading(true);
                    const coursesResult =
                        await CourseServiceApi.getCoursesDetail(
                            user.id,
                            currentPage,
                            8,
                            searchCourse ? searchCourse : null,
                            filter,
                        );
                    // if (infoPagination.totalItem == 0) {
                    setInfoPagination({
                        totalPage: coursesResult.meta.pageCount,
                        totalItem: coursesResult.meta.itemCount,
                        size: coursesResult.meta.size,
                    });
                    // }

                    setCourses(coursesResult.item);
                    setLoading(false);
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourseGeneral();
    }, [currentPage, searchCourse, filter]);


    const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchCourse(e.target.value);
    };

    const handleChangeFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setFilter(+e.target.value);
    };

    const handleCreateNewCourse = async (data: CreateCourse) => {
        try {
            const body = {
                title: data.title,
                categoryID: data.category_id,
                subCategoryID: data.sub_category_id,
            };
            const result = await CourseServiceApi.createCourse(body);

            // Call API crate default curriculum
            const bodyCurriculum = {
                title: "Introduction",
                courseID: result.id,
            };

            const dataCurriculum =
                await CourseServiceApi.createCurriculum(bodyCurriculum);

            toast(<div className="font-bold">Added course successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
            setCourses((courses) => {
                const newCourse = result;
                newCourse.curriculums = [dataCurriculum];
                return [newCourse, ...courses];
            });
            handleCommandCourseModal(false);
        } catch (e) {
            console.log("Err", e);
            toast(<div className="font-bold">Added course failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="pt-18">
            <div className="mx-auto max-w-[1536px]  px-24 pb-16">
                <div className="mb-8 mt-2 text-3xl font-bold text-primary-black">
                    Khóa học
                </div>
                <CreateCourseModal
                    categories={categories}
                    openCreateCourseModal={openCreateCourseModal}
                    handleCreateNewCourse={handleCreateNewCourse}
                    handleCommandCourseModal={handleCommandCourseModal}
                />
                <div className="flex gap-8">
                    <div className="flex flex-1 gap-8">
                        <TextInput
                            type="text"
                            rightIcon={FaSearch}
                            onChange={handleChangeSearch}
                            placeholder="Search for your courses"
                        />
                        <Select onChange={handleChangeFilter}>
                            <option value={NEWEST_FILTER}>Newest</option>
                            <option value={OLDEST_FILTER}>Oldest</option>
                            <option value={AZ_FILTER}>A-Z</option>
                            <option value={ZA_FILTER}>Z-A</option>
                        </Select>
                    </div>
                    <div onClick={() => handleCommandCourseModal(true)}>
                        <Button>New course</Button>
                    </div>
                </div>
                <div className="mb-16 mt-6">
                    {loading ? (
                        <div className="text-center">
                            <Spinner
                                aria-label="Extra large spinner example"
                                size="xl"
                            />
                        </div>
                    ) : courses.length ? (
                        courses.map((course) => (
                            <CourseGeneralItem
                                key={course.id}
                                course={course}
                            />
                        ))
                    ) : (
                        <div className="text-center font-bold">
                            Empty course
                        </div>
                    )}
                </div>
                <div className="mt-10 flex justify-center">
                    <Pagination
                        currentPage={currentPage}
                        totalPages={infoPagination.totalPage}
                        onPageChange={onPageChange}
                        showIcons
                    />
                </div>
            </div>
        </div>
    );
}
