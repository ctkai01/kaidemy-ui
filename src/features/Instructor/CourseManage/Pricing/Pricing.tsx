import { Button, Label, Select, Spinner } from "flowbite-react";
import * as React from "react";
import { toast } from "react-toastify";
import { Course, Price } from "../../../../models";
import { CategoryServiceApi } from "../../../../services/api/categoryServiceApi";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";

export interface IPricingProps {
    courseID: number;
    course?: Course;
    loading: boolean;
    handleSetCourse: React.Dispatch<React.SetStateAction<Course | undefined>>;
    handleCourseNotFound: () => void;
}

export default function Pricing(props: IPricingProps) {
    const [currentPriceID, setCurrentPriceID] = React.useState<number | null>(
        null,
    );
    const [prices, setPrices] = React.useState<Price[]>([]);
    // const [course, setCourse] = React.useState<Course>();
    const [loadingPrice, setLoadingPrice] = React.useState(false);

    const { courseID, course, loading, handleCourseNotFound, handleSetCourse } =
        props;

    React.useEffect(() => {
        const getPrices = async () => {
            try {
                setLoadingPrice(true);
                const pricesResult = await CategoryServiceApi.getPrices();
                setPrices(pricesResult.item);
                setLoadingPrice(false);
            } catch (e) {
                console.log(e);
            }
        };

        // const getCourse = async () => {
        //     try {
        //         const result = await CourseServiceApi.getCourseByID(courseID);
        //         console.log(result);
        //         handleSetCourse(result.course);
        //         setCurrentPriceID(result.course.price_id);
        //         setLoading(false);
        //     } catch (e: any) {
        //         console.log(e);
        //         if (
        //             e.response.data.Message ===
        //             "error handling request: course not found"
        //         ) {
        //             handleCourseNotFound();
        //         }
        //     }
        // };
        // setLoading(true);
        // getCourse();
        getPrices();
    }, []);

    React.useEffect(() => {
        if (course) {
            setCurrentPriceID(course.priceId);
        }
    }, [course]);

    const handleChangePrice = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setCurrentPriceID(+e.target.value);
    };

    const handleSubmitPrice = async () => {
        try {
            const formData = new FormData();

            if (course) {
                formData.append("title", course.title);
                formData.append("categoryID", `${course.categoryId}`);
                formData.append("subCategoryID", `${course.subCategoryId}`);
                formData.append("priceID", `${currentPriceID}`);
                const courseUpdate = await CourseServiceApi.updateCourse(
                    formData,
                    course.id,
                );
                handleSetCourse(courseUpdate);
                toast(<div className="font-bold">Updated successfully</div>, {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                });
            }
        } catch (e) {
            console.log(e);
        }
    };
    return (
        <>
            {(loading || loadingPrice) && (
                <div className="text-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            )}
            {course && (
                <div>
                    <div className="mb-4 max-w-md">
                        <div className="mb-2 block">
                            <Label
                                htmlFor="price"
                                value="Set a price for your course"
                            />
                        </div>
                        <Select
                            onChange={handleChangePrice}
                            id="price"
                            required
                        >
                            <option value={-1}>Select</option>
                            {prices.map((price) => (
                                <option
                                    key={price.id}
                                    selected={price.id === currentPriceID}
                                    value={price.id}
                                >
                                    ${price.value} (tier {price.tier})
                                </option>
                            ))}
                        </Select>
                    </div>
                    <Button
                        onClick={handleSubmitPrice}
                        disabled={
                            !currentPriceID || currentPriceID == -1
                                ? true
                                : false
                        }
                    >
                        Save
                    </Button>
                </div>
            )}
        </>
    );
}
