import { yupResolver } from "@hookform/resolvers/yup";
import { Button, FileInput, Label, Select, TextInput } from "flowbite-react";
import * as React from "react";
import { useForm } from "react-hook-form";
import { ValidationError } from "yup";
import Editor from "../../../../components/ui/core/Editor";
import {
    Category,
    Course,
    Language,
    Level,
    UpdateCourseImage,
    UpdateCourseLandingPage,
    UpdateVideoWatch,
} from "../../../../models";
import { CategoryServiceApi } from "../../../../services/api/categoryServiceApi";
import { schemeUpdateImage } from "../../../../validators";
import {
    schemeUpdateCourseLanding,
    schemeUpdateVideoWatch,
} from "../../../../validators/course";

export interface ILandingPageFormProps {
    course: Course;
    handleUpdateCourseLanding: (
        data: UpdateCourseLandingPage,
        avatar?: File,
    ) => void;
}

export default function LandingPageForm(props: ILandingPageFormProps) {
    const [errorCourseImage, setErrorCourseImage] = React.useState("");
    const [levels, setLevels] = React.useState<Level[]>([]);
    const [languages, setLanguages] = React.useState<Language[]>([]);
    const [categories, setCategories] = React.useState<Category[]>([]);
    const [subCategories, setSubCategories] = React.useState<Category[]>([]);
    const [imagePreviewCourseImage, setImagePreviewCourseImage] =
        React.useState("");
    const [courseImageFile, setImageFile] = React.useState<File | undefined>(
        undefined,
    );

    const { course, handleUpdateCourseLanding } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        setError,
        clearErrors,
        getValues,
        watch,
        reset,
    } = useForm<UpdateCourseLandingPage>({
        mode: "onChange",
        defaultValues: {
            title: course.title,
            category_id: course.categoryId,
            sub_category_id: course.subCategoryId,
            description: course.description ? course.description : "",
            language_id: course.languageId ? course.languageId : undefined,
            level_id: course.levelId ? course.levelId : undefined,
            primarily_teach: course.primarilyTeach ? course.primarilyTeach : "",
            subtile: course.subtitle ? course.subtitle : "",
        },
        resolver: yupResolver(schemeUpdateCourseLanding),
    });

    const handleChangeCourseImage = async (
        e: React.ChangeEvent<HTMLInputElement>,
    ) => {
        try {
            const data: UpdateCourseImage = {
                image: e.target.files ? e.target.files[0] : undefined,
            };

            const dataValidate = await schemeUpdateImage.validate(data);
            const newFile = dataValidate.image;

            if (newFile) {
                const blob = URL.createObjectURL(newFile);
                setImageFile(newFile);
                setErrorCourseImage("");
                setImagePreviewCourseImage(blob);
            }
        } catch (e) {
            if (e instanceof ValidationError) {
                setErrorCourseImage(e.message);
                setImagePreviewCourseImage("");
            } else {
                console.log(e);
            }
        }
    };

    React.useEffect(() => {
        const getUtilData = async () => {
            try {
                const [levelsResult, languagesResult, categoryParentResult] =
                    await Promise.all([
                        CategoryServiceApi.getLevels(),
                        CategoryServiceApi.getLanguages(),
                        CategoryServiceApi.getCategoryParent(),
                    ]);

                setLevels(levelsResult.item);
                setLanguages(languagesResult.item);
                setCategories(categoryParentResult.item);
            } catch (e) {
                console.log(e);
            }
        };

        getUtilData();
    }, []);

    const handleSubmitCourseLanding = (data: UpdateCourseLandingPage) => {
        handleUpdateCourseLanding(data, courseImageFile);
    };

    const handleChangDesc = (value: string) => {
        setValue("description", value);
    };

    const handleChangeCategory = async (
        e: React.ChangeEvent<HTMLSelectElement>,
    ) => {
        const value = e.target.value;
        setValue("category_id", +value);
        if (+value === -1) {
            setError("category_id", {
                message: "Category is require",
            });
        } else {
            try {
                const subCategoryResult =
                    await CategoryServiceApi.getCategoryByParentID(+value);

                clearErrors("category_id");

                setSubCategories(subCategoryResult.item);
            } catch (e) {
                console.log(e);
            }
        }
    };

    React.useEffect(() => {
        const initSubCategory = async () => {
            if (course.categoryId) {
                const subCategoryResult =
                    await CategoryServiceApi.getCategoryByParentID(
                        course.categoryId,
                    );

                setSubCategories(subCategoryResult.item);
            }
        };
        if (course.image) {
            setImagePreviewCourseImage(course.image);
        }
        initSubCategory();
    }, []);

    return (
        <form onSubmit={handleSubmit(handleSubmitCourseLanding)}>
            <div className="flex justify-end">
                <Button className="font-bold" type="submit">
                    Save
                </Button>
            </div>
            <div>
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label htmlFor="title" value="Course title" />
                </div>
                <TextInput
                    placeholder="Insert your course title"
                    id="title"
                    type="text"
                    sizing="md"
                    color={errors.title ? "failure" : ""}
                    {...register("title")}
                    helperText={
                        <>
                            {errors.title ? (
                                <>
                                    <span className="font-medium">Oops! </span>
                                    <span>{errors.title.message}</span>
                                </>
                            ) : (
                                <></>
                            )}
                        </>
                    }
                />
            </div>
            <div className="mt-[30px]">
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label htmlFor="subtile" value="Course subtitle" />
                </div>
                <TextInput
                    placeholder="Insert your course subtitle"
                    // maxLength={60}
                    id="subtile"
                    type="text"
                    sizing="md"
                    color={errors.subtile ? "failure" : ""}
                    {...register("subtile")}
                    helperText={
                        <>
                            {errors.subtile ? (
                                <>
                                    <span className="font-medium">Oops! </span>
                                    <span>{errors.subtile.message}</span>
                                </>
                            ) : (
                                <></>
                            )}
                        </>
                    }
                />
            </div>
            <div className="mt-[30px]">
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label value="Course description" />
                </div>
                <Editor
                    isImage={false}
                    handleSetHTML={handleChangDesc}
                    defaultValue={course.description ? course.description : ""}
                />
                {errors.description ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium">Oops! </span>
                        <span>{errors.description.message}</span>
                    </div>
                ) : (
                    <></>
                )}
            </div>

            <div className="mt-[30px]">
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label value="Basic info" />
                </div>
                <div className="flex gap-5">
                    <Select
                        className="flex-1"
                        id="countries"
                        required
                        {...register("language_id")}
                    >
                        {languages.map((language) => (
                            <option
                                key={language.id}
                                value={language.id}
                                selected={
                                    getValues("language_id") === language.id
                                }
                            >
                                {language.name}
                            </option>
                        ))}
                    </Select>

                    <Select
                        className="flex-1"
                        id="levels"
                        {...register("level_id")}
                        required
                    >
                        <option
                            value={-1}
                            // selected={false}
                            // selected={getValues("level_id") ? false : true}
                        >
                            -- Select Level --
                        </option>
                        {levels.map((level) => (
                            <option
                                selected={getValues("level_id") === level.id}
                                key={level.id}
                                value={level.id}
                            >
                                {level.name}
                            </option>
                        ))}
                    </Select>
                    <div className="flex-1">
                        <Select
                            className=""
                            id="category"
                            required
                            // {...register("category_id")}
                            color={errors.category_id ? "failure" : ""}
                            onChange={handleChangeCategory}
                            helperText={
                                <>
                                    {errors.category_id ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>
                                                {errors.category_id.message}
                                            </span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        >
                            <option value={-1}>-- Chọn Category --</option>
                            {categories.map((category) => (
                                <option
                                    selected={
                                        category.id == getValues("category_id")
                                    }
                                    key={category.id}
                                    value={category.id}
                                >
                                    {category.name}
                                </option>
                            ))}
                        </Select>

                        {subCategories.length > 0 && (
                            <Select
                                className="mt-4"
                                id="category"
                                required
                                color={errors.sub_category_id ? "failure" : ""}
                                {...register("sub_category_id")}
                                helperText={
                                    <>
                                        {errors.sub_category_id ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {
                                                        errors.sub_category_id
                                                            .message
                                                    }
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            >
                                {/* <option>-- Select Subcategory --</option> */}
                                <option value={-1}>
                                    -- Select Subcategory --
                                </option>
                                {subCategories.map((category) => (
                                    <option
                                        selected={
                                            getValues("sub_category_id") ===
                                            category.id
                                        }
                                        key={category.id}
                                        value={category.id}
                                    >
                                        {category.name}
                                    </option>
                                ))}
                            </Select>
                        )}
                    </div>
                </div>
            </div>
            <div className="mt-[30px] w-1/2">
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label
                        htmlFor="primarily"
                        value="What is primarily taught in your course?"
                    />
                </div>
                <TextInput
                    placeholder="Lnadscape Photography"
                    maxLength={200}
                    id="primarily"
                    type="text"
                    sizing="md"
                    color={errors.primarily_teach ? "failure" : ""}
                    {...register("primarily_teach")}
                    helperText={
                        <>
                            {errors.primarily_teach ? (
                                <>
                                    <span className="font-medium">Oops! </span>
                                    <span>
                                        {errors.primarily_teach.message}
                                    </span>
                                </>
                            ) : (
                                <></>
                            )}
                        </>
                    }
                />
            </div>
            <div className="mt-[30px]">
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label htmlFor="primarily" value="Course image" />
                </div>
                <div className="flex">
                    <div className="flex-1 border border-[#d1d7dc]">
                        <img
                            // ref={courseImageRef}
                            className="max-h-[400px] w-full"
                            src={
                                imagePreviewCourseImage
                                    ? imagePreviewCourseImage
                                    : "https://s.udemycdn.com/course/750x422/placeholder.jpg"
                            }
                        />
                    </div>
                    <div className="flex flex-1 flex-col items-center  justify-center pl-[20px]">
                        <FileInput
                            className="mb-4"
                            onChange={handleChangeCourseImage}
                        />
                        {errorCourseImage ? (
                            <div className="text-sm text-red-700">
                                <span className="font-medium">Oops! </span>
                                <span>{errorCourseImage}</span>
                            </div>
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
            </div>

            {/* <div className="mt-[30px]">
                <div className="mb-2 block text-base font-bold text-primary-black">
                    <Label htmlFor="primarily" value="Promotional video" />
                </div>
                <div className="flex">
                    <div className="flex-1 border border-[#d1d7dc]">
                        <img
                            // ref={promotionalVideoImageRef}
                            className="max-h-[400px] w-full"
                            src={
                                "https://s.udemycdn.com/course/750x422/placeholder.jpg"
                            }
                        />
                    </div>
                    <div className="flex flex-1 items-center  justify-center pl-[20px]">
                        <FileInput />
                    </div>
                </div>
            </div> */}
        </form>
    );
}
