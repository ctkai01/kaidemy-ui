import { Spinner } from "flowbite-react";
import * as React from "react";
import { toast } from "react-toastify";
import { Course, UpdateCourseLandingPage } from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import LandingPageForm from "./LandingPageForm";

export interface ILandingPageProps {
    courseID: number;
    course?: Course;
    loading: boolean;
    handleSetCourse: React.Dispatch<React.SetStateAction<Course | undefined>>;
    // handleSetTitle: (value: string) => void;
}

export default function LandingPage(props: ILandingPageProps) {
    const {
        course,
        courseID,
        loading,
        handleSetCourse,
        // handleSetTitle,
    } = props;
    // const [course, setCourse] = React.useState<Course>();
    // const [loading, setLoading] = React.useState(false);

    const handleUpdateCourseLanding = async (
        data: UpdateCourseLandingPage,
        avatar?: File,
    ) => {
        // courseID
        try {
            const formData = new FormData();
            formData.append("title", data.title);
            formData.append("categoryID", `${data.category_id}`);
            formData.append("subCategoryID", `${data.sub_category_id}`);

            if (data.subtile) {
                formData.append("subtitle", data.subtile);
            }

            if (data.description) {
                formData.append("description", data.description);
            }

            if (data.language_id != -1) {
                formData.append("languageID", `${data.language_id}`);
            }

            if (data.level_id != -1) {
                formData.append("levelID", `${data.level_id}`);
            }

            if (data.primarily_teach) {
                formData.append("primarilyTeach", `${data.primarily_teach}`);
            }

            if (avatar) {
                formData.append("image", avatar);
            }
            const courseUpdateResult = await CourseServiceApi.updateCourse(
                formData,
                courseID,
            );
            handleSetCourse(courseUpdateResult);
            // handleSetTitle(courseUpdateResult.course.title);
            toast(<div className="font-bold">Updated successfully</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
            });
        } catch (e) {
            console.log(e);
        }
    };
    // React.useEffect(() => {
    //     const getCourse = async () => {
    //         try {
    //             const result = await CourseServiceApi.getCourseByID(courseID);
    //             console.log(result);
    //             // setCourse(result.course);
    //             handleSetCourse(result.course);
    //             setLoading(false);
    //         } catch (e: any) {
    //             console.log(e);
    //             if (
    //                 e.response.data.Message ===
    //                 "error handling request: course not found"
    //             ) {
    //                 handleCourseNotFound();
    //             }
    //         }
    //     };
    //     setLoading(true);

    //     getCourse();
    // }, []);

    return (
        // <LandingPageForm
        //     handleUpdateCourseLanding={handleUpdateCourseLanding}
        // />
        <>
            {loading && (
                <div className="text-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            )}
            {course && (
                <LandingPageForm
                    course={course}
                    handleUpdateCourseLanding={handleUpdateCourseLanding}
                />
            )}
        </>
    );
}
