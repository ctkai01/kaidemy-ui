import { Spinner } from "flowbite-react";
import * as React from "react";
import { Course } from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import IntendedLearnerForm from "./IntendedLearnerForm";

export interface IIntendedLearnerProps {
    courseID: number;
    course?: Course;
    loading: boolean;
    handleSetCourse: React.Dispatch<React.SetStateAction<Course | undefined>>;
    handleCourseNotFound: () => void;
}

export default function IntendedLearner(props: IIntendedLearnerProps) {
    const { courseID, course, loading, handleSetCourse, handleCourseNotFound } = props;
    // const [course, setCourse] = React.useState<Course>();
    // React.useEffect(() => {
    //     const getCourse = async () => {
    //         try {
    //             const result = await CourseServiceApi.getCourseByID(courseID);
    //             console.log(result);
    //             handleSetCourse(result.course);
    //             setLoading(false);
    //         } catch (e: any) {
    //             console.log(e);
    //             if (
    //                 e.response.data.Message ===
    //                 "error handling request: course not found"
    //             ) {
    //                 handleCourseNotFound();
    //             }
    //         }
    //     };
    //     setLoading(true);

    //     getCourse();
    // }, []);

    const handleUpdateCourse = (data: Course) => {
        handleSetCourse(data);
    }
    return (
        <>
            {loading && (
                <div className="text-center">
                    <Spinner aria-label="Extra large spinner example" size="xl" />
                </div>
            )}
            {course && <IntendedLearnerForm course={course} handleUpdateCourse={handleUpdateCourse}/>}
        </>
    );
}
