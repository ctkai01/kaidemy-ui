import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";
import { Button, TextInput } from "flowbite-react";
import * as React from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { useFieldArray, useForm } from "react-hook-form";
import { FaPlus, FaTrash } from "react-icons/fa";
import { RiDragMove2Line } from "react-icons/ri";
import { toast } from "react-toastify";
import {
    Course,
    CourseItem,
    IntendedLearners,
    IntendedLearnersSend,
} from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import { schemeUpdateIntendedLeaner } from "../../../../validators/course";
export interface IIntendedLearnerFormProps {
    course: Course;
    handleUpdateCourse: (data: Course) => void;
}

export default function IntendedLearnerForm(props: IIntendedLearnerFormProps) {
    const { course, handleUpdateCourse } = props;
    const initOutCome = React.useMemo(() => {
        if (course.outComes) {
            if (!course.outComes.length) {
                return [
                    {
                        value: "",
                    },
                    {
                        value: "",
                    },
                    {
                        value: "",
                    },
                    {
                        value: "",
                    },
                ];
            } else {
                return course.outComes.map((outcome) => {
                    return {
                        value: outcome,
                    };
                });
            }
        } else {
            return [
                {
                    value: "",
                },
                {
                    value: "",
                },
                {
                    value: "",
                },
                {
                    value: "",
                },
            ];
        }
    }, []);

    const initRequirements = React.useMemo(() => {
        if (course.requirements) {
            if (!course.requirements.length) {
                return [
                    {
                        value: "",
                    },
                ];
            } else {
                return course.requirements.map((requirement) => {
                    return {
                        value: requirement,
                    };
                });
            }
        } else {
            return [
                {
                    value: "",
                },
            ];
        }
    }, []);

    const initIntendedFor = React.useMemo(() => {
        if (course.intendedFor) {
            if (!course.intendedFor.length) {
                return [
                    {
                        value: "",
                    },
                ];
            } else {
                return course.intendedFor.map((intended) => {
                    return {
                        value: intended,
                    };
                });
            }
        } else {
            return [
                {
                    value: "",
                },
            ];
        }
    }, []);
    const {
        register,
        control,
        formState: { errors },
        handleSubmit,
    } = useForm<IntendedLearners>({
        defaultValues: {
            // Example initial data for a list of inputs
            outcomes: initOutCome,
            requirements: initRequirements,
            intended_for: initIntendedFor,
        },
        mode: "onSubmit",

        resolver: yupResolver(schemeUpdateIntendedLeaner),
    });

    const {
        fields: outComesFields,
        append: appendOutComes,
        remove: removeOutComes,
        swap: swapOutcomes,
    } = useFieldArray({
        control,
        name: "outcomes",
    });

    const {
        fields: requirementsFields,
        append: appendRequirements,
        remove: removeRequirements,
        swap: swapRequirements,
    } = useFieldArray({
        control,
        name: "requirements",
    });

    const {
        fields: intendedForFields,
        append: appendIntendedFor,
        remove: removeIntendedFor,
        swap: swapIntendedFor,
    } = useFieldArray({
        control,
        name: "intended_for",
    });
    function handleOnDragEndOutComes(result: any) {
        if (!result.destination) return;
        swapOutcomes(result.source.index, result.destination.index);
    }

    function handleOnDragEndRequirements(result: any) {
        if (!result.destination) return;
        swapRequirements(result.source.index, result.destination.index);
    }

    function handleOnDragEndIntendedFor(result: any) {
        if (!result.destination) return;
        swapIntendedFor(result.source.index, result.destination.index);
    }

    const onSubmit = async (data: IntendedLearners) => {
        const dataUpdate: IntendedLearnersSend = {
            outcomes: data.outcomes.map((i) => i.value),
            intended_for: data.intended_for
                ? data.intended_for.map((i) => i.value)
                : [],
            requirements: data.requirements
                ? data.requirements.map((i) => i.value)
                : [],
        };

        try {
            const formData = new FormData();

            dataUpdate.outcomes.forEach((outcome, _) => {
                formData.append(`outcomes`, outcome ? outcome : "");
            });

            dataUpdate.requirements.forEach((requirement, _) => {
                formData.append(`requirements`, requirement ? requirement : "");
            });

            dataUpdate.intended_for.forEach((intended, _) => {
                formData.append(`intendedFor`, intended ? intended : "");
            });

            formData.append("title", course ? course.title : "");
            formData.append("categoryID", course ? `${course.categoryId}` : "");
            formData.append(
                "subCategoryID",
                course ? `${course.subCategoryId}` : "",
            );

            const dataResult = await CourseServiceApi.updateCourse(
                formData,
                course.id,
            );

            handleUpdateCourse(dataResult);
            toast(<div className="font-bold">Updated Successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Update Failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <p className="max-w-[800px] pb-12">
                    <span>
                        The following descriptions will be publicly visible on
                        your Course Landing Page and will have a direct impact
                        on your course performance. These descriptions will help
                        learners decide if your course is right for them.
                    </span>
                </p>
                <div className="mb-4">
                    <div>
                        <div className="mb-2 text-base font-bold text-primary-black">
                            What will students learn in your course?
                        </div>
                        <p className="mb-8 max-w-[800px]">
                            <span className="text-base text-primary-black">
                                You must enter at least 4 learning objectives or
                                outcomes that learners can expect to achieve
                                after completing your course.
                            </span>
                        </p>
                        <div>
                            <DragDropContext
                                onDragEnd={handleOnDragEndOutComes}
                            >
                                <Droppable droppableId="outcomes">
                                    {(provided) => (
                                        <ul
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}
                                        >
                                            {outComesFields.map(
                                                ({ id }, index) => {
                                                    return (
                                                        <Draggable
                                                            key={id}
                                                            draggableId={id}
                                                            index={index}
                                                        >
                                                            {(provided) => (
                                                                <>
                                                                    <li className="mb-4 flex max-w-[700px]">
                                                                        <TextInput
                                                                            className="w-full flex-1"
                                                                            type="text"
                                                                            maxLength={
                                                                                160
                                                                            }
                                                                            {...register(
                                                                                `outcomes.${index}.value`,
                                                                            )}
                                                                        />
                                                                        <div className="ml-2 flex">
                                                                            <Button
                                                                                onClick={() =>
                                                                                    removeOutComes(
                                                                                        index,
                                                                                    )
                                                                                }
                                                                                color="red"
                                                                            >
                                                                                <FaTrash className="h-6 w-6" />
                                                                            </Button>
                                                                            <div
                                                                                className="ml-[2px] flex cursor-move items-center justify-center rounded  border border-cyan-300 px-4 py-2 ring-1  focus:ring-4  focus:ring-cyan-300"
                                                                                ref={
                                                                                    provided.innerRef
                                                                                }
                                                                                {...provided.draggableProps}
                                                                                {...provided.dragHandleProps}
                                                                            >
                                                                                <RiDragMove2Line className="h-6 w-6 text-cyan-900" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    {errors.outcomes &&
                                                                    errors
                                                                        .outcomes[
                                                                        index
                                                                    ] ? (
                                                                        <div className="mb-6 text-sm text-red-600">
                                                                            <span className="font-medium">
                                                                                Oops!{" "}
                                                                            </span>
                                                                            <span>
                                                                                {
                                                                                    errors
                                                                                        .outcomes[
                                                                                        index
                                                                                    ]
                                                                                        ?.value
                                                                                        ?.message
                                                                                }
                                                                            </span>
                                                                        </div>
                                                                    ) : (
                                                                        <></>
                                                                    )}
                                                                </>
                                                            )}
                                                        </Draggable>
                                                    );
                                                },
                                            )}
                                            {provided.placeholder}
                                        </ul>
                                    )}
                                </Droppable>
                            </DragDropContext>

                            <Button
                                type="button"
                                color="success"
                                onClick={() => appendOutComes({ value: "" })}
                            >
                                <FaPlus className="mr-2 h-5 w-5" />
                                Add more to your response
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="mb-4">
                    <div>
                        <div className="mb-2 text-base font-bold text-primary-black">
                            What are the requirements or prerequisites for
                            taking your course?
                        </div>
                        <p className="mb-8 max-w-[800px]">
                            <span className="text-base text-primary-black">
                                List the required skills, experience, tools or
                                equipment learners should have prior to taking
                                your course. If there are no requirements, use
                                this space as an opportunity to lower the
                                barrier for beginners.
                            </span>
                        </p>
                        <div>
                            <DragDropContext
                                onDragEnd={handleOnDragEndRequirements}
                            >
                                <Droppable droppableId="requirements">
                                    {(provided) => (
                                        <ul
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}
                                        >
                                            {requirementsFields.map(
                                                ({ id }, index) => {
                                                    return (
                                                        <Draggable
                                                            key={id}
                                                            draggableId={id}
                                                            index={index}
                                                        >
                                                            {(provided) => (
                                                                <>
                                                                    <li className="mb-4 flex max-w-[700px]">
                                                                        <TextInput
                                                                            className="w-full flex-1"
                                                                            type="text"
                                                                            maxLength={
                                                                                160
                                                                            }
                                                                            {...register(
                                                                                `requirements.${index}.value`,
                                                                            )}
                                                                        />
                                                                        <div className="ml-2 flex">
                                                                            <Button
                                                                                onClick={() =>
                                                                                    removeRequirements(
                                                                                        index,
                                                                                    )
                                                                                }
                                                                                color="red"
                                                                            >
                                                                                <FaTrash className="h-6 w-6" />
                                                                            </Button>
                                                                            <div
                                                                                className="ml-[2px] flex cursor-move items-center justify-center rounded  border border-cyan-300 px-4 py-2 ring-1  focus:ring-4  focus:ring-cyan-300"
                                                                                ref={
                                                                                    provided.innerRef
                                                                                }
                                                                                {...provided.draggableProps}
                                                                                {...provided.dragHandleProps}
                                                                            >
                                                                                <RiDragMove2Line className="h-6 w-6 text-cyan-900" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <div className="mb-6 text-sm text-red-600">
                                                                        {errors.requirements &&
                                                                        errors
                                                                            .requirements
                                                                            .length &&
                                                                        errors
                                                                            .requirements[
                                                                            index
                                                                        ] ? (
                                                                            <>
                                                                                <span className="font-medium">
                                                                                    Oops!{" "}
                                                                                </span>
                                                                                <span>
                                                                                    {
                                                                                        errors
                                                                                            .requirements[
                                                                                            index
                                                                                        ]
                                                                                            .value
                                                                                            ?.message
                                                                                    }
                                                                                </span>
                                                                            </>
                                                                        ) : (
                                                                            ""
                                                                        )}
                                                                    </div>
                                                                </>
                                                            )}
                                                        </Draggable>
                                                    );
                                                },
                                            )}
                                            {provided.placeholder}
                                        </ul>
                                    )}
                                </Droppable>
                            </DragDropContext>
                            <Button
                                type="button"
                                color="success"
                                onClick={() =>
                                    appendRequirements({ value: "" })
                                }
                            >
                                <FaPlus className="mr-2 h-5 w-5" />
                                Add more to your response
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="mb-4">
                    <div>
                        <div className="mb-2 text-base font-bold text-primary-black">
                            Who is this course for?
                        </div>
                        <p className="mb-8 max-w-[800px]">
                            <span className="text-base text-primary-black">
                                Write a clear description of the intended
                                learners for your course who will find your
                                course content valuable. This will help you
                                attract the right learners to your course.
                            </span>
                        </p>
                        <div>
                            <DragDropContext
                                onDragEnd={handleOnDragEndIntendedFor}
                            >
                                <Droppable droppableId="intendedFor">
                                    {(provided) => (
                                        <ul
                                            {...provided.droppableProps}
                                            ref={provided.innerRef}
                                        >
                                            {intendedForFields.map(
                                                ({ id }, index) => {
                                                    return (
                                                        <Draggable
                                                            key={id}
                                                            draggableId={id}
                                                            index={index}
                                                        >
                                                            {(provided) => (
                                                                <>
                                                                    <li
                                                                        key={id}
                                                                        className="mb-4 flex max-w-[700px]"
                                                                    >
                                                                        <TextInput
                                                                            className="w-full flex-1"
                                                                            type="text"
                                                                            maxLength={
                                                                                160
                                                                            }
                                                                            {...register(
                                                                                `intended_for.${index}.value`,
                                                                            )}
                                                                        />
                                                                        <div className="ml-2 flex">
                                                                            <Button
                                                                                onClick={() => {
                                                                                    removeIntendedFor(
                                                                                        index,
                                                                                    );
                                                                                }}
                                                                                color="red"
                                                                            >
                                                                                <FaTrash className="h-6 w-6" />
                                                                            </Button>
                                                                            <div
                                                                                className="ml-[2px] flex cursor-move items-center justify-center rounded  border border-cyan-300 px-4 py-2 ring-1  focus:ring-4  focus:ring-cyan-300"
                                                                                ref={
                                                                                    provided.innerRef
                                                                                }
                                                                                {...provided.draggableProps}
                                                                                {...provided.dragHandleProps}
                                                                            >
                                                                                <RiDragMove2Line className="h-6 w-6 text-cyan-900" />
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <div className="mb-6 text-sm text-red-600">
                                                                        {errors.intended_for &&
                                                                        errors
                                                                            .intended_for[
                                                                            index
                                                                        ] ? (
                                                                            <>
                                                                                <span className="font-medium">
                                                                                    Oops!{" "}
                                                                                </span>
                                                                                <span>
                                                                                    {
                                                                                        errors
                                                                                            .intended_for[
                                                                                            index
                                                                                        ]
                                                                                            .value
                                                                                            ?.message
                                                                                    }
                                                                                </span>
                                                                            </>
                                                                        ) : (
                                                                            ""
                                                                        )}
                                                                    </div>
                                                                </>
                                                            )}
                                                        </Draggable>
                                                    );
                                                },
                                            )}
                                            {provided.placeholder}
                                        </ul>
                                    )}
                                </Droppable>
                            </DragDropContext>
                            <Button
                                type="button"
                                color="success"
                                onClick={() => appendIntendedFor({ value: "" })}
                            >
                                <FaPlus className="mr-2 h-5 w-5" />
                                Add more to your response
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="flex justify-end ">
                    <Button type="submit" className="w-fit  font-bold">
                        Submit
                    </Button>
                </div>
            </form>
        </div>
    );
}
