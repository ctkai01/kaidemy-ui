// import BestSeller from "../../components/ui/core/BestSeller";
// import ReviewStar from "../../components/ui/core/ReviewStar";
// import { formatNumberWithCommas } from "../../utils";

import { Badge, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import { IoIosArrowBack } from "react-icons/io";
import { Link } from "react-router-dom";
import {
    REVIEW_INIT_STATUS,
    REVIEW_PENDING_STATUS,
    REVIEW_VERIFY_STATUS,
} from "../../../constants";
import { Course } from "../../../models";
import { convertSecondsToHoursMinutes } from "../../../utils";

const customTheme: CustomFlowbiteTheme = {
    badge: {
        // root: ""
        root: {
            color: {
                gray: "bg-primary-gray text-[#f7f9fa]",
            },
        },
    },
};

export interface IHeaderCourseManageProps {
    totalDuration: number;
    title: string;
    course?: Course;
}

export default function HeaderCourseManage(props: IHeaderCourseManageProps) {
    const { totalDuration, title, course } = props;
    return (
        <div className="flex items-center gap-x-4 gap-y-2 bg-primary-black px-4 py-[11px] text-white">
            <div className="">
                <Link
                    className="flex items-center text-base text-white"
                    to="/instructor/course"
                >
                    <IoIosArrowBack />
                    <span className="ml-1">Back to courses</span>
                </Link>
            </div>
            <div className="flex items-center gap-x-4 gap-y-2">
                <div className="text-[19px] font-bold text-white">{title}</div>
                {/* <Badge color="gray">DRAFT</Badge> */}
                <Flowbite theme={{ theme: customTheme }}>
                    {/* <Badge className="text-xs" color="gray">
                        DRAFT
                    </Badge> */}
                    {course?.reviewStatus === REVIEW_INIT_STATUS && (
                        <Badge className="text-xs uppercase" color="info">
                            DRAFT
                        </Badge>
                    )}
                    {course?.reviewStatus === REVIEW_PENDING_STATUS && (
                        <Badge className="text-xs uppercase" color="warning">
                            REVIEWING
                        </Badge>
                    )}
                    {course?.reviewStatus === REVIEW_VERIFY_STATUS && (
                        <Badge className="text-xs uppercase" color="success">
                            VERIFIED
                        </Badge>
                    )}
                </Flowbite>
                <span className="text-base text-white">
                    {convertSecondsToHoursMinutes(totalDuration)} of video
                    content uploaded
                </span>
            </div>
            {/* <div className="mb-1overflow-hidden overflow-ellipsis whitespace-nowrap text-base font-bold">
              Ultimate Rust Crash Course
          </div>
          <div>
          ds
          <div className="flex items-center">
              <BestSeller className="mr-2" />
              <span className="mr-2 text-sm font-bold text-[#f69c08]">4.7</span>
              <ReviewStar color="#f69c08" number={4.7} />
              <span className="mr-2 text-sm text-[#8dfdff] underline">
                  ({formatNumberWithCommas(1500)} ratings)
              </span>
              <span className="text-sm text-white">17,917 students</span>
          </div> */}
        </div>
    );
}
