import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { CreateQuiz, UpdateSection } from "../../../../models";
import { schemeUpdateSection } from "../../../../validators/course";
import { Curriculum } from "./Curriculum";

export interface ISectionEditFormProps {
    index: number;
    curriculum: Curriculum;
    handleNormalSectionMode: () => void;
    handleUpdateSection: (data: UpdateSection) => void;
}

export default function SectionEditForm(props: ISectionEditFormProps) {
    const { index, curriculum, handleNormalSectionMode, handleUpdateSection } =
        props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm<UpdateSection>({
        mode: "onChange",
        defaultValues: {
            id: curriculum.id,
            title: curriculum.title,
            description: curriculum.description ? curriculum.description : "",
        },
        resolver: yupResolver(schemeUpdateSection),
    });

    const handleSubmitUpdateSection = (data: UpdateSection) => {
        handleUpdateSection(data);
        handleNormalSectionMode();
        // reset();
    };
    return (
        <div className="p-2">
            <div className="border border-primary-black p-2">
                <form onSubmit={handleSubmit(handleSubmitUpdateSection)}>
                    <div className="flex  gap-2">
                        <div className="flex h-[42px] items-center text-base font-bold text-primary-black">
                            Section {index + 1}:
                        </div>
                        <div className="flex-1">
                            <div className="mb-4">
                                <TextInput
                                    type="text"
                                    placeholder="Enter a title"
                                    color={errors.title ? "failure" : ""}
                                    {...register("title")}
                                    helperText={
                                        <>
                                            {errors.title ? (
                                                <>
                                                    <span className="font-medium">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {errors.title.message}
                                                    </span>
                                                </>
                                            ) : (
                                                <></>
                                            )}
                                        </>
                                    }
                                />
                            </div>
                            <span className="mb-2 block text-sm font-bold text-primary-black">
                                What will students be able to do at the end of
                                this section?
                            </span>
                            <div className="mb-4">
                                <TextInput
                                    type="text"
                                    placeholder="Enter a Course Objective"
                                    color={errors.description ? "failure" : ""}
                                    {...register("description")}
                                    helperText={
                                        <>
                                            {errors.description ? (
                                                <>
                                                    <span className="font-medium">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {
                                                            errors.description
                                                                .message
                                                        }
                                                    </span>
                                                </>
                                            ) : (
                                                <></>
                                            )}
                                        </>
                                    }
                                />
                            </div>
                        </div>
                    </div>
                    <div className="mt-4 flex justify-end gap-2">
                        <Button
                            onClick={() => handleNormalSectionMode()}
                            color="gray"
                        >
                            Cancel
                        </Button>
                        <Button type="submit">Save</Button>
                    </div>
                </form>
            </div>
        </div>
    );
}
