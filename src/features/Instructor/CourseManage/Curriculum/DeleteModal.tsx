import { Button, Modal } from "flowbite-react";
import { HiOutlineExclamationCircle } from "react-icons/hi2";
import { Asset } from "../../../../models";

export interface IDeleteModalProps {
    openModal: boolean;
    lectureID: number;
    curriculumID: number;
    assetDelete: Asset | null;
    handleCommandModal: (command: boolean) => void;
    handleDeleteResource: (
        lectureID: number,
        curriculumID: number,
        assetID: number,
    ) => void;
    handleNullAssetDelete: () => void;
}

export default function DeleteModal(props: IDeleteModalProps) {
    const {
        lectureID,
        curriculumID,
        openModal,
        assetDelete,
        handleCommandModal,
        handleDeleteResource,
        handleNullAssetDelete,
    } = props;

    const handleDelete = () => {
        if (assetDelete) {
            handleDeleteResource(lectureID, curriculumID, assetDelete.id);
            handleCommandModal(false);
            handleNullAssetDelete();
        }
    };
    return (
        <Modal
            show={openModal}
            size="md"
            onClose={() => handleCommandModal(false)}
            popup
        >
            <Modal.Header />
            <Modal.Body>
                <div className="text-center">
                    <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
                    <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                        Are you sure you want to delete this resource?
                    </h3>
                    <div className="flex justify-center gap-4">
                        <Button color="failure" onClick={() => handleDelete()}>
                            {"Yes, I'm sure"}
                        </Button>
                        <Button
                            color="gray"
                            onClick={() => handleCommandModal(false)}
                        >
                            No, cancel
                        </Button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
}
