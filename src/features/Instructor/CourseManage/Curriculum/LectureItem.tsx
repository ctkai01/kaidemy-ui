import {
    Button,
    FileInput,
    Modal,
    Spinner,
    ToggleSwitch,
} from "flowbite-react";
import * as React from "react";
import { AiFillCheckCircle, AiOutlineFile } from "react-icons/ai";
import { FaPlus, FaTrash } from "react-icons/fa";
import { HiOutlineExclamationCircle, HiPlay } from "react-icons/hi2";
import { IoIosArrowDown, IoIosArrowUp, IoMdClose } from "react-icons/io";
import { IoPencil } from "react-icons/io5";
import { PiFolderSimplePlus } from "react-icons/pi";
import {
    LECTURE_RESOURCE_ASSET_TYPE,
    LECTURE_WATCH_ASSET_TYPE,
} from "../../../../constants";
import {
    Asset,
    Lecture,
    UpdateLecture,
    UpdateLectureArticle,
    UpdateLectureDesc,
    UpdateResource,
    UpdateVideoWatch,
} from "../../../../models";
import { convertToHHMM, formatBytes } from "../../../../utils";
import ArticleForm from "./ArticleForm";
import DeleteModal from "./DeleteModal";
import LectureDescForm from "./LectureDescForm";
import ResourceForm from "./ResourceForm";
import UpdateLectureForm from "./UpdateLectureForm";
import VideoForm from "./VideoForm";
export interface ILectureItemProps {
    lecture: Lecture;
    index: number;
    curriculumId: number;
    handleUpdateLecture: (data: UpdateLecture) => void;
    handleUpdateLectureDesc: (data: UpdateLectureDesc) => void;
    handleUpdateLectureArticle: (data: UpdateLectureArticle) => void;
    handleDeleteLecture: (lectureID: number, curriculumID: number) => void;
    handleUploadVideoWatch: (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => void;
    handleReplaceVideoWatch: (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => void;
    handleUploadResource: (
        data: UpdateResource,
        setLoading: (command: boolean) => void,
    ) => void;
    handleDeleteResource: (
        lectureID: number,
        curriculumID: number,
        assetID: number,
    ) => void;
    handleUpdateLectureIsPromotional: (
        isCheck: boolean,
        curriculumId: number,
        lectureId: number,
    ) => void;
}

const MODE_LECTURE = {
    NORMAL: 0,
    OPEN_DESC_RESOURCE: 1,
    OPEN_DESC: 2,
    OPEN_RESOURCE: 3,
    OPEN_CONTENT: 4,
    OPEN_VIDEO: 5,
    OPEN_ARTICLE: 6,
    EDIT: 7,
};

export default function LectureItem(props: ILectureItemProps) {
    const {
        lecture,
        index,
        curriculumId,
        handleUpdateLecture,
        handleDeleteLecture,
        handleUploadVideoWatch,
        handleReplaceVideoWatch,
        handleUpdateLectureDesc,
        handleUpdateLectureArticle,
        handleUploadResource,
        handleDeleteResource,
        handleUpdateLectureIsPromotional,
    } = props;
    const [lectureEdit, setLectureEdit] = React.useState<Lecture | null>(null);
    const [assetDelete, setAssetDelete] = React.useState<Asset | null>(null);
    const [openModalDeleteLecture, setOpenModalDeleteLecture] =
        React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [mode, setMode] = React.useState(MODE_LECTURE.NORMAL);
    const [openModalDeleteResource, setOpenModalDeleteResource] =
        React.useState(false);

    const handleModeNormal = () => {
        setMode(MODE_LECTURE.NORMAL);
    };

    const handleModeDescResource = () => {
        setMode(MODE_LECTURE.OPEN_DESC_RESOURCE);
    };

    const handleDelLecture = () => {
        setOpenModalDeleteLecture(false);
        handleDeleteLecture(lecture.id, curriculumId);
    };

    const handleSetLoading = (command: boolean) => {
        setLoading(command);
    };

    const handleOpenDeleteResource = (command: boolean) => {
        setOpenModalDeleteResource(command);
    };

    const handleNullAssetDelete = () => {
        setAssetDelete(null);
    };

    const assetVideoWatch = lecture.assets.find(
        (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
    );

    const assetResources = lecture.assets.filter(
        (asset) => asset.type === LECTURE_RESOURCE_ASSET_TYPE,
    );
    return (
        <div>
            <Modal
                show={openModalDeleteLecture}
                size="md"
                onClose={() => setOpenModalDeleteLecture(false)}
                popup
            >
                <Modal.Header />
                <Modal.Body>
                    <div className="text-center">
                        <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
                        <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                            Are you sure you want to delete this lecture?
                        </h3>
                        <div className="flex justify-center gap-4">
                            <Button
                                color="failure"
                                onClick={() => handleDelLecture()}
                            >
                                {"Yes, I'm sure"}
                            </Button>
                            <Button
                                color="gray"
                                onClick={() => setOpenModalDeleteLecture(false)}
                            >
                                No, cancel
                            </Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>

            <DeleteModal
                curriculumID={curriculumId}
                lectureID={lecture.id}
                assetDelete={assetDelete}
                openModal={openModalDeleteResource}
                handleNullAssetDelete={handleNullAssetDelete}
                handleCommandModal={handleOpenDeleteResource}
                handleDeleteResource={handleDeleteResource}
            />

            {mode != MODE_LECTURE.EDIT && (
                <div className="group flex justify-between border border-primary-black px-2 py-[11px]">
                    <div className="flex items-center">
                        <AiFillCheckCircle className="mr-1" />
                        <span className="text-base text-primary-black">
                            Lecture {index}:{" "}
                        </span>
                        <div className="ml-2 flex items-center">
                            <AiOutlineFile className="mr-1" />
                            <span>{lecture.title}</span>
                        </div>
                        <div
                            onClick={() => {
                                setMode(MODE_LECTURE.EDIT);
                                setLectureEdit(lecture);
                            }}
                            className="mx-2 hidden cursor-pointer group-hover:block"
                        >
                            <IoPencil />
                        </div>
                        <div
                            onClick={() => {
                                setOpenModalDeleteLecture(true);
                            }}
                            className="mx-2 hidden  cursor-pointer group-hover:block"
                        >
                            <FaTrash />
                        </div>
                    </div>
                    <div className="flex items-center">
                        {mode !== MODE_LECTURE.OPEN_RESOURCE &&
                            mode !== MODE_LECTURE.OPEN_CONTENT &&
                            mode !== MODE_LECTURE.OPEN_ARTICLE &&
                            mode !== MODE_LECTURE.OPEN_VIDEO &&
                            !assetVideoWatch &&
                            !lecture.article && (
                                <Button
                                    onClick={() => {
                                        setMode(MODE_LECTURE.OPEN_CONTENT);
                                    }}
                                    color="success"
                                    className="mr-2 max-h-[34px]"
                                >
                                    <FaPlus className="mr-2 h-5 w-5" />
                                    Content
                                </Button>
                            )}

                        {mode === MODE_LECTURE.NORMAL && (
                            <IoIosArrowDown
                                className="cursor-pointer"
                                onClick={() =>
                                    setMode(MODE_LECTURE.OPEN_DESC_RESOURCE)
                                }
                            />
                        )}
                        {mode === MODE_LECTURE.OPEN_DESC_RESOURCE && (
                            <IoIosArrowUp
                                className="cursor-pointer"
                                onClick={() => setMode(MODE_LECTURE.NORMAL)}
                            />
                        )}
                    </div>
                </div>
            )}
            {mode === MODE_LECTURE.EDIT && lectureEdit && (
                <UpdateLectureForm
                    curriculumId={curriculumId}
                    handleModeNormal={handleModeNormal}
                    handleUpdateLecture={handleUpdateLecture}
                    // handleUpdateQuiz={handleUpdateQuiz}
                    lectureEdit={lectureEdit}
                    lectureIndex={index}
                    // quizEdit={quizEdit}
                />
            )}
            {mode === MODE_LECTURE.OPEN_DESC_RESOURCE && (
                <div className="border border-t-0 border-primary-black p-2">
                    {assetVideoWatch && (
                        <div className="mb-2 border-b border-[#d1d7dc] pb-2">
                            {loading ? (
                                <div className="text-center">
                                    <Spinner
                                        aria-label="Extra large spinner example"
                                        size="xl"
                                    />
                                </div>
                            ) : (
                                <div className="flex justify-between">
                                    <div className="flex">
                                        <img
                                            className="mb-8 h-[60px] w-[110px] object-cover"
                                            loading="lazy"
                                            onError={(e) => {
                                                e.currentTarget.src = `https://vz-eb495bf1-353.b-cdn.net/${assetVideoWatch.bunnyId}/thumbnail.jpg`;
                                            }}
                                            src={`https://vz-eb495bf1-353.b-cdn.net/${assetVideoWatch.bunnyId}/thumbnail.jpg`}
                                        />

                                        <div className="ml-4 flex text-base text-primary-black">
                                            <span>
                                                {convertToHHMM(
                                                    assetVideoWatch.duration,
                                                )}
                                            </span>
                                            <Button
                                                onClick={() => {
                                                    setMode(
                                                        MODE_LECTURE.OPEN_VIDEO,
                                                    );
                                                }}
                                                className="ml-4 h-fit w-fit"
                                            >
                                                Replace
                                            </Button>
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="mb-4 text-xs font-bold text-primary-black">
                                            Mark preview
                                        </div>
                                        <ToggleSwitch
                                            checked={lecture.isPromotional}
                                            onChange={() => {
                                                handleUpdateLectureIsPromotional(
                                                    lecture.isPromotional,
                                                    curriculumId,
                                                    lecture.id,
                                                );
                                            }}
                                        />
                                    </div>
                                </div>
                            )}
                        </div>
                    )}

                    {lecture.article && (
                        <div className="mb-2 flex border-b border-[#d1d7dc]">
                            <div
                                className="mb-8 h-[60px] w-[110px] bg-primary-black bg-cover"
                                style={{
                                    backgroundImage: `url(https://img-c.udemycdn.com/icons/128/FILE.png)`,
                                }}
                            />

                            <div className="ml-4 flex text-base text-primary-black">
                                <Button
                                    onClick={() => {
                                        setMode(MODE_LECTURE.OPEN_ARTICLE);
                                    }}
                                    className="ml-4 h-fit w-fit"
                                >
                                    Edit content
                                </Button>
                            </div>
                        </div>
                    )}

                    {lecture.description && (
                        <div className="mb-2 border-b border-[#d1d7dc]">
                            <div
                                onClick={() => {
                                    setMode(MODE_LECTURE.OPEN_DESC);
                                }}
                                className="mb-2 cursor-pointer border border-transparent  text-sm text-primary-black hover:border-[#d1d7dc] "
                            >
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: lecture.description,
                                    }}
                                ></div>
                            </div>
                        </div>
                    )}

                    {assetResources.length != 0 && (
                        <div className="mb-2 border-b border-[#d1d7dc]">
                            <div className="mb-2 text-sm font-bold text-primary-black">
                                Downloadable materials (only upload 2 materials
                                for each lecture)
                            </div>
                            <ul>
                                {assetResources.map((assetResource) => (
                                    <li
                                        key={assetResource.id}
                                        className="mb-2 flex items-center justify-between text-sm text-primary-black"
                                    >
                                        <div className="flex items-center gap-1">
                                            <PiFolderSimplePlus />
                                            <span>{assetResource.name}</span>
                                            <span>
                                                (
                                                {formatBytes(
                                                    assetResource.size,
                                                )}
                                                )
                                            </span>
                                        </div>
                                        <div
                                            onClick={() => {
                                                handleOpenDeleteResource(true);
                                                setAssetDelete(assetResource);
                                            }}
                                            className="cursor-pointer"
                                        >
                                            <FaTrash />
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    )}

                    {!assetVideoWatch && loading && (
                        <div className="mb-2 text-center">
                            <Spinner
                                aria-label="Extra large spinner example"
                                size="xl"
                            />
                        </div>
                    )}

                    {!lecture.description && (
                        <Button
                            onClick={() => setMode(MODE_LECTURE.OPEN_DESC)}
                            color="success"
                            className="mb-2 mr-2 max-h-[34px]"
                        >
                            <FaPlus className="mr-2 h-5 w-5" />
                            Description
                        </Button>
                    )}
                    {assetResources.length < 2 && (
                        <Button
                            onClick={() => setMode(MODE_LECTURE.OPEN_RESOURCE)}
                            color="success"
                            className="mr-2 max-h-[34px]"
                        >
                            <FaPlus className="mr-2 h-5 w-5" />
                            Recourses
                        </Button>
                    )}
                </div>
            )}

            {mode === MODE_LECTURE.OPEN_DESC && (
                <LectureDescForm
                    curriculumId={curriculumId}
                    description={lecture.description}
                    lectureID={lecture.id}
                    handleModeDescResource={handleModeDescResource}
                    handleUpdateLectureDesc={handleUpdateLectureDesc}
                />
            )}
            {mode === MODE_LECTURE.OPEN_RESOURCE && (
                <ResourceForm
                    curriculumId={curriculumId}
                    lectureId={lecture.id}
                    handleModeDescResource={handleModeDescResource}
                    handleSetLoading={handleSetLoading}
                    handleUploadResource={handleUploadResource}
                />
            )}
            {mode === MODE_LECTURE.OPEN_CONTENT && (
                <div className="relative border border-t-0 border-primary-black  p-2">
                    <div className="absolute -top-[37px] right-2 border border-b-0 border-primary-black">
                        <div className="relative bg-white py-2 pl-2 pr-[30px] text-sm font-bold text-primary-black">
                            Select type of content
                            <IoMdClose
                                onClick={() => setMode(MODE_LECTURE.NORMAL)}
                                className="absolute right-2 top-1/2 h-5 w-5 -translate-y-1/2 cursor-pointer"
                            />
                        </div>
                    </div>
                    <div className="flex justify-center gap-3">
                        <div
                            onClick={() => setMode(MODE_LECTURE.OPEN_VIDEO)}
                            className="group cursor-pointer overflow-hidden border border-primary-hover-gray"
                        >
                            <div className="relative">
                                <div className="h-[60px] w-[70px] bg-[#f7f9fa]  group-hover:bg-primary-black">
                                    <div className="absolute left-0 top-0 flex h-full w-full justify-center p-1 transition-transform duration-200 group-hover:-translate-y-full">
                                        <div className="relative  h-7 w-7 rounded-full bg-[#d1d7dc]">
                                            <HiPlay className="absolute left-1/2 top-1/2 -translate-x-1/2  -translate-y-1/2 text-white" />
                                        </div>
                                    </div>
                                    <div className="absolute left-0 top-0 flex h-full w-full translate-y-full justify-center p-1 transition-transform duration-200 group-hover:-translate-y-0">
                                        <div className="relative  h-7 w-7 rounded-full bg-white ">
                                            <HiPlay className="absolute left-1/2 top-1/2 -translate-x-1/2  -translate-y-1/2 text-black" />
                                        </div>
                                    </div>
                                    <span className="absolute bottom-0 left-0  h-[16px] w-full bg-[#d1d7dc] text-center text-[10px] text-primary-black group-hover:bg-primary-black group-hover:text-white">
                                        Video
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div
                            onClick={() => setMode(MODE_LECTURE.OPEN_ARTICLE)}
                            className="group cursor-pointer overflow-hidden border border-primary-hover-gray"
                        >
                            <div className="relative">
                                <div className="h-[60px] w-[70px] bg-[#f7f9fa]  group-hover:bg-primary-black">
                                    <div className="absolute left-0 top-0 flex h-full w-full justify-center p-1 transition-transform duration-200 group-hover:-translate-y-full">
                                        <AiOutlineFile className="h-7 w-7 text-[#d1d7dc]" />
                                    </div>
                                    <div className="absolute left-0 top-0 flex h-full w-full translate-y-full justify-center p-1 transition-transform duration-200 group-hover:-translate-y-0">
                                        <AiOutlineFile className="h-7 w-7 text-white" />
                                    </div>
                                    <span className="absolute bottom-0 left-0  h-[16px] w-full bg-[#d1d7dc] text-center text-[10px] text-primary-black group-hover:bg-primary-black group-hover:text-white">
                                        Article
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            {mode === MODE_LECTURE.OPEN_VIDEO && (
                <VideoForm
                    curriculumId={curriculumId}
                    lectureId={lecture.id}
                    assetVideoWatch={assetVideoWatch}
                    handleSetLoading={handleSetLoading}
                    handleModeDescResource={handleModeDescResource}
                    handleModeNormal={handleModeNormal}
                    handleUploadVideoWatch={handleUploadVideoWatch}
                    handleReplaceVideoWatch={handleReplaceVideoWatch}
                />
            )}
            {mode === MODE_LECTURE.OPEN_ARTICLE && (
                <ArticleForm
                    curriculumId={curriculumId}
                    lectureID={lecture.id}
                    article={lecture.article}
                    handleModeNormal={handleModeNormal}
                    handleUpdateLectureArticle={handleUpdateLectureArticle}
                    handleModeDescResource={handleModeDescResource}
                />
            )}
        </div>
    );
}
