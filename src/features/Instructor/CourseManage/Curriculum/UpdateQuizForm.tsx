import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { AiFillCheckCircle } from "react-icons/ai";
import { FaRegQuestionCircle } from "react-icons/fa";
import Editor from "../../../../components/ui/core/Editor";
import { CreateQuiz, Lecture, UpdateQuiz } from "../../../../models";
import { schemeAddQuiz, schemeUpdateQuiz } from "../../../../validators/course";

export interface IUpdateQuizFormProps {
    curriculumId: number;
    quizIndex: number;
    quizEdit: Lecture;
    handleModeNormal: () => void;
    handleUpdateQuiz: (data: UpdateQuiz) => void;
    // handleAddQuiz: (data: CreateQuiz) => void;
    // handleFinishAdd: () => void;
}

export default function UpdateQuizForm(props: IUpdateQuizFormProps) {
    const {
        curriculumId,
        quizIndex,
        quizEdit,
        handleModeNormal,
        handleUpdateQuiz,
        // handleFinishAdd,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
    } = useForm<UpdateQuiz>({
        mode: "onSubmit",
        defaultValues: {
            curriculum_id: curriculumId,
            title: quizEdit.title,
            lecture_id: quizEdit.id,
            description: quizEdit.description,
        },
        resolver: yupResolver(schemeUpdateQuiz),
    });

    const handleSubmitQuiz = (data: UpdateQuiz) => {
        // handleAddQuiz(data);
        handleUpdateQuiz(data);
        handleModeNormal();
        // handleFinishAdd();
        // reset();
    };
    const handleSetHTML = (value: string) => {
        setValue("description", value);
    };

    return (
        <div className="border border-primary-black p-2">
            <form onSubmit={handleSubmit(handleSubmitQuiz)}>
                <div className="flex gap-2">
                    <div className="flex h-[42px] items-center text-base text-primary-black">
                        <AiFillCheckCircle className="mr-1" />
                        <span>Quiz {quizIndex}:</span>
                    </div>
                    <div className="flex-1">
                        <div className="mb-4">
                            <TextInput
                                type="text"
                                maxLength={80}
                                placeholder="Enter a title"
                                color={errors.title ? "failure" : ""}
                                {...register("title")}
                                helperText={
                                    <>
                                        {errors.title ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {errors.title.message}
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            />
                        </div>
                        <Editor
                            placeholder="Quiz description"
                            // handleSetValue={handleSetResponse}
                            defaultValue={
                                quizEdit.description ? quizEdit.description : ""
                            }
                            handleSetHTML={handleSetHTML}
                            isImage={false}
                        />
                        {errors.description ? (
                            <div className="mt-2 text-sm text-red-700">
                                <span className="font-medium ">Oops! </span>
                                <span>{errors.description.message}</span>
                            </div>
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
                <div className="mt-4 flex justify-end gap-2">
                    <Button
                        onClick={() => {
                            handleModeNormal();
                        }}
                        color="gray"
                    >
                        Cancel
                    </Button>
                    <Button type="submit">Save Quiz</Button>
                </div>
            </form>
        </div>
    );
}
