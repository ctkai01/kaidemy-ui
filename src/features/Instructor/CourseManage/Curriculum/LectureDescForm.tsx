import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { AiFillCheckCircle } from "react-icons/ai";
import { FaRegQuestionCircle } from "react-icons/fa";
import Editor from "../../../../components/ui/core/Editor";
import {
    CreateQuiz,
    Lecture,
    UpdateLecture,
    UpdateLectureDesc,
    UpdateQuiz,
} from "../../../../models";
import {
    schemeAddQuiz,
    schemeUpdateLecture,
    schemeUpdateLectureDesc,
    schemeUpdateQuiz,
} from "../../../../validators/course";

export interface ILectureDescFormProps {
    curriculumId: number;
    // lectureIndex: number;
    lectureID: number;
    description: string | null;
    handleModeDescResource: () => void;
    handleUpdateLectureDesc: (data: UpdateLectureDesc) => void;
    // handleUpdateQuiz: (data: UpdateQuiz) => void
    // handleAddQuiz: (data: CreateQuiz) => void;
    // handleFinishAdd: () => void;
}

export default function LectureDescForm(props: ILectureDescFormProps) {
    const {
        curriculumId,
        description,
        lectureID,
        handleModeDescResource,
        handleUpdateLectureDesc,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
    } = useForm<UpdateLectureDesc>({
        mode: "onChange",
        defaultValues: {
            curriculum_id: curriculumId,
            description: description ? description : "",
            lecture_id: lectureID,
        },
        resolver: yupResolver(schemeUpdateLectureDesc),
    });

    const handleSubmitLectureDesc = (data: UpdateLectureDesc) => {
        // handleAddQuiz(data);
        // handleUpdateQuiz(data)
        // handleUpdateLecture(data);
        // handleModeNormal();
        handleUpdateLectureDesc(data);
        handleModeDescResource();
        // handleFinishAdd();
        // reset();
    };

    const handleSetHTML = (value: string) => {
        setValue("description", value);
    };

    return (
        <div className="border border-t-0 border-primary-black p-2">
            <form onSubmit={handleSubmit(handleSubmitLectureDesc)}>
                <div className="pb-2 text-sm font-bold text-primary-black">
                    Lecture description
                </div>
                <Editor
                    placeholder="Add a description. Include what students will be able to do after completing the lecture."
                    defaultValue={description ? description : ""}
                    // handleSetValue={handleSetResponse}
                    handleSetHTML={handleSetHTML}
                    isImage={false}
                />
                {errors.description ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium">Oops! </span>
                        <span>{errors.description.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="mt-4 flex justify-end gap-2">
                    <Button
                        onClick={() => handleModeDescResource()}
                        color="gray"
                    >
                        Cancel
                    </Button>
                    <Button type="submit">Save</Button>
                </div>
            </form>
        </div>
    );
}
