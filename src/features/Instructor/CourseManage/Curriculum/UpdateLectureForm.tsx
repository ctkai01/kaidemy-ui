import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { AiFillCheckCircle } from "react-icons/ai";
import { FaRegQuestionCircle } from "react-icons/fa";
import Editor from "../../../../components/ui/core/Editor";
import {
    CreateQuiz,
    Lecture,
    UpdateLecture,
    UpdateQuiz,
} from "../../../../models";
import {
    schemeAddQuiz,
    schemeUpdateLecture,
    schemeUpdateQuiz,
} from "../../../../validators/course";

export interface IUpdateLectureFormProps {
    curriculumId: number;
    lectureIndex: number;
    lectureEdit: Lecture;
    handleModeNormal: () => void;
    handleUpdateLecture: (data: UpdateLecture) => void;
    // handleUpdateQuiz: (data: UpdateQuiz) => void
    // handleAddQuiz: (data: CreateQuiz) => void;
    // handleFinishAdd: () => void;
}

export default function UpdateLectureForm(props: IUpdateLectureFormProps) {
    const {
        curriculumId,
        lectureIndex,
        lectureEdit,
        handleModeNormal,
        handleUpdateLecture,
        // handleUpdateQuiz,
        // handleFinishAdd,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
    } = useForm<UpdateLecture>({
        mode: "onSubmit",
        defaultValues: {
            curriculum_id: curriculumId,
            title: lectureEdit.title,
            lecture_id: lectureEdit.id,
        },
        resolver: yupResolver(schemeUpdateLecture),
    });

    const handleSubmitQuiz = (data: UpdateQuiz) => {
        // handleAddQuiz(data);
        // handleUpdateQuiz(data)
        handleUpdateLecture(data);
        handleModeNormal();
        // handleFinishAdd();
        // reset();
    };

    return (
        <div className="border border-primary-black p-2">
            <form onSubmit={handleSubmit(handleSubmitQuiz)}>
                <div className="flex gap-2">
                    <div className="flex h-[42px] items-center text-base text-primary-black">
                        <AiFillCheckCircle className="mr-1" />
                        <span>Lecture {lectureIndex}:</span>
                    </div>
                    <div className="flex-1">
                        <div className="mb-4">
                            <TextInput
                                type="text"
                                maxLength={80}
                                placeholder="Enter a Title"
                                color={errors.title ? "failure" : ""}
                                {...register("title")}
                                helperText={
                                    <>
                                        {errors.title ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {errors.title.message}
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            />
                        </div>
                    </div>
                </div>
                <div className="mt-4 flex justify-end gap-2">
                    <Button
                        onClick={() => {
                            handleModeNormal();
                        }}
                        color="gray"
                    >
                        Cancel
                    </Button>
                    <Button type="submit">Save lecture</Button>
                </div>
            </form>
        </div>
    );
}
