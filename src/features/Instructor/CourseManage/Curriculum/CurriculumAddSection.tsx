import { Button } from "flowbite-react";
import * as React from "react";
import { FaPlus } from "react-icons/fa";
import { IoClose } from "react-icons/io5";
import { CreateLecture, CreateQuiz } from "../../../../models";
import AddLecture from "./AddLecture";
import AddQuiz from "./AddQuiz";

export interface ICurriculumAddSectionProps {
    curriculumId: number;
    handleAddLecture: (data: CreateLecture) => void;
    handleAddQuiz: (data: CreateQuiz) => void;
}

export default function CurriculumAddSection(
    props: ICurriculumAddSectionProps,
) {
    const [openAdd, setOpenAdd] = React.useState(false);
    const [modeCurriculum, setModeCurriculum] = React.useState(0);

    const { curriculumId, handleAddLecture, handleAddQuiz } = props;

    const handleSetModeNormal = () => {
        setModeCurriculum(0);
    };

    const handleFinishAdd = () => {
        setOpenAdd(false);
    };

    return (
        <div className="relative">
            {!openAdd && (
                <div onClick={() => setOpenAdd(true)}>
                    <Button className="">
                        <FaPlus className="mr-2 h-5 w-5" />
                        Curriculum lecture
                    </Button>
                </div>
            )}

            {openAdd && modeCurriculum == 0 && (
                <div className="relative border border-dashed border-[#6a6f73] bg-white px-4 py-2">
                    <div className="flex gap-2">
                        <Button outline onClick={() => setModeCurriculum(1)}>
                            <FaPlus className="mr-2 h-5 w-5" />
                            Lecture
                        </Button>
                        <Button outline onClick={() => setModeCurriculum(2)}>
                            <FaPlus className="mr-2 h-5 w-5" />
                            Quiz
                        </Button>
                    </div>
                </div>
            )}
            {openAdd && (
                <IoClose
                    onClick={() => {
                        setOpenAdd(false);
                        handleSetModeNormal();
                    }}
                    className="absolute -left-6 -top-5 h-5 w-5 cursor-pointer"
                />
            )}
            {openAdd && modeCurriculum == 1 && (
                <AddLecture
                    curriculumId={curriculumId}
                    handleAddLecture={handleAddLecture}
                    handleSetModeNormal={handleSetModeNormal}
                    handleFinishAdd={handleFinishAdd}
                />
            )}

            {openAdd && modeCurriculum == 2 && (
                <AddQuiz
                    curriculumId={curriculumId}
                    handleAddQuiz={handleAddQuiz}
                    handleFinishAdd={handleFinishAdd}
                    handleSetModeNormal={handleSetModeNormal}
                />
            )}
        </div>
    );
}
