import { Button, Modal } from "flowbite-react";
import React, { useState } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { FaPlus, FaRegQuestionCircle, FaTrash } from "react-icons/fa";
import { HiOutlineExclamationCircle } from "react-icons/hi2";
import { IoIosArrowDown, IoIosArrowUp, IoMdClose } from "react-icons/io";
import { IoPencil } from "react-icons/io5";
import {
    AnswerQuestion,
    Lecture,
    Question,
    UpdateAnswer,
    UpdateQuestion,
    UpdateQuiz,
} from "../../../../models";
import AddQuestion from "./AddQuestion";
import UpdateQuizForm from "./UpdateQuizForm";
// import { Lecture } from "./SectionItem";

export interface IQuizItemProps {
    lecture: Lecture;
    index: number;
    curriculumId: number;
    handleAddQuestionAnswer: (data: AnswerQuestion) => void;
    handleUpdateQuestionAnswer: (
        updateQuestion: UpdateQuestion,
        updateAnswers: UpdateAnswer[],
    ) => void;

    handleDeleteQuestion: (
        lectureID: number,
        curriculumID: number,
        questionID: number,
    ) => void;
    handleUpdateQuiz: (data: UpdateQuiz) => void;
    handleDeleteQuiz: (lectureID: number, curriculumID: number) => void;
}
const MODE_QUIZ = {
    NORMAL: 0,
    OPEN_QUESTION_TYPE: 1,
    ADD_QUESTION: 2,
    SHOW_NORMAL: 3,
    SHOW_UP: 4,
    EDIT: 5,
};
export default function QuizItem(props: IQuizItemProps) {
    const {
        lecture,
        index,
        curriculumId,
        handleAddQuestionAnswer,
        handleUpdateQuestionAnswer,
        handleDeleteQuestion,
        handleUpdateQuiz,
        handleDeleteQuiz,
    } = props;
    const [mode, setMode] = React.useState(
        lecture.questions && lecture.questions.length
            ? MODE_QUIZ.SHOW_NORMAL
            : MODE_QUIZ.NORMAL,
    );
    const [openModal, setOpenModal] = useState(false);
    const [openModalDelete, setOpenModalDelete] = useState(false);
    const [questionEdit, setQuestionEdit] = useState<Question | null>(null);
    const [quizEdit, setQuizEdit] = useState<Lecture | null>(null);
    const [questionIdDelete, setQuestionIdDelete] = useState<number | null>(
        null,
    );
    // const [lectureIdDelete, setLectureIdDelete] = useState<number | null>(
    //     null,
    // );
    const handleQuestionEdit = (question: Question | null) => {
        setQuestionEdit(question);
    };

    const handleModeShowUp = () => {
        setMode(MODE_QUIZ.SHOW_UP);
    };

    const handleModeNormal = () => {
        setMode(MODE_QUIZ.NORMAL);
    };

    const handleDelQuestion = () => {
        setOpenModal(false);
        if (questionIdDelete) {
            handleDeleteQuestion(lecture.id, curriculumId, questionIdDelete);
        }
        setQuestionEdit(null);
    };

    const handleDelQuiz = () => {
        setOpenModalDelete(false);
        handleDeleteQuiz(lecture.id, curriculumId);
    };
    console.log("Lecture: ", lecture);
    return (
        <div>
            <Modal
                show={openModal}
                size="md"
                onClose={() => setOpenModal(false)}
                popup
            >
                <Modal.Header />
                <Modal.Body>
                    <div className="text-center">
                        <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
                        <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                            Are you sure you want to delete this question?
                        </h3>
                        <div className="flex justify-center gap-4">
                            <Button
                                color="failure"
                                onClick={() => handleDelQuestion()}
                            >
                                {"Yes, I'm sure"}
                            </Button>
                            <Button
                                color="gray"
                                onClick={() => setOpenModal(false)}
                            >
                                No, cancel
                            </Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>

            {/* Delete quiz modal */}
            <Modal
                show={openModalDelete}
                size="md"
                onClose={() => setOpenModalDelete(false)}
                popup
            >
                <Modal.Header />
                <Modal.Body>
                    <div className="text-center">
                        <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
                        <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                            Are you sure you want to delete this quiz?
                        </h3>
                        <div className="flex justify-center gap-4">
                            <Button
                                color="failure"
                                onClick={() => handleDelQuiz()}
                            >
                                {"Yes, I'm sure"}
                            </Button>
                            <Button
                                color="gray"
                                onClick={() => setOpenModalDelete(false)}
                            >
                                No, cancel
                            </Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>

            {mode != MODE_QUIZ.EDIT && (
                <div className="group flex justify-between border border-primary-black px-2 py-[11px]">
                    <div className="flex items-center">
                        <AiFillCheckCircle className="mr-1" />
                        <span className="text-base text-primary-black">
                            Lecture {index}:{" "}
                        </span>
                        <div className="ml-2 flex items-center">
                            <FaRegQuestionCircle className="mr-1" />
                            <span>{lecture.title}</span>
                        </div>
                        <div
                            onClick={() => {
                                setMode(MODE_QUIZ.EDIT);
                                setQuizEdit(lecture);
                            }}
                            className="mx-2 hidden cursor-pointer group-hover:block"
                        >
                            <IoPencil />
                        </div>
                        <div
                            onClick={() => {
                                setOpenModalDelete(true);
                            }}
                            className="mx-2 hidden  cursor-pointer group-hover:block"
                        >
                            <FaTrash />
                        </div>
                    </div>

                    <div className="flex items-center">
                        {mode !== MODE_QUIZ.OPEN_QUESTION_TYPE &&
                            mode !== MODE_QUIZ.ADD_QUESTION &&
                            mode !== MODE_QUIZ.SHOW_NORMAL &&
                            mode !== MODE_QUIZ.SHOW_UP &&
                            mode !== MODE_QUIZ.EDIT && (
                                <Button
                                    onClick={() => {
                                        setMode(MODE_QUIZ.OPEN_QUESTION_TYPE);
                                    }}
                                    color="success"
                                    className="mr-4 max-h-[34px]"
                                >
                                    <FaPlus className="mr-2 h-5 w-5" />
                                    Question
                                </Button>
                            )}
                        {mode === MODE_QUIZ.SHOW_NORMAL && (
                            <IoIosArrowDown
                                className="cursor-pointer"
                                onClick={() => setMode(MODE_QUIZ.SHOW_UP)}
                            />
                        )}
                        {mode === MODE_QUIZ.SHOW_UP && (
                            <IoIosArrowUp
                                className="cursor-pointer"
                                onClick={() => setMode(MODE_QUIZ.SHOW_NORMAL)}
                            />
                        )}
                    </div>
                </div>
            )}

            {mode === MODE_QUIZ.EDIT && quizEdit && (
                <UpdateQuizForm
                    curriculumId={curriculumId}
                    handleModeNormal={handleModeNormal}
                    handleUpdateQuiz={handleUpdateQuiz}
                    quizIndex={index}
                    quizEdit={quizEdit}
                />
            )}

            {mode === MODE_QUIZ.OPEN_QUESTION_TYPE && (
                <div className="relative border border-t-0 border-primary-black  p-2">
                    <div className="absolute -top-[37px] right-2 border border-b-0 border-primary-black">
                        <div className="relative bg-white py-2 pl-2 pr-[30px] text-sm font-bold text-primary-black">
                            Select type of question
                            <IoMdClose
                                onClick={() => {
                                    setMode(
                                        lecture.questions.length
                                            ? MODE_QUIZ.SHOW_NORMAL
                                            : MODE_QUIZ.NORMAL,
                                    );
                                }}
                                className="absolute right-2 top-1/2 h-5 w-5 -translate-y-1/2 cursor-pointer"
                            />
                        </div>
                    </div>
                    <div className="flex justify-center gap-3">
                        <div
                            onClick={() => setMode(MODE_QUIZ.ADD_QUESTION)}
                            className="group cursor-pointer overflow-hidden border border-primary-hover-gray"
                        >
                            <div className="relative">
                                <div className="h-[60px] w-[90px] bg-[#f7f9fa]  group-hover:bg-primary-black">
                                    <div className="absolute left-0 top-0 flex h-full w-full justify-center p-1 transition-transform duration-200 group-hover:-translate-y-full">
                                        <FaRegQuestionCircle className="absolute h-7 w-7 text-[#d1d7dc]" />
                                    </div>
                                    <div className="absolute left-0 top-0 flex h-full w-full translate-y-full justify-center p-1 transition-transform duration-200 group-hover:-translate-y-0">
                                        <FaRegQuestionCircle className="absolute h-7 w-7 text-white" />
                                    </div>
                                    <span className="absolute bottom-0 left-0  h-[16px] w-full bg-[#d1d7dc] text-center text-[10px] text-primary-black group-hover:bg-primary-black group-hover:text-white">
                                        Multiple-choice
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}

            {mode === MODE_QUIZ.ADD_QUESTION && (
                <div className="relative border border-t-0 border-primary-black  p-2">
                    <div className="absolute -top-[37px] right-2 border border-b-0 border-primary-black">
                        <div className=" relative bg-white py-2 pl-2 pr-[30px] text-sm font-bold text-primary-black">
                            {questionEdit
                                ? "Update Mutiple Choice"
                                : "Add Mutiple Choice"}
                            <IoMdClose
                                onClick={() =>
                                    setMode(
                                        lecture.questions &&
                                            lecture.questions.length
                                            ? MODE_QUIZ.SHOW_NORMAL
                                            : MODE_QUIZ.NORMAL,
                                    )
                                }
                                className="absolute right-2 top-1/2 h-5 w-5 -translate-y-1/2 cursor-pointer"
                            />
                        </div>
                    </div>
                    <AddQuestion
                        lectureID={lecture.id}
                        curriculumId={curriculumId}
                        questionEdit={questionEdit}
                        handleModeShowUp={handleModeShowUp}
                        handleQuestionEdit={handleQuestionEdit}
                        handleUpdateQuestionAnswer={handleUpdateQuestionAnswer}
                        handleAddQuestionAnswer={handleAddQuestionAnswer}
                    />
                </div>
            )}

            {mode === MODE_QUIZ.SHOW_UP && (
                <div className="relative border border-t-0 border-primary-black  p-2">
                    <div className="flex items-center">
                        <span className="mr-2 text-base font-bold text-primary-black">
                            Câu hỏi
                        </span>
                        <Button
                            onClick={() => {
                                setMode(MODE_QUIZ.OPEN_QUESTION_TYPE);
                            }}
                            color="success"
                            className="mr-4 max-h-[34px]"
                        >
                            New question
                        </Button>
                    </div>
                    <ul>
                        {lecture.questions &&
                            lecture.questions.map((question, i) => (
                                <div
                                    key={question.id}
                                    className="group mt-4 flex justify-between whitespace-nowrap text-sm text-primary-black"
                                >
                                    <div className="flex">
                                        <span className="mr-1 font-bold">
                                            {i + 1}.
                                        </span>
                                        <div className="max-w-[600px] ">
                                            <div
                                                className=" overflow-hidden text-ellipsis whitespace-nowrap"
                                                dangerouslySetInnerHTML={{
                                                    __html: question.title,
                                                }}
                                            ></div>
                                        </div>

                                        <span className="ml-2">
                                            Multiple Choice
                                        </span>
                                    </div>
                                    <div className="flex items-center">
                                        <div className="mx-2 cursor-pointer opacity-0 group-hover:opacity-100">
                                            <IoPencil
                                                onClick={() => {
                                                    handleQuestionEdit(
                                                        question,
                                                    );
                                                    setMode(
                                                        MODE_QUIZ.ADD_QUESTION,
                                                    );
                                                }}
                                            />
                                        </div>
                                        <div
                                            onClick={() => {
                                                setOpenModal(true);
                                                setQuestionIdDelete(
                                                    question.id,
                                                );
                                            }}
                                            className="mx-2  cursor-pointer opacity-0 group-hover:opacity-100"
                                        >
                                            <FaTrash />
                                        </div>
                                    </div>
                                </div>
                            ))}
                    </ul>
                </div>
            )}
        </div>
    );
}
