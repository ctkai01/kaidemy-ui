import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import Editor from "../../../../components/ui/core/Editor";
import { CreateQuiz } from "../../../../models";
import { schemeAddQuiz } from "../../../../validators/course";

export interface IAddQuizProps {
    curriculumId: number;
    handleSetModeNormal: () => void;
    handleAddQuiz: (data: CreateQuiz) => void;
    handleFinishAdd: () => void;
}

export default function AddQuiz(props: IAddQuizProps) {
    const {
        curriculumId,
        handleSetModeNormal,
        handleAddQuiz,
        handleFinishAdd,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm<CreateQuiz>({
        mode: "onSubmit",
        defaultValues: {
            curriculum_id: curriculumId,
        },
        resolver: yupResolver(schemeAddQuiz),
    });
    // const handleSetResponse = (value: string) => {};
    const handleSubmitAddQuiz = (data: CreateQuiz) => {
        handleAddQuiz(data);
        handleFinishAdd();
        reset();
    };
    const handleSetHTML = (value: string) => {
        setValue("description", value);
    };

    return (
        <div className="border border-primary-black p-2">
            <form onSubmit={handleSubmit(handleSubmitAddQuiz)}>
                <div className="flex  gap-2">
                    <div className="flex h-[42px] items-center text-base text-primary-black">
                        New quiz:
                    </div>
                    <div className="flex-1">
                        <div className="mb-4">
                            <TextInput
                                type="text"
                                maxLength={80}
                                placeholder="Nhập tiêu đề"
                                color={errors.title ? "failure" : ""}
                                {...register("title")}
                                helperText={
                                    <>
                                        {errors.title ? (
                                            <>
                                                <span className="font-medium">
                                                    Oops!{" "}
                                                </span>
                                                <span>
                                                    {errors.title.message}
                                                </span>
                                            </>
                                        ) : (
                                            <></>
                                        )}
                                    </>
                                }
                            />
                        </div>
                        <Editor
                            defaultValue={""}
                            placeholder="Description"
                            // handleSetValue={handleSetResponse}
                            handleSetHTML={handleSetHTML}
                            isImage={false}
                        />
                        {errors.description ? (
                            <div className="mt-2 text-sm text-red-700">
                                <span className="font-medium ">Oops! </span>
                                <span>{errors.description.message}</span>
                            </div>
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
                <div className="mt-4 flex justify-end gap-2">
                    <Button onClick={() => handleSetModeNormal()} color="gray">
                        Cancel
                    </Button>
                    <Button type="submit">Add quiz</Button>
                </div>
            </form>
        </div>
    );
}
