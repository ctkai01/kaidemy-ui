import { yupResolver } from "@hookform/resolvers/yup";
import { Button } from "flowbite-react";
import { useForm } from "react-hook-form";
import { IoMdClose } from "react-icons/io";
import Editor from "../../../../components/ui/core/Editor";
import { UpdateLectureArticle } from "../../../../models";
import { schemeUpdateLectureArticle } from "../../../../validators/course";

export interface IArticleFormProps {
    curriculumId: number;
    // lectureIndex: number;
    lectureID: number;
    article: string | null;
    handleModeNormal: () => void;
    handleModeDescResource: () => void;
    handleUpdateLectureArticle: (data: UpdateLectureArticle) => void;
    // handleUpdateQuiz: (data: UpdateQuiz) => void
    // handleAddQuiz: (data: CreateQuiz) => void;
    // handleFinishAdd: () => void;
}

export default function ArticleForm(props: IArticleFormProps) {
    const {
        curriculumId,
        article,
        lectureID,
        handleModeNormal,
        handleModeDescResource,
        handleUpdateLectureArticle,
        // handleUpdateLectureDesc,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
    } = useForm<UpdateLectureArticle>({
        mode: "onChange",
        defaultValues: {
            curriculum_id: curriculumId,
            article: article ? article : "",
            lecture_id: lectureID,
        },
        resolver: yupResolver(schemeUpdateLectureArticle),
    });

    const handleSubmitLectureDesc = (data: UpdateLectureArticle) => {

        handleUpdateLectureArticle(data);
        handleModeDescResource();

        // handleAddQuiz(data);
        // handleUpdateQuiz(data)
        // handleUpdateLecture(data);
        // handleModeNormal();
        // handleUpdateLectureDesc(data)
        // handleModeDescResource()
        // handleFinishAdd();
        // reset();
    };

    const handleSetHTML = (value: string) => {
        setValue("article", value);
    };
    return (
        <div className="relative border border-t-0 border-primary-black p-2">
            <form onSubmit={handleSubmit(handleSubmitLectureDesc)}>
                <div className="absolute -top-[37px] right-2 border border-b-0 border-primary-black">
                    <div className=" relative bg-white py-2 pl-2 pr-[30px] text-sm font-bold text-primary-black">
                        Thêm bài viết
                        <IoMdClose
                            onClick={() => handleModeDescResource()}
                            className="absolute right-2 top-1/2 h-5 w-5 -translate-y-1/2 cursor-pointer"
                        />
                    </div>
                </div>
                <div className="pb-2 text-sm font-bold text-primary-black">
                    Chữ
                </div>
                <Editor
                    placeholder=""
                    defaultValue={article ? article : ""}
                    handleSetHTML={handleSetHTML}
                    isImage={false}
                />
                {errors.article ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium">Oops! </span>
                        <span>{errors.article.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="mt-4 flex justify-end gap-2">
                    <Button type="submit">Save</Button>
                </div>
            </form>
        </div>
    );
}
