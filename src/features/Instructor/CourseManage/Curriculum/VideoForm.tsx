import { yupResolver } from "@hookform/resolvers/yup";
import { Button, FileInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { IoMdClose } from "react-icons/io";
import { Asset, UpdateVideoWatch } from "../../../../models";
import { schemeUpdateVideoWatch } from "../../../../validators/course";

export interface IVideoFormProps {
    curriculumId: number;
    lectureId: number;
    assetVideoWatch?: Asset;
    handleModeNormal: () => void;
    handleModeDescResource: () => void;
    handleSetLoading: (command: boolean) => void;
    handleUploadVideoWatch: (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => void;
    handleReplaceVideoWatch: (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => void;
}

export default function VideoForm(props: IVideoFormProps) {
    const {
        curriculumId,
        lectureId,
        assetVideoWatch,
        handleModeNormal,
        handleUploadVideoWatch,
        handleSetLoading,
        handleModeDescResource,
        handleReplaceVideoWatch,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
        reset,
    } = useForm<UpdateVideoWatch>({
        mode: "onChange",
        defaultValues: {
            curriculum_id: curriculumId,
            lecture_id: lectureId,
        },
        resolver: yupResolver(schemeUpdateVideoWatch),
    });

    const handleUploadVideoW = (data: UpdateVideoWatch) => {
        if (!assetVideoWatch) {
            handleUploadVideoWatch(data, handleSetLoading);
        } else {
            handleReplaceVideoWatch(data, handleSetLoading);
        }

        handleModeDescResource();
    };

    return (
        <div className="relative border border-t-0 border-primary-black  p-2">
            <form onSubmit={handleSubmit(handleUploadVideoW)}>
                <div className="absolute -top-[37px] right-2 border border-b-0 border-primary-black">
                    <div className=" relative bg-white py-2 pl-2 pr-[30px] text-sm font-bold text-primary-black">
                        Add video
                        <IoMdClose
                            onClick={() => handleModeNormal()}
                            className="absolute right-2 top-1/2 h-5 w-5 -translate-y-1/2 cursor-pointer"
                        />
                    </div>
                </div>
                <FileInput {...register("video")} />
                <div>
                    <span className="text-xs text-[#6a6f73]">
                        <b>Notes:</b>All files must be at least 720p and less
                        than 500 MB.
                    </span>
                </div>
                {errors.video ? (
                    <div className="text-sm text-red-700">
                        <span className="font-medium">Oops! </span>
                        <span>{errors.video.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="flex justify-end">
                    <Button type="submit">
                        {assetVideoWatch ? "Replace" : "Upload"}
                    </Button>
                </div>
            </form>
        </div>
    );
}
