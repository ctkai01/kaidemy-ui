import { Button } from "flowbite-react";
import * as React from "react";
import { AiOutlineFile } from "react-icons/ai";
import { FaPlus, FaTrash } from "react-icons/fa";
import { IoPencil } from "react-icons/io5";
import { LECTURE_TYPE } from "../../../../constants";
import {
    AnswerQuestion,
    CreateLecture,
    CreateQuiz,
    UpdateAnswer,
    UpdateLecture,
    UpdateLectureArticle,
    UpdateLectureDesc,
    UpdateQuestion,
    UpdateQuiz,
    UpdateResource,
    UpdateSection,
    UpdateVideoWatch,
} from "../../../../models";
import { Curriculum } from "./Curriculum";
import CurriculumAddSection from "./CurriculumAddSection";
import DeleteSectionModal from "./DeleteSectionModal";
import LectureItem from "./LectureItem";
import QuizItem from "./QuizItem";
import SectionEditForm from "./SectionEditForm";

export interface ISectionItemProps {
    curriculum: Curriculum;
    index: number;
    handleAddLecture: (data: CreateLecture) => void;
    handleAddQuiz: (data: CreateQuiz) => void;
    handleAddQuestionAnswer: (data: AnswerQuestion) => void;

    handleUpdateQuestionAnswer: (
        updateQuestion: UpdateQuestion,
        updateAnswers: UpdateAnswer[],
    ) => void;
    handleDeleteQuestion: (
        lectureID: number,
        curriculumID: number,
        questionID: number,
    ) => void;
    handleUpdateQuiz: (data: UpdateQuiz) => void;
    handleUpdateLecture: (data: UpdateLecture) => void;
    handleUpdateLectureDesc: (data: UpdateLectureDesc) => void;
    handleUpdateLectureArticle: (data: UpdateLectureArticle) => void;
    handleDeleteQuiz: (lectureID: number, curriculumID: number) => void;
    handleDeleteLecture: (lectureID: number, curriculumID: number) => void;
    handleUploadVideoWatch: (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => void;
    handleReplaceVideoWatch: (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => void;
    handleUploadResource: (
        data: UpdateResource,
        setLoading: (command: boolean) => void,
    ) => void;
    handleDeleteResource: (
        lectureID: number,
        curriculumID: number,
        assetID: number,
    ) => void;
    handleUpdateLectureIsPromotional: (
        isCheck: boolean,
        curriculumId: number,
        lectureId: number,
    ) => void;
    handleUpdateSection: (data: UpdateSection) => void;
    handleDeleteSection: (curriculumID: number) => void;
}

const MODE_SECTION = {
    NORMAL: 0,
    OPEN_EDIT: 1,
};

export default function SectionItem(props: ISectionItemProps) {
    const {
        curriculum,
        index,
        handleAddLecture,
        handleAddQuiz,
        handleAddQuestionAnswer,
        handleUpdateQuestionAnswer,
        handleDeleteQuestion,
        handleUpdateQuiz,
        handleDeleteQuiz,
        handleUpdateLecture,
        handleDeleteLecture,
        handleUploadVideoWatch,
        handleReplaceVideoWatch,
        handleUpdateLectureDesc,
        handleUpdateLectureArticle,
        handleUploadResource,
        handleDeleteResource,
        handleUpdateLectureIsPromotional,
        handleUpdateSection,
        handleDeleteSection,
    } = props;
    // const [lectures, setLectures] = React.useState<Lecture[]>([]);
    let lectureCount = 0;
    let quizCount = 0;
    const [modeSection, setModeSection] = React.useState(MODE_SECTION.NORMAL);
    const [openModalDeleteSection, setOpenModalDeleteSection] =
        React.useState(false);
    // const handleAddLecture = (newLecture: Lecture) => {
    //   setLectures(lectures => [...lectures, newLecture])
    // }
    // console.log("Lectures: ", lectures)
    const handleNormalSectionMode = () => {
        setModeSection(MODE_SECTION.NORMAL);
    };

    const handleCommandModalDeleteSection = (command: boolean) => {
        setOpenModalDeleteSection(command);
    };
    return (
        <div className="mt-16 border border-primary-black bg-[#f7f9fa] pb-6 first:mt-0">
            <DeleteSectionModal
                curriculumID={curriculum.id}
                openModal={openModalDeleteSection}
                handleCommandModal={handleCommandModalDeleteSection}
                handleDeleteSection={handleDeleteSection}
            />
            {modeSection === MODE_SECTION.NORMAL && (
                <div className="group flex items-center px-2 py-[19px] text-primary-black">
                    <span className="text-base font-bold text-primary-black">
                        Section {index + 1}:
                    </span>
                    <span className="ml-2 flex items-center">
                        <AiOutlineFile className="mr-1" />
                        <span>{curriculum.title}</span>
                    </span>
                    <div
                        onClick={() => setModeSection(MODE_SECTION.OPEN_EDIT)}
                        className="mx-2 hidden cursor-pointer group-hover:block"
                    >
                        <IoPencil />
                    </div>
                    <div
                        onClick={() => handleCommandModalDeleteSection(true)}
                        className="mx-2  hidden cursor-pointer group-hover:block"
                    >
                        <FaTrash />
                    </div>
                </div>
            )}

            {modeSection === MODE_SECTION.OPEN_EDIT && (
                <SectionEditForm
                    handleUpdateSection={handleUpdateSection}
                    index={index}
                    curriculum={curriculum}
                    handleNormalSectionMode={handleNormalSectionMode}
                />
            )}

            <div className="flex flex-col gap-4 pl-[60px] pr-2">
                {curriculum.lectures &&
                    curriculum.lectures.map((lecture, _) => {
                        if (lecture.type === LECTURE_TYPE) {
                            lectureCount++;
                            return (
                                <LectureItem
                                    key={lecture.id}
                                    curriculumId={curriculum.id}
                                    lecture={lecture}
                                    index={lectureCount} // Use lectureCount for numbering
                                    handleUpdateLecture={handleUpdateLecture}
                                    handleUploadVideoWatch={
                                        handleUploadVideoWatch
                                    }
                                    handleDeleteLecture={handleDeleteLecture}
                                    handleUpdateLectureDesc={
                                        handleUpdateLectureDesc
                                    }
                                    handleReplaceVideoWatch={
                                        handleReplaceVideoWatch
                                    }
                                    handleUpdateLectureArticle={
                                        handleUpdateLectureArticle
                                    }
                                    handleUpdateLectureIsPromotional={
                                        handleUpdateLectureIsPromotional
                                    }
                                    handleDeleteResource={handleDeleteResource}
                                    handleUploadResource={handleUploadResource}
                                />
                            );
                        } else {
                            quizCount++;
                            return (
                                <QuizItem
                                    key={lecture.id}
                                    handleAddQuestionAnswer={
                                        handleAddQuestionAnswer
                                    }
                                    handleUpdateQuestionAnswer={
                                        handleUpdateQuestionAnswer
                                    }
                                    handleDeleteQuestion={handleDeleteQuestion}
                                    handleUpdateQuiz={handleUpdateQuiz}
                                    handleDeleteQuiz={handleDeleteQuiz}
                                    curriculumId={curriculum.id}
                                    lecture={lecture}
                                    index={quizCount} // Use lectureCount for numbering
                                />
                            );
                        }
                    })}

                <CurriculumAddSection
                    curriculumId={curriculum.id}
                    handleAddLecture={handleAddLecture}
                    handleAddQuiz={handleAddQuiz}
                />
            </div>
        </div>
    );
}
