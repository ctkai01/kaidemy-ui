import { yupResolver } from "@hookform/resolvers/yup";
import { Button, FileInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { IoMdClose } from "react-icons/io";
import { UpdateResource } from "../../../../models";
import { schemeUploadResource } from "../../../../validators/course";

export interface IResourceFormProps {
    curriculumId: number;
    lectureId: number;
    // assetResource?: Asset;
    handleModeDescResource: () => void;
    handleSetLoading: (command: boolean) => void;
    handleUploadResource: (
        data: UpdateResource,
        setLoading: (command: boolean) => void,
    ) => void;
    // handleUploadVideoWatch: (
    //     data: UpdateVideoWatch,
    //     setLoading: (command: boolean) => void,
    // ) => void;
    // handleReplaceVideoWatch: (
    //     data: UpdateVideoWatch,
    //     setLoading: (command: boolean) => void,
    // ) => void;
}

export default function ResourceForm(props: IResourceFormProps) {
    const {
        curriculumId,
        lectureId,
        // assetVideoWatch,
        // handleUploadVideoWatch,
        handleSetLoading,
        handleUploadResource,
        handleModeDescResource,
        // handleReplaceVideoWatch,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
        reset,
    } = useForm<UpdateResource>({
        mode: "onChange",
        defaultValues: {
            curriculum_id: curriculumId,
            lecture_id: lectureId,
        },
        resolver: yupResolver(schemeUploadResource),
    });

    const handleUploadResourceW = (data: UpdateResource) => {
        console.log("Data: ", data);
        handleUploadResource(data, handleSetLoading);
        // if (!assetVideoWatch) {
        //     handleUploadVideoWatch(data, handleSetLoading);
        // } else {
        //     handleReplaceVideoWatch(data, handleSetLoading);
        // }

        handleModeDescResource();
    };

    return (
        <div className="relative border border-t-0 border-primary-black  p-2">
            <form onSubmit={handleSubmit(handleUploadResourceW)}>
                <div className="absolute -top-[37px] right-2 border border-b-0 border-primary-black">
                    <div className=" relative bg-white py-2 pl-2 pr-[30px] text-sm font-bold text-primary-black">
                        Add Resource
                        <IoMdClose
                            onClick={() => handleModeDescResource()}
                            className="absolute right-2 top-1/2 h-5 w-5 -translate-y-1/2 cursor-pointer"
                        />
                    </div>
                </div>
                <FileInput title="Chọn tệp" {...register("resource")} />
                <div>
                    <span className="text-xs text-[#6a6f73]">
                        <b>Notes:</b> Resources for any type of materials which
                        can be used to assist students in the lesson lecture.
                        This file will be considered a supplementary lecture
                        fig. Make sure everything is clear and the file size is
                        small more than 100 MB
                    </span>
                </div>
                {errors.resource ? (
                    <div className="text-sm text-red-700">
                        <span className="font-medium">Oops! </span>
                        <span>{errors.resource.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="flex justify-end">
                    <Button type="submit">Upload</Button>
                </div>
            </form>
        </div>
    );
}
