import { Button, Spinner } from "flowbite-react";
import * as React from "react";
import { FaPlus } from "react-icons/fa";
import { toast } from "react-toastify";
import {
    LECTURE_REMOVE_ASSET_TYPE,
    LECTURE_RESOURCE_ASSET_TYPE,
    LECTURE_UPLOAD_ARTICLE_TYPE,
    LECTURE_WATCH_ASSET_TYPE,
    REVIEW_INIT_STATUS,
} from "../../../../constants";
import {
    AnswerQuestion,
    Course,
    CreateLecture,
    CreateQuiz,
    CreateSection,
    Curriculum,
    Lecture,
    UpdateAnswer,
    UpdateLecture,
    UpdateLectureArticle,
    UpdateLectureDesc,
    UpdateQuestion,
    UpdateQuiz,
    UpdateResource,
    UpdateSection,
    UpdateVideoWatch,
} from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import AddCurriculumForm from "./AddCurriculumForm";
import SectionItem from "./SectionItem";
export interface ICurriculumSectionProps {
    courseID: number;
    curriculums: Curriculum[];
    loading: boolean;
    handleCourseNotFound: () => void;
    setCurriculums: React.Dispatch<React.SetStateAction<Curriculum[]>>;
    handleSetCourse: React.Dispatch<React.SetStateAction<Course | undefined>>;
}

// export interface Curriculum {
//     id: number;
//     course_id: number;
//     title: string;
//     lectures: Lecture[];
//     created_at: string;
//     updated_at: string;
//     description: string | null;
// }

const MODE_CURRICULUM = {
    NORMAL: 0,
    OPEN_NEW: 1,
};

export default function CurriculumSection(props: ICurriculumSectionProps) {
    const {
        courseID,
        curriculums,
        loading,
        handleSetCourse,
        setCurriculums,
        handleCourseNotFound,
    } = props;
    // const [curriculums, setCurriculums] = React.useState<Curriculum[]>([]);
    // const [loading, setIsLoading] = React.useState(true);
    const [modeCurriculum, setModeCurriculum] = React.useState(
        MODE_CURRICULUM.NORMAL,
    );

    const handleNormalModeCurriculum = () => {
        setModeCurriculum(MODE_CURRICULUM.NORMAL);
    };

    const handleAddLecture = async (data: CreateLecture) => {
        try {
            const body = {
                curriculumID: data.curriculum_id,
                title: data.title,
            };
            // formData.append("curriculum_id", `${data.curriculum_id}`);
            // formData.append("title", data.title);
            const result = await CourseServiceApi.createLecture(body);

            const newLecture = result;
            newLecture.assets = [];
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = prevCurriculums.map((curriculum) => {
                    if (curriculum.id === data.curriculum_id) {
                        if (!curriculum.lectures) {
                            return {
                                ...curriculum,
                                lectures: [newLecture],
                            };
                        } else {
                            return {
                                ...curriculum,
                                lectures: [...curriculum.lectures, newLecture],
                            };
                        }
                    }
                    return curriculum;
                });
                return updatedCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(
                <div className="font-bold">Added lecture successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Added lecture failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleAddQuiz = async (data: CreateQuiz) => {
        try {
            const body: any = {
                curriculumID: data.curriculum_id,
                title: data.title,
            };

            if (data.description) {
                body.description = data.description;
            }
            const result = await CourseServiceApi.createQuiz(body);
            const newLecture = result;
            newLecture.assets = [];
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = prevCurriculums.map((curriculum) => {
                    if (curriculum.id === data.curriculum_id) {
                        return {
                            ...curriculum,
                            lectures: [...curriculum.lectures, newLecture],
                        };
                    }
                    return curriculum;
                });
                return updatedCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Add quiz successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Add quiz failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleAddQuestionAnswer = async (data: AnswerQuestion) => {
        try {

            const body: any = {
                title: data.question,
                lectureID: data.lectureID,
            };
            const resultQuestion = await CourseServiceApi.createQuestion(body);

            const resultAnswers = await Promise.all(
                data.answers.map((answer, i) => {
                    const bodyAnswer: any = {
                        answerText: answer.answer_text,
                        questionID: resultQuestion.id,
                    };

                    if (answer.explain) {
                        bodyAnswer.explain = answer.explain;
                    }

                    if (i === +data.isCorrect) {
                        bodyAnswer.isCorrect = 1;
                    } else {
                        bodyAnswer.isCorrect = 0;
                    }
                    return CourseServiceApi.createAnswer(bodyAnswer);
                }),
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculumID,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lectureID,
                    );

                    if (lectureIndex !== -1) {
                        // Update Question
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };
                        const newQuestion = resultQuestion;

                        const newAnswers = resultAnswers
                            .map((resultAnswer) => {
                                return {
                                    id: resultAnswer.id,
                                    answerText: resultAnswer.answerText,
                                    isCorrect: resultAnswer.isCorrect,
                                    explain: resultAnswer.explain,
                                    questionID: resultAnswer.questionID,
                                    updated_at: resultAnswer.updated_at,
                                    created_at: resultAnswer.created_at,
                                };
                            })
                            .sort((a, b) => a.id - b.id);

                        newQuestion.answers = newAnswers;
                        if (!updatedLecture.questions) {
                            updatedLecture.questions = [];
                        }
                        updatedLecture.questions.push(newQuestion);

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ] = updatedLecture;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Add successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });

        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Add failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateQuestionAnswer = async (
        updateQuestion: UpdateQuestion,
        updateAnswer: UpdateAnswer[],
    ) => {
        try {
            //Update Question
            const body = {
                title: updateQuestion.title,
            };
            const updateQuestionResult = await CourseServiceApi.updateQuestion(
                body,
                updateQuestion.id,
            );

            //Update Question answers
            const updateAnswerResult = await Promise.all(
                updateAnswer.map((answer, _) => {
                    const bodyAnswer: any = {
                        answerText: answer.answerText,
                        questionID: updateQuestion.id,
                    };

                    if (answer.explain) {
                        bodyAnswer.explain = answer.explain;
                    }

                    if (answer.isCorrect) {
                        bodyAnswer.isCorrect = 1;
                    } else {
                        bodyAnswer.isCorrect = 0;
                    }
                    return CourseServiceApi.updateAnswer(bodyAnswer, answer.id);
                }),
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) =>
                        curriculum.id === updateQuestion.curriculumID,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === updateQuestion.lectureID,
                    );

                    if (lectureIndex !== -1) {
                        // Update Question
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };

                        const questionIndex =
                            updatedLecture.questions.findIndex(
                                (question) => question.id === updateQuestion.id,
                            );

                        const updatedQuestion = updateQuestionResult;


                        const updateAnswers = updateAnswerResult
                            .map((resultAnswer) => {
                                return {
                                    id: resultAnswer.id,
                                    answerText: resultAnswer.answerText,
                                    isCorrect: resultAnswer.isCorrect,
                                    explain: resultAnswer.explain,
                                    questionID: resultAnswer.questionID,
                                    updated_at: resultAnswer.updated_at,
                                    created_at: resultAnswer.created_at,
                                };
                            })
                            .sort((a, b) => a.id - b.id);

                        updatedQuestion.answers = updateAnswers;

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ].questions[questionIndex] = updatedQuestion;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Updated successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });

        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteQuestion = async (
        lectureID: number,
        curriculumID: number,
        questionID: number,
    ) => {
        try {
            await CourseServiceApi.deleteQuestion(questionID);
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === curriculumID,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === lectureID,
                    );

                    if (lectureIndex !== -1) {
                        // Update Question
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };

                        const questionIndex =
                            updatedLecture.questions.findIndex(
                                (question) => question.id === questionID,
                            );

                        const questions = [...updatedLecture.questions];

                        questions.splice(questionIndex, 1);

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ].questions = questions;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(
                <div className="font-bold">Deleted question sucessfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Deleted question failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateQuiz = async (data: UpdateQuiz) => {
        try {
            const body = {
                title: data.title,
                description: data.description || "",
            };
            const result = await CourseServiceApi.updateQuiz(
                body,
                data.lecture_id,
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        // Update Question
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };
                        updatedLecture.title = result.title;
                        updatedLecture.description = result.description;

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ] = updatedLecture;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Updated quiz successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated quiz failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteQuiz = async (
        lectureID: number,
        curriculumID: number,
    ) => {
        try {
            await CourseServiceApi.deleteQuiz(lectureID);
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === curriculumID,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === lectureID,
                    );

                    if (lectureIndex !== -1) {
                        const lectures = [...updatedCurriculum.lectures];

                        lectures.splice(lectureIndex, 1);

                        updatedCurriculums[curriculumIndex].lectures = lectures;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Deleted quiz successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            toast(<div className="font-bold">Deleted quiz failed !</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
            console.log(e);
        }
    };

    const handleUpdateLecture = async (data: UpdateLecture) => {
        try {
            const body = {
                title: data.title,
                typeUpdate: LECTURE_UPLOAD_ARTICLE_TYPE,
            };

            const result = await CourseServiceApi.updateLecture(
                body,
                data.lecture_id,
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        // Update Question
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };
                        updatedLecture.title = result.title;

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ] = updatedLecture;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });

            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(
                <div className="font-bold">Updated lecture successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated lecture failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        }
    };

    const handleDeleteLecture = async (
        lectureID: number,
        curriculumID: number,
    ) => {
        try {
            await CourseServiceApi.deleteLecture(lectureID);
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === curriculumID,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === lectureID,
                    );

                    if (lectureIndex !== -1) {
                        const lectures = [...updatedCurriculum.lectures];

                        lectures.splice(lectureIndex, 1);

                        updatedCurriculums[curriculumIndex].lectures = lectures;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });

            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });

            toast(
                <div className="font-bold">Deleted lecture sucessfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Deleted lecture failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUploadVideoWatch = async (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => {
        try {
            setLoading(true);
            const formData = new FormData();
            formData.append("asset", data.video[0]);
            const uploadVideoResult = await CourseServiceApi.updateVideoLecture(
                formData,
                data.lecture_id,
            );

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ].assets = uploadVideoResult.assets;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });

            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(
                <div className="font-bold">Uploaded video successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
            setLoading(false);
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Uploaded video failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleReplaceVideoWatch = async (
        data: UpdateVideoWatch,
        setLoading: (command: boolean) => void,
    ) => {
        try {
            setLoading(true);
            //Remove video

            const formDataRemoveVideo = new FormData();
            formDataRemoveVideo.append(
                "typeUpdate",
                `${LECTURE_REMOVE_ASSET_TYPE}`,
            );
            formDataRemoveVideo.append(
                "asset_type",
                `${LECTURE_WATCH_ASSET_TYPE}`,
            );

            await CourseServiceApi.updateLecture(
                formDataRemoveVideo,
                data.lecture_id,
            );

            // Upload again
            const formData = new FormData();
            formData.append("asset", data.video[0]);
            formData.append("isReplace", "1");

            const uploadVideoResult = await CourseServiceApi.updateVideoLecture(
                formData,
                data.lecture_id,
            );

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ].assets = uploadVideoResult.assets;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            setLoading(false);
            toast(
                <div className="font-bold">Replaced video successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            setLoading(false);
            toast(<div className="font-bold">Replaced video failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateLectureDesc = async (data: UpdateLectureDesc) => {
        try {
            const body = {
                description: data.description ? data.description : "",
                typeUpdate: LECTURE_UPLOAD_ARTICLE_TYPE,
            };
            const result = await CourseServiceApi.updateLecture(
                body,
                data.lecture_id,
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };
                        updatedLecture.description = result.description;

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ] = updatedLecture;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Updated successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateLectureArticle = async (data: UpdateLectureArticle) => {
        try {
            const formData = new FormData();
            const body = {
                article: data.article ? data.article : "",
                typeUpdate: LECTURE_UPLOAD_ARTICLE_TYPE,
            };
            const result = await CourseServiceApi.updateLecture(
                body,
                data.lecture_id,
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };
                        updatedLecture.article = result.article;

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ] = updatedLecture;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Updated successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUploadResource = async (
        data: UpdateResource,
        setLoading: (command: boolean) => void,
    ) => {
        try {
            setLoading(true);
            const formData = new FormData();
            formData.append("asset", data.resource[0]);
            const uploadVideoResult =
                await CourseServiceApi.updateResourceLecture(
                    formData,
                    data.lecture_id,
                );

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.curriculum_id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === data.lecture_id,
                    );

                    if (lectureIndex !== -1) {
                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ].assets = uploadVideoResult.assets;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            setLoading(false);
            toast(<div className="font-bold">Uploaded successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Uploaded failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteResource = async (
        lectureID: number,
        curriculumID: number,
        assetID: number,
    ) => {
        try {
            const formData = new FormData();
            const body = {
                typeUpdate: LECTURE_REMOVE_ASSET_TYPE,
                assetType: LECTURE_RESOURCE_ASSET_TYPE,
                assetID: assetID,
            };

            const result = await CourseServiceApi.updateLecture(
                body,
                lectureID,
            );

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === curriculumID,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === lectureID,
                    );

                    if (lectureIndex !== -1) {
                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ].assets = result.assets;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Deleted successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Deleted failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleUpdateLectureIsPromotional = async (
        isCheck: boolean,
        curriculumId: number,
        lectureId: number,
    ) => {
        try {
            const body: any = {
                typeUpdate: LECTURE_UPLOAD_ARTICLE_TYPE,
            };
            if (isCheck) {
                body.isPromotional = 0;
            } else {
                body.isPromotional = 1;
            }
            const result = await CourseServiceApi.updateLecture(
                body,
                lectureId,
            );
            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === curriculumId,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    const lectureIndex = updatedCurriculum.lectures.findIndex(
                        (lecture) => lecture.id === lectureId,
                    );

                    if (lectureIndex !== -1) {
                        const updatedLecture = {
                            ...updatedCurriculum.lectures[lectureIndex],
                        };
                        updatedLecture.isPromotional = result.isPromotional;

                        updatedCurriculums[curriculumIndex].lectures[
                            lectureIndex
                        ] = updatedLecture;

                        return updatedCurriculums;
                    }
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Updated successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    // React.useEffect(() => {
    //     const getCurriculums = async () => {
    //         try {
    //             const result =
    //                 await CourseServiceApi.getCurriCulumsByCourseByID(courseID);

    //             setCurriculums(result.course.curriculums);
    //             setIsLoading(false);
    //         } catch (e: any) {
    //             console.log(e);
    //             if (
    //                 e.response.data.Message ===
    //                 "error handling request: course not found"
    //             ) {
    //                 handleCourseNotFound();
    //             }
    //         }
    //     };
    //     setIsLoading(true);

    //     getCurriculums();
    // }, []);

    const handleUpdateSection = async (data: UpdateSection) => {
        try {
            const body: any = {
                title: data.title,
            };

            if (data.description) {
                body.description = data.description;
            }

            const result = await CourseServiceApi.updateCurriculum(
                body,
                data.id,
            );

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === data.id,
                );
                if (curriculumIndex !== -1) {
                    const updatedCurriculum = {
                        ...updatedCurriculums[curriculumIndex],
                    };

                    updatedCurriculum.title = result.title;
                    updatedCurriculum.description = result.description;

                    updatedCurriculums[curriculumIndex] = updatedCurriculum;

                    return updatedCurriculums;
                }

                return prevCurriculums;
            });

            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = result.course.reviewStatus;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Updated successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Updated failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleDeleteSection = async (curriculumID: number) => {
        // console.log("Delete curriculum ID: ", curriculumID)
        try {
            await CourseServiceApi.deleteCurriculum(curriculumID);

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];
                const curriculumIndex = updatedCurriculums.findIndex(
                    (curriculum) => curriculum.id === curriculumID,
                );
                if (curriculumIndex !== -1) {
                    updatedCurriculums.splice(curriculumIndex, 1);

                    return updatedCurriculums;
                }

                return prevCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Deleted successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Deleted failed</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleAddCurriculum = async (data: CreateSection) => {
        try {
            const body: any = {
                title: data.title,
                courseID: courseID,
            };

            if (data.description) {
                body.description = data.description;
            }

            const createCurriculumResult =
                await CourseServiceApi.createCurriculum(body);

            setCurriculums((prevCurriculums) => {
                const updatedCurriculums = [...prevCurriculums];

                updatedCurriculums.push(createCurriculumResult);

                return updatedCurriculums;
            });
            handleSetCourse((course?: Course) => {
                if (course) {
                    const courseUpdate = { ...course };
                    courseUpdate.reviewStatus = REVIEW_INIT_STATUS;
                    return courseUpdate;
                } else {
                    return course;
                }
            });
            toast(<div className="font-bold">Added successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Added failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    return (
        <>
            {loading ? (
                <div className="flex justify-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            ) : (
                <div>
                    {curriculums.map((curriculum, index) => (
                        <SectionItem
                            key={index}
                            handleDeleteSection={handleDeleteSection}
                            handleAddLecture={handleAddLecture}
                            handleAddQuiz={handleAddQuiz}
                            handleAddQuestionAnswer={handleAddQuestionAnswer}
                            handleDeleteQuestion={handleDeleteQuestion}
                            handleUpdateQuiz={handleUpdateQuiz}
                            handleDeleteQuiz={handleDeleteQuiz}
                            handleDeleteLecture={handleDeleteLecture}
                            handleUpdateLecture={handleUpdateLecture}
                            handleUploadVideoWatch={handleUploadVideoWatch}
                            handleUpdateLectureDesc={handleUpdateLectureDesc}
                            handleReplaceVideoWatch={handleReplaceVideoWatch}
                            handleUpdateLectureArticle={
                                handleUpdateLectureArticle
                            }
                            handleUploadResource={handleUploadResource}
                            handleUpdateQuestionAnswer={
                                handleUpdateQuestionAnswer
                            }
                            handleUpdateLectureIsPromotional={
                                handleUpdateLectureIsPromotional
                            }
                            handleDeleteResource={handleDeleteResource}
                            curriculum={curriculum}
                            handleUpdateSection={handleUpdateSection}
                            index={index}
                        />
                    ))}
                    <div className="mt-4">
                        {modeCurriculum === MODE_CURRICULUM.NORMAL && (
                            <Button
                                onClick={() =>
                                    setModeCurriculum(MODE_CURRICULUM.OPEN_NEW)
                                }
                            >
                                <FaPlus className="mr-2" />
                                Section
                            </Button>
                        )}

                        {modeCurriculum === MODE_CURRICULUM.OPEN_NEW && (
                            <AddCurriculumForm
                                handleNormalMode={handleNormalModeCurriculum}
                                handleCreateSection={handleAddCurriculum}
                            />
                        )}
                    </div>
                </div>
            )}
        </>
    );
}
