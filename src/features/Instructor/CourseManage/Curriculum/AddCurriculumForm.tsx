import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { IoClose } from "react-icons/io5";
import { CreateSection } from "../../../../models";
import { schemeCreateSection } from "../../../../validators/course";

export interface IAddCurriculumFormProps {
    handleNormalMode: () => void;
    handleCreateSection: (data: CreateSection) => void;
}

export default function AddCurriculumForm(props: IAddCurriculumFormProps) {
    const { handleNormalMode, handleCreateSection } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        reset,
    } = useForm<CreateSection>({
        mode: "onChange",
      
        resolver: yupResolver(schemeCreateSection),
    });

    const handleSubmitCreateSection = (data: CreateSection) => {
        handleCreateSection(data);
        handleNormalMode();
        // reset();
    };
    return (
        <div>
            <div className="mb-4">
                <IoClose
                    onClick={() => handleNormalMode()}
                    className="h-5 w-5 cursor-pointer"
                />
            </div>
            <div className="">
                <div className="border border-primary-black p-2">
                    <form onSubmit={handleSubmit(handleSubmitCreateSection)}>
                        <div className="flex  gap-2">
                            <div className="flex h-[42px] items-center text-base font-bold text-primary-black">
                                New Section:
                            </div>
                            <div className="flex-1">
                                <div className="mb-4">
                                    <TextInput
                                        type="text"
                                        placeholder="Enter a title"
                                        color={errors.title ? "failure" : ""}
                                        {...register("title")}
                                        helperText={
                                            <>
                                                {errors.title ? (
                                                    <>
                                                        <span className="font-medium">
                                                            Oops!{" "}
                                                        </span>
                                                        <span>
                                                            {
                                                                errors.title
                                                                    .message
                                                            }
                                                        </span>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </>
                                        }
                                    />
                                </div>
                                <span className="mb-2 block text-sm font-bold text-primary-black">
                                    What will students be able to do at the end
                                    of this section?
                                </span>
                                <div className="mb-4">
                                    <TextInput
                                        type="text"
                                        placeholder="Enter a Learning Objective"
                                        color={
                                            errors.description ? "failure" : ""
                                        }
                                        {...register("description")}
                                        helperText={
                                            <>
                                                {errors.description ? (
                                                    <>
                                                        <span className="font-medium">
                                                            Oops!{" "}
                                                        </span>
                                                        <span>
                                                            {
                                                                errors
                                                                    .description
                                                                    .message
                                                            }
                                                        </span>
                                                    </>
                                                ) : (
                                                    <></>
                                                )}
                                            </>
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="mt-4 flex justify-end gap-2">
                            <Button
                                onClick={() => handleNormalMode()}
                                color="gray"
                            >
                                Cancel
                            </Button>
                            <Button type="submit">Add Section</Button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}
