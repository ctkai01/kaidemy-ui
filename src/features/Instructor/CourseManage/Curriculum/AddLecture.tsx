import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { CreateLecture } from "../../../../models";
import { schemeAddLecture } from "../../../../validators/course";

export interface IAddLectureProps {
    curriculumId: number;
    handleSetModeNormal: () => void;
    handleAddLecture: (data: CreateLecture) => void;
    handleFinishAdd: () => void;
}

export default function AddLecture(props: IAddLectureProps) {
    const {
        curriculumId,
        handleSetModeNormal,
        handleAddLecture,
        handleFinishAdd,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<CreateLecture>({
        mode: "onSubmit",
        defaultValues: {
            curriculum_id: curriculumId,
        },
        resolver: yupResolver(schemeAddLecture),
    });

    const handleSubmitAddLecture = (data: CreateLecture) => {
        // Call API create lecture to get ID

        handleAddLecture(data);
        handleFinishAdd();
        reset();
    };
    return (
        <div className="border border-primary-black p-2">
            <form onSubmit={handleSubmit(handleSubmitAddLecture)}>
                <div className="flex items-center gap-2">
                    <div className="text-base text-primary-black">
                        New lecture:
                    </div>
                    <div className="flex-1">
                        <TextInput
                            type="text"
                            maxLength={80}
                            placeholder="Enter title"
                            color={errors.title ? "failure" : ""}
                            {...register("title")}
                            helperText={
                                <>
                                    {errors.title ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.title.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                    </div>
                </div>
                <div className="mt-4 flex justify-end gap-2">
                    <Button onClick={handleSetModeNormal} color="gray">
                        Cancel
                    </Button>
                    <Button type="submit">Add lecture</Button>
                </div>
            </form>
        </div>
    );
}
