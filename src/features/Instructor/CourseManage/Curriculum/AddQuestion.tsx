import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Radio, Textarea, TextInput } from "flowbite-react";
import * as React from "react";
import { useFieldArray, useForm } from "react-hook-form";
import Editor from "../../../../components/ui/core/Editor";
import {
    AnswerQuestion,
    Question,
    UpdateAnswer,
    UpdateQuestion,
} from "../../../../models";
import { schemeCreateQuestionAnswer } from "../../../../validators/course";

export interface IAddQuestionProps {
    lectureID: number;
    questionEdit: Question | null;
    curriculumId: number;
    handleAddQuestionAnswer: (data: AnswerQuestion) => void;
    handleQuestionEdit: (data: Question | null) => void;
    handleUpdateQuestionAnswer: (
        updateQuestion: UpdateQuestion,
        updateAnswers: UpdateAnswer[],
    ) => void;
    handleModeShowUp: () => void;
}

export default function AddQuestion(props: IAddQuestionProps) {
    const {
        lectureID,
        questionEdit,
        curriculumId,
        handleAddQuestionAnswer,
        handleQuestionEdit,
        handleUpdateQuestionAnswer,
        handleModeShowUp,
    } = props;
    const initAnswers = React.useMemo(() => {
        if (questionEdit) {
            const answers = questionEdit.answers.map((answer) => {
                return {
                    answer_text: answer.answerText,
                    id: answer.id,
                    explain: answer.explain ? answer.explain : "",
                };
            });
            return answers;
        } else {
            return [
                {
                    answer_text: "",
                    explain: "",
                    id: 0,
                },
                {
                    answer_text: "",
                    explain: "",
                    id: 0,
                },
                {
                    answer_text: "",
                    explain: "",
                    id: 0,
                },
                {
                    answer_text: "",
                    explain: "",
                    id: 0,
                },
            ];
        }
    }, []);

    const initCorrect = React.useMemo(() => {
        if (questionEdit) {
            const indexCorrect = questionEdit.answers.findIndex(
                (answer) => answer.isCorrect,
            );

            if (indexCorrect != -1) {
                return `${indexCorrect}`;
            }
            return "";
        } else {
            return "";
        }
    }, []);
    const {
        register,
        control,
        handleSubmit,
        watch,
        formState: { errors },
        setValue,
        reset,
    } = useForm<AnswerQuestion>({
        mode: "onChange",
        defaultValues: {
            lectureID: lectureID,
            curriculumID: curriculumId,
            isCorrect: initCorrect,
            question: questionEdit ? questionEdit.title : "",
            answers: initAnswers,
        },
        resolver: yupResolver(schemeCreateQuestionAnswer),
    });

    React.useEffect(() => {
        return () => {
            handleQuestionEdit(null);
        };
    }, []);

    const { fields: answersFields } = useFieldArray({
        control,
        name: "answers",
    });

    const handleSubmitQuestion = (data: AnswerQuestion) => {
        if (questionEdit) {
            const updateQuestion: UpdateQuestion = {
                id: questionEdit.id,
                title: data.question,
                curriculumID: curriculumId,
                lectureID: lectureID,
            };

            const updateAnswers: UpdateAnswer[] = data.answers.map(
                (answer, index) => {
                    return {
                        answerText: answer.answer_text,
                        explain: answer.explain,
                        id: answer.id,
                        isCorrect: +data.isCorrect === index,
                        curriculumID: curriculumId,
                        lectureID: lectureID,
                    };
                },
            );
            handleUpdateQuestionAnswer(updateQuestion, updateAnswers);
            handleModeShowUp();
        } else {
            handleAddQuestionAnswer(data);
            handleModeShowUp();
        }
    };

    const handleSetQuestion = (value: string) => {
        setValue("question", value);
    };
    // const handle
    return (
        <>
            <form onSubmit={handleSubmit(handleSubmitQuestion)}>
                <div className="pb-2 text-sm font-bold text-primary-black">
                    Question
                </div>
                <Editor
                    defaultValue={questionEdit ? questionEdit.title : ""}
                    isImage={false}
                    handleSetHTML={handleSetQuestion}
                />
                {errors.question ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium ">Oops! </span>
                        <span>{errors.question.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="pb-4 pt-4 text-sm font-bold text-primary-black">
                    Answer
                </div>
                <div>
                    {answersFields.map(({ id }, index) => {
                        return (
                            <div key={id} className="mb-4 flex">
                                <div className="ml-4 mr-4">
                                    <Radio
                                        value={index}
                                        {...register(`isCorrect`)}
                                        // checked={is_correct}
                                        defaultChecked={
                                            initCorrect != "" &&
                                            +initCorrect == index
                                                ? true
                                                : false
                                        }
                                    />
                                </div>
                                <div className="flex-1">
                                    <Textarea
                                        placeholder="Add answer."
                                        maxLength={600}
                                        {...register(
                                            `answers.${index}.answer_text`,
                                        )}
                                        rows={4}
                                        helperText={
                                            errors.answers &&
                                            errors.answers[index] &&
                                            errors.answers[index]?.answer_text
                                                ?.message ? (
                                                <p className="mt-2 text-sm text-red-700">
                                                    <span className="font-medium ">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {
                                                            errors.answers[
                                                                index
                                                            ]?.answer_text
                                                                ?.message
                                                        }
                                                    </span>
                                                </p>
                                            ) : (
                                                <></>
                                            )
                                        }
                                    />
                                    <TextInput
                                        className="ml-10 mt-4"
                                        type="text"
                                        maxLength={600}
                                        placeholder="Explain why this is or is not the best answer."
                                        {...register(
                                            `answers.${index}.explain`,
                                        )}
                                        helperText={
                                            errors.answers &&
                                            errors.answers[index] &&
                                            errors.answers[index]?.explain
                                                ?.message ? (
                                                <p className="mt-2 text-sm text-red-700">
                                                    <span className="font-medium ">
                                                        Oops!{" "}
                                                    </span>
                                                    <span>
                                                        {
                                                            errors.answers[
                                                                index
                                                            ]?.explain?.message
                                                        }
                                                    </span>
                                                </p>
                                            ) : (
                                                <></>
                                            )
                                        }
                                    />
                                </div>
                                <div className="min-w-[6%]"></div>
                            </div>
                        );
                    })}
                </div>
                {errors.isCorrect ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium ">Oops! </span>
                        <span>{errors.isCorrect.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                {/* {errors.answers ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium ">Oops! </span>
                        <span>{errors.answers[0]?.answer_text.message}</span>
                    </div>
                ) : (
                    <></>
                )} */}
                <div className="mt-4 flex justify-end gap-2">
                    <Button type="submit">Save</Button>
                </div>
            </form>
        </>
    );
}
