import { Button } from "flowbite-react";
import { useState } from "react";
import { FaRegCheckCircle, FaRegCircle } from "react-icons/fa";
import { toast } from "react-toastify";
import {
    REVIEW_INIT_STATUS,
    REVIEW_PENDING_STATUS,
    REVIEW_VERIFY_STATUS,
    topicCourseManager,
} from "../../../constants";
import { Course } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { isTopicComplete } from "../../../utils";
import { Curriculum } from "./Curriculum/Curriculum";

export interface ISidebarCourseManagerProps {
    currentPath?: string;
    course: Course;
    curriculums: Curriculum[];
    handleChangeTab: (path: string) => void;
    handleSetCourse: React.Dispatch<React.SetStateAction<Course | undefined>>;
}

interface CompleteTitle {
    intended_learner: boolean;
    curriculums: boolean;
    landing_page: boolean;
    pricing: boolean;
}

export default function SidebarCourseManager(
    props: ISidebarCourseManagerProps,
) {
    const {
        course,
        currentPath,
        curriculums,
        handleChangeTab,
        handleSetCourse,
    } = props;
    const [completeTitle, setCompleteTitle] = useState<CompleteTitle>({
        curriculums: false,
        intended_learner: false,
        landing_page: false,
        pricing: false,
    });

    const handleCheckComplete = (
        course: Course,
        title: string,
        curriculums: Curriculum[],
    ): boolean => {
        const isDone = isTopicComplete(course, title, curriculums);


        if (
            title === "Intended learners" &&
            isDone !== completeTitle.intended_learner
        ) {
            setCompleteTitle((completeTitle: CompleteTitle) => {
                return {
                    ...completeTitle,
                    intended_learner: isDone,
                };
            });
        }

        if (title === "Curriculum" && isDone !== completeTitle.curriculums) {
            setCompleteTitle((completeTitle: CompleteTitle) => {
                return {
                    ...completeTitle,
                    curriculums: isDone,
                };
            });
        }

        if (
            title === "Course landing page" &&
            isDone !== completeTitle.landing_page
        ) {
            setCompleteTitle((completeTitle: CompleteTitle) => {
                return {
                    ...completeTitle,
                    landing_page: isDone,
                };
            });
        }

        if (title === "Pricing" && isDone !== completeTitle.pricing) {
            setCompleteTitle((completeTitle: CompleteTitle) => {
                return {
                    ...completeTitle,
                    pricing: isDone,
                };
            });
        }

        return isDone;
    };
    const handleSubmitReview = async () => {
        try {
            if (course.reviewStatus != REVIEW_INIT_STATUS) {
                toast(
                    <div className="font-bold">
                        This course must be initial review status!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "warning",
                        theme: "colored",
                    },
                );
            } else {
                const result = await CourseServiceApi.submitReviewCourseByID(
                    course.id,
                );
                handleSetCourse(result);
                toast(
                    <div className="font-bold">
                        Submit for Review successfully!
                    </div>,
                    {
                        draggable: false,
                        position: "top-right",
                        type: "success",
                        theme: "colored",
                    },
                );
            }
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Submit for Review failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="py-8 pr-6">
            <ul>
                {topicCourseManager.map((topic, index) => (
                    <li
                        key={index}
                        className="mt-8 text-base text-primary-black first:mt-0"
                    >
                        <div className="py-2 pl-6 font-bold">{topic.title}</div>
                        <ul>
                            {topic.children.map((item, i) => (
                                <li
                                    onClick={() => handleChangeTab(item.path)}
                                    key={i}
                                    className={`cursor-pointer border-l-4 hover:bg-primary-hover-gray ${
                                        currentPath === item.path &&
                                        "border-cyan-400"
                                    }`}
                                >
                                    <div
                                        // to={item.path}
                                        className="flex items-center gap-2 px-8 py-2"
                                    >
                                        {handleCheckComplete(
                                            course,
                                            item.title,
                                            curriculums,
                                        ) ? (
                                            <FaRegCheckCircle className="text-lime-600" />
                                        ) : (
                                            <FaRegCircle />
                                        )}

                                        {/* <CiCircleCheck/> */}
                                        <span>{item.title}</span>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </li>
                ))}
            </ul>
            {course.reviewStatus === REVIEW_PENDING_STATUS && (
                <Button
                    disabled={true}
                    className="mt-4 w-full font-bold"
                    color="warning"
                >
                    Pending Review
                </Button>
            )}

            {course.reviewStatus === REVIEW_INIT_STATUS && (
                <Button
                    onClick={handleSubmitReview}
                    disabled={
                        !completeTitle.curriculums ||
                        !completeTitle.intended_learner ||
                        !completeTitle.landing_page ||
                        !completeTitle.pricing
                    }
                    className="mt-4 w-full font-bold"
                    color="purple"
                >
                    Submit for Review
                </Button>
            )}

            {course.reviewStatus === REVIEW_VERIFY_STATUS && (
                <Button
                    // onClick={handleSubmitReview}
                    disabled={
                        completeTitle.curriculums &&
                        completeTitle.intended_learner &&
                        completeTitle.landing_page &&
                        completeTitle.pricing
                    }
                    className="mt-4 w-full font-bold"
                    color="success"
                >
                    Published
                </Button>
            )}
        </div>
    );
}
