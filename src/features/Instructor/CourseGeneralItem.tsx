import { Badge, Progress } from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import { DRAFT_STATUS, REVIEW_INIT_STATUS, REVIEW_PENDING_STATUS, REVIEW_VERIFY_STATUS } from "../../constants";
import { Course } from "../../models";
import { convertPercentComplete } from "../../utils";

export interface ICourseGeneralItemProps {
    course: Course
}

export default function CourseGeneralItem(props: ICourseGeneralItemProps) {
    const { course } = props
    return (
        <div className="mt-6 flex h-30 border border-primary-hover-gray bg-white">
            {/* https://s.udemycdn.com/course/200_H/placeholder.jpg */}
            <div className="flex items-center justify-center bg-[#e7e1e1]">
                <img
                    className="w-[118px]"
                    loading="lazy"
                    src={
                        course.image
                            ? course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
            </div>
            <div className="group relative flex flex-1 cursor-pointer">
                <div className="flex h-full w-[30%] flex-col justify-between p-4">
                    <div className="line-clamp-2 text-base font-bold text-primary-black">
                        {course.title}
                    </div>
                    <div className="flex text-xs font-bold uppercase text-primary-black">
                        {/* {course.status === DRAFT_STATUS ? "Draft" : "Live"} */}
                        {course.review_status === REVIEW_INIT_STATUS && (
                            <Badge className="text-xs uppercase" color="info">
                               Draft
                            </Badge>
                        )}
                        {course.review_status === REVIEW_PENDING_STATUS && (
                            <Badge
                                className="text-xs uppercase"
                                color="warning"
                            >
                                Pending Review
                            </Badge>
                        )}
                        {course.review_status === REVIEW_VERIFY_STATUS && (
                            <Badge
                                className="text-xs uppercase"
                                color="success"
                            >
                                Verified
                            </Badge>
                        )}
                    </div>
                </div>
                <div className="flex flex-1 items-center gap-4 px-4 py-2">
                    <div className="text-base font-bold text-primary-black">
                        Finish your course
                    </div>
                    <div className="flex-1">
                        <Progress
                            progress={convertPercentComplete(course)}
                            progressLabelPosition="inside"
                            textLabel=""
                            textLabelPosition="outside"
                            //   size=""
                        />
                    </div>
                </div>
                <Link to={`/instructor/course/${course.id}/manage/basics`}>
                    <div className="absolute  bottom-0 left-0 right-0 top-0 p-2">
                        <div
                            className="flex h-full w-full items-center justify-center bg-[#ffffffe6] text-[19px] font-bold text-cyan-500 opacity-0
group-hover:opacity-100"
                        >
                            Edit/ manage course
                        </div>
                    </div>
                </Link>
            </div>
        </div>
    );
}
