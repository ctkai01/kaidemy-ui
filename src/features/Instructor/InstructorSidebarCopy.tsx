import {
    CustomFlowbiteTheme,
    Flowbite,
    Sidebar,
    Tabs,
    TabsComponent,
} from "flowbite-react";
import { useState } from "react";
import { MdBarChart, MdOutlineMessage } from "react-icons/md";
import { PiVideoBold } from "react-icons/pi";
import { Link } from "react-router-dom";
import CourseGeneral from "./CourseGeneral";
import PerformanceGeneral from "./Performance/PerformanceGeneral";

const customTheme: CustomFlowbiteTheme = {
    tab: {
        base: "flex h-full",
        tablist: {
            base: "w-[56px] fixed top-0 left-0 h-full  transition-[width] duration-400 ease-in-out  hover:w-[250px] bg-primary-black border-none group",
            tabitem: {
                base: "w-full text-base relative flex justify-start hover:bg-[#3e4143] hover:text-white rounded-none items-center p-4  font-bold first:ml-0 disabled:cursor-pointer disabled:text-gray-400 disabled:dark:text-gray-500",
                styles: {
                    underline: {
                        active: {
                            on: "text-white  rounded-none border-l-4 border-cyan-600 active dark:text-cyan-500 dark:border-cyan-500",
                            off: "border-l-2  rounded-none border-transparent text-gray-500  dark:text-gray-400 dark:hover:text-gray-300",
                        },
                    },
                },
                icon: "shrink-0 mr-2 h-5 w-5",
            },
        },
        tabitemcontainer: {
            base: "w-full",
        },
        tabpanel: "",
    },
};

export interface IInstructorSidebarCopyProps {}

interface SidebarCollapse {
    community: boolean;
    performance: boolean;
}

export default function InstructorSidebarCopy(props: IInstructorSidebarCopyProps) {
    const [sidebarCollapse, setSidebarCollapse] = useState<SidebarCollapse>({
        community: false,
        performance: false,
    });

    const {} = props;
    return (
        <div className="lef-0 absolute top-0 h-full w-full">
            {/* <div className="h-full"> */}
            <Flowbite theme={{ theme: customTheme }}>
                <TabsComponent
                    aria-label="Tabs with underline"
                    style="underline"
                >
                    <Tabs.Item
                        title={
                            <Link
                                className="after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                                to="/"
                            >
                                <img
                                    loading="lazy"
                                    src={
                                        "https://kaidemy.b-cdn.net/avatar/h-high-resolution-logo-transparent.png"
                                    }
                                    className="h-[34px] group-hover:h-[34px]"
                                />
                            </Link>
                        }
                        disabled
                    ></Tabs.Item>
                    <Tabs.Item
                        active
                        title={
                            <div className=" opacity-0 transition-[opacity] duration-400 ease-linear group-hover:opacity-100">
                                Courses
                            </div>
                        }
                        className="fuck"
                        icon={PiVideoBold}
                    >
                        <div className="">
                            <CourseGeneral />
                        </div>
                    </Tabs.Item>
                    <Tabs.Item
                        title={
                            <div className="opacity-0 transition-[opacity] duration-400 ease-linear group-hover:opacity-100">
                                Communication
                            </div>
                        }
                        icon={MdOutlineMessage}
                    >
                        This is{" "}
                        <span className="font-medium text-gray-800 dark:text-white">
                            Dashboard tab's associated content
                        </span>
                        . Clicking another tab will toggle the visibility of
                        this one for the next. The tab JavaScript swaps classes
                        to control the content visibility and styling.
                    </Tabs.Item>
                    <Tabs.Item
                        title={
                            <div className="opacity-0 transition-[opacity] duration-400 ease-linear group-hover:opacity-100">
                                Performance
                            </div>
                        }
                        icon={MdBarChart}
                    >
                        <div>
                            <PerformanceGeneral />
                        </div>
                    </Tabs.Item>
                </TabsComponent>
            </Flowbite>
            {/* </div> */}
        </div>
    );
}
