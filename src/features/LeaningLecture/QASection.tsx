import { Button, Label, Select, TextInput } from "flowbite-react";
import _ from "lodash";
import * as React from "react";
import { FaSearch } from "react-icons/fa";
import { RiContactsBookLine } from "react-icons/ri";
import {
    AnswerLecture,
    ContentCourse,
    CourseShow,
    PaginationInfo,
    QuestionLecture,
} from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { getTitleLectureByID, InfoIndexLecture } from "../../utils";
import AnswerModal from "./AnswerModal";
import CreateQuestionModal from "./CreateQuestionModal";
import QuestionItem from "./QuestionItem";

export interface IQASectionProps {
    lectureID: number;
    courseID: number;
    contentCourse: ContentCourse;
    course: CourseShow;
}
export const SIZE_PAGE = 5;
export const SIZE_PAGE_ANSWER = 12;

export interface QuestionLectureModal {
    question: QuestionLecture;
    infoIndex: InfoIndexLecture;
}

export const ENTIRE_COURSE = 1;
export const THIS_LECTURE = 2;

export default function QASection(props: IQASectionProps) {
    const [openAnswerModal, setOpenAnswerModal] = React.useState(false);
    const [questionLectureModal, setQuestionLectureModal] =
        React.useState<QuestionLectureModal | null>(null);
    const [openCreateQuestionModal, setOpenCreateQuestionModal] =
        React.useState(false);
    const { courseID, lectureID, course, contentCourse } = props;
    const [questionLectures, setQuestionLectures] = React.useState<
        QuestionLecture[]
    >([]);

    const [answerLectures, setAnswerLectures] = React.useState<AnswerLecture[]>(
        [],
    );
    const [filterQuestion, setFilterQUestion] = React.useState(ENTIRE_COURSE);

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    const [infoPaginationAnswer, setInfoPaginationAnswer] =
        React.useState<PaginationInfo>({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
    const [currentPage, setCurrentPage] = React.useState(1);
    const [currentPageAnswer, setCurrentPageAnswer] = React.useState(1);
    const [search, setSearch] = React.useState("");

    React.useEffect(() => {
        const fetchQuestionLectures = async () => {
            try {
                if (
                    filterQuestion != ENTIRE_COURSE &&
                    questionLectures.length &&
                    contentCourse.id !== questionLectures[0].lectureID
                ) {
                    setCurrentPage(1);
                }
                const dataQuestionLectures: any =
                    await CourseServiceApi.getQuestionLectures(
                        courseID,
                        lectureID,
                        currentPage,
                        SIZE_PAGE,
                        filterQuestion,
                        search,
                    );
                setInfoPagination({
                    totalPage: dataQuestionLectures.meta.pageCount,
                    totalItem: dataQuestionLectures.meta.itemCount,
                    size: dataQuestionLectures.meta.size,
                });

                if (!dataQuestionLectures?.item.length) {
                    setQuestionLectures([]);
                } else {
                    if (filterQuestion != ENTIRE_COURSE) {
                        if (
                            questionLectures.length &&
                            contentCourse.id === questionLectures[0].lectureID
                        ) {
                            const newQuestionLectures = [
                                ...questionLectures,
                                ...dataQuestionLectures.item,
                            ];
                            // setCurrentPage(1);
                            // setCurrentPageAnswer(1);
                            const uniqueQuestions = _.uniqBy(
                                newQuestionLectures,
                                "id",
                            );

                            setQuestionLectures(uniqueQuestions);
                        } else {
                            const newQuestionLectures = [
                                ...dataQuestionLectures.item,
                            ];
                            setCurrentPage(1);
                            setCurrentPageAnswer(1);
                            const uniqueQuestions = _.uniqBy(
                                newQuestionLectures,
                                "id",
                            );
                            setQuestionLectures(uniqueQuestions);
                        }
                    } else {
                        const newQuestionLectures = [
                            ...questionLectures,
                            ...dataQuestionLectures.item,
                        ];

                        const uniqueQuestions = _.uniqBy(
                            newQuestionLectures,
                            "id",
                        );

                        setQuestionLectures(uniqueQuestions);
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchQuestionLectures();
    }, [currentPage, contentCourse, search, filterQuestion]);

    React.useEffect(() => {
        const fetchAnswerLectures = async () => {
            try {
                if (questionLectureModal) {
                    if (
                        answerLectures.length &&
                        questionLectureModal.question.id !==
                            answerLectures[0].questionLectureID
                    ) {
                        setCurrentPageAnswer(1);
                    }
                    const dataAnswerLectures: any =
                        await CourseServiceApi.getAnswerLectures(
                            questionLectureModal.question.id,
                            currentPageAnswer,
                            SIZE_PAGE_ANSWER,
                        );

                    setInfoPaginationAnswer({
                        totalPage: dataAnswerLectures.meta.pageCount,
                        totalItem: dataAnswerLectures.meta.itemCount,
                        size: dataAnswerLectures.meta.size,
                    });
                    if (!dataAnswerLectures.item) {
                        setAnswerLectures([]);
                    } else {
                        if (answerLectures.length) {
                            if (answerLectures.length) {
                                if (
                                    questionLectureModal.question.id !==
                                    answerLectures[0].questionLectureID
                                ) {
                                    setAnswerLectures([
                                        ...dataAnswerLectures.item,
                                    ]);
                                } else {
                                    const newAnswerLectures = [
                                        ...answerLectures,
                                        ...dataAnswerLectures.item,
                                    ];
                                    const uniqueAnswers = _.uniqBy(
                                        newAnswerLectures,
                                        "id",
                                    );

                                    setAnswerLectures(uniqueAnswers);
                                }
                            } else {
                                setAnswerLectures([...dataAnswerLectures.item]);
                            }
                        } else {
                            setAnswerLectures([...dataAnswerLectures.item]);
                        }
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchAnswerLectures();
    }, [currentPageAnswer, questionLectureModal]);

    const handleSetAnswerModal = (command: boolean) => {
        setOpenAnswerModal(command);
        if (!command) {
            // setQuestionLectureModal(null);
        }
    };

    const handleSetCreateQuestionModal = (command: boolean) => {
        setOpenCreateQuestionModal(command);
    };

    const handleNextPage = () => {
        setCurrentPage((currentPage) => currentPage + 1);
    };
    const handleSetQuestionLectureID = (
        question: QuestionLecture,
        infoIndexLecture: InfoIndexLecture,
    ) => {
        setQuestionLectureModal({
            infoIndex: infoIndexLecture,
            question: question,
        });
    };

    const handleChangeFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setFilterQUestion(+e.target.value);
        setInfoPagination({
            totalItem: 0,
            totalPage: 0,
            size: 0,
        });
        setCurrentPage(1);
        setQuestionLectures([]);
    };
    const handleNextPageAnswer = () => {
        setCurrentPageAnswer((currentPageAnswer) => currentPageAnswer + 1);
    };

    const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
    };

    return (
        <div className="pt-4">
            <AnswerModal
                openAnswerModal={openAnswerModal}
                answerLectures={answerLectures}
                infoPaginationAnswer={infoPaginationAnswer}
                currentPageAnswer={currentPageAnswer}
                questionLectureModal={questionLectureModal}
                handleNextPageAnswer={handleNextPageAnswer}
                handleSetAnswerModal={handleSetAnswerModal}
                setQuestionLectures={setQuestionLectures}
                setAnswerLectures={setAnswerLectures}
                setQuestionLectureModal={setQuestionLectureModal}
                setInfoPagination={setInfoPagination}
                setInfoPaginationAnswer={setInfoPaginationAnswer}
            />
            <CreateQuestionModal
                courseID={courseID}
                lectureID={lectureID}
                currentPage={currentPage}
                setQuestionLectures={setQuestionLectures}
                setInfoPagination={setInfoPagination}
                infoPagination={infoPagination}
                handleSetCreateQuestionModal={handleSetCreateQuestionModal}
                openCreateQuestionModal={openCreateQuestionModal}
            />
            <div className="mx-auto max-w-[848px] px-6 pt-8">
                <div>
                    <TextInput
                        type="text"
                        rightIcon={FaSearch}
                        placeholder="Search all course questions"
                        required
                        onChange={handleChangeSearch}
                        value={search}
                    />
                    <div className="flex gap-20 pb-8 pt-2">
                        <div>
                            <div className="block pb-2">
                                <Label htmlFor="filter" value="Filters:" />
                            </div>

                            <Select
                                onChange={handleChangeFilter}
                                id="filter"
                                required
                            >
                                <option value={ENTIRE_COURSE}>
                                    All lecture
                                </option>
                                <option value={THIS_LECTURE}>
                                    Current lecture
                                </option>
                            </Select>
                        </div>
                        <div>
                            <div className="block pb-2">
                                <Label value="Sort by:" />
                            </div>
                            <Select required>
                                <option>Sort by most recent</option>
                            </Select>
                        </div>
                    </div>
                    <div className="mb-4 mt-4 text-[19px] font-bold text-primary-black">
                        All questions in this{" "}
                        {filterQuestion === ENTIRE_COURSE
                            ? "course"
                            : "lecture"}{" "}
                        <span className="text-primary-gray">
                            ({infoPagination.totalItem})
                        </span>
                    </div>
                    <div>
                        {questionLectures.map((questionLecture) => (
                            <QuestionItem
                                key={questionLecture.id}
                                handleSetQuestionLectureID={
                                    handleSetQuestionLectureID
                                }
                                infoIndexLecture={getTitleLectureByID(
                                    course,
                                    questionLecture.lectureID,
                                )}
                                questionLecture={questionLecture}
                                handleSetAnswerModal={handleSetAnswerModal}
                            />
                        ))}

                        {/* <QuestionItem
                            handleSetAnswerModal={handleSetAnswerModal}
                        />
                        <QuestionItem
                            handleSetAnswerModal={handleSetAnswerModal}
                        /> */}
                    </div>
                    {infoPagination.totalPage > 1 &&
                        currentPage < infoPagination.totalPage && (
                            <div className="mt-8">
                                <Button
                                    onClick={() => handleNextPage()}
                                    className="w-full"
                                >
                                    See more
                                </Button>
                            </div>
                        )}

                    <div className="mb-30 mt-8">
                        <Button
                            onClick={() => handleSetCreateQuestionModal(true)}
                            outline
                        >
                            Ask a new question
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}
