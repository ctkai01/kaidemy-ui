import { Button, Modal } from 'flowbite-react';
import * as React from 'react';
import { HiOutlineExclamationCircle } from 'react-icons/hi2';

export interface IDeleteQuestionLectureModalProps {
    openModal: boolean;
    handleOpenModal: (command: boolean) => void;
    handleDeleteQuestionLecture: () => void
}

export default function DeleteQuestionLectureModal (props: IDeleteQuestionLectureModalProps) {
    const { openModal, handleOpenModal, handleDeleteQuestionLecture } = props;
    return (
        <div>
            <Modal
                show={openModal}
                size="md"
                onClose={() => handleOpenModal(false)}
                popup
            >
                <Modal.Header />
                <Modal.Body>
                    <div className="text-center">
                        <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" />
                        <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                        Are you sure you want to delete this question?
                        </h3>
                        <div className="flex justify-center gap-4">
                            <Button
                                color="gray"
                                className="font-bold"
                                onClick={() => handleOpenModal(false)}
                            >
                                No, cancel
                            </Button>
                            <Button
                                color="failure"
                                className="font-bold"
                                onClick={() =>
                                    handleDeleteQuestionLecture()
                                }
                            >
                                {"Yes, I'm sure"}
                            </Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}
