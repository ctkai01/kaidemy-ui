import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import * as React from "react";
import ReviewStar from "../../components/ui/core/ReviewStar";
import { LearningReview } from "../../models";
import { akaName, formatDistanceToNowTime } from "../../utils";
import DescriptionToggle from "../Course/DescriptionToggle";

export interface IReviewItemProps {
    review: LearningReview
}
const customTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            initials: {
                base: "bg-primary-black flex justify-center items-center",
                text: "text-white font-bold",
            },
        },
    },
};
export default function ReviewItem(props: IReviewItemProps) {
    const { review } = props;
    return (
        <div className="border-t border-primary-hover-gray pb-8 pt-6 first:border-t-0">
            <div className="mb-4">
                <div className="mb-4 flex">
                    <Flowbite theme={{ theme: customTheme }}>
                        {review.user.avatar ? (
                            <Avatar
                                img={review.user.avatar}
                                className="mr-4"
                                rounded
                            />
                        ) : (
                            <Avatar
                                placeholderInitials={akaName(review.user.name)}
                                rounded
                                className="mr-4"
                            />
                        )}
                    </Flowbite>
                    <div className="">
                        <p className="text-base font-bold text-primary-black">
                            {review.user.name}
                        </p>
                        <div className="mt-1 flex items-center">
                            <ReviewStar
                                color="#f69c08"
                                number={review.starCount ? review.starCount : 0}
                                size={3}
                            />
                            <div className="ml-1 text-xs font-bold text-primary-gray">
                                {formatDistanceToNowTime(
                                    review.updatedStarCount,
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <DescriptionToggle
                        leastHeight="176"
                        text={review.comment ? review.comment : ""}
                    />
                </div>
            </div>
        </div>
    );
}
