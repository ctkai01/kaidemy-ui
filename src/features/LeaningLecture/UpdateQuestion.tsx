import { yupResolver } from "@hookform/resolvers/yup";
import { Button, TextInput } from "flowbite-react";
import * as React from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Editor from "../../components/ui/core/Editor";
import {
    CreateQuestionLecture,
    QuestionLecture,
    UpdateQuestionLecture,
} from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { formatDistanceToNowTime } from "../../utils";
import {
    schemeCreateQuestionLecture,
    schemeUpdateQuestionLecture,
} from "../../validators/course";
import { QuestionLectureModal } from "./QASection";

export interface IUpdateQuestionProps {
    questionLectureModal: QuestionLectureModal;
    handleUpdateNormalMode: () => void;
    setQuestionLectures: React.Dispatch<
        React.SetStateAction<QuestionLecture[]>
    >;
    setQuestionLectureModal: React.Dispatch<
        React.SetStateAction<QuestionLectureModal | null>
    >;
}

export default function UpdateQuestion(props: IUpdateQuestionProps) {
    const {
        questionLectureModal,
        handleUpdateNormalMode,
        setQuestionLectures,
        setQuestionLectureModal,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        getValues,
        setValue,
    } = useForm<UpdateQuestionLecture>({
        mode: "onChange",
        defaultValues: {
            idQuestionLecture: questionLectureModal.question.id,
            title: questionLectureModal.question.title,
            description: questionLectureModal.question.description
                ? questionLectureModal.question.description
                : undefined,
        },
        resolver: yupResolver(schemeUpdateQuestionLecture),
    });

    const handleSetHTML = (value: string) => {
          if (value === "<p><br></p>") {
              setValue("description", "");
          } else {
              setValue("description", value);
          }
    };

    const handleUpdateQuestionLecture = async (data: UpdateQuestionLecture) => {
        // setQuestionLectures
        try {
            const body = {
                title: data.title,
                description: data.description ? data.description : "",
            };
         
            const dataUpdate = await CourseServiceApi.updateQuestionLectures(
                data.idQuestionLecture,
                body,
            );

            setQuestionLectures((questionLectures) => {
                const questionLecturesUpdate = [...questionLectures];

                const findIndex = questionLecturesUpdate.findIndex(
                    (questionLecture) =>
                        questionLecture.id === dataUpdate.id,
                );

                if (findIndex != -1) {
                    questionLecturesUpdate[findIndex] =
                        dataUpdate;
                    return questionLecturesUpdate;
                }

                return questionLectures;
            });
            setQuestionLectureModal((questionLectureModal) => {
                if (questionLectureModal) {
                    return {
                        ...questionLectureModal,
                        question: dataUpdate,
                    };
                } else {
                    return questionLectureModal;
                }
            });
            handleUpdateNormalMode();
            toast(
                <div className="font-bold">Update question successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Update question failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="w-full">
            <div className="mt-1 flex items-center text-xs">
                <Link
                    className="mr-[2px] text-primary-blue underline"
                    to={`/user/${questionLectureModal.question.user.id}`}
                >
                    {questionLectureModal.question.user.name}
                </Link>
                {" · "}
                <span className="ml-[2px] mr-[2px] text-primary-blue">
                    {questionLectureModal.infoIndex.type === "quiz"
                        ? `Quiz ${questionLectureModal.infoIndex.index}`
                        : `Lecture ${questionLectureModal.infoIndex.index}`}
                </span>
                {" · "}
                <span className="ml-[2px] text-primary-black">
                    {formatDistanceToNowTime(
                        questionLectureModal.question.createdAt,
                    )}
                </span>
            </div>
            <div className="mt-2">
                <form onSubmit={handleSubmit(handleUpdateQuestionLecture)}>
                    <TextInput
                        type="text"
                        color={errors.title ? "failure" : ""}
                        {...register("title")}
                        helperText={
                            <>
                                {errors.title ? (
                                    <>
                                        <span className="font-medium">
                                            Oops!{" "}
                                        </span>
                                        <span>{errors.title.message}</span>
                                    </>
                                ) : (
                                    <></>
                                )}
                            </>
                        }
                    />
                    <div className="mt-4">
                        <Editor
                            defaultValue={
                                getValues("description")
                                    ? getValues("description")
                                    : ""
                            }
                            handleSetHTML={handleSetHTML}
                            isImage={false}
                        />
                    </div>
                    <div className="mt-8 flex justify-end gap-4">
                        <Button
                            onClick={() => {
                                handleUpdateNormalMode();
                            }}
                            color="dark"
                        >
                          Cancel
                        </Button>
                        <Button type="submit">Save</Button>
                    </div>
                </form>
            </div>
        </div>
    );
}
