import {
    CustomFlowbiteTheme,
    Flowbite,
    Tabs,
    TabsComponent,
} from "flowbite-react";
import { useState } from "react";
import { ContentCourse, CourseShow } from "../../models";
import QASection from "./QASection";
import ReviewsSection from "./ReviewsSection";

const customTheme: CustomFlowbiteTheme = {
    tab: {
        tablist: {
            tabitem: {
                base: "flex items-center justify-center p-4 rounded-t-lg text-sm font-medium first:ml-0 disabled:cursor-not-allowed disabled:text-gray-400 disabled:dark:text-gray-500",
            },
            base: "flex text-center",
        },
    },
};

export interface IActionLectureTabsProps {
    lectureID: number;
    courseID: number;
    course: CourseShow;
    contentCourse: ContentCourse
}

export default function ActionLectureTabs(props: IActionLectureTabsProps) {
    const [tab, setActiveTab] = useState(0);
    const { courseID, lectureID, course, contentCourse } = props;
    return (
        <div>
            <Flowbite theme={{ theme: customTheme }}>
                <TabsComponent
                    onActiveTabChange={(tab) => setActiveTab(tab)}
                    aria-label="Default tabs"
                    style="underline"
                >
                    <Tabs.Item active title="Q&A">
                        <QASection
                            contentCourse={contentCourse}
                            course={course}
                            lectureID={lectureID}
                            courseID={courseID}
                        />
                    </Tabs.Item>
                    <Tabs.Item title="Reviews">
                        <ReviewsSection tab={tab} courseID={course.id} />
                    </Tabs.Item>
                </TabsComponent>
            </Flowbite>
        </div>
    );
}
