import * as React from "react";
import { QUIZ_TYPE } from "../../constants";
import { ContentCourse, CourseShow, NavigateLecture } from "../../models";
import { initContentCourse } from "../../utils";
import ActionLectureTabs from "./ActionLectureTabs";
import LearningHeader from "./LearningHeader";
import SectionLectureSidebar from "./SectionLectureSibar";
import VideoLearning from "./VideoLearning";

export interface ILearningLectureDetailProps {
    handleSetOpenLeaveRatingModal: (command: boolean) => void;
    handleMarkLecture: (curriculumID: number, lectureID: number) => void
    course: CourseShow;
}

export default function LearningLectureDetail(
    props: ILearningLectureDetailProps,
) {
    const { course, handleSetOpenLeaveRatingModal, handleMarkLecture } = props;

    const [contentCourse, setContentCourse] =
        React.useState<ContentCourse | null>(
            initContentCourse(course.curriculums),
        );
    const [isFinishCourse, setIsFinishCourse] = React.useState(false);
    const handleContentCourse = (content: ContentCourse | null) => {
        setContentCourse(content);
    };

    const handleFinishCourse = () => {
        setIsFinishCourse(true);
    };

    const handleResetFinishCourse = () => {
        setIsFinishCourse(false);
    };

    const lectureList = React.useMemo(() => {
        const result: NavigateLecture[] = [];
        const indexLecture = {
            quiz: 1,
            article: 1,
        };
        course.curriculums.forEach((curriculum) => {
            if (curriculum.lectures) {
                curriculum.lectures.forEach((lecture) => {
                    const indexNumber =
                        lecture.type === QUIZ_TYPE
                            ? indexLecture.quiz++
                            : indexLecture.article++;
                    result.push({
                        lecture: lecture,
                        indexNumber: indexNumber,
                        curriculumID: curriculum.id,
                    });
                });
            }
                
        });
        return result;
    }, []);


    // cosnt openLeaveRatingModal = () => {
        
    // }

    return (
        <div>
            <LearningHeader
                course={course}
                handleSetOpenLeaveRatingModal={handleSetOpenLeaveRatingModal}
            />
            {/* <LeaveRatingModal
                        openLeaveRatingModal={openLeaveRatingModal}
                        handleSetOpenLeaveRatingModal={
                            handleSetOpenLeaveRatingModal
                        }
                    /> */}
            <div className="w-[75%]">
                <VideoLearning
                    lectureList={lectureList}
                    contentCourse={contentCourse}
                    isFinishCourse={isFinishCourse}
                    handleFinishCourse={handleFinishCourse}
                    handleContentCourse={handleContentCourse}
                    handleResetFinishCourse={handleResetFinishCourse}
                />
                <SectionLectureSidebar
                    course={course}
                    contentCourse={contentCourse}
                    handleMarkLecture={handleMarkLecture}
                    handleContentCourse={handleContentCourse}
                    handleResetFinishCourse={handleResetFinishCourse}
                />
                {contentCourse && (
                    <div className="px-4">
                        <ActionLectureTabs
                            course={course}
                            contentCourse={contentCourse}
                            lectureID={contentCourse.id}
                            courseID={course.id}
                        />
                    </div>
                )}
            </div>
        </div>
    );
}
