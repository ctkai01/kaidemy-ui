import { Button, Label, Rating, Select, TextInput } from "flowbite-react";
import React, { useEffect } from "react";
import { FaSearch } from "react-icons/fa";
import RatingMUI from "@mui/material/Rating";
import {
    LearningReview,
    OverallReviewCourse,
    PaginationInfo,
} from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { formatNumber } from "../../utils";
import ReviewItem from "./ReviewItem";

export interface IReviewsSectionProps {
    courseID: number;
    tab: number;
}
const SIZE_PAGE = 5;

export const ALL_RATING = 6;
export const FIVE_RATING = 5;
export const FOUR_RATING = 4;
export const THREE_RATING = 3;
export const TWO_RATING = 2;
export const ONE_RATING = 1;
export default function ReviewsSection(props: IReviewsSectionProps) {
    const { tab } = props;

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    const [learningReviews, getLearningReviews] = React.useState<
        LearningReview[]
    >([]);

    const [search, setSearch] = React.useState("");
    const [filterRating, setFilterRating] = React.useState(ALL_RATING);

    const [overallReview, setOverallReview] =
        React.useState<OverallReviewCourse>({
            averageReview: 0,
            overall: {
                fiveStar: 0,
                fourStar: 0,
                threeStar: 0,
                twoStar: 0,
                oneStar: 0,
            },
            totalReview: 0,
        });

    const [currentPage, setCurrentPage] = React.useState(1);
    const { courseID } = props;

    React.useEffect(() => {
        const fetchReviews = async () => {
            try {
                const data = await CourseServiceApi.getReviews(
                    courseID,
                    currentPage,
                    SIZE_PAGE,
                    filterRating,
                    search,
                );
                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data.item) {
                    getLearningReviews([]);
                } else {
                    if (
                        filterRating == ALL_RATING &&
                        !search &&
                        currentPage !== 1
                    ) {
                        getLearningReviews((learningReview) => [
                            ...learningReview,
                            ...data.item,
                        ]);
                    } else {
                        getLearningReviews([...data.item]);
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchReviews();
    }, [currentPage, filterRating, search, tab]);

    React.useEffect(() => {
        const fetchOverallOverview = async () => {
            try {
                const data =
                    await CourseServiceApi.getOverallReviewByCourseID(courseID);
                setOverallReview({
                    averageReview: data.averageReview,
                    totalReview: data.totalReview,
                    overall: data.overall,
                });
            } catch (e) {
                console.log(e);
            }
        };
        fetchOverallOverview();
    }, []);
    const handleChangeFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setFilterRating(+e.target.value);
        setCurrentPage(1);
    };

    const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
        if (e.target.value) {
            setCurrentPage(1);
        }
    };
    const handleNextPage = () => {
        setCurrentPage((currentPage) => currentPage + 1);
    };
    return (
        <div className="pt-4">
            <div className="mx-auto max-w-[848px] px-6 pt-8">
                <div>
                    <h2 className="mb-6 text-2xl font-bold text-primary-black">
                  Student feedback
                    </h2>
                    <div className="mb-8 flex items-center">
                        <div className="flex-1">
                            <Rating.Advanced
                                percentFilled={overallReview.overall.fiveStar}
                                className="mb-2"
                            >
                                5 star
                            </Rating.Advanced>
                            <Rating.Advanced
                                percentFilled={overallReview.overall.fourStar}
                                className="mb-2"
                            >
                                4 star
                            </Rating.Advanced>
                            <Rating.Advanced
                                percentFilled={overallReview.overall.threeStar}
                                className="mb-2"
                            >
                                3 star
                            </Rating.Advanced>
                            <Rating.Advanced
                                percentFilled={overallReview.overall.twoStar}
                                className="mb-2"
                            >
                                2 star
                            </Rating.Advanced>
                            <Rating.Advanced
                                percentFilled={overallReview.overall.oneStar}
                            >
                                1 star
                            </Rating.Advanced>
                        </div>
                        <div>
                            <div className="">
                                <h1 className="text-6xl font-bold text-[#b4690e]">
                                    {overallReview.averageReview.toFixed(1)}
                                </h1>
                                <div className="mb-2">
                                    <RatingMUI
                                        value={overallReview.averageReview}
                                        precision={0.5}
                                        readOnly
                                    />
                                </div>

                                <p className="mb-4 text-sm font-bold text-[#b4690e]">
                                    {formatNumber(overallReview.totalReview)}{" "}
                                    reviews
                                </p>
                            </div>
                        </div>
                    </div>
                    <h2 className="mb-4 text-2xl font-bold text-primary-black">
                       Reviews
                    </h2>
                    <div className="flex mb-6  items-end pt-4">
                        <div className="flex-1">
                            <TextInput
                                type="text"
                                rightIcon={FaSearch}
                                placeholder="Search reviews"
                                required
                                onChange={handleChangeSearch}
                                value={search}
                            />
                        </div>
                        <div className="ml-6">
                            <div className="block pb-2">
                                <Label htmlFor="star" value="Lọc xếp hạng:" />
                            </div>
                            <Select
                                onChange={handleChangeFilter}
                                id="star"
                                required
                            >
                                <option value={ALL_RATING}>
                                All ratings
                                </option>
                                <option value={FIVE_RATING}>Five stars</option>
                                <option value={FOUR_RATING}>Four stars</option>
                                <option value={THREE_RATING}>Three stars</option>
                                <option value={TWO_RATING}>Two stars</option>
                                <option value={ONE_RATING}>One stars</option>
                            </Select>
                        </div>
                    </div>
                    <div>
                        {learningReviews.map((review) => (
                            <ReviewItem key={review.id} review={review} />
                        ))}
                    </div>
                    {infoPagination.totalPage > 1 &&
                        currentPage < infoPagination.totalPage && (
                            <div className="mt-8">
                                <Button
                                    onClick={() => handleNextPage()}
                                    className="w-full font-bold"
                                >
                                   See more
                                </Button>
                            </div>
                        )}
                </div>
            </div>
        </div>
    );
}
