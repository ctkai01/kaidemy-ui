// import icon from '../../assets/'

import {
    CustomFlowbiteTheme,
    Dropdown,
    Flowbite,
    Tooltip,
} from "flowbite-react";
import { useMemo } from "react";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { BsThreeDotsVertical } from "react-icons/bs";
import { FaFolder, FaStar } from "react-icons/fa";
import { IoIosArrowDown } from "react-icons/io";
import { IoPencilSharp } from "react-icons/io5";
import { TfiCup } from "react-icons/tfi";
import { Link } from "react-router-dom";
import { CourseShow } from "../../models";
import InfoProcessLecture from "./InfoProcessLecture";

export interface ILearningHeaderProps {
    handleSetOpenLeaveRatingModal: (command: boolean) => void;
    course: CourseShow;
}

const customTheme: CustomFlowbiteTheme = {
    tooltip: {
        target: "w-fit h-full",
    },
};

export default function LearningHeader(props: ILearningHeaderProps) {
    const { course, handleSetOpenLeaveRatingModal } = props;

    const [courseProcess, countComplete, totalLecture] = useMemo(() => {
        let numberProcess = 0;
        let totalLecture = 0;
        let countComplete = 0;

        course.curriculums.forEach((curriculum) => {
            if (curriculum.lectures) {
                totalLecture += curriculum.lectures.length;

                curriculum.lectures.forEach((lecture) => {
                    if (
                        lecture.learningLectures.length &&
                        lecture.learningLectures[0].isDone
                    ) {
                        countComplete++;
                    }
                });
            }
        });


        numberProcess = Math.round((countComplete / totalLecture) * 100);
        return [numberProcess, countComplete, totalLecture];
    }, [course]);
    return (
        <div className="h-14 border-b border-[#3e4143] bg-primary-black px-4">
            <header className="flex h-full items-center justify-between gap-6">
                <Link to={"/"}>
                    <img
                        height={34}
                        width={91}
                        src="https://kaidemy.b-cdn.net/avatar/hustdemy-high-resolution-logo-transparent.png"
                        loading="lazy"
                        className="h-[34px] object-contain"
                    />
                </Link>
                <div className="h-[40%] w-[1px] border-l border-[#3e4143]"></div>
                <div className="flex-1">
                    <h1>
                        <Link
                            to={`/course/${course.id}`}
                            className="white overflow-hidden text-ellipsis text-base text-white"
                        >
                            {course.title}
                        </Link>
                    </h1>
                </div>
                {/* <div
                    onClick={() => handleSetOpenLeaveRatingModal(true)}
                    className="group flex h-full cursor-pointer items-center"
                >
                    <FaStar className="text-primary-gray" />
                    <span className="ml-1 text-sm font-normal text-white group-hover:text-[#ccc]">
                        Để lại đánh giá
                    </span>
                </div> */}
                <div className="h-full">
                    <Flowbite theme={{ theme: customTheme }}>
                        <Tooltip
                            style="light"
                            content={
                                <InfoProcessLecture
                                    countComplete={countComplete}
                                    totalLecture={totalLecture}
                                />
                            }
                            trigger="click"
                            // className=""
                        >
                            <div className="group flex h-full cursor-pointer items-center">
                                <button className="h-[35px] w-[35px]">
                                    <CircularProgressbarWithChildren
                                        value={courseProcess}
                                    >
                                        <TfiCup className="text-primary-gray" />
                                    </CircularProgressbarWithChildren>
                                </button>
                                <div className="ml-2 text-sm text-white group-hover:text-[#ccc]">
                                    You progress
                                </div>
                                <IoIosArrowDown className="ml-1 text-white group-hover:text-[#ccc]" />
                            </div>
                        </Tooltip>
                    </Flowbite>
                </div>
                <div>
                    {/* <Tooltip
                        trigger="click"
                        content={<ActionLecture />}
                        style="light"
                    >
                        <div className="flex h-10 w-10 cursor-pointer items-center justify-center rounded border border-white bg-primary-black hover:bg-[#ffffff14]">
                            <BsThreeDotsVertical className="text-white" />
                        </div>
                    </Tooltip> */}

                    {/* <Dropdown
                        label=""
                        renderTrigger={() => (
                            <div className="flex h-10 w-10 cursor-pointer items-center justify-center rounded border border-white bg-primary-black hover:bg-[#ffffff14]">
                                <BsThreeDotsVertical className="text-white" />
                            </div>
                        )}
                        dismissOnClick={false}
                    >
                        <Dropdown.Item>
                            <div className="group flex items-center">
                                <FaFolder className="mr-2 group-hover:text-primary-blue" />
                                <span className="group-hover:text-primary-blue">
                                    Save trữ khóa học này
                                </span>
                            </div>
                        </Dropdown.Item>
                        <Dropdown.Item>
                            <div
                                onClick={() =>
                                    handleSetOpenLeaveRatingModal(true)
                                }
                                className="group flex items-center"
                            >
                                <IoPencilSharp className="mr-2 group-hover:text-primary-blue" />
                                <span className="group-hover:text-primary-blue">
                                    Chỉnh sửa xếp hạng của bạn
                                </span>
                            </div>
                        </Dropdown.Item>
                    </Dropdown> */}
                </div>
            </header>
        </div>
    );
}
