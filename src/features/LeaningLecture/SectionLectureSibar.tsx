import * as React from "react";
import { IoClose } from "react-icons/io5";
import { LECTURE_TYPE, QUIZ_TYPE } from "../../constants";
import { ContentCourse, CourseShow } from "../../models";
import { totalTimeLecture } from "../../utils";
import AccordionController from "../Course/AccordionController";
import AccordionItem from "../Course/AccordionItem";
import LearningLectureItem from "./LearningLectureItem";
import LecturePanel from "./LecturePanel";
export interface ISectionLectureSidebarProps {
    course: CourseShow;
    contentCourse: ContentCourse | null;
    handleContentCourse: (content: ContentCourse | null) => void;
    handleResetFinishCourse: () => void;
    handleMarkLecture: (curriculumID: number, lectureID: number) => void;
}

export default function SectionLectureSidebar(
    props: ISectionLectureSidebarProps,
) { 
    const {
        course,
        contentCourse,
        handleContentCourse,
        handleResetFinishCourse,
        handleMarkLecture,
    } = props;

     const indexLecture = {
         quiz: 1,
         article: 1,
         general: 0,
     };

    const [prevScrollPos, setPrevScrollPos] = React.useState(0);
    const [isTopSidebar, setIsTopSidebar] = React.useState(false);

    React.useEffect(() => {
        const handleScroll = () => {
            const currentScrollPos = window.scrollY;
            // If scrolling down and past a certain distance (e.g., 100 pixels), hide the header
            if (currentScrollPos > 56) {
                setIsTopSidebar(true);
            } else {
                setIsTopSidebar(false);
            }

            setPrevScrollPos(currentScrollPos);
        };

        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, [prevScrollPos]);

    return (
        <div
            className={`fixed right-0 ${
                isTopSidebar ? "top-0 h-full" : "h-56-calc top-14 "
            } flex w-1/4 flex-col bg-white transition-all`}
        >
            <div className="flex">
                <div className="flex w-full items-center justify-between border border-primary-hover-gray py-2 pl-4 pr-2 text-base">
                    <h2 className="text-base font-bold text-primary-black">
                        Nội dung khóa học
                    </h2>
                    <button className="flex items-center justify-between p-2">
                        <IoClose className="h-5 w-5" />
                    </button>
                </div>
            </div>
            <div className="flex-1 overflow-y-auto overflow-x-hidden">
                <AccordionController>
                    {course.curriculums.map((curriculum, indexCurriculum) => (
                        <AccordionItem
                            paddingPanel="p-4"
                            key={curriculum.id}
                            isOpen={
                                contentCourse?.curriculumID === curriculum.id
                                    ? true
                                    : false
                            }
                            positionIcon="end"
                            title={
                                <LecturePanel
                                    index={indexCurriculum}
                                    numberLecture={
                                        curriculum.lectures
                                            ? curriculum.lectures.length
                                            : 0
                                    }
                                    time={totalTimeLecture(
                                        curriculum.lectures
                                            ? curriculum.lectures
                                            : [],
                                    )}
                                    title={curriculum.title}
                                />
                            }
                        >
                            <ul className="border-t border-primary-hover-gray">
                                {curriculum.lectures &&
                                    curriculum.lectures.map(
                                        (lecture, index) => {
                                            const indexNumber =
                                                lecture.type === QUIZ_TYPE
                                                    ? indexLecture.quiz++
                                                    : indexLecture.article++;

                                          
                                            return (
                                                <LearningLectureItem
                                                    key={lecture.id}
                                                    handleContentCourse={
                                                        handleContentCourse
                                                    }
                                                    handleResetFinishCourse={
                                                        handleResetFinishCourse
                                                    }
                                                    indexList={
                                                        indexLecture.general++
                                                    }
                                                    curriculumID={curriculum.id}
                                                    index={indexNumber}
                                                    selected={
                                                        contentCourse
                                                            ? contentCourse.id ===
                                                              lecture.id
                                                            : false
                                                    }
                                                    lecture={lecture}
                                                    // handleSetVideoURL={
                                                    //     handleSetVideoURL
                                                    // }
                                                    handleMarkLecture={
                                                        handleMarkLecture
                                                    }
                                                    type={
                                                        lecture.type ===
                                                        LECTURE_TYPE
                                                            ? lecture.article
                                                                ? "article"
                                                                : "video"
                                                            : "quiz"
                                                    }
                                                />
                                            );
                                        },
                                    )}
                            </ul>
                        </AccordionItem>
                    ))}
                </AccordionController>
            </div>
        </div>
    );
}
