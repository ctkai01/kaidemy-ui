import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Label, Modal, TextInput } from "flowbite-react";
import * as React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import Editor from "../../components/ui/core/Editor";
import {
    CreateQuestionLecture,
    ForgotPassword,
    PaginationInfo,
    QuestionLecture,
} from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { schemeCreateQuestionLecture } from "../../validators/course";
import { SIZE_PAGE } from "./QASection";

export interface ICreateQuestionModalProps {
    openCreateQuestionModal: boolean;
    lectureID: number;
    courseID: number;
    currentPage: number;
    infoPagination: PaginationInfo
    setQuestionLectures: React.Dispatch<
        React.SetStateAction<QuestionLecture[]>
    >;
    setInfoPagination: React.Dispatch<React.SetStateAction<PaginationInfo>>;
    handleSetCreateQuestionModal: (command: boolean) => void;
}

export default function CreateQuestionModal(props: ICreateQuestionModalProps) {
    const {
        lectureID,
        courseID,
        currentPage,
        infoPagination,
        setInfoPagination,
        handleSetCreateQuestionModal,
        setQuestionLectures,
        openCreateQuestionModal,
    } = props;
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
    } = useForm<CreateQuestionLecture>({
        mode: "onChange",
        defaultValues: {
            courseID: courseID,
            lectureID: lectureID,
        },
        resolver: yupResolver(schemeCreateQuestionLecture),
    });

    const handleSetHTML = (value: string) => {
        setValue("description", value);
    };

    const handleCreateQuestion = async (data: CreateQuestionLecture) => {
        try {
            const body: any = {
                courseID,
                lectureID,
                title: data.title
            };
            if (data.description) {
                body.description = data.description;
            }
            const dataCreate =
                await CourseServiceApi.createQuestionLecture(body);
            setQuestionLectures((questionLectures) => {
                const questionLecturesUpdate = [...questionLectures];
                let newData = [
                    dataCreate,
                    ...questionLecturesUpdate,
                ];

               if (
                   infoPagination.totalItem + 1 >
                   SIZE_PAGE * infoPagination.totalPage
               ) {
                   //    newData = newData.splice(0, SIZE_PAGE);
                   setInfoPagination((data) => {
                       return {
                           ...data,
                           totalPage: data.totalPage + 1,
                           totalItem: data.totalItem + 1,
                       };
                   });
               } else {
                   setInfoPagination((data) => {
                       return {
                           ...data,
                           totalItem: data.totalItem + 1,
                       };
                   });
               }
                return newData;
            });
            toast(<div className="font-bold">Create question successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
            handleSetCreateQuestionModal(false);
            reset();
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Create question failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    return (
        <div>
            <Modal
                dismissible
                show={openCreateQuestionModal}
                onClose={() => {
                    handleSetCreateQuestionModal(false);
                    reset();
                }}
            >
                <form onSubmit={handleSubmit(handleCreateQuestion)}>
                    <Modal.Header>Create question</Modal.Header>
                    <Modal.Body>
                        <div className="mb-2 block">
                            <Label value="Title or summary" />
                        </div>
                        <TextInput
                            type="text"
                            placeholder="Example: Why do we use fit_transform() for Training_Set?"
                            color={errors.title ? "failure" : ""}
                            {...register("title")}
                            helperText={
                                <>
                                    {errors.title ? (
                                        <>
                                            <span className="font-medium">
                                                Oops!{" "}
                                            </span>
                                            <span>{errors.title.message}</span>
                                        </>
                                    ) : (
                                        <></>
                                    )}
                                </>
                            }
                        />
                        <div className="mt-2">
                            <div className="mb-2 block">
                                <Label value="Details (Optional)" />
                            </div>
                            <Editor
                                handleSetHTML={handleSetHTML}
                                isImage={false}
                                placeholder="Example: Why do we use fit_transform() for the Training_Set?."
                            />
                            {errors.description ? (
                                <div className="mt-2 text-sm text-red-700">
                                    <span className="font-medium ">Oops! </span>
                                    <span>{errors.description.message}</span>
                                </div>
                            ) : (
                                <></>
                            )}
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className="flex w-full justify-end">
                            <Button
                                color="gray"
                                className="mr-4"
                                onClick={() => {
                                    handleSetCreateQuestionModal(false);
                                    reset();
                                }}
                            >
                                Cancel
                            </Button>
                            <Button type="submit">Create</Button>
                        </div>
                        {/* <Button color="gray" onClick={() => setOpenModal(false)}>
                        Decline
                    </Button> */}
                    </Modal.Footer>
                </form>
            </Modal>
        </div>
    );
}
