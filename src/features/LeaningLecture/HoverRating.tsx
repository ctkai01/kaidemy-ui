import * as React from "react";
import Rating from "@mui/material/Rating";
import Box from "@mui/material/Box";
import StarIcon from "@mui/icons-material/Star";

const labels: { [index: string]: string } = {
    "1": "Absolutely awful, not what I expected at all",
    "1.5": "Awful/Poor",
    "2": "Quite disappointing",
    "2.5": "Poor/Average",
    "3": "Average, could be better",
    "3.5": "Average/Good",
    "4": "Good, meets my expectations",
    "4.5": "Good/Amazing",
    "5": "Excellent, exceeds expectations",
};

function getLabelText(value: number) {
    return `${value} Star${value !== 1 ? "s" : ""}, ${labels[value]}`;
}

interface IHoverRating {
    handleSetValue: (value: number) => void;
    value: number | null
}

export default function HoverRating(props: IHoverRating) {
    const [hover, setHover] = React.useState(-1);
    const { value, handleSetValue } = props;
    return (
        <div className="w-full">
            <Box
                sx={{
                    // width: 200,
                    display: "flex",
                    justifyContent: "center",
                    flexDirection: "column",
                    alignItems: "center",
                }}
            >
                <Rating
                    name="hover-feedback"
                    value={value}
                    precision={1}
                    getLabelText={getLabelText}
                    onChange={(event, newValue) => {
                        if (newValue) {
                            handleSetValue(newValue)
                        }
                    }}
                    onChangeActive={(event, newHover) => {
                        setHover(newHover);
                    }}
                    emptyIcon={
                        <StarIcon
                            style={{ opacity: 0.55 }}
                            fontSize="inherit"
                        />
                    }
                />
                {value !== null && (
                    <Box sx={{ ml: 2, height: 12, mt: 2 }}>
                        {labels[hover !== -1 ? hover : value]}
                    </Box>
                )}
            </Box>
        </div>
    );
}
