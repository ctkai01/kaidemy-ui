import { Button, Modal, Spinner } from 'flowbite-react';
import * as React from 'react';
import Editor from '../../components/ui/core/Editor';
import { useAppSelector } from '../../hooks/redux-hook';
import { AnswerLecture, PaginationInfo, QuestionLecture } from '../../models';
import { selectUserAuth } from '../../services/state/redux/authSlide';
import AnswerItemModalModal from './AnswerItemModal';
import CreateAnswer from './CreateAnswer';
import { QuestionLectureModal } from './QASection';
import QuestionItemModal from './QuestionItemModal';

export interface IAnswerModalProps {
    openAnswerModal: boolean;
    answerLectures: AnswerLecture[];
    questionLectureModal: QuestionLectureModal | null;
    infoPaginationAnswer: PaginationInfo;
    currentPageAnswer: number;
    handleNextPageAnswer: () => void;
    handleSetAnswerModal: (command: boolean) => void;
    setQuestionLectures: React.Dispatch<
        React.SetStateAction<QuestionLecture[]>
    >;
    setAnswerLectures: React.Dispatch<React.SetStateAction<AnswerLecture[]>>;
    setQuestionLectureModal: React.Dispatch<
        React.SetStateAction<QuestionLectureModal | null>
    >;
    setInfoPagination: React.Dispatch<React.SetStateAction<PaginationInfo>>;
    setInfoPaginationAnswer: React.Dispatch<
        React.SetStateAction<PaginationInfo>
    >;
}

export default function AnswerModal (props: IAnswerModalProps) {
    const {
        openAnswerModal,
        questionLectureModal,
        answerLectures,
        infoPaginationAnswer,
        currentPageAnswer,
        handleNextPageAnswer,
        handleSetAnswerModal,
        setQuestionLectures,
        setQuestionLectureModal,
        setInfoPagination,
        setAnswerLectures,
        setInfoPaginationAnswer,
    } = props;
    const user = useAppSelector(selectUserAuth)
 

    return (
        <Modal
            dismissible
            show={openAnswerModal}
            size="5xl"
            onClose={() => handleSetAnswerModal(false)}
        >
            <Modal.Header className="text-center">Answers</Modal.Header>
            <Modal.Body>
                {questionLectureModal ? (
                    <QuestionItemModal
                        questionLectureModal={questionLectureModal}
                        setQuestionLectures={setQuestionLectures}
                        setQuestionLectureModal={setQuestionLectureModal}
                        setInfoPagination={setInfoPagination}
                        handleSetAnswerModal={handleSetAnswerModal}
                        user={user}
                    />
                ) : (
                    <div className="text-center">
                        <Spinner />
                    </div>
                )}

                <h2 className="text-base font-bold text-primary-black">
                    {answerLectures.length} anwers
                </h2>
                <div>
                    {answerLectures.map((answer) => (
                        <AnswerItemModalModal
                            user={user}
                            key={answer.id}
                            answer={answer}
                            setQuestionLectures={setQuestionLectures}
                            currentPageAnswer={currentPageAnswer}
                            setAnswerLectures={setAnswerLectures}
                            setInfoPaginationAnswer={setInfoPaginationAnswer}
                        />
                    ))}
                </div>
                {infoPaginationAnswer.totalPage > 1 &&
                    currentPageAnswer < infoPaginationAnswer.totalPage && (
                        <div className="mt-8 flex justify-center">
                            <Button
                                className="w-1/2"
                                onClick={() => handleNextPageAnswer()}
                            >
                                Upload more answer
                            </Button>
                        </div>
                    )}

                <div className="mt-6">
                    <div className="mb-3 text-sm font-bold text-primary-black">
                        Write your answer
                    </div>
                    {questionLectureModal && (
                        <CreateAnswer
                            setInfoPaginationAnswer={setInfoPaginationAnswer}
                            currentPage={currentPageAnswer}
                            infoPaginationAnswer={infoPaginationAnswer}
                            setAnswerLectures={setAnswerLectures}
                            questionLectureModal={questionLectureModal}
                            setQuestionLectures={setQuestionLectures}
                        />
                    )}
                </div>
            </Modal.Body>
        </Modal>
    );
}
