import { Avatar, Dropdown } from "flowbite-react";
import * as React from "react";
import { BsThreeDotsVertical } from "react-icons/bs";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { AnswerLecture, PaginationInfo, QuestionLecture, User } from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { akaName, formatDistanceToNowTime } from "../../utils";
import DeleteAnswerLectureModal from "./DeleteAnswerLectureModal";
import { SIZE_PAGE_ANSWER } from "./QASection";
import UpdateAnswer from "./UpdateAnswer";

export interface IAnswerItemModalModalProps {
    answer: AnswerLecture;
    user?: User;
    currentPageAnswer: number
    setAnswerLectures: React.Dispatch<React.SetStateAction<AnswerLecture[]>>;
    setInfoPaginationAnswer: React.Dispatch<
        React.SetStateAction<PaginationInfo>
    >;
    setQuestionLectures: React.Dispatch<React.SetStateAction<QuestionLecture[]>>;

    
}
const ANSWER_MODE = {
    NORMAL: 0,
    UPDATE: 1,
};
export default function AnswerItemModalModal(
    props: IAnswerItemModalModalProps,
) {
    const {
        answer,
        user,
        currentPageAnswer, setAnswerLectures,
        setQuestionLectures,
        setInfoPaginationAnswer,
    } = props;
    const [modeAnswer, getModeAnswer] = React.useState<number>(
        ANSWER_MODE.NORMAL,
    );
    const [openDeleteAnswerModal, setOpenDeleteAnswerModal] =
        React.useState(false);
    const handleUpdateMode = () => {
        getModeAnswer(ANSWER_MODE.UPDATE);
    };

    const handleUpdateNormalMode = () => {
        getModeAnswer(ANSWER_MODE.NORMAL);
    };

    const handleOpenDeleteAnswerModal = (command: boolean) => {
        setOpenDeleteAnswerModal(command);
    };

    const handleDeleteAnswerLecture = async () => {
        try {
            await CourseServiceApi.deleteAnswerLectures(answer.id);
            setAnswerLectures((answerLectures) => {
                const newAnswerLectures = answerLectures.filter(
                    (answerLecture) => answerLecture.id != answer.id,
                );

               setInfoPaginationAnswer((infoPagination: PaginationInfo) => {
                   return {
                       ...infoPagination,
                       totalItem: infoPagination.totalItem - 1,
                   };
               });
                
                return newAnswerLectures;
            });
            setQuestionLectures((questionLectures) => {
                const questionLecturesUpdate = [...questionLectures];

                const findIndex = questionLecturesUpdate.findIndex(
                    (questionLecture) =>
                        questionLecture.id === answer.questionLectureID,
                );

                if (findIndex != -1) {
                    const questionLectureUpdate = {
                        ...questionLecturesUpdate[findIndex],
                    };

                    questionLectureUpdate.totalAnswer =
                        questionLectureUpdate.totalAnswer - 1;

                    questionLecturesUpdate[findIndex] = questionLectureUpdate;

                    return questionLecturesUpdate;
                }

                return questionLectures;
            });
            handleOpenDeleteAnswerModal(false);

            toast(
                <div className="font-bold">Deleted answer successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Deleted answer failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="flex px-6 py-4">
            <DeleteAnswerLectureModal
                openModal={openDeleteAnswerModal}
                handleDeleteAnswerLecture={handleDeleteAnswerLecture}
                handleOpenModal={handleOpenDeleteAnswerModal}
            />
            {/* {modeAnswer === ANSWER_MODE.NORMAL && ( */}
            <>
                <div>
                    <div className="h-8 w-8">
                        {/* <img
                        className="h-8 w-8 rounded-full"
                        src="https://img-c.udemycdn.com/user/50x50/88114364_d991.jpg"
                        loading="lazy"
                    /> */}
                        {answer.user.avatar ? (
                            <Avatar
                                img={answer.user.avatar}
                                alt="avatar"
                                rounded
                                size="sm"
                            />
                        ) : (
                            <Avatar
                                placeholderInitials={akaName(answer.user.name)}
                                rounded
                                size="sm"
                            />
                        )}
                    </div>
                </div>
                <div className="min-w-[1px] flex-1 pl-4">
                    <div className="mb-4 flex justify-between">
                        <div className="flex-1 pr-4">
                            <div className="mb-1 mt-1 flex items-center justify-between text-xs">
                                <Link
                                    className="mr-[2px] text-primary-blue underline"
                                    to={`/user/${answer.user.id}`}
                                >
                                    {answer.user.name}
                                </Link>
                                <div>
                                    {user && user.id === answer.user.id && (
                                        <div>
                                            <Dropdown
                                                renderTrigger={() => (
                                                    <div className="group relative z-30 flex h-full w-full items-center justify-center">
                                                        <BsThreeDotsVertical className="cursor-pointer" />
                                                    </div>
                                                )}
                                                label=""
                                                dismissOnClick={true}
                                            >
                                                <Dropdown.Item
                                                    onClick={() =>
                                                        handleUpdateMode()
                                                    }
                                                >
                                                    Edit
                                                </Dropdown.Item>
                                                <Dropdown.Item
                                                    onClick={() =>
                                                        handleOpenDeleteAnswerModal(
                                                            true,
                                                        )
                                                    }
                                                >
                                                    Delete
                                                </Dropdown.Item>
                                            </Dropdown>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="text-xs">
                                <span className="ml-[2px] text-primary-black">
                                    {formatDistanceToNowTime(answer.createdAt)}
                                </span>
                            </div>
                        </div>
                    </div>
                    {modeAnswer === ANSWER_MODE.NORMAL && (
                        <div className=" block text-sm text-primary-black">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: answer.answer,
                                }}
                            ></div>
                        </div>
                    )}
                    {modeAnswer === ANSWER_MODE.UPDATE && (
                        <UpdateAnswer
                            answer={answer}
                            setAnswerLectures={setAnswerLectures}
                            handleUpdateNormalMode={handleUpdateNormalMode}
                        />
                    )}
                </div>
            </>
            {/* )} */}
        </div>
    );
}
