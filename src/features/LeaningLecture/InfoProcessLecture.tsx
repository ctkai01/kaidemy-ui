import * as React from "react";

export interface IInfoProcessLectureProps {
    countComplete: number;
    totalLecture: number;
}

export default function InfoProcessLecture(props: IInfoProcessLectureProps) {
    const { countComplete, totalLecture } = props;
    return (
        <div className="bg-white p-6">
            <div className="text-sm font-bold text-primary-black">
                {countComplete} out of {totalLecture} completed
            </div>
            <div className="mt-4 text-sm font-normal text-primary-black">
            Complete the course to receive the certificate
            </div>
        </div>
    );
}
