import { Avatar, Dropdown } from "flowbite-react";
import * as React from "react";
import { BsThreeDotsVertical } from "react-icons/bs";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { PaginationInfo, QuestionLecture, User } from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { akaName, formatDistanceToNowTime } from "../../utils";
import DeleteQuestionLectureModal from "./DeleteQuestionLectureModal";
import { QuestionLectureModal } from "./QASection";
import UpdateQuestion from "./UpdateQuestion";

export interface IQuestionItemModalProps {
    questionLectureModal: QuestionLectureModal;
    user?: User;
    setQuestionLectures: React.Dispatch<
        React.SetStateAction<QuestionLecture[]>
    >;
    setQuestionLectureModal: React.Dispatch<
        React.SetStateAction<QuestionLectureModal | null>
    >;
    handleSetAnswerModal: (command: boolean) => void;
    setInfoPagination: React.Dispatch<React.SetStateAction<PaginationInfo>>;
}

const QUESTION_MODE = {
    NORMAL: 0,
    UPDATE: 1,
};

export default function QuestionItemModal(props: IQuestionItemModalProps) {
    const {
        user,
        questionLectureModal,
        setQuestionLectures,
        setQuestionLectureModal,
        handleSetAnswerModal,
        setInfoPagination,
    } = props;
    const [modeQuestion, getModeQuestion] = React.useState<number>(
        QUESTION_MODE.NORMAL,
    );
    const [openDeleteQuestionModal, setOpenDeleteQuestionModal] =
        React.useState(false);
    const handleUpdateMode = () => {
        getModeQuestion(QUESTION_MODE.UPDATE);
    };

    const handleUpdateNormalMode = () => {
        getModeQuestion(QUESTION_MODE.NORMAL);
    };
    const handleOpenDeleteQuestionModal = (command: boolean) => {
        setOpenDeleteQuestionModal(command);
    };

    const handleDeleteQuestionLecture = async () => {
        try {
            await CourseServiceApi.deleteQuestionLectures(questionLectureModal.question.id);
            setQuestionLectureModal(null)
            setQuestionLectures((questionLectures) =>
                questionLectures.filter(
                    (questionLecture) =>
                        questionLecture.id != questionLectureModal.question.id,
                ),
            );
            handleSetAnswerModal(false)
            setInfoPagination((infoPagination: PaginationInfo) => {
                return {
                    ...infoPagination,
                    totalItem: infoPagination.totalItem - 1
                };
            })
            toast(<div className="font-bold">Deleted question sucessfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Deleted question failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="flex px-6 py-4">
            <DeleteQuestionLectureModal
                openModal={openDeleteQuestionModal}
                handleDeleteQuestionLecture={handleDeleteQuestionLecture}
                handleOpenModal={handleOpenDeleteQuestionModal}
            />
            <div>
                <div className="h-8 w-8">
                    {questionLectureModal.question.user.avatar ? (
                        <Avatar
                            img={questionLectureModal.question.user.avatar}
                            alt="avatar"
                            rounded
                            size="sm"
                        />
                    ) : (
                        <Avatar
                            placeholderInitials={akaName(
                                questionLectureModal.question.user.name,
                            )}
                            rounded
                            size="sm"
                        />
                    )}
                </div>
            </div>
            <div className="min-w-[1px] flex-1 pl-4">
                {modeQuestion === QUESTION_MODE.NORMAL && (
                    <>
                        <div className="mb-4 flex justify-between">
                            <div className="flex-1 pr-4">
                                <div className="flex justify-between">
                                    <h2 className="">
                                        <div className="text-base font-bold text-primary-black">
                                            <span className="block">
                                                {
                                                    questionLectureModal
                                                        .question.title
                                                }
                                            </span>
                                        </div>
                                    </h2>
                                    {user &&
                                        user.id ===
                                            questionLectureModal.question.user
                                                .id && (
                                            <div>
                                                <Dropdown
                                                    renderTrigger={() => (
                                                        <div className="group relative z-30 flex h-full w-full items-center justify-center">
                                                            <BsThreeDotsVertical className="cursor-pointer" />
                                                        </div>
                                                    )}
                                                    label=""
                                                    dismissOnClick={true}
                                                >
                                                    <Dropdown.Item
                                                        onClick={() =>
                                                            handleUpdateMode()
                                                        }
                                                    >
                                                        Edit
                                                    </Dropdown.Item>
                                                    <Dropdown.Item
                                                        onClick={() =>
                                                            handleOpenDeleteQuestionModal(
                                                                true,
                                                            )
                                                        }
                                                    >
                                                      Delete
                                                    </Dropdown.Item>
                                                </Dropdown>
                                            </div>
                                        )}
                                </div>

                                <div className="mt-1 flex items-center text-xs">
                                    <Link
                                        className="mr-[2px] text-primary-blue underline"
                                        to={`/user/${questionLectureModal.question.user.id}`}
                                    >
                                        {
                                            questionLectureModal.question.user
                                                .name
                                        }
                                    </Link>
                                    {" · "}
                                    <span className="ml-[2px] mr-[2px] text-primary-blue">
                                        {questionLectureModal.infoIndex.type ===
                                        "quiz"
                                            ? `Quiz ${questionLectureModal.infoIndex.index}`
                                            : `Lecture ${questionLectureModal.infoIndex.index}`}
                                    </span>
                                    {" · "}
                                    <span className="ml-[2px] text-primary-black">
                                        {formatDistanceToNowTime(
                                            questionLectureModal.question
                                                .createdAt,
                                        )}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="mt-1 block text-sm text-primary-black">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: questionLectureModal.question
                                        .description
                                        ? questionLectureModal.question
                                              .description
                                        : "",
                                }}
                            ></div>
                        </div>
                    </>
                )}

                {modeQuestion === QUESTION_MODE.UPDATE && (
                    <UpdateQuestion
                        setQuestionLectures={setQuestionLectures}
                        setQuestionLectureModal={setQuestionLectureModal}
                        handleUpdateNormalMode={handleUpdateNormalMode}
                        questionLectureModal={questionLectureModal}
                    />
                )}
            </div>
        </div>
    );
}
