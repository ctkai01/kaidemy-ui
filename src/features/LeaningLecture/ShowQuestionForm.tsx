import { Alert, Button, Label, Radio } from "flowbite-react";
import * as React from "react";
import { useFieldArray, useForm } from "react-hook-form";
import { AiFillCheckCircle, AiFillExclamationCircle } from "react-icons/ai";
import { HiInformationCircle } from "react-icons/hi";
import { Answer, Question } from "../../models";
import { ResultQuestion } from "./VideoLearning";

export interface IShowQuestionFormProps {
    question: Question;
    stepQuiz: number;
    totalQuestion: number;
    resultQuestion: ResultQuestion[]
    handleNextStepQuiz: () => void;
    handleSetResultQuestion: (
        isTrue: boolean,
        questionID: number,
        numberQuestion: number,
    ) => void;
}
const SHOW_QUESTION_MODE = {
    INIT: 0,
    NORMAL: 1,
    INCORRECT: 2,
    CORRECT: 3,
};
export default function ShowQuestionForm(props: IShowQuestionFormProps) {
    const {
        stepQuiz,
        question,
        totalQuestion,
        resultQuestion,
        handleNextStepQuiz,
        handleSetResultQuestion,
    } = props;

    const [selectAnswer, setSelectAnswer] = React.useState<Answer | null>(null);
    const [showQuestionMode, setShowQuestionMode] = React.useState(
        SHOW_QUESTION_MODE.INIT,
    );
    React.useState<Answer | null>(null);

    const handleSubmitQuestion = () => {
        if (selectAnswer) {
            if (selectAnswer?.isCorrect) {
                setShowQuestionMode(SHOW_QUESTION_MODE.CORRECT);

                //Check exist 
                const isExist = resultQuestion.find(
                    (item) => item.questionID === question.id,
                );

                if (!isExist) {
                    handleSetResultQuestion(true, question.id, stepQuiz);
                }
            } else {
                setShowQuestionMode(SHOW_QUESTION_MODE.INCORRECT);
                 const isExist = resultQuestion.find(
                     (item) => item.questionID === question.id,
                 );

                 if (!isExist) {
                     handleSetResultQuestion(false, question.id, stepQuiz);
                 }
            }
        }
    };
    const handleNextQuestion = () => {
        handleNextStepQuiz();
        setShowQuestionMode(SHOW_QUESTION_MODE.NORMAL);
        setSelectAnswer(null);
    };

    return (
        <div className="mt-6 flex h-full flex-col">
            {showQuestionMode === SHOW_QUESTION_MODE.INCORRECT && (
                <Alert color="failure" icon={AiFillExclamationCircle}>
                    <span className="font-medium">
                        Incorrect answer. Please try again.
                    </span>{" "}
                    {selectAnswer?.explain ? selectAnswer.explain : ""}
                </Alert>
            )}

            {showQuestionMode === SHOW_QUESTION_MODE.CORRECT && (
                <Alert color="success" icon={AiFillCheckCircle}>
                    <span className="font-medium">Good job!</span>{" "}
                    {selectAnswer?.explain ? selectAnswer.explain : ""}
                </Alert>
            )}

            <div className="text-base text-primary-black">
                Question {stepQuiz}:{" "}
            </div>
            <div className="mt-2">
                <div
                    dangerouslySetInnerHTML={{
                        __html: question.title,
                    }}
                ></div>
            </div>
            <div className="mt-2 flex-1">
                <form
                    className="flex h-full flex-col"
                    // onSubmit={handleSubmit(handleSubmitQuestion)}
                >
                    {question.answers.map((answer) => (
                        <div className="flex items-center gap-2">
                            <Radio
                                id="answers"
                                name="answers"
                                // defaultChecked={false}
                                checked={
                                    showQuestionMode === SHOW_QUESTION_MODE.INIT
                                        ? false
                                        : answer.id === selectAnswer?.id
                                          ? true
                                          : false
                                }
                                onChange={() => {
                                    setSelectAnswer(answer);
                                    setShowQuestionMode(
                                        SHOW_QUESTION_MODE.NORMAL,
                                    );
                                }}
                                // {...register("answer1")}
                            />
                            <Label className="text-lg" htmlFor="answers">
                                {answer.answerText}
                            </Label>
                        </div>
                    ))}

                    <div className="flex flex-1 items-end justify-between pb-8">
                        <div className="flex w-full items-center justify-between rounded border-[2px] p-2">
                            <div>
                                Question {stepQuiz} of {totalQuestion}
                            </div>
                            {stepQuiz === totalQuestion &&
                            showQuestionMode === SHOW_QUESTION_MODE.CORRECT ? (
                                <Button
                                    onClick={() => handleNextQuestion()}
                                    disabled={!selectAnswer ? true : false}
                                >
                                    See results
                                </Button>
                            ) : showQuestionMode ===
                              SHOW_QUESTION_MODE.CORRECT ? (
                                <Button
                                    onClick={() => handleNextQuestion()}
                                    disabled={!selectAnswer ? true : false}
                                >
                                    Next
                                </Button>
                            ) : (
                                <Button
                                    onClick={() => handleSubmitQuestion()}
                                    disabled={!selectAnswer ? true : false}
                                >
                                    Check answer
                                </Button>
                            )}

                            {/* <Button
                                onClick={() => handleSubmitQuestion()}
                                disabled={!selectAnswer ? true : false}
                            >
                                Check answer
                            </Button> */}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}
