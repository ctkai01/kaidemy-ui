import { yupResolver } from "@hookform/resolvers/yup";
import { Button, Label, Modal, TextInput } from "flowbite-react";
import * as React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import Editor from "../../components/ui/core/Editor";
import {
    AnswerLecture,
    CreateAnswerLecture,
    CreateQuestionLecture,
    ForgotPassword,
    PaginationInfo,
    QuestionLecture,
} from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import {
    schemeCreateAnswerLecture,
    schemeCreateQuestionLecture,
} from "../../validators/course";
import { QuestionLectureModal, SIZE_PAGE, SIZE_PAGE_ANSWER } from "./QASection";

export interface ICreateAnswerProps {
    currentPage: number;
    questionLectureModal: QuestionLectureModal;
    infoPaginationAnswer: PaginationInfo
    setAnswerLectures: React.Dispatch<React.SetStateAction<AnswerLecture[]>>;
    setQuestionLectures: React.Dispatch<
        React.SetStateAction<QuestionLecture[]>
    >;

    setInfoPaginationAnswer: React.Dispatch<
        React.SetStateAction<PaginationInfo>
    >;
}

export default function CreateAnswer(props: ICreateAnswerProps) {
    const {
        currentPage,
        questionLectureModal,
        infoPaginationAnswer,
        setAnswerLectures,
        setInfoPaginationAnswer,
        setQuestionLectures,
    } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        getValues
    } = useForm<CreateAnswerLecture>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateAnswerLecture),
    });

    const handleSetHTML = (value: string) => {
        setValue("answer", value);
    };

    const handleCreateAnswer = async (data: CreateAnswerLecture) => {
        try {
            // const formData = new FormData();
            const body = {
                questionLectureID: questionLectureModal.question.id,
                answer: data.answer,
            };
            // formData.append(
            //     "question_lecture_id",
            //     `${questionLectureModal.question.id}`,
            // );
            // formData.append("answer", data.answer);
            const dataCreateAnswer =
                await CourseServiceApi.createAnswerLecture(body);
                
            setAnswerLectures((answerLectures) => {
                const answerLecturesUpdate = [...answerLectures];
                let newData = [
                    dataCreateAnswer,
                    ...answerLecturesUpdate,
                ];

                if (infoPaginationAnswer.totalItem + 1 > SIZE_PAGE_ANSWER * infoPaginationAnswer.totalPage) {
                    // newData = newData.splice(0, SIZE_PAGE_ANSWER);
                    setInfoPaginationAnswer(data => {
                        return {
                            ...data,
                            totalPage: data.totalPage + 1,
                            totalItem: data.totalItem + 1,
                        };
                    })
                } else {
                    setInfoPaginationAnswer((data) => {
                        return {
                            ...data,
                            totalItem: data.totalItem + 1,
                        };
                    });
                }
                return newData;
            });
            setQuestionLectures((questionLectures) => {
                const questionLecturesUpdate = [...questionLectures];

                const findIndex = questionLecturesUpdate.findIndex(
                    (questionLecture) =>
                        questionLecture.id === questionLectureModal.question.id,
                );

                if (findIndex != -1) {
                    const questionLectureUpdate = {
                        ...questionLecturesUpdate[findIndex],
                    };

                    questionLectureUpdate.totalAnswer =
                        questionLectureUpdate.totalAnswer + 1;

                    questionLecturesUpdate[findIndex] = questionLectureUpdate;

                    return questionLecturesUpdate;
                }

                return questionLectures;
            });
              toast(<div className="font-bold"> Answered question successfully!</div>, {
                  draggable: false,
                  position: "top-right",
                  type: "success",
                  theme: "colored",
              });

              reset()
    
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold"> Answered question failed!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
    
        }
    };

    
    return (
        <>
            <form onSubmit={handleSubmit(handleCreateAnswer)}>
                <Editor
                    defaultValue={getValues('answer')}
                    handleSetHTML={handleSetHTML}
                    isImage={false}
                />
               
                {errors.answer ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium ">Oops! </span>
                        <span>{errors.answer.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="mb-4 mt-6 flex justify-end">
                    <Button type="submit" color="dark">
                        Add answer
                    </Button>
                </div>
            </form>
            {/* <form onSubmit={handleSubmit(handleCreateQuestion)}>
                   
                </form> */}
        </>
    );
}
