
export interface ILecturePanelProps {
    title: string;
    numberLecture: number;
    time: string;
    index: number;
}

export default function LecturePanel (props: ILecturePanelProps) {
 const { numberLecture, index, time, title } = props;
  return (
      <div className="">
          <div>
              <span className="line-clamp-2 flex-1 text-base font-bold text-primary-black">
                  Section {index + 1}: {title}
              </span>
          </div>

          <span className="mt-1 text-xs text-primary-black">Total {numberLecture} | {time}</span>
      </div>
  );
}
