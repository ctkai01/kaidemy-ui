import { Button } from "flowbite-react";
import { useEffect, useState } from "react";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { LECTURE_WATCH_ASSET_TYPE, QUIZ_TYPE } from "../../constants";
import {
    ContentArticle,
    ContentCourse,
    ContentQuiz,
    ContentVideo,
    NavigateLecture,
} from "../../models";
import { generatePrefixNavigateLecture, generateTextEllipsis } from "../../utils";
import QuizResult from "./QuizResult";
import ShowQuestionForm from "./ShowQuestionForm";

export interface IVideoLearningProps {
    contentCourse: ContentCourse | null;
    lectureList: NavigateLecture[];
    isFinishCourse: boolean;
    handleContentCourse: (content: ContentCourse | null) => void;
    handleFinishCourse: () => void;
    handleResetFinishCourse: () => void;
}

export interface ResultQuestion {
    numberQuestion: number;
    questionID: number;
    isTrue: boolean;
}

export default function VideoLearning(props: IVideoLearningProps) {
    const {
        contentCourse,
        lectureList,
        isFinishCourse,
        handleContentCourse,
        handleFinishCourse,
        handleResetFinishCourse,
    } = props;
    const [stepQuiz, setStepQuiz] = useState(0);
    const [resultQuestion, setResultQuestion] = useState<ResultQuestion[]>([]);

    useEffect(() => {
        if (contentCourse?.type === "quiz") {
            setStepQuiz(0);
            setResultQuestion([]);
        }
    }, [contentCourse]);

    const handleNextStepQuiz = () => {
        setStepQuiz((step) => step + 1);
    };

    const handleResetStepQuiz = () => {
        setStepQuiz(0);
    };

    const handleSetResultQuestion = (
        isTrue: boolean,
        questionID: number,
        numberQuestion: number,
    ) => {
        setResultQuestion((result) => [
            ...result,
            {
                isTrue,
                questionID,
                numberQuestion,
            },
        ]);
    };

    const handleNextLecture = () => {
        if (contentCourse) {
            if (contentCourse.index < lectureList.length - 1) {
                const navigateLecture = lectureList[contentCourse.index + 1];
                if (navigateLecture.lecture.type === QUIZ_TYPE) {
                    const contentQuiz: ContentQuiz = {
                        // article: curriculums[0].lectures[0].article,
                        indexNumber: `Quiz ${navigateLecture.indexNumber}`,
                        title: navigateLecture.lecture.title,
                        questions: navigateLecture.lecture.questions,
                    };

                    const newContentCourse: ContentCourse = {
                        type: "quiz",
                        id: navigateLecture.lecture.id,
                        content: contentQuiz,
                        index: contentCourse.index + 1,
                        curriculumID: navigateLecture.curriculumID,
                    };
                    handleContentCourse(newContentCourse);
                } else {
                    if (navigateLecture.lecture.article) {
                        const contentArticle: ContentArticle = {
                            article: navigateLecture.lecture.article,
                            title: navigateLecture.lecture.title,
                        };
                        const newContentCourse: ContentCourse = {
                            type: "article",
                            id: navigateLecture.lecture.id,
                            content: contentArticle,
                            index: contentCourse.index + 1,
                            curriculumID: navigateLecture.curriculumID,
                        };
                        handleContentCourse(newContentCourse);
                    } else {
                        const assetWatch = navigateLecture.lecture.assets.find(
                            (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
                        );

                        if (assetWatch) {
                            const contentVideo: ContentVideo = {
                                url: assetWatch.url,
                            };
                            const newContentCourse: ContentCourse = {
                                type: "video",
                                id: navigateLecture.lecture.id,
                                content: contentVideo,
                                index: contentCourse.index + 1,
                                curriculumID: navigateLecture.curriculumID,
                            };
                            handleContentCourse(newContentCourse);
                        } else {
                            // handleContentCourse(null);

                            const newContentCourse: ContentCourse = {
                                type: "undefine",
                                id: navigateLecture.lecture.id,
                                content: null,
                                index: contentCourse.index + 1,
                                curriculumID: navigateLecture.curriculumID,
                            };
                            handleContentCourse(newContentCourse);
                        }
                    }
                }
            } else {
                handleFinishCourse();
            }
        }
    };

    const handlePrevLecture = () => {
        if (contentCourse) {
            const navigateLecture = lectureList[contentCourse.index - 1];
            if (navigateLecture.lecture.type === QUIZ_TYPE) {
                const contentQuiz: ContentQuiz = {
                    // article: curriculums[0].lectures[0].article,
                    indexNumber: `Quiz ${navigateLecture.indexNumber}`,
                    title: navigateLecture.lecture.title,
                    questions: navigateLecture.lecture.questions,
                };

                const newContentCourse: ContentCourse = {
                    type: "quiz",
                    id: navigateLecture.lecture.id,
                    content: contentQuiz,
                    index: contentCourse.index - 1,
                    curriculumID: navigateLecture.curriculumID,
                };
                handleContentCourse(newContentCourse);
            } else {
                if (navigateLecture.lecture.article) {
                    const contentArticle: ContentArticle = {
                        article: navigateLecture.lecture.article,
                        title: navigateLecture.lecture.title,
                    };
                    const newContentCourse: ContentCourse = {
                        type: "article",
                        id: navigateLecture.lecture.id,
                        content: contentArticle,
                        index: contentCourse.index - 1,
                        curriculumID: navigateLecture.curriculumID,
                    };
                    handleContentCourse(newContentCourse);
                } else {
                    const assetWatch = navigateLecture.lecture.assets.find(
                        (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
                    );

                    if (assetWatch) {
                        const contentVideo: ContentVideo = {
                            url: assetWatch.url,
                        };
                        const newContentCourse: ContentCourse = {
                            type: "video",
                            id: navigateLecture.lecture.id,
                            content: contentVideo,
                            index: contentCourse.index - 1,
                            curriculumID: navigateLecture.curriculumID,
                        };
                        handleContentCourse(newContentCourse);
                    } else {
                        handleContentCourse(null);
                    }
                }
            }

            if (isFinishCourse) {
                handleResetFinishCourse();
            }
        }
    };

    const handleSkip = () => {
        if (contentCourse) {
            if (contentCourse.index == lectureList.length - 1) {
                handleFinishCourse();
            } else {
                // handleNextStepQuiz();
                handleNextLecture();
            }
        }
    };
    return (
        <div
            className={`h-290-calc relative w-full
        
            ${
                contentCourse &&
                contentCourse.type === "video" &&
                !isFinishCourse &&
                "bg-primary-black"
            }
        
        `}
        >
            <div className="h-full px-10">
                <div className="absolute left-0 top-0 h-full w-full">
                    <div
                        style={{
                            position: "relative",
                            // background: "#2d2f31",
                            height: "100%",
                        }}
                        className={`${
                            contentCourse &&
                            contentCourse.type !== "video" &&
                            "px-10"
                        }`}
                    >
                        {isFinishCourse && (
                            <div className="flex h-full items-center justify-center">
                                <div className="text-2xl font-bold text-primary-black">
                                    🙌 You have finished the last lesson in this
                                    course
                                </div>
                            </div>
                        )}
                        {contentCourse &&
                            !isFinishCourse &&
                            contentCourse.type === "video" && (
                                <iframe
                                    src={`https://iframe.mediadelivery.net/embed/155247/${contentCourse.content?.url}?autoplay=false&loop=false&muted=false&preload=true`}
                                    loading="lazy"
                                    style={{
                                        border: 0,
                                        position: "absolute",
                                        top: 0,
                                        height: "100%",
                                        width: "100%",
                                    }}
                                    allow="accelerometer;gyroscope;autoplay;encrypted-media;picture-in-picture;"
                                    allowFullScreen={true}
                                ></iframe>
                            )}
                        {contentCourse && (
                            <>
                                {contentCourse.index <=
                                    lectureList.length - 1 &&
                                    !isFinishCourse && (
                                        <div
                                            onClick={() => handleNextLecture()}
                                            className="absolute right-0 top-1/2 h-12 w-[28px]  -translate-y-1/2 cursor-pointer"
                                        >
                                            <div className="relative h-full w-full">
                                                <div className="group absolute flex h-full w-full items-center justify-center border border-[#6a6f73] bg-[#2d2f31] opacity-0  hover:opacity-100 ">
                                                    <IoIosArrowForward className="h-8 w-8 text-white" />
                                                    <div className="invisible absolute h-full w-full  group-hover:visible">
                                                        <div className="relative  h-full w-full">
                                                            <div className="absolute -left-[6px] -top-[1px] -translate-x-full cursor-text">
                                                                <div className="flex h-12 items-center justify-center whitespace-nowrap border border-[#6a6f73] bg-[#2d2f31] p-2 text-sm text-white shadow-md">
                                                                    {contentCourse.index ===
                                                                    lectureList.length -
                                                                        1
                                                                        ? "Finish course"
                                                                        : generateTextEllipsis(
                                                                              generatePrefixNavigateLecture(
                                                                                  lectureList[
                                                                                      contentCourse.index +
                                                                                          1
                                                                                  ],
                                                                              ),
                                                                          )}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}

                                {contentCourse.index !== 0 && (
                                    <div
                                        onClick={() => handlePrevLecture()}
                                        className="absolute left-0 top-1/2 h-12 w-[28px]  -translate-y-1/2 cursor-pointer"
                                    >
                                        <div className="relative h-full w-full">
                                            <div className="group absolute flex h-full w-full items-center justify-center border border-[#6a6f73] bg-[#2d2f31] opacity-0  hover:opacity-100 ">
                                                <IoIosArrowBack className="h-8 w-8 text-white" />
                                                <div className="invisible absolute h-full w-full  group-hover:visible">
                                                    <div className="relative  h-full w-full">
                                                        <div className="absolute -right-[8px] -top-[1px] translate-x-full cursor-text">
                                                            <div className="flex h-12 items-center justify-center whitespace-nowrap border border-[#6a6f73] bg-[#2d2f31] p-2 text-sm text-white shadow-md">
                                                                {generateTextEllipsis(
                                                                    generatePrefixNavigateLecture(
                                                                        lectureList[
                                                                            contentCourse.index -
                                                                                1
                                                                        ],
                                                                    ),
                                                                )}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </>
                        )}
                        {contentCourse &&
                            !isFinishCourse &&
                            contentCourse.type === "article" && (
                                <div>
                                    <h2 className="mb-6 mt-6 text-center text-xl font-bold">
                                        {contentCourse.content.title}
                                    </h2>
                                    <div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: contentCourse.content
                                                    .article,
                                            }}
                                        ></div>
                                    </div>
                                </div>
                            )}
                        {contentCourse &&
                            !isFinishCourse &&
                            contentCourse.type === "quiz" &&
                            contentCourse.content.questions && (
                                <div className="flex h-full flex-col px-12">
                                    {stepQuiz === 0 && (
                                        <>
                                            <h2 className="mb-6 mt-6 text-[32px] font-bold">
                                                {contentCourse.content.title}
                                            </h2>
                                            <div className="mb-4 mt-2 text-base">
                                                <span>
                                                    {
                                                        contentCourse.content
                                                            .indexNumber
                                                    }
                                                </span>
                                                <span className="mx-4">|</span>
                                                <span>
                                                    {contentCourse.content
                                                        .questions &&
                                                        contentCourse.content
                                                            .questions
                                                            .length}{" "}
                                                    questions
                                                </span>
                                            </div>
                                            <div className="mt-12 flex gap-4">
                                                <Button
                                                    onClick={() =>
                                                        setStepQuiz(1)
                                                    }
                                                >
                                                    Start quiz
                                                </Button>
                                                <Button
                                                    onClick={() => handleSkip()}
                                                    color="light"
                                                >
                                                    Skip
                                                </Button>
                                            </div>
                                        </>
                                    )}

                                    {stepQuiz !== 0 &&
                                        stepQuiz <=
                                            contentCourse.content.questions
                                                .length && (
                                            <ShowQuestionForm
                                                question={
                                                    contentCourse.content
                                                        .questions[stepQuiz - 1]
                                                }
                                                resultQuestion={resultQuestion}
                                                totalQuestion={
                                                    contentCourse.content
                                                        .questions.length
                                                }
                                                handleNextStepQuiz={
                                                    handleNextStepQuiz
                                                }
                                                handleSetResultQuestion={
                                                    handleSetResultQuestion
                                                }
                                                stepQuiz={stepQuiz}
                                            />
                                        )}
                                    {stepQuiz ===
                                        contentCourse.content.questions.length +
                                            1 && (
                                        <QuizResult
                                            totalQuestion={
                                                contentCourse.content.questions
                                                    .length
                                            }
                                            contentCourse={contentCourse}
                                            handleNextStepQuiz={
                                                handleNextStepQuiz
                                            }
                                            handleFinishCourse={
                                                handleFinishCourse
                                            }
                                            lectureList={lectureList}
                                            handleResetStepQuiz={
                                                handleResetStepQuiz
                                            }
                                            handleNextLecture={
                                                handleNextLecture
                                            }
                                            resultQuestion={resultQuestion}
                                        />
                                    )}

                                    {/* indexNumber */}
                                    {/* <div>
                                    <div
                                        dangerouslySetInnerHTML={{
                                            __html: contentCourse.content
                                                .article,
                                        }}
                                    ></div>
                                </div> */}
                                </div>
                            )}
                    </div>
                </div>
            </div>
        </div>
    );
}
