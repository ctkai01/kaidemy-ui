import { yupResolver } from "@hookform/resolvers/yup";
import { Button } from "flowbite-react";
import * as React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import Editor from "../../components/ui/core/Editor";
import { AnswerLecture, UpdateAnswerLecture } from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { schemeUpdateAnswerLecture } from "../../validators/course";

export interface IUpdateAnswerProps {
    answer: AnswerLecture;
    handleUpdateNormalMode: () => void;
    setAnswerLectures: React.Dispatch<React.SetStateAction<AnswerLecture[]>>;
}

export default function UpdateAnswer(props: IUpdateAnswerProps) {
    const { answer, handleUpdateNormalMode, setAnswerLectures } = props;

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        getValues,
        setValue,
    } = useForm<UpdateAnswerLecture>({
        mode: "onChange",
        defaultValues: {
            answer: answer.answer
        },
        resolver: yupResolver(schemeUpdateAnswerLecture),
    });

    const handleSetHTML = (value: string) => {
        //   setValue("answer", value);
        setValue("answer", value);
    };

    const handleUpdateAnswer = async(data: UpdateAnswerLecture) => {
        try {
            // const formData = new FormData();
            // formData.append(
            //     "answer",
            //     data.answer
            // );
            const body = {
                answer: data.answer
            };
            const dataUpdate = await CourseServiceApi.updateAnswerLectures(
                answer.id,
                body,
            );

            setAnswerLectures((answerLectures) => {
                const answerLecturesUpdate = [...answerLectures];

                const findIndex = answerLecturesUpdate.findIndex(
                    (answerLecture) =>
                        answerLecture.id === dataUpdate.id,
                );

                if (findIndex != -1) {
                    answerLecturesUpdate[findIndex] = dataUpdate;
                    return answerLecturesUpdate;
                }

                return answerLectures;
            });
          
            handleUpdateNormalMode();
            toast(
                <div className="font-bold">Updated answer successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">Updated answer failed!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    }
    return (
        <div>
            <form onSubmit={handleSubmit(handleUpdateAnswer)}>
                <Editor
                    defaultValue={getValues("answer")}
                    handleSetHTML={handleSetHTML}
                    isImage={false}
                />
                {errors.answer ? (
                    <div className="mt-2 text-sm text-red-700">
                        <span className="font-medium ">Oops! </span>
                        <span>{errors.answer.message}</span>
                    </div>
                ) : (
                    <></>
                )}
                <div className="mt-8 flex justify-end gap-4">
                    <Button
                        onClick={() => {
                            handleUpdateNormalMode();
                        }}
                        color="dark"
                    >
                        Cancel
                    </Button>
                    <Button type="submit">Save</Button>
                </div>
            </form>
        </div>
    );
}
