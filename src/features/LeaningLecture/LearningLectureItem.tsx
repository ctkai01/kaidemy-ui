import { Checkbox, Dropdown } from "flowbite-react";
import * as React from "react";
import { AiOutlineFile } from "react-icons/ai";
import { CiYoutube } from "react-icons/ci";
import { FaFolderOpen } from "react-icons/fa";
import { GoLightBulb } from "react-icons/go";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
import { BiLinkExternal } from "react-icons/bi";
import ResourceDropdown from "./ResourceDropdown";
import { ContentArticle, ContentCourse, ContentQuiz, ContentVideo, Lecture } from "../../models";
import { LECTURE_WATCH_ASSET_TYPE, QUIZ_TYPE } from "../../constants";
import { convertSecondsToHoursMinutes, getResources, totalTimeAsset } from "../../utils";
import { Link } from "react-router-dom";
export interface ILearningLectureItemProps {
    type: "video" | "article" | "quiz";
    lecture: Lecture;
    index: number;
    indexList: number;
    selected: boolean;
    curriculumID: number;
    handleContentCourse: (content: ContentCourse | null) => void;
    handleResetFinishCourse: () => void;
    handleMarkLecture: (curriculumID: number, lectureID: number) => void;

}

export default function LearningLectureItem(props: ILearningLectureItemProps) {
    const {
        type,
        lecture,
        index,
        indexList,
        selected,
        curriculumID,
        handleContentCourse,
        handleResetFinishCourse,
        handleMarkLecture,
    } = props;

    const handleMark = () => {
        handleMarkLecture(curriculumID, lecture.id)
    }
    return (
        <li
            className={`group relative cursor-pointer  hover:bg-primary-hover-gray ${
                selected ? "bg-primary-hover-gray" : "bg-white"
            }`}
        >
            <div className="absolute left-3 top-1/2 -translate-y-1/2">
                {lecture.learningLectures.length ? (
                    lecture.learningLectures[0].isDone ? (
                        <Checkbox checked={true} />
                    ) : (
                        <Checkbox
                            onChange={(e) => {
                                if (e.target.checked) {
                                    handleMark();
                                }
                            }}
                        />
                    )
                ) : (
                    <Checkbox
                        onChange={(e) => {
                            if (e.target.checked) {
                                handleMark();
                            }
                        }}
                    />
                )}
            </div>
            <div
                onClick={() => {
                    if (lecture.type === QUIZ_TYPE) {
                        const contentQuiz: ContentQuiz = {
                            // article: curriculums[0].lectures[0].article,
                            indexNumber: `Quiz ${index}`,
                            title: lecture.title,
                            questions: lecture.questions,
                        };

                        const contentCourse: ContentCourse = {
                            type: "quiz",
                            id: lecture.id,
                            content: contentQuiz,
                            index: indexList,
                            curriculumID: curriculumID,
                        };
                        handleContentCourse(contentCourse);
                    } else {
                        if (lecture.article) {
                            const contentArticle: ContentArticle = {
                                article: lecture.article,
                                title: lecture.title,
                            };
                            const contentCourse: ContentCourse = {
                                type: "article",
                                id: lecture.id,
                                content: contentArticle,
                                index: indexList,
                                curriculumID: curriculumID,
                            };
                            handleContentCourse(contentCourse);
                        } else {
                            const assetWatch = lecture.assets.find(
                                (asset) =>
                                    asset.type === LECTURE_WATCH_ASSET_TYPE,
                            );

                            if (assetWatch) {
                                const contentVideo: ContentVideo = {
                                    url: assetWatch.url,
                                };
                                const contentCourse: ContentCourse = {
                                    type: "video",
                                    id: lecture.id,
                                    content: contentVideo,
                                    index: indexList,
                                    curriculumID: curriculumID,
                                };
                                handleContentCourse(contentCourse);
                            } else {
                                const contentCourse: ContentCourse = {
                                    type: "undefine",
                                    id: lecture.id,
                                    content: null,
                                    index: indexList,
                                    curriculumID: curriculumID,
                                };
                                handleContentCourse(contentCourse);
                            }
                        }
                    }
                    handleResetFinishCourse();
                }}
                className="flex justify-start gap-4 px-4 py-2 pl-10"
            >
                {/* chekbox */}

                <div className="flex-1">
                    <div className="line-clamp-2">
                        <span className="text-sm text-primary-black">
                            {type === "quiz"
                                ? `Quiz ${index}: ${lecture.title}`
                                : `${index}. ${lecture.title}`}
                        </span>
                    </div>
                    <div className="flex justify-between pt-2">
                        <div>
                            <div className="flex items-center gap-1 text-xs text-primary-gray">
                                {type === "video" && (
                                    <>
                                        <CiYoutube className="h-4 w-4 group-hover:text-primary-black" />
                                        <span className="group-hover:text-primary-black">
                                            {convertSecondsToHoursMinutes(
                                                totalTimeAsset(
                                                    lecture.assets
                                                        ? lecture.assets
                                                        : [],
                                                ),
                                            )}
                                        </span>
                                    </>
                                )}
                                {type === "article" && (
                                    <>
                                        <AiOutlineFile className="h-4 w-4 group-hover:text-primary-black" />
                                        <span className="group-hover:text-primary-black">
                                            1m
                                        </span>
                                    </>
                                )}
                            </div>
                        </div>
                        {getResources(lecture.assets).length != 0 && (
                            <div>
                                <Dropdown
                                    className="relative"
                                    label="Dropdown button"
                                    renderTrigger={() => (
                                        <div>
                                            <ResourceDropdown />
                                        </div>
                                    )}
                                >
                                    {getResources(lecture.assets).map(
                                        (resource, index) => (
                                            <Link
                                                key={resource.id}
                                                to={resource.url}
                                            >
                                                <Dropdown.Item key={index}>
                                                    <div className="group flex w-[260px] items-center">
                                                        <BiLinkExternal className="mr-2 group-hover:text-primary-blue" />
                                                        <span className="overflow-hidden text-ellipsis whitespace-nowrap group-hover:text-primary-blue">
                                                            {resource.name}
                                                        </span>
                                                    </div>
                                                </Dropdown.Item>
                                            </Link>
                                        ),
                                    )}
                                </Dropdown>
                            </div>
                        )}
                    </div>
                </div>
                {/* {type === "video" && <CiYoutube />}
                {type === "article" && <AiOutlineFile />}
                {type === "quiz" && <GoLightBulb />} */}

                {/* <div className="-mt-1 ml-4 flex flex-1">
                    <div>
                        <div className="flex">
                            <button className="max-w-[400px] overflow-hidden text-ellipsis whitespace-nowrap text-sm text-primary-blue underline">
                                <span>Introduction</span>
                            </button>
                           
                        </div>
                    </div>
                    <div className="flex-1"></div>
                    <span className="ml-8 text-sm text-primary-blue underline">
                        Preview
                    </span>
                    <span className="ml-8 text-sm text-primary-gray">
                        01:56
                    </span>
                    <span></span>
                </div> */}
            </div>
        </li>
    );
}
