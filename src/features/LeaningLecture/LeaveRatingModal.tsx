import { Button, CustomFlowbiteTheme, Flowbite, Modal, Textarea } from "flowbite-react";
import React from "react";
import HoverRating from "./HoverRating";

export interface ILeaveRatingModalProps {
    openLeaveRatingModal: boolean;
    starCount: number | null
    comment: string | null
    handleSetOpenLeaveRatingModal: (command: boolean) => void;
    handleSaveReview: (countStar: number, comment: string) => void;
}

const customTheme: CustomFlowbiteTheme = {
    modal: {
        footer: {
            base: "flex items-center space-x-2 rounded-b border-gray-200 p-6 dark:border-gray-600",
        },
    },
};

export default function LeaveRatingModal(props: ILeaveRatingModalProps) {
    const {
        starCount,
        comment: commentInit,
        handleSetOpenLeaveRatingModal,
        openLeaveRatingModal,
        handleSaveReview,
    } = props;
    const [value, setValue] = React.useState<number | null>(starCount ? starCount : 5);
    const [comment, setComment] = React.useState(commentInit ? commentInit: "");
    const handleSetValue = (value: number) => {
        setValue(value)
    }

    const handleSubmit = () => {
        if (value) {
            handleSaveReview(value, comment);
        }
    }

    

    return (
        <div>
            <Flowbite theme={{ theme: customTheme }}>
                <Modal
                    dismissible
                    show={openLeaveRatingModal}
                    onClose={() => handleSetOpenLeaveRatingModal(false)}
                >
                    <Modal.Body>
                        <div>
                            <h2 className="text-center text-2xl font-bold text-primary-black">
                               Rate
                            </h2>
                            <div className="mt-4  flex flex-col items-center justify-center">
                                <HoverRating
                                    value={value}
                                    handleSetValue={handleSetValue}
                                />
                            </div>
                            <div className="mt-6">
                                <Textarea
                                    placeholder="HTell us about your own personal experience taking this course. Was it a good match for you?"
                                    required
                                    rows={4}
                                    value={comment}
                                    maxLength={200}
                                    onChange={(e) => setComment(e.target.value)}
                                />
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div
                            onClick={() => handleSubmit()}
                            className="flex w-full justify-end"
                        >
                            <Button
                                disabled={!value ? true : false}
                                className=""
                            >
                               Save
                            </Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </Flowbite>
        </div>
    );
}
