import { Avatar } from "flowbite-react";
import * as React from "react";
import { FaMessage } from "react-icons/fa6";
import { Link } from "react-router-dom";
import { QuestionLecture } from "../../models";
import {
    akaName,
    formatDistanceToNowTime,
    InfoIndexLecture,
} from "../../utils";

export interface IQuestionItemProps {
    questionLecture: QuestionLecture;
    infoIndexLecture: InfoIndexLecture;
    handleSetAnswerModal: (command: boolean) => void;
    handleSetQuestionLectureID: (
        question: QuestionLecture,
        infoIndexLecture: InfoIndexLecture,
    ) => void;
}

export default function QuestionItem(props: IQuestionItemProps) {
    const {
        questionLecture,
        infoIndexLecture,
        handleSetQuestionLectureID,
        handleSetAnswerModal,
    } = props;
    return (
        <div className="flex px-6 py-4 hover:bg-primary-hover-gray">
            <div>
                <div className="h-8 w-8">
                    {/* <img
                            className="h-8 w-8 rounded-full"
                            src="https://img-c.udemycdn.com/user/50x50/7414966_adc4.jpg"
                            loading="lazy"
                        /> */}
                    {questionLecture.user.avatar ? (
                        <Avatar
                            img={questionLecture.user.avatar}
                            alt="avatar"
                            rounded
                            size="sm"
                        />
                    ) : (
                        <Avatar
                            placeholderInitials={akaName(
                                questionLecture.user.name,
                            )}
                            rounded
                            size="sm"
                        />
                    )}
                </div>
            </div>
            <div className="min-w-[1px] flex-1 pl-4">
                <div className="mb-4 flex justify-between">
                    <div className="max-w-[600px] flex-1 pr-4">
                        <h2 className="">
                            <div
                                className="text-base font-bold text-primary-black"
                                // to=""
                            >
                                <span className="block overflow-hidden text-ellipsis whitespace-nowrap">
                                    {questionLecture.title}
                                </span>
                            </div>
                        </h2>
                        <div className="mt-1 block overflow-hidden text-ellipsis whitespace-nowrap text-sm text-primary-black">
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: questionLecture.description
                                        ? questionLecture.description
                                        : "",
                                }}
                            ></div>
                        </div>
                    </div>
                    {/* number */}
                    <div
                        onClick={() => {
                            handleSetAnswerModal(true);
                            handleSetQuestionLectureID(
                                questionLecture,
                                infoIndexLecture,
                            );
                        }}
                        className="group flex cursor-pointer items-center text-primary-gray"
                    >
                        <span className="font-bold">
                            {questionLecture.totalAnswer}
                        </span>
                        <FaMessage className="ml-1 group-hover:text-primary-black" />
                    </div>
                </div>
                <div className="flex items-center text-xs">
                    <Link
                        className="mr-[2px] text-primary-blue underline"
                        to={`/user/${questionLecture.user.id}`}
                    >
                        {questionLecture.user.name}
                    </Link>
                    {" · "}
                    <span className="ml-[2px] mr-[2px] text-primary-blue">
                        {infoIndexLecture.type === "quiz"
                            ? `Quiz ${infoIndexLecture.index}`
                            : `Lecture ${infoIndexLecture.index}`}
                        {/* Lecture 0 */}
                    </span>
                    {" · "}
                    <span className="ml-[2px] text-primary-black">
                        {formatDistanceToNowTime(questionLecture.createdAt, true)}
                    </span>
                </div>
            </div>
        </div>
    );
}
