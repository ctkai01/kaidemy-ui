import { Alert, Button } from "flowbite-react";
import * as React from "react";
import { HiInformationCircle } from "react-icons/hi";
import { ContentCourse, NavigateLecture } from "../../models";
import { ResultQuestion } from "./VideoLearning";

export interface IQuizResultProps {
    resultQuestion: ResultQuestion[];
    totalQuestion: number;
    contentCourse: ContentCourse;
    lectureList: NavigateLecture[];
    handleResetStepQuiz: () => void;
    handleNextStepQuiz: () => void;
    handleFinishCourse: () => void;
    handleNextLecture: () => void;
}

export default function QuizResult(props: IQuizResultProps) {
    const {
        resultQuestion,
        totalQuestion,
        contentCourse,
        lectureList,
        handleFinishCourse,
        handleNextStepQuiz,
        handleResetStepQuiz,
        handleNextLecture,
    } = props;
    const numberCorrect = React.useMemo(() => {
        const numberTrue = resultQuestion.filter(item => item.isTrue)
        return numberTrue.length
    }, [])

    const handleContinue = () => {
        if (contentCourse.index == lectureList.length - 1) {
            handleFinishCourse()
        } else {
            // handleNextStepQuiz()

            handleNextLecture()
        }
    }
    return (
        <div className="relative flex h-full flex-col items-center justify-center">
            <div className="absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
                <Alert color="success" icon={HiInformationCircle}>
                    <div className="mb-4 text-center text-2xl font-bold">
                        Review the course materials to expand your learning.
                    </div>
                    <div className="text-center text-[18px]">
                        You got {numberCorrect} out of {totalQuestion} correct.
                    </div>
                </Alert>
            </div>

            <div className="w-full flex-1">
                <div className="flex h-full justify-end">
                    <div className="flex items-end gap-4 pb-8">
                        <Button onClick={() => handleContinue()}>Continue</Button>
                        <Button
                            onClick={() => handleResetStepQuiz()}
                            color="dark"
                        >
                            Retry quiz
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}
