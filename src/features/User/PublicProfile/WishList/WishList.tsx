import { Pagination, Spinner, Tooltip } from "flowbite-react";
import * as React from "react";
import IntroCourseCard from "../../../../components/ui/core/IntroCourseCard";
import { WISH_LIST_TYPE } from "../../../../constants";
import { courses } from "../../../../data/common";
import { Learning, PaginationInfo, User } from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import IntroCourseCardLearning from "../Leanrning/IntroCourseCardLearning";
import WishItem from "./WishItem";
import LearningItem from "./WishItem";

export interface IWishListProps {
    user: User;
}
const TOTAL_PER_PAGE = 10;

export default function WishList(props: IWishListProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [courseWishList, setCourseWishList] = React.useState<Learning[]>([]);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const { user } = props;
    const [loading, setLoading] = React.useState(false);
    React.useEffect(() => {
        const fetchWishList = async () => {
            try {
                setLoading(true);
                const data = await CourseServiceApi.getLearnings(
                    [WISH_LIST_TYPE],
                    currentPage,
                    TOTAL_PER_PAGE,
                    user.id,
                );

                setCourseWishList(data.item);
                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchWishList();
    }, []);
    return (
        <div>
            {loading ? (
                <div className="text-center">
                    <Spinner />
                </div>
            ) : (
                <div className="mx-auto max-w-[1028px] px-6 pb-16">
                    <div className="-mx-2 flex flex-wrap justify-center">
                        {courseWishList.map((item) => (
                            <div
                                key={item.id}
                                className="min-w-1/4 group relative mb-6 min-h-[290px] w-1/4 px-2"
                            >
                                <Tooltip
                                    style="light"
                                    placement="right"
                                    content={
                                        <IntroCourseCardLearning
                                            course={item.course}
                                            type="view"
                                        />
                                    }
                                    trigger="hover"
                                >
                                    <WishItem item={item} />
                                </Tooltip>
                            </div>
                        ))}
                        {infoPagination.totalPage > 1 && (
                            <div className="mt-10 flex justify-center">
                                <Pagination
                                    // layout="table"
                                    currentPage={currentPage}
                                    totalPages={infoPagination.totalPage}
                                    onPageChange={onPageChange}
                                    showIcons
                                />
                            </div>
                        )}
                    </div>
                </div>
            )}
        </div>
    );
}
