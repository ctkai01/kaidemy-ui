import * as React from "react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import BestSeller from "../../../../components/ui/core/BestSeller";
import ReviewStar from "../../../../components/ui/core/ReviewStar";
import { Learning } from "../../../../models";
import { formatNumberWithCommas } from "../../../../utils";

export interface IWishItemProps {
    item: Learning;
}

export default function WishItem(props: IWishItemProps) {
    const { item } = props;
    return (
        <div className=" group relative min-h-[290px] px-2">
            {/* <div className="min-w-1/4 group relative min-h-[290px] w-1/4 px-2"> */}

            <div className="relative mb-1">
                <img
                    src={
                        item.course.image
                            ? item.course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
                <div className="absolute left-0 top-0 h-full w-full bg-primary-black opacity-0 transition-all group-hover:opacity-50"></div>
            </div>
            <div>
                <Link
                    to={`/course/${item.courseID}`}
                    className="mb-1 line-clamp-2 text-base  font-bold text-primary-black after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                >
                    {item.course.title}
                </Link>
                <div className="mb-1 text-xs text-primary-gray">
                    {item.course.author.name}
                </div>
                <div className="mb-1 flex items-center">
                    <span className="mr-2 text-sm font-bold text-[#4d3105]">
                        {item.averageReview}
                    </span>
                    <ReviewStar
                        number={item.averageReview ? item.averageReview : 0}
                    />
                    <span className="text-xs text-[#6a6f73]">
                        (
                        {formatNumberWithCommas(
                            item.countReview ? item.countReview : 0,
                        )}
                        )
                    </span>
                </div>

                <div className="mb-1 flex items-center text-base font-bold text-primary-black">
                    <BsCurrencyDollar />
                    <span> {item.course.price?.value}</span>
                </div>
                {/* <BestSeller /> */}
            </div>
        </div>
    );
}
