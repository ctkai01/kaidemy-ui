import { Avatar, Pagination, Spinner } from "flowbite-react";
import * as React from "react";
import { FaFacebook, FaTwitter, FaYoutube } from "react-icons/fa";
import { IoLink } from "react-icons/io5";
import { Link } from "react-router-dom";
import { CourseShow, PaginationInfo, User } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { akaName } from "../../../utils";
import DescriptionToggle from "../../Course/DescriptionToggle";
import SameCourse from "../../Course/SameCourse";

export interface IUserInstructorProps {
    user: User;
}
// const TOTAL = 38;
const TOTAL_PER_PAGE = 10;
// const TOTAL_PAGE = 3;
export default function UserInstructor(props: IUserInstructorProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [courses, setCourses] = React.useState<CourseShow[]>([]);
    const { user } = props;
    const [loading, setLoading] = React.useState(false);

    React.useEffect(() => {
        const fetchCourses = async () => {
            try {
                setLoading(true);
                const data = await CourseServiceApi.getCourseByUserID(
                    currentPage,
                    TOTAL_PER_PAGE,
                    user.id,
                );

                setCourses(data.item);
                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCourses();
    }, []);

    return (
        <div className="flex-1">
            {loading ? (
                <div className="text-center">
                    <Spinner />
                </div>
            ) : (
                <div className="mx-auto mt-12 flex max-w-[912px] px-6">
                    <div className="w-200-calc pb-6 pr-12">
                        <div className="text-sm font-bold uppercase text-primary-gray">
                         Instructor
                        </div>
                        <h1 className="text-[40px] font-bold text-primary-black">
                            {user.name}
                        </h1>
                        <h2 className="py-2 text-base font-bold text-primary-black">
                        Online Education
                        </h2>
                        <div className="mt-4 flex">
                            <div>
                                <div className="mb-2 text-sm font-bold text-primary-gray">
                                    Total student
                                </div>
                                <div className="text-2xl font-bold text-primary-black">
                                    2
                                </div>
                            </div>
                            <div className="ml-6">
                                <div className="mb-2 text-sm font-bold text-primary-gray">
                                    Reviews
                                </div>
                                <div className="text-2xl font-bold text-primary-black">
                                    1
                                </div>
                            </div>
                        </div>

                        <div className="pb-4 pt-12 text-[19px] font-bold text-primary-black">
                            About me
                        </div>
                        <div>
                            <DescriptionToggle
                                leastHeight="96"
                                text={`
                                ${user.biography}
                    `}
                            />
                        </div>

                        <div className="mt-4 pb-6 text-[19px] font-bold text-primary-black">
                            My courses ({courses.length})
                        </div>
                        <div className="flex flex-wrap">
                            {courses.map((course) => (
                                <div key={course.id} className="mb-5 w-1/2">
                                    <SameCourse user={user} course={course} />
                                </div>
                            ))}

                            {/* <div className="mb-5 w-1/2">
                                <SameCourse />
                            </div>
                            <div className="mb-5 w-1/2">
                                <SameCourse />
                            </div> */}
                        </div>
                        {infoPagination.totalPage > 1 && (
                            <div className="mt-10 flex justify-center">
                                <Pagination
                                    // layout="table"
                                    currentPage={currentPage}
                                    totalPages={infoPagination.totalPage}
                                    onPageChange={onPageChange}
                                    showIcons
                                />
                            </div>
                        )}
                    </div>
                    {/* Image */}
                    <div>
                        <div className="mb-6">
                            {/* <img
                                className="h-[200px] w-[200px] rounded-[50%] object-cover"
                                loading="lazy"
                                src={}
                            /> */}
                            {user.avatar ? (
                                <Avatar
                                    img={user.avatar}
                                    alt="avatar"
                                    rounded
                                    size="xl"
                                />
                            ) : (
                                <Avatar
                                    placeholderInitials={akaName(
                                        user ? user.name : "",
                                    )}
                                    rounded
                                    size="xl"
                                />
                            )}
                        </div>
                        <div>
                            {user.websiteURL && (
                                <Link
                                    to={user.websiteURL}
                                    className="mt-2 flex h-12 items-center justify-center border border-primary-black px-3 hover:bg-primary-hover-gray"
                                >
                                    <IoLink />
                                    <div className="ml-1 text-base font-bold text-primary-black">
                                        Website
                                    </div>
                                </Link>
                            )}
                            {user.twitterURL && (
                                <Link
                                    to={`https://twitter.com/${user.twitterURL}`}
                                    className="mt-2 flex h-12 items-center justify-center border border-primary-black px-3 hover:bg-primary-hover-gray"
                                >
                                    <FaTwitter />
                                    <div className="ml-1 text-base font-bold text-primary-black">
                                        Twitter
                                    </div>
                                </Link>
                            )}

                            {user.facebookURL && (
                                <Link
                                    to={`https://www.facebook.com/${user.facebookURL}`}
                                    className="mt-2 flex h-12 items-center justify-center border border-primary-black px-3 hover:bg-primary-hover-gray"
                                >
                                    <FaFacebook />
                                    <div className="ml-1 text-base font-bold text-primary-black">
                                        Facebook
                                    </div>
                                </Link>
                            )}
                            {user.youtubeURL && (
                                <Link
                                    to={`https://www.youtube.com/${user.youtubeURL}`}
                                    className="mt-2 flex h-12 items-center justify-center border border-primary-black px-3 hover:bg-primary-hover-gray"
                                >
                                    <FaYoutube />
                                    <div className="ml-1 text-base font-bold text-primary-black">
                                        Youtube
                                    </div>
                                </Link>
                            )}
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}
