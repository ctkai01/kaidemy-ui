import { Pagination, Spinner, Tooltip } from "flowbite-react";
import * as React from "react";
import IntroCourseCard from "../../../../components/ui/core/IntroCourseCard";
import { courses } from "../../../../data/common";
import { Learning, PaginationInfo, User } from "../../../../models";
import { CourseServiceApi } from "../../../../services/api/courseServiceApi";
import IntroCourseCardLearning from "./IntroCourseCardLearning";
import LearningItem from "./LearningItem";

export interface ILearningListProps {
    user: User;
}
// const TOTAL = 38;
const TOTAL_PER_PAGE = 10;
// const TOTAL_PAGE = 3;

export default function LearningList(props: ILearningListProps) {
    const [currentPage, setCurrentPage] = React.useState(1);
    const [coursePurChase, setCoursePurchase] = React.useState<Learning[]>([]);
    const onPageChange = (page: number) => setCurrentPage(page);
    const { user } = props;
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [loading, setLoading] = React.useState(false);
    React.useEffect(() => {
        const fetchCoursePurchase = async () => {
            try {
                setLoading(true);
                const data = await CourseServiceApi.getCoursePurchase(
                    currentPage,
                    TOTAL_PER_PAGE,
                    user.id,
                );

                setCoursePurchase(data.item);
                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                setLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCoursePurchase();
    }, []);
    return (
        <div>
            {loading ? (
                <div className="text-center">
                    <Spinner />
                </div>
            ) : (
                <div className="mx-auto max-w-[1028px] px-6 pb-16">
                    <div className="-mx-2 flex flex-wrap justify-center">
                        {coursePurChase.map((learning, _) => (
                            <div
                                key={learning.id}
                                className="min-w-1/4 group relative min-h-[290px] w-1/4 px-2"
                            >
                                <Tooltip
                                    style="light"
                                    placement="right"
                                    content={
                                        <IntroCourseCardLearning
                                            course={learning.course}
                                            type="view"
                                        />
                                    }
                                    trigger="hover"
                                >
                                    <LearningItem learning={learning} />
                                </Tooltip>
                            </div>
                        ))}
                        {/* <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div> */}
                    </div>
                    {infoPagination.totalPage > 1 && (
                        <div className="mt-10 flex justify-center">
                            <Pagination
                                // layout="table"
                                currentPage={currentPage}
                                totalPages={infoPagination.totalPage}
                                onPageChange={onPageChange}
                                showIcons
                            />
                        </div>
                    )}
                </div>
            )}
        </div>
    );
}
