import * as React from "react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import ReviewStar from "../../../../components/ui/core/ReviewStar";
import { Learning } from "../../../../models";
import { formatNumberWithCommas } from "../../../../utils";

export interface ILearningItemProps {
    learning: Learning;
}

export default function LearningItem(props: ILearningItemProps) {
    const { learning } = props;
    return (
        <div className=" group relative min-h-[290px] px-2">
            {/* <div className="min-w-1/4 group relative min-h-[290px] w-1/4 px-2"> */}

            <div className="relative mb-1">
                <img
                    src={
                        learning.course.image
                            ? learning.course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
                <div className="absolute left-0 top-0 h-full w-full bg-primary-black opacity-0 transition-all group-hover:opacity-50"></div>
            </div>
            <div>
                <Link
                    to={`/course/${learning.course.id}`}
                    className="mb-1 line-clamp-2 text-base  font-bold text-primary-black after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                >
                    {learning.course.title}
                </Link>
                <div className="mb-1 text-xs text-primary-gray">
                    {learning.course.author.name}
                </div>
                <div className="mb-1 flex items-center">
                    <span className="mr-2 text-sm font-bold text-[#4d3105]">
                        {learning.averageReview}
                    </span>
                    <ReviewStar
                        number={learning.startCount ? learning.startCount : 0}
                    />
                    <span className="text-xs text-[#6a6f73]">
                        (
                        {formatNumberWithCommas(
                            learning.countReview ? learning.countReview : 0,
                        )}
                        )
                    </span>
                </div>

                <div className="mb-1 flex items-center text-base font-bold text-primary-black">
                    {/* {true ? (
                        "Tree"
                    ) : (
                        <> */}
                    <BsCurrencyDollar />
                    <span>{learning.course.price.value}</span>
                    {/* </> */}
                    {/* )} */}
                </div>
            </div>
        </div>
    );
}
