import { Button } from "flowbite-react";
import React from "react";
import { IoCheckmarkSharp } from "react-icons/io5";
import { Link, useNavigate } from "react-router-dom";
import BestSeller from "../../../../components/ui/core/BestSeller";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../../../../constants";
import { useAppDispatch } from "../../../../hooks/redux-hook";
import { CourseLearning, Learning, User } from "../../../../models";
import {
    convertSecondsToHoursMinutes,
    formatMonthLongYear,
} from "../../../../utils";

interface IIntroCourseCardLearning {
    course: CourseLearning;
    user?: User;
    type?: "buy" | "view";
}

const IntroCourseCardLearning: React.FC<IIntroCourseCardLearning> = (
    props: IIntroCourseCardLearning,
) => {
    const { course, type, user } = props;
    // const dispatch = useAppDispatch();
    // const totalDuration = React.useMemo(() => {
    //     let totalDuration = 0;

    //     course.curriculums.forEach((curriculum) => {
    //         if (curriculum.lectures) {
    //             curriculum.lectures.forEach((lecture) => {
    //                 if (lecture.type === LECTURE_TYPE) {
    //                     lecture.assets.forEach((asset) => {
    //                         if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
    //                             totalDuration += asset.duration;
    //                         }
    //                     });
    //                 }
    //             });
    //         }
    //     });

    //     return totalDuration;
    // }, []);
    // console.log("totalDuration: ", totalDuration);
    // const navigate = useNavigate();
    // const handleAddCourse = async () => {
    //     console.log("Add course: ", course.id);
    //     if (!user) {
    //         navigate("/login");
    //     } else {
    //         try {
    //             const formData = new FormData();
    //             formData.append("course_id", `${course.id}`);
    //             const dataAddCartItem =
    //                 await PaymentServiceApi.addCartItem(formData);
    //             dispatch(addCartItem(dataAddCartItem.cart_item));
    //             console.log(dataAddCartItem.cart_item);
    //             toast(<div className="font-bold">Thêm giỏ hàng thành công!</div>, {
    //                 draggable: false,
    //                 position: "top-right",
    //                 type: "success",
    //                 theme: "colored",
    //             });
    //         } catch (e) {
    //             console.log(e);
    //             toast(
    //                 <div className="font-bold">
    //                     Thêm giỏ hàng không thành công!
    //                 </div>,
    //                 {
    //                     draggable: false,
    //                     position: "top-right",
    //                     type: "error",
    //                     theme: "colored",
    //                 },
    //             );
    //         }
    //     }
    // };

    // const handleAddWishList = async () => {
    //     console.log("Add course");
    //     if (!user) {
    //         navigate("/login");
    //     } else {
    //         try {
    //             const formData = new FormData();
    //             formData.append("course_id", `${course.id}`);
    //             const dataCreate = await CourseServiceApi.addWishList(formData);
    //             console.log("dataCreate: ", dataCreate);
    //             dispatch(addWishList(dataCreate.learning));
    //             toast(
    //                 <div className="font-bold">
    //                     Thêm danh sách khóa học yêu thích thành công!
    //                 </div>,
    //                 {
    //                     draggable: false,
    //                     position: "top-right",
    //                     type: "success",
    //                     theme: "colored",
    //                 },
    //             );
    //         } catch (e: any) {
    //             console.log(e);
    //             if (
    //                 e.response.data.Message.includes(
    //                     "cannot add this course to wish list",
    //                 )
    //             ) {
    //             } else {
    //                 toast(
    //                     <div className="font-bold">
    //                         Thêm danh sách khóa học yêu thích không thành công!
    //                     </div>,
    //                     {
    //                         draggable: false,
    //                         position: "top-right",
    //                         type: "error",
    //                         theme: "colored",
    //                     },
    //                 );
    //             }
    //         }
    //     }
    // };
    return (
        <div className="w-82 p-6">
            <Link
                className="text-lg font-bold text-[#2d2f31] hover:text-primary-blue"
                to={`/course/${course.id}`}
            >
                {course.title}
            </Link>
            {/* <div className="mt-2"> */}
            {/* <BestSeller className="mr-1" /> */}
            {/* <span className="text-xs text-[#1e6055]">
                    Đã cập nhật
                    <span className="ml-1 font-bold">
                        {formatMonthLongYear(course.updated_at)}
                    </span>
                </span> */}
            {/* </div> */}
            <div className="mt-3 text-xs text-[#6a6f73]">
                <span className="after:mx-2 after:text-[6px] after:content-['\25CF']">
                    {/* {course.totalTime} */}
                    {convertSecondsToHoursMinutes(course.duration)}
                </span>
                <span>{course.level ? course.level.name : "None"}</span>
            </div>
            <div className="mt-2">{course.subtitle}</div>
            <div className="mb-4 mt-2">
                <ul>
                    {course.outComes &&
                        course.outComes
                            .splice(0, 3)
                            //
                            .map((el, i) => (
                                <li key={i}>
                                    <div className="flex pt-1 ">
                                        <IoCheckmarkSharp className="h-4 min-w-[16px]" />
                                        <div className="ml-4">{el}</div>
                                    </div>
                                </li>
                            ))}
                </ul>
            </div>
            <div className="flex">
                {/* <button className="h-12 w-full rounded bg-primary-blue text-base font-bold text-white hover:bg-blue-700">
                    Add to cart
                </button> */}
                {type === "buy" ? (
                    <>
                        <Button
                            // onClick={() => handleAddCourse()}
                            className="h-12 w-full text-base font-bold text-white"
                        >
                            Add To Cart
                        </Button>
                        <div className="ml-2">
                            <button
                                // onClick={() => handleAddWishList()}
                                className="flex h-full w-12 items-center justify-center rounded-full px-3 ring-1 ring-black hover:bg-[#1739531f]"
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="h-6 w-6"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z"
                                    />
                                </svg>
                            </button>
                        </div>
                    </>
                ) : (
                    <Button className="h-12 w-full text-base font-bold text-white">
                        <Link to={`/course/${course.id}`}>View course</Link>
                    </Button>
                )}
            </div>
        </div>
    );
};
IntroCourseCardLearning.defaultProps = {
    type: "buy",
};
export default IntroCourseCardLearning;
