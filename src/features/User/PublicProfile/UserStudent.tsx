import { Avatar, CustomFlowbiteTheme, Flowbite, Tabs, TabsComponent, TabsRef } from "flowbite-react";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
import { useNavigate, useSearchParams } from "react-router-dom";
import { PUBLIC_PROFILE_TABS } from "../../../constants/public-profile";
import { User } from "../../../models";
import { akaName } from "../../../utils";
import LearningList from "./Leanrning/LearningList";
import WishList from "./WishList/WishList";

export interface IUserStudentProps {
    user: User
}

const customTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            initials: {
                base: "bg-primary-black flex justify-center items-center",
                text: "text-white font-bold text-3xl",
            },
            size: {
                xl: "w-30 h-30",
            },
        },
    },
};

const customTabTheme: CustomFlowbiteTheme = {
    tab: {
        tablist: {
            base: "flex justify-center border-none",
            tabitem: {
                base: "text-base px-1 py-2 text-primary-gray font-bold mr-2",
                styles: {
                    underline: {
                        active: {
                            on: "border-b-primary-black border-b text-[#2d2f31]   !bg-white",
                        },
                    },
                },
            },
        },
        tabitemcontainer: {
            base: "bg-[#f7f9fa]",
        },
    },
};

export default function UserStudent(props: IUserStudentProps) {
    const biographyContainer = useRef<HTMLDivElement>(null);
    const [isShowButtonBiography, setIsShowButtonBiography] = useState(false);
    const [isToggleShowMore, setIsToggleShowMore] = useState(false);

    const [searchParams, setSearchParams] = useSearchParams();
    const tabsRef = useRef<TabsRef>(null);
    const navigate = useNavigate();
    const { user} = props
    useLayoutEffect(() => {
        const key = searchParams.get("key");
        if (key) {
            if (key === "subscribed-courses") {
                tabsRef.current?.setActiveTab(
                    PUBLIC_PROFILE_TABS.SUBSCRIBED_COURSE,
                );
            } else if (key === "wishlisted-courses") {
                tabsRef.current?.setActiveTab(
                    PUBLIC_PROFILE_TABS.WISHLIST_COURSE,
                );
            } else {
                tabsRef.current?.setActiveTab(
                    PUBLIC_PROFILE_TABS.SUBSCRIBED_COURSE,
                );
            }
        } else {
            tabsRef.current?.setActiveTab(
                PUBLIC_PROFILE_TABS.SUBSCRIBED_COURSE,
            );
        }

    }, []);

    useEffect(() => {
        if (biographyContainer.current) {
            if (biographyContainer.current.clientHeight > 400) {
                setIsShowButtonBiography(true);
            }
        }
    }, []);
    const handleToggleMoreButton = () => {
        setIsToggleShowMore((isToggleShowMore) => !isToggleShowMore);
    };

    const handleChangeTab = (tab: number) => {
        if (tab === PUBLIC_PROFILE_TABS.SUBSCRIBED_COURSE) {
            navigate("?key=subscribed-courses");
        } else {
            navigate("?key=wishlisted-courses");
        }
    };
   
    return (
        <div className="flex-1">
            <div className="bg-primary-black py-8">
                <div className="mx-auto max-w-[1028px]  px-6">
                    <h1 className="my-4 text-3xl font-bold text-white">
                        {user.name}
                    </h1>
                </div>
            </div>
            <div className="mx-auto flex max-w-[1028px] gap-6 px-6 py-8">
                <div className="w-[232px]">
                    <Flowbite theme={{ theme: customTheme }}>
                        {user.avatar ? (
                            <Avatar
                                color="gray"
                                size="xl"
                                img={user.avatar}
                                rounded
                            />
                        ) : (
                            <Avatar
                                color="gray"
                                size="xl"
                                rounded
                                placeholderInitials={akaName(user.name)}
                            />
                        )}
                    </Flowbite>
                </div>
                <div className="flex-1">
                    <div className="flex flex-col items-start">
                        <div
                            className={`${
                                isShowButtonBiography && !isToggleShowMore
                                    ? "custom-mask"
                                    : ""
                            } ${
                                isToggleShowMore
                                    ? ""
                                    : "max-h-[400px] overflow-hidden"
                            }  text-base text-primary-black`}
                        >
                            <div ref={biographyContainer} className="aaa">
                                <p>
                                   <div dangerouslySetInnerHTML={{
                                    __html: user.biography ? user.biography : ""
                                   }}></div>
                                </p>
                            </div>
                        </div>
                        {isShowButtonBiography && (
                            <button
                                onClick={handleToggleMoreButton}
                                className="flex h-[40px] items-center text-sm font-bold text-primary-blue"
                            >
                                <span className="mr-2">
                                    Show full biography
                                </span>
                                {isToggleShowMore ? (
                                    <IoIosArrowUp />
                                ) : (
                                    <IoIosArrowDown />
                                )}
                            </button>
                        )}
                    </div>
                </div>
            </div>
            <div>
                <Flowbite theme={{ theme: customTabTheme }}>
                    <TabsComponent
                        ref={tabsRef}
                        aria-label="Default tabs"
                        style="underline"
                        onActiveTabChange={handleChangeTab}
                    >
                        <Tabs.Item active title="Learning">
                            <LearningList user={user}/>
                        </Tabs.Item>
                        <Tabs.Item title="Wishlist">
                            <WishList user={user}/>
                        </Tabs.Item>
                    </TabsComponent>
                </Flowbite>
            </div>
        </div>
    );
}
