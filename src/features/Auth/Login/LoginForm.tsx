import { yupResolver } from "@hookform/resolvers/yup";
import { useGoogleLogin } from "@react-oauth/google";
import {
    Alert,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Label,
    TextInput,
} from "flowbite-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { RiErrorWarningFill } from "react-icons/ri";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { useAppDispatch } from "../../../hooks/redux-hook";
import { ResponseLogin, SignIn, SignInByGoogle } from "../../../models/auth";
import { UserServiceApi } from "../../../services/api/userServiceApi";
import {
    finishLogin,
    startLogin,
} from "../../../services/state/redux/authSlide";
import { customToast, setAuthLocalStorage } from "../../../utils";
import { schemeSignIn } from "../../../validators";

interface ILoginForm {
    className?: string;
}
const customAlertTheme: CustomFlowbiteTheme = {
    alert: {
        icon: "h-8 w-8 mr-4",
    },
};
const LoginForm = (props: ILoginForm) => {
    const { className } = props;
    const [errResponse, setErrResponse] = useState("");
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const loginByGoogle = useGoogleLogin({
        onSuccess: async (tokenResponse) => {
            try {
                dispatch(startLogin());

                const data: SignInByGoogle = {
                    token: tokenResponse.access_token,
                };
                const result = await UserServiceApi.loginByGoogle(data);
                setAuthLocalStorage(result.token);

                const payloadLogin: ResponseLogin = {
                    token: result.token,
                    user: result.user,
                };
                dispatch(finishLogin(payloadLogin));
                navigate("/");
            } catch (err: any) {
                console.log("What: ", err);
                //  if (
                //     err.response.data.Message.includes(
                //         "this email has been use",
                //     )
                // ) {
                //     toast(
                //         <div className="font-bold">
                //             Email này đã được sử dụng
                //         </div>,
                //         {
                //             draggable: false,
                //             position: "top-center",
                //             type: "error",
                //         },
                //     );
                // } else
                if (err.response.status === 403) {
                    setErrResponse("Tài khoản của bạn đã bị khóa!");
                } else {
                    setErrResponse("Máy chủ bị lỗi gì đó");
                }
            }
        },
    });

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<SignIn>({
        mode: "onChange",
        resolver: yupResolver(schemeSignIn),
    });

    const handleLoginByGoogle = async () => {
        loginByGoogle();
    };

    const onSubmit = async (data: SignIn) => {
        try {
            dispatch(startLogin());

            const result = await UserServiceApi.login(data);
            const payloadLogin: ResponseLogin = {
                token: result.token,
                user: result.user,
            };
            setAuthLocalStorage(result.token);
            reset();

            setErrResponse("");
            dispatch(finishLogin(payloadLogin));
            navigate("/");
        } catch (err: any) {
            // errors.root = "Something error server"
            console.log("err: ", err);
            if (err.response.status === 401) {
                setErrResponse("Email or password is incorrect!");
            } else if (err.response.status === 403) {
                setErrResponse("Your account is blocked!");
            } else {
                setErrResponse("Something error server");
            }
        }
    };

    const handleToggleShowPassword = () => {
        setIsPasswordVisible((isPasswordVisible) => !isPasswordVisible);
    };
    return (
        <div className={`${className} w-96 rounded-lg px-6 py-12`}>
            <h2 className="mb-4 text-center text-lg font-bold text-[#2D2F31]">
                Login Hustdemy account
            </h2>
            {errResponse && (
                <Flowbite theme={{ theme: customAlertTheme }}>
                    <Alert
                        color="failure"
                        icon={RiErrorWarningFill}
                        rounded
                        withBorderAccent
                        className="mb-4"
                    >
                        <span>{errResponse}</span>
                    </Alert>
                </Flowbite>
            )}
            <Button
                onClick={handleLoginByGoogle}
                color="purple"
                className="flex w-full items-center ring-1 ring-black"
            >
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                    className="h-8 w-8"
                >
                    <path
                        style={{ fill: "#167EE6" }}
                        d="M492.668,211.489l-208.84-0.01c-9.222,0-16.697,7.474-16.697,16.696v66.715
	c0,9.22,7.475,16.696,16.696,16.696h117.606c-12.878,33.421-36.914,61.41-67.58,79.194L384,477.589
	c80.442-46.523,128-128.152,128-219.53c0-13.011-0.959-22.312-2.877-32.785C507.665,217.317,500.757,211.489,492.668,211.489z"
                    />
                    <path
                        style={{ fill: "#12B347" }}
                        d="M256,411.826c-57.554,0-107.798-31.446-134.783-77.979l-86.806,50.034
	C78.586,460.443,161.34,512,256,512c46.437,0,90.254-12.503,128-34.292v-0.119l-50.147-86.81
	C310.915,404.083,284.371,411.826,256,411.826z"
                    />
                    <path
                        style={{ fill: "#0F993E" }}
                        d="M384,477.708v-0.119l-50.147-86.81c-22.938,13.303-49.48,21.047-77.853,21.047V512
	C302.437,512,346.256,499.497,384,477.708z"
                    />
                    <path
                        style={{ fill: "#FFD500" }}
                        d="M100.174,256c0-28.369,7.742-54.91,21.043-77.847l-86.806-50.034C12.502,165.746,0,209.444,0,256
	s12.502,90.254,34.411,127.881l86.806-50.034C107.916,310.91,100.174,284.369,100.174,256z"
                    />
                    <path
                        style={{ fill: "#FF4B26" }}
                        d="M256,100.174c37.531,0,72.005,13.336,98.932,35.519c6.643,5.472,16.298,5.077,22.383-1.008
	l47.27-47.27c6.904-6.904,6.412-18.205-0.963-24.603C378.507,23.673,319.807,0,256,0C161.34,0,78.586,51.557,34.411,128.119
	l86.806,50.034C148.202,131.62,198.446,100.174,256,100.174z"
                    />
                    <path
                        style={{ fill: "#D93F21" }}
                        d="M354.932,135.693c6.643,5.472,16.299,5.077,22.383-1.008l47.27-47.27
	c6.903-6.904,6.411-18.205-0.963-24.603C378.507,23.672,319.807,0,256,0v100.174C293.53,100.174,328.005,113.51,354.932,135.693z"
                    />
                </svg>
                <div className="ml-3 font-bold">Login by Google</div>
            </Button>
            <form
                onSubmit={handleSubmit(onSubmit)}
                className="mt-4 flex max-w-md flex-col gap-4"
            >
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="email1" value="Email" />
                    </div>
                    <TextInput
                        id="email1"
                        type="email"
                        color={errors.email ? "failure" : ""}
                        {...register("email")}
                        helperText={
                            <>
                                {errors.email ? (
                                    <>
                                        <span className="font-medium">
                                            Oops!{" "}
                                        </span>
                                        <span>{errors.email.message}</span>
                                    </>
                                ) : (
                                    <></>
                                )}
                            </>
                        }
                    />
                </div>
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="password1" value="Mật khẩu" />
                    </div>
                    <div className="relative">
                        <TextInput
                            id="password1"
                            type={isPasswordVisible ? "text" : "password"}
                            color={errors.password ? "failure" : ""}
                            {...register("password")}
                        />
                        <div
                            onClick={handleToggleShowPassword}
                            className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                        >
                            {!isPasswordVisible ? (
                                <AiFillEye />
                            ) : (
                                <AiFillEyeInvisible />
                            )}
                        </div>
                    </div>
                    {errors.password && (
                        <div className="text-sm text-[#e02424]">
                            <span className="font-medium">Oops! </span>
                            <span>{errors.password.message}</span>
                        </div>
                    )}
                </div>

                <Button className="font-bold" color="blue" type="submit">
                    Login
                </Button>
            </form>
            <div className="mt-4">
                <div className="text-center">
                    <span className="text-[#2D2F31]">
                        or{" "}
                        <Link
                            className="font-bold text-primary-blue underline"
                            to="/forgot-password"
                        >
                            Forget password
                        </Link>
                    </span>
                </div>
                <div className="my-4 border-t border-[#d1d7dc]"></div>
                <div className="text-center">
                    <span className="text-[#2D2F31]">
                        Don't have an account yet?
                        <Link
                            className="font-bold text-primary-blue underline"
                            to="/sign-up"
                        >
                            Sign up
                        </Link>
                    </span>
                </div>
            </div>
        </div>
    );
};

export default LoginForm;
