import { yupResolver } from "@hookform/resolvers/yup";
import {
    Alert,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Label,
    TextInput,
} from "flowbite-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { RiErrorWarningFill } from "react-icons/ri";
import { Link } from "react-router-dom";
import { ForgotPassword, ResetPassword } from "../../../models/auth";
import { schemeResetPassword } from "../../../validators";
interface IResetPasswordForm {
    className?: string;
    handleSendRequestResetPassword: (password: string) => Promise<void>;
    errResponse: string;
}

const customAlertTheme: CustomFlowbiteTheme = {
    alert: {
        icon: "h-8 w-8 mr-4",
    },
};

const ResetPasswordForm = (props: IResetPasswordForm) => {
    const { className, handleSendRequestResetPassword, errResponse } = props;
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [isPasswordConfirmVisible, setIsPasswordConfirmVisible] =
        useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<ResetPassword>({
        mode: "onChange",
        resolver: yupResolver(schemeResetPassword),
    });

    const onSubmit = async (data: ResetPassword) => {
        await handleSendRequestResetPassword(data.password);
        reset();
    };

    const handleToggleShowPassword = () => {
        setIsPasswordVisible((isPasswordVisible) => !isPasswordVisible);
    };

    const handleToggleShowPasswordConfirm = () => {
        setIsPasswordConfirmVisible(
            (isPasswordConfirmVisible) => !isPasswordConfirmVisible,
        );
    };

    return (
        <div className={`${className} w-96 rounded-lg px-6 py-12`}>
            <h2 className="mb-4 text-center text-lg font-bold text-[#2D2F31]">
                Reset Password
            </h2>
            {errResponse && (
                <Flowbite theme={{ theme: customAlertTheme }}>
                    <Alert
                        color="failure"
                        icon={RiErrorWarningFill}
                        rounded
                        withBorderAccent
                    >
                        <span>{errResponse}</span>
                    </Alert>
                </Flowbite>
            )}

            <form
                className="mt-4 flex max-w-md flex-col gap-4"
                onSubmit={handleSubmit(onSubmit)}
            >
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="password" value="Password" />
                    </div>
                    <div className="relative text-[#2D2F31]">
                        <TextInput
                            id="password"
                            type={isPasswordVisible ? "text" : "password"}
                            color={errors.password ? "failure" : ""}
                            {...register("password")}
                        />
                        <div
                            onClick={handleToggleShowPassword}
                            className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                        >
                            {!isPasswordVisible ? (
                                <AiFillEye />
                            ) : (
                                <AiFillEyeInvisible />
                            )}
                        </div>
                    </div>
                    {errors.password ? (
                        <div className="text-sm text-[#e02424]">
                            <span className="font-medium">Oops! </span>
                            <span>{errors.password.message}</span>
                        </div>
                    ) : (
                        <></>
                    )}
                </div>
                <div>
                    <div className="mb-2 block">
                        <Label
                            htmlFor="password-confirm"
                            value="Confirm  Password"
                        />
                    </div>
                    <div className="relative text-[#2D2F31]">
                        <TextInput
                            id="password-confirm"
                            type={
                                isPasswordConfirmVisible ? "text" : "password"
                            }
                            color={errors.confirmPassword ? "failure" : ""}
                            {...register("confirmPassword")}
                        />
                        <div
                            onClick={handleToggleShowPasswordConfirm}
                            className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                        >
                            {!isPasswordConfirmVisible ? (
                                <AiFillEye />
                            ) : (
                                <AiFillEyeInvisible />
                            )}
                        </div>
                    </div>
                    {errors.confirmPassword && (
                        <div className="text-sm text-[#e02424]">
                            <span className="font-medium">Oops! </span>
                            <span>{errors.confirmPassword.message}</span>
                        </div>
                    )}
                </div>

                <Button className="font-bold" color="blue" type="submit">
                    Submit
                </Button>
            </form>
        </div>
    );
};

export default ResetPasswordForm;
