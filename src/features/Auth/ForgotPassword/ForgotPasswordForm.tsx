import { yupResolver } from "@hookform/resolvers/yup";
import { Alert, Button, CustomFlowbiteTheme, Flowbite, Label, TextInput } from "flowbite-react";
import { useForm } from "react-hook-form";
import { RiErrorWarningFill } from "react-icons/ri";
import { Link } from "react-router-dom";
import { ForgotPassword } from "../../../models/auth";
import { schemeForgotPassword } from "../../../validators";
interface IForgotPasswordForm {
    className?: string;
    handleSendRequestForgotPassword: (data: ForgotPassword) => Promise<void>;
    errResponse: string;
}

const customAlertTheme: CustomFlowbiteTheme = {
    alert: {
        icon: "h-8 w-8 mr-4",
    },
};

const ForgotPasswordForm = (props: IForgotPasswordForm) => {
    const { className, handleSendRequestForgotPassword, errResponse } = props;
    
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<ForgotPassword>({
        mode: "onChange",
        resolver: yupResolver(schemeForgotPassword),
    });


    const onSubmit = async (data: ForgotPassword) => {
        await handleSendRequestForgotPassword(data)
        reset()
    };

    return (
        <div className={`${className} w-96 rounded-lg px-6 py-12`}>
            <h2 className="mb-4 text-center text-lg font-bold text-[#2D2F31]">
                Forgot Password
            </h2>
            {errResponse && (
                <Flowbite theme={{ theme: customAlertTheme }}>
                    <Alert
                        color="failure"
                        icon={RiErrorWarningFill}
                        rounded
                        withBorderAccent
                    >
                        <span>{errResponse}</span>
                    </Alert>
                </Flowbite>
            )}
            <form
                className="mt-4 flex max-w-md flex-col gap-4"
                onSubmit={handleSubmit(onSubmit)}
            >
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="email" value="Email" />
                    </div>
                    <TextInput
                        id="email"
                        type="email"
                        color={errors.email ? "failure" : ""}
                        {...register("email")}
                        helperText={
                            <>
                                {errors.email ? (
                                    <>
                                        <span className="font-medium">
                                            Oops!{" "}
                                        </span>
                                        <span>{errors.email.message}</span>
                                    </>
                                ) : (
                                    <></>
                                )}
                            </>
                        }
                    />
                </div>

                <Button className="font-bold" color="blue" type="submit">
                    Reset Password
                </Button>
            </form>
            <div className="mt-4">
                <div className="my-4 border-t border-[#d1d7dc]"></div>
                <div className="text-center">
                    <span className="text-[#2D2F31]">
                        or?
                        <Link
                            className="font-bold text-primary-blue underline"
                            to="/login"
                        >
                            Login
                        </Link>
                    </span>
                </div>
            </div>
        </div>
    );
};

export default ForgotPasswordForm;
