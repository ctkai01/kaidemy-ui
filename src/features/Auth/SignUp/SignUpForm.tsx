import { yupResolver } from "@hookform/resolvers/yup";
import {
    Alert,
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Label,
    TextInput,
} from "flowbite-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { RiErrorWarningFill } from "react-icons/ri";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch } from "../../../hooks/redux-hook";
import { ResponseSignUp, SignUp } from "../../../models/auth";
import { UserServiceApi } from "../../../services/api/userServiceApi";
import { signUp } from "../../../services/state/redux/authSlide";
import { customToast, setAuthLocalStorage } from "../../../utils";
import { schemeSignUp } from "../../../validators";
interface ISignUpForm {
    className?: string;
}

const customAlertTheme: CustomFlowbiteTheme = {
    alert: {
        icon: "h-8 w-8 mr-4",
    },
};

const SignUpForm = (props: ISignUpForm) => {
    const { className } = props;
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm<SignUp>({
        mode: "onChange",
        resolver: yupResolver(schemeSignUp),
    });

    const [errResponse, setErrResponse] = useState("");

    const onSubmit = async (data: SignUp) => {
        try {
            const result = await UserServiceApi.register(data);
            const payloadSignUp: ResponseSignUp = {
                token: result.token,
                user: result.user,
            };
            dispatch(signUp(payloadSignUp));
            setAuthLocalStorage(result.token);
            reset();
            setErrResponse("");
            customToast("Signed up successfully!");
            navigate("/");
        } catch (err: any) {
            // errors.root = "Something error server"
            console.log(err.response.data.Message);
            if (err.response.data.Message.includes("email already existed")) {
                setErrResponse("Email already existed");
            } else {
                setErrResponse("Something error server");
            }
        }
    };

    const handleToggleShowPassword = () => {
        setIsPasswordVisible((isPasswordVisible) => !isPasswordVisible);
    };

    return (
        <div className={`${className} w-96 rounded-lg px-6 py-12`}>
            <h2 className="mb-4 text-center text-lg font-bold text-[#2D2F31]">
                Sign up and start learning
            </h2>
            {errResponse && (
                <Flowbite theme={{ theme: customAlertTheme }}>
                    <Alert
                        color="failure"
                        icon={RiErrorWarningFill}
                        rounded
                        withBorderAccent
                    >
                        <span>{errResponse}</span>
                    </Alert>
                </Flowbite>
            )}
            <form
                className="mt-4 flex max-w-md flex-col gap-4"
                onSubmit={handleSubmit(onSubmit)}
            >
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="full-name" value="Full name" />
                    </div>

                    <TextInput
                        id="full-name"
                        type="text"
                        color={errors.name ? "failure" : ""}
                        {...register("name")}
                        helperText={
                            <>
                                {errors.name ? (
                                    <>
                                        <span className="font-medium">
                                            Oops!{" "}
                                        </span>
                                        <span>{errors.name.message}</span>
                                    </>
                                ) : (
                                    <></>
                                )}
                            </>
                        }
                    />
                </div>
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="email" value="Email" />
                    </div>
                    <TextInput
                        id="email"
                        type="email"
                        color={errors.email ? "failure" : ""}
                        {...register("email")}
                        helperText={
                            <>
                                {errors.email ? (
                                    <>
                                        <span className="font-medium">
                                            Oops!{" "}
                                        </span>
                                        <span>{errors.email.message}</span>
                                    </>
                                ) : (
                                    <></>
                                )}
                            </>
                        }
                    />
                </div>
                <div>
                    <div className="mb-2 block">
                        <Label htmlFor="password" value="Password" />
                    </div>
                    <div className="relative text-[#2D2F31]">
                        <TextInput
                            id="password"
                            type={isPasswordVisible ? "text" : "password"}
                            color={errors.password ? "failure" : ""}
                            {...register("password")}
                        />
                        <div
                            onClick={handleToggleShowPassword}
                            className="absolute right-3 top-1/2 -translate-y-1/2 cursor-pointer"
                        >
                            {!isPasswordVisible ? (
                                <AiFillEye />
                            ) : (
                                <AiFillEyeInvisible />
                            )}
                        </div>
                    </div>
                    {errors.password ? (
                        <div className="text-sm text-[#e02424]">
                            <span className="font-medium">Oops! </span>
                            <span>{errors.password.message}</span>
                        </div>
                    ) : (
                        <></>
                    )}
                </div>

                <Button className="font-bold" color="blue" type="submit">
                    Sign up
                </Button>
            </form>
            <div className="mt-4">
                <div className="my-4 border-t border-[#d1d7dc]"></div>
                <div className="text-center">
                    <span className="text-[#2D2F31]">
                        Already have an account?
                        <Link
                            className="font-bold text-primary-blue underline"
                            to="/login"
                        >
                            Login
                        </Link>
                    </span>
                </div>
            </div>
        </div>
    );
};

export default SignUpForm;
