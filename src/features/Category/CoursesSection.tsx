import {
    CustomFlowbiteTheme,
    Flowbite,
    Pagination,
    Tooltip,
} from "flowbite-react";
import * as React from "react";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/redux-hook";
import useAddCart from "../../hooks/useAddCart";
import { CategoryCourse, PaginationInfo } from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { selectUserAuth } from "../../services/state/redux/authSlide";
import { selectWishList } from "../../services/state/redux/wishListSlide";
import CourseCategoryItem from "./CourseCategoryItem";
import CourseTooltip from "./CourseTooltip";

export interface ICoursesSectionProps {
    currentPage: number;
    totalPages: number;
    categoryCourses: CategoryCourse[];
    onPageChange: (page: number) => void;
}

const customTheme: CustomFlowbiteTheme = {
    tooltip: {
        target: "",
    },
};

export default function CoursesSection(props: ICoursesSectionProps) {
    const { categoryCourses, currentPage, totalPages, onPageChange } = props;

    return (
        <div>
            {categoryCourses.map((categoryCourse, index) => (
                <Flowbite
                    key={categoryCourse.id}
                    theme={{ theme: customTheme }}
                >
                    <Tooltip
                        animation="duration-300"
                        content={
                            <CourseTooltip
                                outComes={categoryCourse.outComes}
                                courseID={categoryCourse.id}
                                categoryCourse={categoryCourse}
                                type={
                                    categoryCourse.isPurchased ? "own" : "buy"
                                }
                            />
                        }
                        style="light"
                        arrow={false}
                        placement="bottom"
                        // trigger='click'
                    >
                        <CourseCategoryItem
                            categoryCourse={categoryCourse}
                            index={index}
                            key={index}
                        />
                    </Tooltip>
                </Flowbite>
            ))}
            {totalPages !== 0 ? (
                <div className="mt-10 flex justify-center">
                    <Pagination
                        currentPage={currentPage}
                        totalPages={totalPages}
                        onPageChange={onPageChange}
                        showIcons
                    />
                </div>
            ) : (
                <div className="text-center font-bold">No courses found</div>
            )}
        </div>
    );
}
