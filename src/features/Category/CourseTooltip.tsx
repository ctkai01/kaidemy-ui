import { Button } from "flowbite-react";
import * as React from "react";
import { AiOutlineHeart } from "react-icons/ai";
import { FaHeart } from "react-icons/fa";
import { IoCheckmarkSharp } from "react-icons/io5";
import { RiErrorWarningFill } from "react-icons/ri";
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks/redux-hook";
import useAddCart from "../../hooks/useAddCart";
import useAddWishList from "../../hooks/useAddWishList";
import useRemoveWishList from "../../hooks/useRemoveWishList";
import { CategoryCourse } from "../../models";
import { selectUserAuth } from "../../services/state/redux/authSlide";
import { selectCart } from "../../services/state/redux/cartSlide";
import { selectWishList } from "../../services/state/redux/wishListSlide";
import { formatDayMonthYear } from "../../utils";

export interface ICourseTooltipProps {
    type?: "own" | "buy";
    outComes: string[] | null;
    courseID: number;
    categoryCourse: CategoryCourse;
}

const CourseTooltip: React.FC<ICourseTooltipProps> = (
    props: ICourseTooltipProps,
) => {
    const { type, outComes, courseID, categoryCourse } = props;

    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const wishList = useAppSelector(selectWishList);
    const user = useAppSelector(selectUserAuth);
    const cart = useAppSelector(selectCart);

    const [addCart] = useAddCart(courseID, user, navigate, dispatch, wishList);

    const checkCart = (): boolean => {
        if (cart) {
            const isExist = cart.cartItems.find(
                (item) => item.course.id === courseID,
            );
            if (isExist) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };

    const checkWishList = (): boolean => {
        const isExist = wishList.find((item) => item.courseID === courseID);
        if (isExist) {
            return true;
        } else {
            return false;
        }
    };

    const [handleRemoveWishList] = useRemoveWishList(
        courseID,
        user,
        navigate,
        dispatch,
        wishList,
    );
    const [handleAddWishList] = useAddWishList(
        courseID,
        user,
        navigate,
        dispatch,
        cart,
    );
    return (
        <div className="w-82 p-4">
            {type === "buy" && (
                <>
                    <div className="text-base font-bold text-primary-black">
                        What you will learn
                    </div>
                    <div className="mb-4 mt-2">
                        <ul>
                            {outComes &&
                                outComes.slice(0, 3).map((item, i) => (
                                    <li key={i}>
                                        <div className="flex pt-1">
                                            <IoCheckmarkSharp className="h-4 min-w-[16px]" />
                                            <div className="ml-4">{item}</div>
                                        </div>
                                    </li>
                                ))}
                        </ul>
                    </div>
                </>
            )}

            <div className="flex">
                {type === "buy" ? (
                    <>
                        <Button
                            onClick={() => {
                                if (checkCart()) {
                                    navigate("/cart");
                                } else {
                                    addCart();
                                }
                            }}
                            className="h-12 w-full text-base font-bold text-white"
                        >
                            {checkCart() ? "Go to cart" : "Add to cart"}
                        </Button>
                        <div className="ml-2">
                            <button
                                // onClick={() => handleAddWishList()}
                                onClick={() => {
                                    if (checkWishList()) {
                                        handleRemoveWishList();
                                    } else {
                                        handleAddWishList();
                                    }
                                }}
                                className="flex h-full w-12 items-center justify-center rounded-full px-3 ring-1 ring-black hover:bg-[#1739531f]"
                            >
                                {checkWishList() ? (
                                    <FaHeart className="h-6 w-6" />
                                ) : (
                                    <AiOutlineHeart className="h-6 w-6" />
                                )}
                            </button>
                        </div>
                    </>
                ) : (
                    <div>
                        <div className="flex">
                            <RiErrorWarningFill className="h-6 w-6 text-cyan-500" />
                            <div className=" font-base ml-4 font-bold text-primary-black">
                                You have purchased the course on{" "}
                                {/* {formatDayMonthYear(
                                    getCoursePurchaseByID(
                                        coursePurchase,
                                        course.id,
                                    )?.created_at,
                                )} */}
                                {formatDayMonthYear(
                                    categoryCourse.purchasedDate,
                                )}
                            </div>
                        </div>
                        <Link to={`/course/${courseID}/learn/`}>
                            <Button
                                color="dark"
                                className="mt-2 w-full font-bold"
                            >
                                Go to course
                            </Button>
                        </Link>
                    </div>
                    // <Button className="h-12 w-full text-base font-bold text-white">
                    //     <Link to="">Xem khóa học</Link>
                    // </Button>
                )}
            </div>
        </div>
    );
};
CourseTooltip.defaultProps = {
    type: "buy",
};
export default CourseTooltip;
