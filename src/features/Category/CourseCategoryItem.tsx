import { Rating } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Link, NavLink } from "react-router-dom";
import { CategoryCourse } from "../../models";
import {
    convertSecondsToHoursMinutes,
    formatNumberWithCommas,
} from "../../utils";

export interface ICourseCategoryItemProps {
    index: number;
    categoryCourse: CategoryCourse;
}

const useStyles: any = makeStyles({
    "icon-1": { color: "#b16f03" },
});

export default function CourseCategoryItem(props: ICourseCategoryItemProps) {
    const classes = useStyles();
    const { index, categoryCourse } = props;
    return (
        <div
            className={`group relative flex cursor-pointer gap-4 border-b border-[#d1d7dc] py-4 ${
                !index && "pt-0"
            }`}
        >
            <div className="relative mb-1 w-[33%]">
                <img
                    className="w-full"
                    src={
                        categoryCourse.image
                            ? categoryCourse.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                        // "https://img-c.udemycdn.com/course/240x135/666914_6c60_3.jpg"
                    }
                    loading="lazy"
                    // src={""}
                />
                <div className="absolute left-0 top-0 h-full w-full bg-primary-black opacity-0 group-hover:opacity-50"></div>
            </div>
            <div className="w-[67%]">
                <div className="flex gap-2">
                    <div className="flex-1">
                        <h3 className="mb-1 flex justify-between  font-bold text-[#2d2f31]">
                            <Link
                                to={`/course/${categoryCourse.id}`}
                                className="line-clamp-2 overflow-hidden text-ellipsis  after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:block after:content-['']"
                            >
                                {categoryCourse.title}
                            </Link>
                        </h3>
                        <div className="mb-1 text-sm text-primary-black">
                            {categoryCourse.subtitle}
                        </div>
                        <div className="mb-1 overflow-hidden text-ellipsis whitespace-nowrap text-xs text-[#6a6f73]">
                            {categoryCourse.user.name}
                        </div>
                        <div className="mb-1 flex items-center">
                            <span className="mr-2 mt-[2px] text-sm font-bold text-[#4d3105]">
                                {categoryCourse.averageReview}
                            </span>
                            <Rating
                                classes={{
                                    iconFilled: classes["icon-1"],
                                }}
                                readOnly
                                value={categoryCourse.averageReview}
                                precision={0.5}
                                size="small"
                            />
                            <span className="text-xs text-[#6a6f73]">
                                (
                                {formatNumberWithCommas(
                                    categoryCourse.countReview,
                                )}
                                )
                            </span>
                        </div>
                        <div className="flex items-center text-xs text-primary-gray">
                            <span>
                                {convertSecondsToHoursMinutes(
                                    categoryCourse.duration,
                                )}
                            </span>
                            <span className="flex items-center before:mx-1 before:text-[6px] before:content-['\25CF']">
                                {categoryCourse.totalLecture} lectures
                            </span>
                            <span className="flex items-center before:mx-1 before:text-[6px] before:content-['\25CF']">
                                {categoryCourse.level.name}
                            </span>
                        </div>
                    </div>
                    <div className="font-bold text-[#2d2f31]">
                        $ {categoryCourse.price.value}
                    </div>
                </div>
            </div>
        </div>
    );
}
