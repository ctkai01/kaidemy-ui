import {
    Accordion,
    Checkbox,
    CustomFlowbiteTheme,
    Flowbite,
    Label,
    Radio,
} from "flowbite-react";
import { makeStyles } from "@mui/styles";

import Rating from "@mui/material/Rating";
import { useForm, UseFormRegister } from "react-hook-form";
import AccordionController from "../Course/AccordionController";
import AccordionItem from "../Course/AccordionItem";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";
import { useEffect } from "react";
import { Level, OverallCategoryCourse } from "../../models";
import { getCountLevel } from "../../utils";

export interface IFilterSectionProps {
    levels: Level[];
    overallCategoryCourse: OverallCategoryCourse;
    register: UseFormRegister<FilterCategory>;
}

const useStyles: any = makeStyles({
    "icon-1": { color: "#b16f03" },
});
const customTheme: CustomFlowbiteTheme = {
    accordion: {
        root: {
            base: "border-0",
            flush: {
                off: "rounded-lg border-0",
                // on: "border-b",
            },
        },
        title: {
            base: "font-bold flex w-full items-center justify-between first:rounded-t-lg last:rounded-b-lg py-5 px-5 text-left text-gray-500 dark:text-gray-400",
            arrow: {
                base: "h-6 w-6 shrink-0 duration-150 ease-linear delay-0 transition-[transform]",
                open: {
                    off: "",
                    on: "rotate-180",
                },
            },
            flush: {
                off: "hover:bg-gray-100 dark:hover:bg-gray-800 border-t",
                // on: "bg-transparent dark:bg-transparent",
            },
        },
    },
};
interface FilterCategory {
    rate: string;
    sort: string;
    duration: string[] | false;
    level: string[] | false;
}
export default function FilterSection(props: IFilterSectionProps) {
    // const {
    //     register,
    //     handleSubmit,
    //     formState: { errors },
    //     reset,
    //     watch,
    // } = useForm<FilterCategory>({
    //     mode: "onChange",
    // });

    const { levels, overallCategoryCourse, register } = props;

    const classes = useStyles();

    const location = useLocation();
    const navigate = useNavigate();
    // const searchParams = new URLSearchParams(location.search);

    const onSubmit = async (data: FilterCategory) => {
        console.log("Data: ", data);
    };

    // useEffect(() => {
    // const testValues = searchParams.getAll("test");

    //     console.log("Params: ", testValues);
    //     console.log("Params 1: ", searchParams.toString());
    //     if ()
    //     searchParams.append('hi', "1")
    //      navigate(`?${searchParams.toString()}`, { replace: true })
    // }, [])

    return (
        <div>
            <Flowbite theme={{ theme: customTheme }}>
                {/* <form onSubmit={handleSubmit(onSubmit)}> */}
                {/* <button type="submit">SS</button> */}
                <AccordionController>
                    <AccordionItem
                        isOpen={true}
                        positionIcon="end"
                        title={
                            <h1 className="cursor-pointer font-bold">
                                Ratings
                            </h1>
                        }
                    >
                        <div className="p-5">
                            <fieldset className="flex max-w-md flex-col gap-4">
                                <div className="flex items-center gap-2">
                                    <Radio
                                        id="4.5"
                                        value="4.5"
                                        // defaultChecked
                                        {...register("rate")}
                                    />
                                    <Label htmlFor="4.5">
                                        <div className="flex items-center gap-2">
                                            <Rating
                                                classes={{
                                                    iconFilled:
                                                        classes["icon-1"],
                                                }}
                                                value={4.5}
                                                precision={0.5}
                                                readOnly
                                                size="small"
                                            />
                                            <span>4.5 & up</span>
                                            <span className="text-primary-gray">
                                                (
                                                {overallCategoryCourse?.tierOneRatingCount
                                                    ? overallCategoryCourse?.tierOneRatingCount
                                                    : 0}
                                                )
                                            </span>
                                        </div>
                                    </Label>
                                </div>
                                <div className="flex items-center gap-2">
                                    <Radio
                                        id="4.0"
                                        value="4.0"
                                        // defaultChecked
                                        {...register("rate")}
                                    />
                                    <Label htmlFor="4.0">
                                        <div className="flex items-center gap-2">
                                            <Rating
                                                classes={{
                                                    iconFilled:
                                                        classes["icon-1"],
                                                }}
                                                value={4.0}
                                                precision={0.5}
                                                readOnly
                                                size="small"
                                            />
                                            <span>4.0 & up</span>
                                            <span className="text-primary-gray">
                                                (
                                                {overallCategoryCourse?.tierTwoRatingCount
                                                    ? overallCategoryCourse?.tierTwoRatingCount
                                                    : 0}
                                                )
                                            </span>
                                        </div>
                                    </Label>
                                </div>
                                <div className="flex items-center gap-2">
                                    <Radio
                                        id="3.5"
                                        value="3.5"
                                        // defaultChecked
                                        {...register("rate")}
                                    />
                                    <Label htmlFor="3.5">
                                        <div className="flex items-center gap-2">
                                            <Rating
                                                classes={{
                                                    iconFilled:
                                                        classes["icon-1"],
                                                }}
                                                value={3.5}
                                                precision={0.5}
                                                readOnly
                                                size="small"
                                            />
                                            <span>3.5 & up</span>
                                            <span className="text-primary-gray">
                                                (
                                                {overallCategoryCourse?.tierThreeRatingCount
                                                    ? overallCategoryCourse?.tierThreeRatingCount
                                                    : 0}
                                                )
                                            </span>
                                        </div>
                                    </Label>
                                </div>
                                <div className="flex items-center gap-2">
                                    <Radio
                                        id="3.0"
                                        value="3.0"
                                        // defaultChecked
                                        {...register("rate")}
                                    />
                                    <Label htmlFor="3.0">
                                        <div className="flex items-center gap-2">
                                            <Rating
                                                classes={{
                                                    iconFilled:
                                                        classes["icon-1"],
                                                }}
                                                value={3.0}
                                                precision={0.5}
                                                readOnly
                                                size="small"
                                            />
                                            <span>3.0 & up</span>
                                            <span className="text-primary-gray">
                                                (
                                                {overallCategoryCourse?.tierFourRatingCount
                                                    ? overallCategoryCourse?.tierFourRatingCount
                                                    : 0}
                                                )
                                            </span>
                                        </div>
                                    </Label>
                                </div>
                            </fieldset>
                        </div>
                    </AccordionItem>

                    <AccordionItem
                        isOpen={true}
                        positionIcon="end"
                        title={
                            <h1 className="cursor-pointer font-bold">
                                Video duration
                            </h1>
                        }
                    >
                        <div className="p-5">
                            <div className="flex items-center gap-2">
                                <Checkbox
                                    value="short"
                                    id="accept"
                                    {...register("duration")}
                                />
                                <Label
                                    htmlFor="accept"
                                    className="flex text-sm text-primary-black"
                                >
                                    0 - 1 Hour
                                    <span className="ml-2 text-primary-gray">
                                        (
                                        {overallCategoryCourse?.tierOneDurationCount
                                            ? overallCategoryCourse?.tierOneDurationCount
                                            : 0}
                                        )
                                    </span>
                                </Label>
                            </div>
                            <div className="flex items-center gap-2">
                                <Checkbox
                                    value="extraShort"
                                    id="accept"
                                    {...register("duration")}
                                />
                                <Label
                                    htmlFor="accept"
                                    className="flex text-sm text-primary-black"
                                >
                                    1+ Hour
                                    <span className="ml-2 text-primary-gray">
                                        (
                                        {overallCategoryCourse?.tierTwoDurationCount
                                            ? overallCategoryCourse?.tierTwoDurationCount
                                            : 0}
                                        )
                                    </span>
                                </Label>
                            </div>
                        </div>
                    </AccordionItem>

                    <AccordionItem
                        isOpen={true}
                        positionIcon="end"
                        title={
                            <h1 className="cursor-pointer font-bold">Level</h1>
                        }
                    >
                        <div className="p-5">
                            {levels.map((level) => (
                                <div
                                    key={level.id}
                                    className="flex items-center gap-2"
                                >
                                    <Checkbox
                                        value={level.id}
                                        id="accept"
                                        {...register("level")}
                                    />
                                    <Label
                                        htmlFor="accept"
                                        className="flex text-sm text-primary-black"
                                    >
                                        {level.name}
                                        <span className="ml-2 text-primary-gray">
                                            (
                                            {getCountLevel(
                                                level.name,
                                                overallCategoryCourse,
                                            )}
                                            )
                                        </span>
                                    </Label>
                                </div>
                            ))}
                            {/* <div className="flex items-center gap-2">
                                <Checkbox
                                    value={1}
                                    id="accept"
                                    {...register("level")}
                                />
                                <Label
                                    htmlFor="accept"
                                    className="flex text-sm text-primary-black"
                                >
                                    Bắt đầu
                                    <span className="ml-2 text-primary-gray">
                                        (805)
                                    </span>
                                </Label>
                            </div>
                            <div className="flex items-center gap-2">
                                <Checkbox
                                    value={2}
                                    id="accept"
                                    {...register("level")}
                                />
                                <Label
                                    htmlFor="accept"
                                    className="flex text-sm text-primary-black"
                                >
                                    Trung cấp
                                    <span className="ml-2 text-primary-gray">
                                        (805)
                                    </span>
                                </Label>
                            </div> */}
                        </div>
                    </AccordionItem>
                </AccordionController>

                {/* </form> */}
            </Flowbite>
        </div>
    );
}
