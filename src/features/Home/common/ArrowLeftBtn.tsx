
interface IArrowLeftBtn {
    handlePrevSlide: () => void
    isShow: boolean
    className?: string
}

const ArrowLeftBtn = (props: IArrowLeftBtn) => {
    const { isShow, className, handlePrevSlide } = props;
    // console.log("Is show: ", isShow)
    // swiper.slideTo(index, speed, runCallbacks)
    return (
        <div
            onClick={handlePrevSlide}
            className={`${
                isShow ? "block" : "hidden"
            } ${className} absolute left-0 top-1/2 z-50 flex h-12 w-12 -translate-y-1/2 cursor-pointer items-center justify-center rounded-full bg-[#2D2F31]`}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="h-6 w-6 text-white"
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.75 19.5L8.25 12l7.5-7.5"
                />
            </svg>
        </div>
    );
};

export default ArrowLeftBtn;