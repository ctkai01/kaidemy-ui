
interface IArrowLeftBtn {
    handleNextSlide: () => void;
    isShow: boolean;
    className?: string
}

const ArrowRightBtn = (props: IArrowLeftBtn) => {
    const { isShow, className, handleNextSlide } = props;
    return (
        <div
            onClick={handleNextSlide}
            className={`${
                isShow ? "block" : "hidden"
            } absolute right-0 top-1/2 z-50 flex h-12 w-12 -translate-y-1/2 cursor-pointer items-center justify-center rounded-full bg-[#2D2F31] ${className}`}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="h-6 w-6 text-white"
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M8.25 4.5l7.5 7.5-7.5 7.5"
                />
            </svg>
        </div>
    );
};

export default ArrowRightBtn;
