import { Link } from "react-router-dom";
import { categoriesList } from "../../../data/common";

interface ITopCategories {}


const TopCategories = () => {
   
    return (
        <div className="mt-16 mb-4">
            <h2 className="text-2xl font-bold text-[#2D2F31]">
                Top categories
            </h2>
            <div className="flex flex-wrap justify-between">
                {/* <div className="grid grid-cols-4 gap-3"> */}
                {categoriesList.map((category, i) => (
                    <Link className="" key={i} to={category.url}>
                        <div className="overflow-hidden">
                            <img
                                className="hover:scale-105 hover:transition-transform"
                                src={category.image}
                                alt={category.title}
                                width="300"
                                height="300"
                                loading="lazy"
                            />
                        </div>
                        <div className="pb-4 pt-2 font-bold text-[#2d2f31]">
                            <span>{category.title}</span>
                        </div>
                    </Link>
                ))}
            </div>
        </div>
    );
};
export default TopCategories;
