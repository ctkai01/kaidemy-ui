import { Link } from "react-router-dom";
import SwiperCore from "swiper";
import { Navigation } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import LectureCardItem from "./LectureCardItem";

import * as React from "react";
import { ContinueCourse } from "../../../data/common";
import ArrowLeftBtn from "../common/ArrowLeftBtn";
import ArrowRightBtn from "../common/ArrowRightBtn";

const SLIDE_SHOW = 3;

interface IContinueCourseList {
    continueCourseList: ContinueCourse[];
}

const ContinueCourseList = (props: IContinueCourseList) => {
    const { continueCourseList } = props;

    const [swiper, setSwiper] = React.useState<SwiperCore>();
    const [currentSlide, setCurrentSlide] = React.useState(0);

    const handleNextSlide = () => {
        setCurrentSlide(swiper.activeIndex + SLIDE_SHOW - 1);
        swiper.slideTo(swiper.activeIndex + SLIDE_SHOW - 1);
    };

    const handlePrevSlide = () => {
        setCurrentSlide(
            swiper.activeIndex - SLIDE_SHOW - 1 > 0
                ? swiper.activeIndex - SLIDE_SHOW - 1
                : 0,
        );
        swiper.slideTo(swiper.activeIndex - SLIDE_SHOW - 1);
    };
    return (
        <div className="mb-10">
            <div className="flex items-center justify-between pb-4 pr-6">
                <h2 className="mb-2 text-3xl font-bold">
                    Let's start learning, Bùi Quốc Thuận
                </h2>
                <div className="font-bold text-primary-blue underline">
                    <Link to={"/"}>My leanring</Link>
                </div>
            </div>
            <div className="relative">
                <Swiper
                    allowTouchMove={false}
                    slidesPerView={3}
                    spaceBetween={10}
                    // className="overflow-visible"
                    onSwiper={(swiper: SwiperCore) => setSwiper(swiper)}
                    navigation={true}
                    // effect="coverflow"
                    modules={[Navigation]}
                >
                    {continueCourseList.map((item, index) => (
                        <SwiperSlide key={index}>
                            <LectureCardItem lectureCardItem={item} />
                        </SwiperSlide>
                    ))}
                </Swiper>
                <ArrowLeftBtn
                    isShow={currentSlide != 0 ? true : false}
                    handlePrevSlide={handlePrevSlide}
                    className="-left-[22px] top-18"
                />
                <ArrowRightBtn
                    isShow={
                        currentSlide + SLIDE_SHOW - 1 >=
                        continueCourseList.length - 2
                            ? false
                            : true
                    }
                    className="-right-[24px] top-18"
                    handleNextSlide={handleNextSlide}
                />
            </div>
        </div>
    );
};

export default ContinueCourseList;
