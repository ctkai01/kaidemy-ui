import { Link } from "react-router-dom";
import { TbPlayerPlayFilled } from "react-icons/tb";
import { ContinueCourse } from "../../../data/common";
interface ILectureCardItem {
    lectureCardItem: ContinueCourse;
}

const LectureCardItem = (props: ILectureCardItem) => {
    const { lectureCardItem } = props;
    return (
        <div className="flex overflow-hidden border border-[#d1d7dc]">
            <div className="relative h-36 w-32 min-w-[8rem] overflow-hidden bg-slate-500">
                <img
                    className="absolute left-1/2 h-full w-auto max-w-none -translate-x-1/2"
                    src={lectureCardItem.thumbnail}
                />
                <div className="absolute left-0 top-0 h-full w-full bg-[#2d2f3180]"></div>
                <div className=" absolute left-1/2 top-1/2 z-30 h-12 w-12 -translate-x-1/2 -translate-y-1/2">
                    <div className="flex h-full items-center justify-center rounded-full bg-white">
                        <TbPlayerPlayFilled className="h-6 w-6" />
                    </div>
                </div>
            </div>
            <div className="flex min-w-[1px] flex-col justify-between p-4">
                <div>
                    <div className="overflow-hidden overflow-ellipsis whitespace-nowrap pb-2 pr-4 text-xs font-bold text-[#6a6f73]">
                        {lectureCardItem.title}
                    </div>
                    <Link
                        className="after-right-0 line-clamp-3 pb-2 pr-4 text-base  font-bold text-[#2d2f31] after:absolute after:bottom-9 after:left-0 after:top-0 after:z-40 after:block after:h-full after:w-full after:content-[''] "
                        to="/"
                    >
                        {lectureCardItem.titleLecture}
                    </Link>
                </div>
                <div>
                    <span className="text-[#6a6f73]">
                        <span className="text-xs font-bold">Lecture </span>•
                        <span className="text-xs"> {lectureCardItem.time}</span>
                    </span>
                </div>
            </div>
        </div>
    );
};

export default LectureCardItem;
