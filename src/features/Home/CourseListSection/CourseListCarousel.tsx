import { Tooltip } from "flowbite-react";
import * as React from "react";
import SwiperCore from "swiper";
import "swiper/css/effect-coverflow";
import { Navigation } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import IntroCourseCard from "../../../components/ui/core/IntroCourseCard";
import { useAppSelector } from "../../../hooks/redux-hook";
import { CourseItem, CourseShow, Learning, PaginationInfo } from "../../../models";
import { selectUserAuth } from "../../../services/state/redux/authSlide";
import { getCoursePurchaseByID } from "../../../utils";
import ArrowLeftBtn from "../common/ArrowLeftBtn";
import ArrowRightBtn from "../common/ArrowRightBtn";
import CourseCard from "./CourseCard";

// const SLIDE_SHOW = 2;

interface ICourseListCarousel {
    courseList: CourseShow[];
    coursePurchase: Learning[];
    infoPagination: PaginationInfo;
    handleNextPage: () => void;
    slideShow: number;
    currentPage: number;
}

const CourseListCarousel = (props: ICourseListCarousel) => {
    const {
        courseList,
        coursePurchase,
        infoPagination,
        slideShow,
        handleNextPage,
        currentPage,
    } = props;
    const user = useAppSelector(selectUserAuth);

    const [swiper, setSwiper] = React.useState<SwiperCore>();
    const [currentSlide, setCurrentSlide] = React.useState(0);

    const handleNextSlide = () => {
        setCurrentSlide(swiper.activeIndex + slideShow - 1);
        swiper.slideTo(swiper.activeIndex + slideShow - 1);

        if (
            (currentPage + 1) * infoPagination.size - infoPagination.size <
            infoPagination.totalItem
        ) {
            handleNextPage();
        } 
    };

    const handlePrevSlide = () => {
        setCurrentSlide(
            swiper.activeIndex - slideShow - 1 > 0
                ? swiper.activeIndex - slideShow - 1
                : 0,
        );
        swiper.slideTo(swiper.activeIndex - slideShow - 1);
    };

    return (
        <>
            <Swiper
                allowTouchMove={false}
                slidesPerView={slideShow}
                spaceBetween={35}
                className="overflow-visible"
                onSwiper={(swiper: SwiperCore) => setSwiper(swiper)}
                navigation={true}
                effect="coverflow"
                modules={[Navigation]}
            >
                {courseList.map((course, i) => {
                    return (
                        <SwiperSlide key={i}>
                            <div
                                id={`slider-${course.id}`}
                                className="relative flex items-center  justify-center"
                            >
                                <Tooltip
                                    style="light"
                                    placement="right"
                                    content={
                                        <IntroCourseCard
                                            user={user}
                                            course={course}
                                            type={
                                                getCoursePurchaseByID(
                                                    coursePurchase,
                                                    course.id,
                                                )
                                                    ? "view"
                                                    : "buy"
                                            }
                                        />
                                    }
                                    trigger="hover"
                                >
                                    <CourseCard course={course} />
                                </Tooltip>
                            </div>
                        </SwiperSlide>
                    );
                })}
                <ArrowLeftBtn
                    isShow={currentSlide != 0 ? true : false}
                    handlePrevSlide={handlePrevSlide}
                    className="-left-[22px] top-18"
                />
                <ArrowRightBtn
                    isShow={
                        currentSlide + slideShow - 1 >= courseList.length - 1
                            ? false
                            : true

                        // courseList.length < infoPagination.totalItem ? true : false
                    }
                    handleNextSlide={handleNextSlide}
                    className="-right-[24px] top-18"
                />
            </Swiper>
        </>
    );
};

export default CourseListCarousel;
