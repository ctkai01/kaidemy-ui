import { Tooltip } from "flowbite-react";
import * as React from "react";
import { Link } from "react-router-dom";
import SwiperCore from "swiper";
import "swiper/css/effect-coverflow";
import { Navigation } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import { courses } from "../../../data/common";
import {
    CourseItem,
    CourseShow,
    PaginationInfo,
    TopCategory,
} from "../../../models";
import ArrowLeftBtn from "../common/ArrowLeftBtn";
import ArrowRightBtn from "../common/ArrowRightBtn";
import CourseCard from "./CourseCard";
import CourseListCarousel from "./CourseListCarousel";
import IntroCourseCard from "../../../components/ui/core/IntroCourseCard";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { useAppSelector } from "../../../hooks/redux-hook";
import { selectCoursePurchase } from "../../../services/state/redux/coursePurchaseSlide";

const SLIDE_SHOW = 5;

interface ICourseListSection {
    category: TopCategory;
    // courseList: CourseItem[];
}

const CourseListSection = (props: ICourseListSection) => {
    const { category } = props;
    const [currentPage, setCurrentPage] = React.useState(1);
    const [coursesCategories, setCoursesCategories] = React.useState<
        CourseShow[]
    >([]);
    const [isLoading, setIsLoading] = React.useState(false);

    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    let coursePurchase = useAppSelector(selectCoursePurchase);

    // const [swiper, setSwiper] = React.useState<SwiperCore>();
    // const [currentSlide, setCurrentSlide] = React.useState(0);

    // const handleNextSlide = () => {
    //     setCurrentSlide(swiper.activeIndex + SLIDE_SHOW - 1);
    //     swiper.slideTo(swiper.activeIndex + SLIDE_SHOW - 1);
    //     console.log("Current: ", swiper.activeIndex);
    // };

    // const handlePrevSlide = () => {
    //     setCurrentSlide(
    //         swiper.activeIndex - SLIDE_SHOW - 1 > 0
    //             ? swiper.activeIndex - SLIDE_SHOW - 1
    //             : 0,
    //     );
    //     swiper.slideTo(swiper.activeIndex - SLIDE_SHOW - 1);
    //     console.log("Current: ", swiper.activeIndex);
    // };
    const handleNextPage = () => {
        setCurrentPage(page => page + 1)
    }
    React.useEffect(() => {
        const fetchCoursesCategory = async () => {
            try {
                setIsLoading(true);
                // if (currentPageNotificationTeacher === 1) {
                //     dispatch(addListNotificationTeacher([]));
                // }
                const data =
                    await CourseServiceApi.getNotAuthCoursesCategoryShow(
                        category.id,
                        SLIDE_SHOW * 2,
                        currentPage,
                    );

                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });

                setCoursesCategories((prev) => [...prev, ...data.item]);
                setIsLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCoursesCategory();
    }, [currentPage]);

    return (
        <div className="relative border-2 border-[#d1d7dc] p-8">
            {/* <TooltipHTML placement="right" content={<div>Hello</div>} children={<div>Test</div>}/> */}
            <div className="mb-8">
                <div className="mb-2 text-2xl font-bold text-[#2D2F31]">
                    Broaden your career prospects with {category.name}
                </div>
                <div className="mb-4 max-w-3xl">
                    <p className="line-clamp-3 text-sm text-[#2D2F31]">
                        You will learn to build everything {category.name}.
                    </p>
                </div>
                <Link
                    className="block w-fit px-3 py-2 text-sm  font-bold ring-1 ring-black hover:bg-[#1739531f]"
                    to=""
                >
                    <button>Explore {category.name}</button>
                </Link>
            </div>
            <CourseListCarousel
                coursePurchase={coursePurchase}
                courseList={coursesCategories}
                handleNextPage={handleNextPage}
                infoPagination={infoPagination}
                slideShow={SLIDE_SHOW}
                currentPage={currentPage}
            />
            {/* <Swiper
                allowTouchMove={false}
                slidesPerView={5}
                spaceBetween={35}
                className="overflow-visible"
                onSwiper={(swiper: SwiperCore) => setSwiper(swiper)}
                navigation={true}
                effect="coverflow"
                modules={[Navigation]}
            >
                {courseList.map((course, i) => {
                    return (
                        <SwiperSlide key={i}>
                            <div
                                id={`slider-${i}`}
                                className="relative flex items-center  justify-center"
                            >
                                <Tooltip
                                    style="light"
                                    placement="right"
                                    content={
                                        <IntroCourseCard course={course} />
                                    }
                                    trigger="hover"
                                >
                                    <CourseCard course={course} />
                                </Tooltip>
                            </div>
                        </SwiperSlide>
                    );
                })}
            </Swiper>

            <ArrowLeftBtn
                isShow={currentSlide != 0 ? true : false}
                handlePrevSlide={handlePrevSlide}
                className="left-2"
            />
            <ArrowRightBtn
                isShow={
                    currentSlide + SLIDE_SHOW - 1 >= courseList.length - 2
                        ? false
                        : true
                }
                handleNextSlide={handleNextSlide}
                className="right-2"
            /> */}
        </div>
    );
};

export default CourseListSection;
