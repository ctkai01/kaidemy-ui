import { Button, Card, Tooltip } from "flowbite-react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import BestSeller from "../../../components/ui/core/BestSeller";
import ReviewStar from "../../../components/ui/core/ReviewStar";
import { CourseItem, CourseShow } from "../../../models";
import { formatNumberWithCommas } from "../../../utils";

interface ICourseCard {
    course: CourseShow;
}

const CourseCard = (props: ICourseCard) => {
    const { course } = props;
    return (
        <div className="group relative">
            <div className="mb-2 border-2 border-[#d1d7dc] group-hover:contrast-50">
                <img
                    className="w-full "
                    src={
                        course.image
                            ? course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
            </div>
            <div>
                <h3 className="mb-1 line-clamp-2  h-[48px] overflow-hidden whitespace-normal">
                    <Link
                        to={`/course/${course.id}`}
                        className="font-bold text-[#2d2f31] after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:block after:content-['']"
                    >
                        {course.title}
                    </Link>
                </h3>
                <div className="mb-1 text-xs text-[#6a6f73]">
                    {course.user.name}
                </div>
                <div className="flex items-center">
                    <span className="mr-2 text-sm font-bold text-[#4d3105]">
                        {/* {course.reviews} */}
                        {course.averageReview}
                    </span>
                    <ReviewStar number={course.averageReview} />
                    {/* <ReviewStar number={course.reviews} /> */}
                    <span className="text-xs text-[#6a6f73]">
                        ({formatNumberWithCommas(course.countReview)})
                        {/* ({formatNumberWithCommas(course.countReviews)}) */}
                    </span>
                </div>
                <p className="my-1 flex items-center text-lg font-bold text-gray-900 dark:text-white">
                    <BsCurrencyDollar />
                    {formatNumberWithCommas(
                        course.price ? course.price.value : 0,
                    )}
                </p>
                {/* <div className="mb-1 mt-2">
                    <BestSeller />
                </div> */}
            </div>
        </div>
    );
};

export default CourseCard;
