import { universities } from "../../../data/common";

const TrustList = () => {
    return (
        <div className="w-full bg-[#f7f9fa] p-16  mb-16 shadow-md">
            <h2 className="mb-4 text-center font-bold text-[#6a6f73]">
                Trusted by over 20 universities and thousands of learners around
                the world
            </h2>

            <div className="flex items-center justify-center gap-x-2">
                {universities.map((university, index) => (
                    <div key={index}>
                        <img
                            src={`./images/${university}`}
                            className="h-14 w-14 object-contain"
                        />
                    </div>
                ))}
            </div>
        </div>
    );
}

export default TrustList