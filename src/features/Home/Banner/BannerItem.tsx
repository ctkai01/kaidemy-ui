import { Card } from 'flowbite-react';
import * as React from 'react';

export interface IBannerItemProps {
    image: string,
    title: string
    desc: string
}

export default function BannerItem (props: IBannerItemProps) {
    const { desc, image, title} = props
    return (
        <div className="relative">
            <img className="w-full" src={image} />
            <Card className="absolute left-20 top-10 z-40 max-w-sm">
                <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    {title}
                </h5>
                <p className="font-normal text-gray-700 dark:text-gray-400">
                    {desc}
                </p>
            </Card>
        </div>
    );
}
