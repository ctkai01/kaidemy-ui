import * as React from "react";
import SwiperCore from "swiper";
import "swiper/css";
import "swiper/css/effect-fade";
import { Autoplay, EffectFade, Navigation } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import { banners } from "../../../data/common";
import ArrowLeftBtn from "../common/ArrowLeftBtn";
import ArrowRightBtn from "../common/ArrowRightBtn";
import BannerItem from "./BannerItem";
// import "swiper/css/navigation";
// import banner1 from "../Show/ArrowRightBtn";
const SLIDE_SHOW = 1;

interface IBanner {
}

const Banner = (props: IBanner) => {
    const [swiper, setSwiper] = React.useState<SwiperCore>();

    const handleNextSlide = () => {
        swiper.slideNext();
    };

    const handlePrevSlide = () => {
        swiper.slidePrev();
    };

    return (
        <div className="mt-4 ">
            <div className=" mb-12 flex">
                {/* {isLoggedIn} */}
                {/* <div className=".px-56 mb-12  flex h-96 justify-center">
                    <img
                    className=" object-cover"
                    src="./images/banner.jpg"
                    alt="Banner"
                />
                </div> */}
               <div className="relative h-96 w-full">
                    <Swiper
                        className="max-h-96 max-w-full"
                        slidesPerView={SLIDE_SHOW}
                        navigation={true}
                        loop={true}
                        effect="fade"
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        onSwiper={(swiper: SwiperCore) => setSwiper(swiper)}
                        modules={[Navigation, EffectFade, Autoplay]}
                    >
                        {banners.map((banner, i) => (
                            <SwiperSlide key={i}>
                                <BannerItem
                                    title={banner.title}
                                    desc={banner.desc}
                                    image={banner.image}
                                />
                            </SwiperSlide>
                        ))}
                    </Swiper>
                    <ArrowLeftBtn
                        isShow={true}
                        handlePrevSlide={handlePrevSlide}
                    />
                    <ArrowRightBtn
                        isShow={true}
                        handleNextSlide={handleNextSlide}
                    />
                </div>
            </div>
        </div>
    );
};

export default Banner;
