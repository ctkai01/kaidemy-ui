export interface ITeachIntroProps {}

export default function TeachIntro(props: ITeachIntroProps) {
    return (
        <div className="mb-10 mt-10 flex justify-center">
            <img
                className="mr-24"
                src="https://s.udemycdn.com/home/non-student-cta/instructor-1x-v3.jpg"
            />
            <div className="flex flex-col justify-center">
                <h3 className="mb-4 text-3xl font-bold text-[#2d2f31]">
                    Become Instructor
                </h3>
                <div className="max-w-sm">
                    Instructors from around the world teach millions of learners
                    on Hustdemy. We provide the tools and skills to teach what
                    you love.
                </div>
                <button className="mt-4 w-fit rounded-md bg-black px-3 py-2 font-bold text-white hover:bg-[#3e4143]">
                    <a>Start teaching today</a>
                </button>
            </div>
        </div>
    );
}
