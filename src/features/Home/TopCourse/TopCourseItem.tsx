import { useEffect, useState } from "react";
import { CourseShow, TopCategory } from "../../../models";
import CourseListCarousel from "../CourseListSection/CourseListCarousel";
// import CourseListSection from "../CourseListSection/CourseListSection";
import { PaginationInfo } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { useAppSelector } from "../../../hooks/redux-hook";
import { selectCoursePurchase } from "../../../services/state/redux/coursePurchaseSlide";
interface ITopCourseItem {
    topCategory: TopCategory;
}
const SIZE = 5;
const TopCourseItem = (props: ITopCourseItem) => {
    const { topCategory } = props;
    let coursePurchase = useAppSelector(selectCoursePurchase);

    const [currentPage, setCurrentPage] = useState(1);
    const [coursesCategories, setCoursesCategories] = useState<CourseShow[]>(
        [],
    );
    const [isLoading, setIsLoading] = useState(false);

    const [infoPagination, setInfoPagination] = useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });

    const handleNextPage = () => {
        setCurrentPage(page => page + 1)
    }

    useEffect(() => {
        const fetchCoursesCategory = async () => {
            try {
                setIsLoading(true);
                // if (currentPageNotificationTeacher === 1) {
                //     dispatch(addListNotificationTeacher([]));
                // }
                const data =
                    await CourseServiceApi.getNotAuthCoursesCategoryShow(
                        topCategory.id,
                        SIZE * 2,
                        currentPage,
                    );

                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });

                setCoursesCategories((prev) => [...prev, ...data.item]);
                setIsLoading(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchCoursesCategory();
    }, [currentPage]);

    return (
        <div className="mt-12">
            <h2 className="mb-4 text-2xl font-bold text-[#2d2f31]">
                Top courses <i>{topCategory.name}</i>
            </h2>
            <div className="">
                {/* <div className="after:clear-both relative before:block before:h-0 before:overflow-hidden after:content-[''] before:content-[''] after:block after:h-0 after:overflow-hidden"> */}
                <CourseListCarousel
                    coursePurchase={coursePurchase}
                    courseList={coursesCategories}
                    infoPagination={infoPagination}
                    handleNextPage={handleNextPage}
                    slideShow={SIZE}
                    currentPage={currentPage}
                />
            </div>
        </div>
    );
};

export default TopCourseItem;
