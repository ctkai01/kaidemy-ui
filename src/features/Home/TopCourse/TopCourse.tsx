import { Spinner } from "flowbite-react";
import { TopCategory } from "../../../models";
import TopCourseItem from "./TopCourseItem";

interface ITopCourse {
    topCategories: TopCategory[];
    isLoadingTopCategories: boolean;
}

const TopCourse = (props: ITopCourse) => {
    const { topCategories, isLoadingTopCategories } = props;
    return (
        <div className="mt-12 ">
            <h2 className="text-3xl font-bold text-[#2d2f31]">
                What to learn next
            </h2>
            {isLoadingTopCategories ? (
                <div className="mt-4 text-center">
                    <Spinner />
                </div>
            ) : (
                topCategories.map((category) => (
                    <TopCourseItem key={category.id} topCategory={category} />
                ))
            )}
        </div>
    );
};

export default TopCourse;
