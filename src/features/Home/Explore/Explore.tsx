import {
    CustomFlowbiteTheme,
    Flowbite,
    Spinner,
    Tabs,
    TabsRef,
} from "flowbite-react";
import { useRef, useState } from "react";
import "swiper/css";
import "swiper/css/pagination";
import CourseListSection from "../CourseListSection/CourseListSection";
import { courses } from "../../../data/common";
import { TopCategory } from "../../../models";

const customTheme: CustomFlowbiteTheme = {
    tab: {
        tablist: {
            tabitem: {
                styles: {
                    underline: {
                        base: "focus:ring-1",
                    },
                },
            },
        },
    },
};

interface IExplore {
    topCategories: TopCategory[];
    isLoadingTopCategories: boolean;
}
const Explore = (props: IExplore) => {
    const [activeTab, setActiveTab] = useState(0);
    const tabsRef = useRef<TabsRef>(null);

    const { topCategories, isLoadingTopCategories } = props;
    return (
        <>
            <h2 className="text-[32px] font-bold text-[#2D2F31]">
                A broad selection of courses
            </h2>
            <p className="pt-3 text-[#2D2F31]">
                Choose from over 210,000 online video courses with new additions
                published every month
            </p>
            {isLoadingTopCategories ? (
                <div className="mt-4 text-center">
                    <Spinner />
                </div>
            ) : (
                <Flowbite theme={{ theme: customTheme }}>
                    <Tabs.Group
                        className="relative mt-4 overflow-hidden"
                        aria-label="Default tabs"
                        style="underline"
                        ref={tabsRef}
                        onActiveTabChange={(tab) => setActiveTab(tab)}
                    >
                        {topCategories.map((category) => (
                            <Tabs.Item
                                key={category.id}
                                className="overflow-hidden"
                                active
                                title={category.name}
                            >
                                <CourseListSection category={category} />
                            </Tabs.Item>
                        ))}
                    </Tabs.Group>
                </Flowbite>
            )}
        </>
    );
};

export default Explore;
