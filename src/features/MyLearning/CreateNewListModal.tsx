import { yupResolver } from "@hookform/resolvers/yup";
import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Modal,
    Textarea,
    TextInput,
} from "flowbite-react";
import { useForm } from "react-hook-form";
import { CreateNewList } from "../../models";
import { schemeCreateNewList } from "../../validators/course";

interface ICreateNewListModal {
    openModal: boolean;
    idLearning: number
    handleOpenModal: (option: boolean) => void;
    createNewTopic?: (data: CreateNewList, idLearning: number) => void;
}

const customTheme: CustomFlowbiteTheme = {
    textInput: {
        field: {
            input: {
                colors: {
                    gray: "bg-gray-50 border-gray-300  focus:border-cyan-500 focus:ring-cyan-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-cyan-500 dark:focus:ring-cyan-500",
                },
            },
        },
    },
};
const CreateNewListModal = (props: ICreateNewListModal) => {
    const { openModal, idLearning, handleOpenModal, createNewTopic } = props;
    //
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
        watch,
    } = useForm<CreateNewList>({
        mode: "onChange",
        resolver: yupResolver(schemeCreateNewList),
    });
    const onSubmit = async (data: CreateNewList) => {
        if (createNewTopic) {
            createNewTopic(data, idLearning);
            reset();
            handleOpenModal(false)
        }
    };
    return (
        <Modal
            dismissible
            show={openModal}
            onClose={() => handleOpenModal(false)}
        >
            <form onSubmit={handleSubmit(onSubmit)} className="">
                <Modal.Header className="text-primary-black">
                    Create New List
                </Modal.Header>
                <Modal.Body>
                    <div className="relative mb-6">
                        <Flowbite theme={{ theme: customTheme }}>
                            <TextInput
                                placeholder="Name your list e.g HTML skills"
                                type="text"
                                className="text-primary-gray"
                                maxLength={60}
                                color={errors.name ? "failure" : "gray"}
                                {...register("name")}
                            />
                        </Flowbite>
                        <span className="absolute right-4 top-1/2 -translate-y-1/2 text-sm text-primary-gray">
                            {60 - (watch("name") ? watch("name").length : 0)}
                        </span>
                    </div>
                    {errors.name ? (
                        <div className="mb-5 text-red-600">
                            <span className="font-medium">Oops! </span>
                            <span>{errors.name.message}</span>
                        </div>
                    ) : (
                        <></>
                    )}
                    <div className="relative">
                        <Textarea
                            id="comment"
                            placeholder="Why did you create this list? e.g. To start a new business. To get a new job. To become a web developer"
                            rows={3}
                            className="pr-10 text-primary-gray"
                            maxLength={200}
                            color={errors.description ? "failure" : "gray"}
                            {...register("description")}
                        />
                        <span className="absolute right-4 top-1/2 -translate-y-1/2 text-sm text-primary-gray">
                            {200 -
                                (watch("description")
                                    ? (watch("description") || "").length
                                    : 0)}
                        </span>
                    </div>
                    {errors.description ? (
                        <div className="mb-5 text-red-600">
                            <span className="font-medium">Oops! </span>
                            <span>{errors.description.message}</span>
                        </div>
                    ) : (
                        <></>
                    )}
                </Modal.Body>
                <Modal.Footer className="flex justify-end">
                    <Button color="gray" onClick={() => handleOpenModal(false)}>
                       Cancel
                    </Button>
                    <Button type="submit">Save</Button>
                </Modal.Footer>
            </form>
        </Modal>
    );
};

export default CreateNewListModal;
