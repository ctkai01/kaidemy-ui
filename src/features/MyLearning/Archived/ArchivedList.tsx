import { Spinner } from "flowbite-react";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { ARCHIE, STANDARD_TYPE } from "../../../constants";
import { useAppSelector } from "../../../hooks/redux-hook";
import { Learning } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { selectUserAuth } from "../../../services/state/redux/authSlide";
import CourseLearningItem from "../AllCourse/CourseLearningItem";
import EmptyCourse from "../EmptyCourse";

export interface IArchivedListListProps {
    loading: boolean;
    activeTab: number;
    handleChangeTab: (tab: number) => void;
}

export default function ArchivedListList(props: IArchivedListListProps) {
    const { loading, activeTab, handleChangeTab } = props;
    const [learningArchive, setLearningArchive] = useState<Learning[]>([]);
    const [loadingFetch, setLoadingFetch] = useState(false);
    const user = useAppSelector(selectUserAuth);

    useEffect(() => {
        const fetchLearningList = async () => {
            try {
                setLoadingFetch(true);
                const dataLearning = await CourseServiceApi.getLearnings(
                    [ARCHIE],
                    1,
                    100,
                    user ? user.id : 0,
                );

                setLearningArchive(dataLearning.item);
                setLoadingFetch(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchLearningList();
    }, [activeTab]);

    const handleUnArchive = async (learningID: number) => {
        try {
            const body = {
                type: STANDARD_TYPE,
            };
            const dataUpdate = await CourseServiceApi.updateLearnings(
                learningID,
                body,
            );
            setLearningArchive((learnings) =>
                learnings.filter((learning) => learning.id != learningID),
            );
            toast(<div className="font-bold">Unarchived successfully!</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Unarchived failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <>
            {(loading || loadingFetch) && (
                <div className="flex w-full justify-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            )}

            {!loading && !learningArchive.length && (
                <EmptyCourse
                    type="achieved"
                    handleChangeTab={handleChangeTab}
                />
            )}

            {!loading && !loadingFetch && (
                <div className="-mx-2 flex flex-wrap gap-y-8">
                    {learningArchive.map((courseLearning, index) => (
                        <CourseLearningItem
                            key={index}
                            courseLearning={courseLearning}
                            handleUnArchive={handleUnArchive}
                            type="achieved"
                        />
                    ))}
                </div>
            )}
        </>
    );
}
