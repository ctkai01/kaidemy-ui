import { Pagination, Spinner } from "flowbite-react";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import {
    EditList,
    Learning,
    PaginationInfo,
    TopicLearning,
} from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { formatBetweenNumberPaginate } from "../../../utils";
import EmptyCourse from "../EmptyCourse";
import MyListsItemItem from "./MyListsItem";

export interface IMyListsProps {
    loading: boolean;
    activeTab: number;
    handleChangeTab: (tab: number) => void;
}

const TOTAL_PER_PAGE = 6;
export default function MyLists(props: IMyListsProps) {
    const { loading, activeTab, handleChangeTab } = props;
    const [currentPage, setCurrentPage] = useState(1);
    const onPageChange = (page: number) => setCurrentPage(page);
    const [learningTopics, setLearningTopics] = useState<TopicLearning[]>([]);
    const [loadingFetch, setLoadingFetch] = useState(false);
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    useEffect(() => {
        const fetchTopicLearningList = async () => {
            try {
                setLoadingFetch(true);
                const dataLearning = await CourseServiceApi.getTopicLearnings(
                    currentPage,
                    TOTAL_PER_PAGE,
                );

                setLearningTopics(dataLearning.item);
                setInfoPagination({
                    totalPage: dataLearning.meta.pageCount,
                    totalItem: dataLearning.meta.itemCount,
                    size: dataLearning.meta.size,
                });
                setLoadingFetch(false);
            } catch (e) {
                console.log(e);
            }
        };
        fetchTopicLearningList();
    }, [activeTab]);

    const handleEditLearningTopic = async (data: EditList) => {
        try {
            const body = {
                title: data.name,
                description: data.description ? data.description : "",
            };

            const dataLearning = await CourseServiceApi.updateTopicLearnings(
                data.id,
                body,
            );
            setLearningTopics((learningTopics: TopicLearning[]) => {
                const learningTopicsUpdate = [...learningTopics];

                const learningTopicIndex = learningTopicsUpdate.findIndex(
                    (item) => item.id === data.id,
                );

                if (learningTopicIndex != -1) {
                    const learningTopicUpdate = {
                        ...learningTopicsUpdate[learningTopicIndex],
                    };
                    learningTopicUpdate.title = dataLearning.title;
                    learningTopicUpdate.description = dataLearning.description;
                    learningTopicUpdate.updated_at = dataLearning.updated_at;

                    learningTopicsUpdate[learningTopicIndex] =
                        learningTopicUpdate;
                    return learningTopicsUpdate;
                }
                return learningTopics;
            });

            toast(
                <div className="font-bold">{`Your "${data.name}" list is updated successfully.`}</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">{`Your "${data.name}" list is updated failed.`}</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleDeleteLearningTopic = async (id: number, title: string) => {
        try {
            await CourseServiceApi.deleteTopicLearnings(id);

            setLearningTopics((learningTopics: TopicLearning[]) => {
                const learningTopicsUpdate = [...learningTopics];

                return learningTopicsUpdate.filter((item) => item.id != id);
            });

            toast(
                <div className="font-bold">{`Your "${title}" list is deleted successfully`}</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">{`Your "${title}" list is deleted failed`}</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleRemoveCourseMyList = async (
        topicLearningID: number,
        learningID: number,
    ) => {
        try {
            await CourseServiceApi.removeCourseToTopicLearnings(
                topicLearningID,
                learningID,
            );

            setLearningTopics((learningTopics) => {
                const learningTopicsUpdate = [...learningTopics];

                const learningTopicIndex = learningTopicsUpdate.findIndex(
                    (item) => item.id === topicLearningID,
                );

                if (learningTopicIndex != -1) {
                    const learningTopicUpdate = {
                        ...learningTopicsUpdate[learningTopicIndex],
                    };

                    let learnings = [...learningTopicUpdate.learnings];
                    learnings = learnings.filter(
                        (item) => item.id !== learningID,
                    );
                    learningTopicUpdate.learnings = learnings;

                    learningTopicsUpdate[learningTopicIndex] =
                        learningTopicUpdate;
                    return learningTopicsUpdate;
                }
                return learningTopics;
            });
            toast(
                <div className="font-bold">
                    Removed course from list successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Removed course from list failed!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    return (
        <>
            {(loading || loadingFetch) && (
                <div className="flex w-full justify-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            )}

            {!loading && !learningTopics.length && (
                <EmptyCourse type="mylist" handleChangeTab={handleChangeTab} />
            )}
            {!loading &&
                !loadingFetch &&
                learningTopics.map((learningTopic) => (
                    <MyListsItemItem
                        key={learningTopic.id}
                        handleRemoveCourseMyList={handleRemoveCourseMyList}
                        learningTopic={learningTopic}
                        handleEditLearningTopic={handleEditLearningTopic}
                        handleDeleteLearningTopic={handleDeleteLearningTopic}
                    />
                ))}

            {infoPagination.totalPage > 1 && (
                <>
                    <div className="mt-10 flex justify-center">
                        <Pagination
                            // layout="table"
                            currentPage={currentPage}
                            totalPages={infoPagination.totalPage}
                            onPageChange={onPageChange}
                            showIcons
                        />
                    </div>
                    <div className="mt-4 text-center text-xs text-primary-gray">
                        {formatBetweenNumberPaginate(
                            currentPage,
                            TOTAL_PER_PAGE,
                            infoPagination.totalItem,
                        )}{" "}
                        of {TOTAL_PER_PAGE} course
                    </div>
                </>
            )}

            {/* <MyListsItemItem /> */}
        </>
    );
}
