import { Button } from "flowbite-react";
import { useState } from "react";
import { FaPen } from "react-icons/fa";
import { MdDelete } from "react-icons/md";
import { EditList, TopicLearning } from "../../../models";
import CourseLearningItem from "../AllCourse/CourseLearningItem";
import DeleteListModal from "../DeleteListModal";
import EditListModal from "../EditListModal";

export interface IMyListsItemProps {
    learningTopic: TopicLearning;
    handleEditLearningTopic: (data: EditList) => void;
    handleRemoveCourseMyList: (
        topicLearningID: number,
        learningID: number,
    ) => void;
    handleDeleteLearningTopic: (id: number, title: string) => void;
}

export default function MyListsItemItem(props: IMyListsItemProps) {
    const {
        learningTopic,
        handleEditLearningTopic,
        handleRemoveCourseMyList,
        handleDeleteLearningTopic,
    } = props;
    const [openEditModal, setOpenEditModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);

    const handleOpenEditModal = (option: boolean) => {
        setOpenEditModal(option);
    };

    const handleOpenDeleteModal = (option: boolean) => {
        setOpenDeleteModal(option);
    };

    return (
        <>
            <EditListModal
                handleOpenModal={handleOpenEditModal}
                handleEditLearningTopic={handleEditLearningTopic}
                openModal={openEditModal}
                defaultValues={{
                    id: learningTopic.id,
                    name: learningTopic.title,
                    description: learningTopic.description
                        ? learningTopic.description
                        : "",
                }}
            />
            <DeleteListModal
                handleOpenModal={handleOpenDeleteModal}
                handleDeleteLearningTopic={handleDeleteLearningTopic}
                openModal={openDeleteModal}
                title={learningTopic.title}
                id={learningTopic.id}
            />
            <div className="mb-6 mt-12 first:mt-0">
                <div className="flex items-center">
                    <p className="mr-2 text-[19px] font-bold  text-primary-black">
                        {learningTopic.title}
                    </p>
                    {/* <butt */}
                    <Button
                        onClick={() => handleOpenEditModal(true)}
                        color="success"
                        className="mr-2"
                    >
                        <FaPen />
                    </Button>
                    <Button
                        onClick={() => handleOpenDeleteModal(true)}
                        color="failure"
                    >
                        <MdDelete />
                    </Button>
                </div>
                <div className="mt-2 line-clamp-1 block overflow-ellipsis whitespace-nowrap text-base">
                    {learningTopic.description}
                </div>
            </div>
            <div className="-mx-2 flex flex-wrap gap-y-8">
                {learningTopic.learnings.map((courseLearning, index) => (
                    <CourseLearningItem
                        key={index}
                        handleRemoveCourseMyList={handleRemoveCourseMyList}
                        courseLearning={courseLearning}
                        learningTopic={learningTopic}
                        type="myLists"
                    />
                ))}
            </div>
            {!learningTopic.learnings.length && (
                <div>No courses in this list yet</div>
            )}
        </>
    );
}
