import {
    Button, Modal
} from "flowbite-react";

interface IDeleteListModal {
    openModal: boolean;
    title: string;
    id: number;
    handleOpenModal: (option: boolean) => void;
    handleDeleteLearningTopic: (id: number, title: string) => void;
}

const DeleteListModal = (props: IDeleteListModal) => {
    const { openModal, title, id, handleOpenModal, handleDeleteLearningTopic } =
        props;
 
    const handleConfirmDelete = () => {
        handleDeleteLearningTopic(id, title);
        handleOpenModal(false)
       
    };
    
    return (
        <Modal
            dismissible
            show={openModal}
            onClose={() => handleOpenModal(false)}
        >
            <Modal.Header className="text-primary-black">
            Please confirm
            </Modal.Header>
            <Modal.Body>
            Are you sure you want to delete your list? You will still be able to access your courses.
            </Modal.Body>
            <Modal.Footer className="flex justify-end">
                <Button color="gray" onClick={() => handleOpenModal(false)}>
                   Cancel
                </Button>
                <Button onClick={handleConfirmDelete}>OK</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default DeleteListModal;
