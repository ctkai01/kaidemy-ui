import { Dropdown, Progress, Spinner } from "flowbite-react";
import { PropsWithChildren, useMemo, useState } from "react";
import {
    AiFillFolder,
    AiFillMinusCircle,
    AiOutlineMinusCircle,
    AiOutlinePlus,
} from "react-icons/ai";
import { BsPlusLg, BsThreeDotsVertical } from "react-icons/bs";
import { TbPlayerPlayFilled } from "react-icons/tb";
import { TiTick } from "react-icons/ti";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import ReviewStar from "../../../components/ui/core/ReviewStar";
import { CreateNewList, Learning, TopicLearning } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import LeaveRatingModal from "../../LeaningLecture/LeaveRatingModal";
import FormListModal from "../CreateNewListModal";
import CreateNewListModal from "../CreateNewListModal";

interface ICourseLearningItem extends PropsWithChildren<any> {
    courseLearning: Learning;
    learningTopic?: TopicLearning;
    learningTopics?: TopicLearning[];
    type?: "normal" | "achieved" | "myLists";
    handleRemoveCourseMyList?: (
        topicLearningID: number,
        learningID: number,
    ) => void;
    createNewTopic?: (data: CreateNewList, idLearning: number) => void;
    handleArchive?: (learningID: number) => void;
    handleUnArchive?: (learningID: number) => void;
    setLearningTopics?: (value: React.SetStateAction<TopicLearning[]>) => void;
    setLearningStandard?: (value: React.SetStateAction<Learning[]>) => void;
}

// const defaultProps: ICourseLearningItem = {
//     type: "normal",
// };
interface LoadingLearningItem {
    loading: boolean;
    topicLearningID: number;
}
const CourseLearningItem: React.FC<ICourseLearningItem> = (props) => {
    const {
        courseLearning,
        type,
        learningTopic,
        learningTopics,
        handleRemoveCourseMyList,
        handleArchive,
        handleUnArchive,
        createNewTopic,
        setLearningTopics,
        setLearningStandard,
    } = props;
    const [openModal, setOpenModal] = useState(false);
    const [loadingLearningItem, setLoadingLearningItem] =
        useState<LoadingLearningItem>({
            loading: false,
            topicLearningID: 0,
        });

    const [openLeaveRatingModal, setOpenLeaveRatingModal] = useState(false);

    const handleSetOpenLeaveRatingModal = (command: boolean) => {
        setOpenLeaveRatingModal(command);
    };
    const handleOpenModal = (option: boolean) => {
        setOpenModal(option);
    };
    const checkBelongToTopic = (learningTopic: TopicLearning): boolean => {
        const isExist = learningTopic.learnings.find(
            (item) => item.id == courseLearning.id,
        );
        if (isExist) {
            return true;
        } else {
            return false;
        }
    };

    const handleAddCourseToTopicLearning = async (topicLearningID: number) => {
        try {
            setLoadingLearningItem({
                loading: true,
                topicLearningID: topicLearningID,
            });

            const body = {
                learningID: courseLearning.id,
                topicLearningID: topicLearningID,
            };

            const dataAddTopic =
                await CourseServiceApi.addCourseToTopicLearnings(body);

            if (setLearningTopics) {
                setLearningTopics((learningTopics) => {
                    const learningTopicsUpdate = [...learningTopics];
                    const findTopicIndex = learningTopicsUpdate.findIndex(
                        (item) => item.id === topicLearningID,
                    );
                    if (findTopicIndex != -1) {
                        learningTopicsUpdate[findTopicIndex] = dataAddTopic;

                        return learningTopicsUpdate;
                    }

                    return learningTopics;
                });
            }
            setLoadingLearningItem({
                loading: false,
                topicLearningID: 0,
            });
            toast(
                <div className="font-bold">
                    Added course to list successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">Added course to list failed!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleRemoveCourseToTopicLearning = async (
        topicLearningID: number,
    ) => {
        try {
            setLoadingLearningItem({
                loading: true,
                topicLearningID: topicLearningID,
            });

            await CourseServiceApi.removeCourseToTopicLearnings(
                topicLearningID,
                courseLearning.id,
            );

            if (setLearningTopics) {
                setLearningTopics((learningTopics) => {
                    const learningTopicsUpdate = [...learningTopics];
                    const findTopicIndex = learningTopicsUpdate.findIndex(
                        (item) => item.id === topicLearningID,
                    );
                    if (findTopicIndex != -1) {
                        const learningTopicUpdate = {
                            ...learningTopicsUpdate[findTopicIndex],
                        };

                        learningTopicUpdate.learnings =
                            learningTopicUpdate.learnings.filter(
                                (item) => item.id === topicLearningID,
                            );

                        learningTopicsUpdate[findTopicIndex] =
                            learningTopicUpdate;

                        return learningTopicsUpdate;
                    }

                    return learningTopics;
                });
            }
            setLoadingLearningItem({
                loading: false,
                topicLearningID: 0,
            });
            toast(
                <div className="font-bold">
                    Removed course from list successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Removed course from list failed!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };
    const handleSaveReview = async (countStar: number, comment: string) => {
        try {
            const body = {
                starCount: countStar,
                type: courseLearning.type,
                comment: comment ? comment : "",
            };

            const data = await CourseServiceApi.updateLearnings(
                courseLearning.id,
                body,
            );
            if (setLearningStandard) {
                setLearningStandard((learningStandard) => {
                    const learningStandardUpdate = [...learningStandard];

                    const findIndex = learningStandardUpdate.findIndex(
                        (item) => item.id === courseLearning.id,
                    );

                    if (findIndex != -1) {
                        const learningUpdate = {
                            ...learningStandardUpdate[findIndex],
                        };
                        learningUpdate.startCount = data.startCount;
                        learningUpdate.comment = data.comment;
                        learningStandardUpdate[findIndex] = learningUpdate;
                        return learningStandardUpdate;
                    } else {
                        return learningStandard;
                    }
                });
            }
            toast(<div className="font-bold">Rated successfully</div>, {
                draggable: false,
                position: "top-right",
                type: "success",
                theme: "colored",
            });
            handleSetOpenLeaveRatingModal(false);
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Rated failed</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    return (
        <div className="min-w-1/4 group relative w-1/4 px-2">
            <CreateNewListModal
                openModal={openModal}
                createNewTopic={createNewTopic}
                idLearning={courseLearning.id}
                handleOpenModal={handleOpenModal}
            />
            <LeaveRatingModal
                starCount={courseLearning.startCount}
                comment={courseLearning.comment}
                handleSaveReview={handleSaveReview}
                openLeaveRatingModal={openLeaveRatingModal}
                handleSetOpenLeaveRatingModal={handleSetOpenLeaveRatingModal}
            />
            <div className="relative mb-1">
                <img
                    src={
                        courseLearning.course.image
                            ? courseLearning.course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
                <div className="absolute left-0 top-0 h-full w-full bg-primary-black opacity-0 group-hover:opacity-50"></div>
                <div className="invisible absolute left-1/2 top-1/2 z-30 h-12 w-12 -translate-x-1/2 -translate-y-1/2 scale-75 transition-all group-hover:visible group-hover:scale-100 group-hover:transition-all">
                    <div className="flex h-full items-center justify-center rounded-full bg-white">
                        <TbPlayerPlayFilled className="h-6 w-6" />
                    </div>
                </div>
                <div className="focus-bg-black absolute right-2 top-2 z-40 flex h-[34px] w-[34px] cursor-pointer items-center justify-center rounded bg-white">
                    {/* <div className="group relative flex h-full w-full items-center justify-center"> */}
                    <div className="relative">
                        <Dropdown
                            placement="bottom-end"
                            label="Dropdown button"
                            className="w-[260px]"
                            dismissOnClick={true}
                            renderTrigger={() => (
                                <div className="group relative z-30 flex h-full w-full items-center justify-center">
                                    <BsThreeDotsVertical />
                                </div>
                            )}
                        >
                            {type === "achieved" && (
                                <Dropdown.Item
                                    onClick={() => {
                                        if (handleUnArchive) {
                                            handleUnArchive(courseLearning.id);
                                        }
                                    }}
                                >
                                    <div className="group flex items-center">
                                        <AiFillFolder className="group-hover:text-primary-blue" />
                                        <div className="ml-4 text-sm text-primary-black group-hover:text-primary-blue">
                                            Unarchive
                                        </div>
                                    </div>
                                </Dropdown.Item>
                            )}

                            {type === "myLists" && (
                                <Dropdown.Item
                                    onClick={() => {
                                        if (
                                            learningTopic &&
                                            handleRemoveCourseMyList
                                        ) {
                                            handleRemoveCourseMyList(
                                                learningTopic.id,
                                                courseLearning.id,
                                            );
                                        }
                                    }}
                                >
                                    <div className="group flex w-[200px] items-center">
                                        <AiOutlineMinusCircle className="group-hover:text-primary-blue" />
                                        <div className="ml-4 text-sm text-primary-black group-hover:text-primary-blue">
                                            Remove course from this list
                                        </div>
                                    </div>
                                </Dropdown.Item>
                            )}

                            {type === "normal" && (
                                <>
                                    <div className="px-4 pt-4 text-sm font-bold">
                                        Lists
                                    </div>
                                    <ul className="max-h-30 overflow-auto border-b border-[#d1d7dc] py-2 text-sm ">
                                        {/* <div className="px-4 py-2 text-sm text-primary-gray">
                                        You have no list
                                    </div> */}
                                        {learningTopics &&
                                            learningTopics.map((item) => (
                                                <li
                                                    key={item.id}
                                                    onClick={() => {
                                                        if (
                                                            checkBelongToTopic(
                                                                item,
                                                            )
                                                        ) {
                                                            handleRemoveCourseToTopicLearning(
                                                                item.id,
                                                            );
                                                        } else {
                                                            handleAddCourseToTopicLearning(
                                                                item.id,
                                                            );
                                                        }
                                                    }}
                                                    className="px-4 py-2 text-primary-black hover:bg-[#1739531f] hover:text-primary-blue"
                                                >
                                                    <button className="flex w-full text-left">
                                                        <div className="min-w-[1px] flex-1">
                                                            <span className="block overflow-hidden text-ellipsis whitespace-nowrap">
                                                                {item.title}
                                                            </span>
                                                        </div>
                                                        {loadingLearningItem.loading &&
                                                        loadingLearningItem.topicLearningID ===
                                                            item.id ? (
                                                            <Spinner size="sm" />
                                                        ) : (
                                                            checkBelongToTopic(
                                                                item,
                                                            ) && (
                                                                <TiTick className="ml-2 text-[#2db83d]" />
                                                            )
                                                        )}
                                                    </button>
                                                </li>
                                            ))}
                                    </ul>
                                    <Dropdown.Item className="group/create">
                                        <div
                                            onClick={() => {
                                                handleOpenModal(true);
                                            }}
                                            className=" flex w-[200px] items-center"
                                        >
                                            <AiOutlinePlus className="group-hover/create:text-primary-blue" />
                                            <div className="ml-4 text-sm text-primary-black group-hover/create:text-primary-blue">
                                                Create new list
                                            </div>
                                        </div>
                                    </Dropdown.Item>
                                    {/* <Dropdown.Item className="group/remove">
                                        <div className=" flex items-center">
                                            <AiFillFolder className="group-hover/remove:text-primary-blue" />
                                            <div className="ml-4 text-sm text-primary-black group-hover/remove:text-primary-blue">
                                                Remove course from this list
                                            </div>
                                        </div>
                                    </Dropdown.Item> */}
                                    <Dropdown.Item
                                        className="group/archive"
                                        onClick={() => {
                                            if (handleArchive) {
                                                handleArchive(
                                                    courseLearning.id,
                                                );
                                            }
                                        }}
                                    >
                                        <div className=" flex items-center">
                                            <AiFillFolder className="group-hover/archive:text-primary-blue" />
                                            <div className="ml-4 text-sm text-primary-black group-hover/archive:text-primary-blue">
                                                Archive
                                            </div>
                                        </div>
                                    </Dropdown.Item>
                                </>
                            )}
                        </Dropdown>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <h3 className="mb-1 text-base font-bold text-primary-black">
                        <Link
                            to={`/course/${courseLearning.course.id}/learn`}
                            className="line-clamp-2 after:absolute after:left-0 after:top-0 after:z-30 after:h-full after:w-full after:content-['']"
                        >
                            {courseLearning.course.title}
                        </Link>
                    </h3>
                    <div className="mb-4 line-clamp-1 text-xs text-primary-gray">
                        {courseLearning.course.author.name}
                    </div>
                    <Progress
                        progress={
                            courseLearning.process ? courseLearning.process : 0
                        }
                        // progress={courseLearning.process}
                        className="mb-2"
                    />
                    {courseLearning.process ? (
                        <div className="flex justify-between text-xs text-primary-black">
                            <div>
                                {Math.round(courseLearning.process)}% completed
                            </div>
                            <div
                                className="relative z-30 cursor-pointer"
                                onClick={() => {
                                    handleSetOpenLeaveRatingModal(true);
                                }}
                            >
                                <ReviewStar
                                    number={
                                        courseLearning.startCount
                                            ? courseLearning.startCount
                                            : 0
                                    }
                                />
                                <div>
                                    {courseLearning.startCount
                                        ? "Your rating"
                                        : "Leave a rating"}
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="text-xs uppercase text-primary-black">
                            Start course
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};
CourseLearningItem.defaultProps = {
    type: "normal",
};

export default CourseLearningItem;
