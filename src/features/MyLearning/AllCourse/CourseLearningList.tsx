import { Spinner } from "flowbite-react";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { ARCHIE, STANDARD_TYPE } from "../../../constants";
// import { allCourseLearnings } from "../../../data/common";
import { useAppSelector } from "../../../hooks/redux-hook";
import { CreateNewList, Learning, TopicLearning } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { selectUserAuth } from "../../../services/state/redux/authSlide";
import CourseLearningItem from "./CourseLearningItem";

interface ICourseLearningList {
    loading: boolean;
    activeTab: number;
}

const CourseLearningList = (props: ICourseLearningList) => {
    const { loading, activeTab } = props;
    const [learningStandard, setLearningStandard] = useState<Learning[]>([]);
    const [learningTopics, setLearningTopics] = useState<TopicLearning[]>([]);
    const [loadingFetch, setLoadingFetch] = useState(false);

    const user = useAppSelector(selectUserAuth);

    useEffect(() => {
        const fetchLearningList = async () => {
            try {
                setLoadingFetch(true);

                const dataLearning = await CourseServiceApi.getLearnings(
                    [STANDARD_TYPE],
                    1,
                    100,
                    user ? user.id : 0,
                );
                if (!dataLearning.item) {
                    setLearningStandard([]);
                } else {
                    setLearningStandard(dataLearning.item);
                }
                setLoadingFetch(false);
            } catch (e) {
                console.log(e);
            }
        };
        const fetchTopicLearningList = async () => {
            try {
                setLoadingFetch(true);
                const dataLearning = await CourseServiceApi.getTopicLearnings(
                    1,
                    100,
                );
                if (!dataLearning?.item) {
                    setLearningTopics([]);
                } else {
                    setLearningTopics(dataLearning.item);
                }

                setLoadingFetch(false);
            } catch (e) {
                console.log(e);
            }
        };

        const fetchData = async () => {
            await Promise.all([fetchLearningList(), fetchTopicLearningList()]);
        };
        fetchData();
    }, [activeTab]);

    const handleArchive = async (learningID: number) => {
        try {
            const body = {
                type: ARCHIE,
            };
            const dataUpdate = await CourseServiceApi.updateLearnings(
                learningID,
                body,
            );
            setLearningStandard((learnings) =>
                learnings.filter((learning) => learning.id != learningID),
            );
            toast(
                <div className="font-bold">Archived successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Archived failed!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const createNewTopic = async (data: CreateNewList, idLearning: number) => {
        try {
            // new await();

            const bodyCreateTopic: any = {
                title: data.name,
            };

            if (data.description) {
                bodyCreateTopic.description = data.description;
            }
            const dataCreate =
                await CourseServiceApi.createTopicLearnings(bodyCreateTopic);

            const bodyAddCourseTopic: any = {
                learningID: idLearning,
                topicLearningID: dataCreate.id,
            };

            const dataAddTopic =
                await CourseServiceApi.addCourseToTopicLearnings(
                    bodyAddCourseTopic,
                );
            setLearningTopics((learningTopics) => {
                return [dataAddTopic, ...learningTopics];
            });

            toast(
                <div className="font-bold">Added new list successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
            // dataCreate.topic_learning;
            // const dataCreate
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                   Added new list failed!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    return (
        <div className="-mx-2 flex flex-wrap gap-y-8">
            {(loading || loadingFetch) && (
                <div className="flex w-full justify-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            )}
            {!loading &&
                !loadingFetch &&
                learningStandard.map((learning, index) => (
                    <CourseLearningItem
                        setLearningStandard={setLearningStandard}
                        learningTopics={learningTopics}
                        handleArchive={handleArchive}
                        createNewTopic={createNewTopic}
                        setLearningTopics={setLearningTopics}
                        type="normal"
                        key={index}
                        courseLearning={learning}
                    />
                ))}
        </div>
    );
};

export default CourseLearningList;
