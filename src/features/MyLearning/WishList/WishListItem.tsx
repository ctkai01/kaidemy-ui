import React from "react";
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import BestSeller from "../../../components/ui/core/BestSeller";
import ReviewStar from "../../../components/ui/core/ReviewStar";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../../../constants";
import { useAppDispatch } from "../../../hooks/redux-hook";
import { Learning } from "../../../models";
import { CourseServiceApi } from "../../../services/api/courseServiceApi";
import { removeWishList } from "../../../services/state/redux/wishListSlide";
import {
    convertSecondsToHoursMinutes,
    formatNumberWithCommas,
} from "../../../utils";

interface IWishListItem {
    wishListItem: Learning;
}

const WishListItem = (props: IWishListItem) => {
    const { wishListItem } = props;

    const dispatch = useAppDispatch();
    const { totalDuration, totalLecture } = React.useMemo(() => {
        let totalDuration = 0;
        let totalLecture = 0;
        if (wishListItem.course.curriculums) {
            wishListItem.course.curriculums.forEach((curriculum) => {
                if (curriculum.lectures) {
                    totalLecture += curriculum.lectures.length;
                    curriculum.lectures.forEach((lecture) => {
                        if (lecture.type === LECTURE_TYPE) {
                            if (lecture.assets) {
                                lecture.assets.forEach((asset) => {
                                    if (
                                        asset.type === LECTURE_WATCH_ASSET_TYPE
                                    ) {
                                        totalDuration += asset.duration;
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
        return { totalDuration, totalLecture };
    }, []);

    const handleRemoveWishList = async () => {
        try {
            await CourseServiceApi.removeWishList(wishListItem.id);
            dispatch(removeWishList(wishListItem.id));
            toast(
                <div className="font-bold">
                    Removed course from Wishlist successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">
                    Removed course from Wishlist failed!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };
    return (
        <div className="min-w-1/4 group relative w-1/4 px-2">
            <div className="relative mb-1">
                <img
                    src={
                        wishListItem.course.image
                            ? wishListItem.course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
                <div className="absolute left-0 top-0 h-full w-full bg-primary-black opacity-0 transition-all group-hover:opacity-50"></div>

                <AiFillHeart
                    onClick={() => {
                        handleRemoveWishList();
                    }}
                    className="absolute right-2 top-2 z-40 flex h-6 w-6 cursor-pointer text-white"
                />
                {/* <AiOutlineHeart/> */}
            </div>
            <div>
                <Link
                    to={`/course/${wishListItem.course.id}`}
                    className="mb-1 line-clamp-2 text-base  font-bold text-primary-black after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                >
                    {wishListItem.course.title}
                </Link>
                <div className="mb-1 text-xs text-primary-gray">
                    {wishListItem.course.author.name}
                </div>
                <div className="mb-1 flex items-center">
                    <span className="mr-2 text-sm font-bold text-[#4d3105]">
                        {wishListItem.averageReview}
                    </span>
                    <ReviewStar
                        number={
                            wishListItem.averageReview
                                ? wishListItem.averageReview
                                : 0
                        }
                    />
                    <span className="text-xs text-[#6a6f73]">
                        (
                        {formatNumberWithCommas(
                            wishListItem.countReview
                                ? wishListItem.countReview
                                : 0,
                        )}
                        )
                    </span>
                </div>
                <div className="mb-1">
                    <div className="flex text-xs text-primary-gray">
                        <span>
                            {convertSecondsToHoursMinutes(totalDuration)}
                        </span>
                        <span className="flex before:mx-1 before:text-[6px] before:content-['\25CF']">
                            {totalLecture} lectures
                        </span>
                    </div>
                </div>
                <div className="mb-1 flex items-center text-base font-bold text-primary-black">
                    <BsCurrencyDollar />
                    <span>{wishListItem.course.price?.value}</span>
                </div>
                {/* <BestSeller /> */}
            </div>
        </div>
    );
};

export default WishListItem;
