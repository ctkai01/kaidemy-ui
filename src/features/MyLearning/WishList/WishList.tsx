import { Button, Pagination, Spinner, TextInput } from "flowbite-react";
import { useState } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { Link } from "react-router-dom";
import { useAppSelector } from "../../../hooks/redux-hook";
import { selectWishList } from "../../../services/state/redux/wishListSlide";
import { formatBetweenNumberPaginate } from "../../../utils";
import WishListItem from "./WishListItem";

interface IWishList {
    loading: boolean
}

const TOTAL = 38
const TOTAL_PER_PAGE = 12
const TOTAL_PAGE = 4
const WishList = (props: IWishList) => {
    const {loading } = props
    const [currentPage, setCurrentPage] = useState(1);
    const wishList = useAppSelector(selectWishList)
    const onPageChange = (page: number) => setCurrentPage(page);

    return (
        <div className="">
            {loading && (
                <div className="flex w-full justify-center">
                    <Spinner
                        aria-label="Extra large spinner example"
                        size="xl"
                    />
                </div>
            )}

            {!loading && (
                <>
                    {/* <div className="mb-8 flex justify-end">
                        <div className="max-w-[230px]">
                            <TextInput
                                type="email"
                                rightIcon={AiOutlineSearch}
                            />
                        </div>
                    </div> */}
                    <div className="-mx-2 mb-10 flex flex-wrap gap-y-8">
                        {/* {Array.from({ length: 12 }).map((_, i) => (
                            <WishListItem key={i} />
                        ))} */}
                        {wishList.map((item) => (
                            <WishListItem key={item.id} wishListItem={item} />
                        ))}
                    </div>
                    {wishList.length === 0 && (
                        <div className="flex justify-center">
                            <Link className="w-fit" to="/">
                            <Button className="font-bold">
                                Explore course now
                            </Button>
                            </Link>
                        </div>
                    )}
                    {/* <div className="flex justify-center">
                        <Pagination
                            // layout="table"
                            currentPage={currentPage}
                            totalPages={TOTAL_PAGE}
                            onPageChange={onPageChange}
                            showIcons
                        />
                    </div>
                    <div className="mt-4 text-center text-xs text-primary-gray">
                        {formatBetweenNumberPaginate(
                            currentPage,
                            TOTAL_PER_PAGE,
                            TOTAL
                        )}{" "}
                        of {TOTAL} course
                    </div> */}
                </>
            )}
        </div>
    );
};

export default WishList;
