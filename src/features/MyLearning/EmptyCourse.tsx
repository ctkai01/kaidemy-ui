import { Link, useNavigate } from "react-router-dom";
import { MY_LEARNING_TABS } from "../../constants";

interface IEmptyCourse {
    type: "mylist" | "achieved";
    handleChangeTab?: (tab: number) => void;
}

const EmptyCourse = (props: IEmptyCourse) => {
    const { type, handleChangeTab } = props;
    const navigate = useNavigate()
    return (
        <div className="pb-12 pt-16">
            <div className="flex flex-col justify-center text-center">
                <h3 className="text-[19px] font-bold text-primary-black">
                    {type === "achieved" &&
                        "Only focus on the courses that are important to you.."}

                    {type === "mylist" &&
                        "Organize and access your courses faster!"}
                </h3>
                <p className="mt-4 text-base">
                    <span>
                        <span
                            className=" cursor-pointer font-bold text-primary-blue underline"
                            // to="/home/my-courses/learning"
                            onClick={() => {
                                if (handleChangeTab) {
                                    handleChangeTab(
                                        MY_LEARNING_TABS.ALL_COURSE,
                                    );
                                }
                            }}
                        >
                            Go to All Courses
                        </span>
                        {type === "achieved" && "to archive"}
                        {type === "mylist" && " to create list"}
                    </span>
                </p>
            </div>
        </div>
    );
}

export default EmptyCourse;