import { CustomFlowbiteTheme, Flowbite, Modal } from "flowbite-react";
import { TiMediaPlay } from "react-icons/ti";
import { CourseShow } from "../../models";
import { VideoPreview } from "../../pages/CourseDetail";
import { convertSecondsToHoursMinutes } from "../../utils";

const customTheme: CustomFlowbiteTheme = {
    modal: {
        content: {
            inner: "bg-[#2d2f31]",
        },
        header: {
            base: "flex items-start justify-between rounded-t dark:border-gray-600 p-5",
        },
        body: {
            base: " flex-1 overflow-auto",
        },
        footer: {
            popup: "",
        },
        root: {
            sizes: {
                "2xl": "max-w-[600px]",
            },
        },
    },
};

interface IVideoPreviewListProps {
    openPreviewVideoModal: boolean;
    videoPreviewID: string;
    course: CourseShow;
    videosPreviewID: VideoPreview[];
    handleChangeVideoPreviewID: (videoID: string) => void;
    handleSetPreviewVideoModal: (command: boolean, init: boolean) => void;
}

export default function VideoPreviewList(props: IVideoPreviewListProps) {
    const {
        videoPreviewID,
        videosPreviewID,
        course,
        openPreviewVideoModal,
        handleSetPreviewVideoModal,
        handleChangeVideoPreviewID,
    } = props;

    return (
        <Flowbite theme={{ theme: customTheme }}>
            <Modal
                show={openPreviewVideoModal}
                dismissible
                onClose={() => handleSetPreviewVideoModal(false, false)}
            >
                <Modal.Header>
                    <div className="pr-9 pt-6">
                        <div className="pb-2 text-sm font-bold text-[#d1d7dc]">
                            Preview course
                        </div>
                        <div className="text-[19px] font-bold text-white">
                            {course.title}
                        </div>
                    </div>
                </Modal.Header>
                <Modal.Body>
                    <div className="px-[20px]">
                        <div
                            style={{
                                position: "relative",
                                paddingTop: "56.25%",
                            }}
                        >
                            <iframe
                                src={`https://iframe.mediadelivery.net/embed/155247/${videoPreviewID}?autoplay=true&loop=false&muted=false&preload=true`}
                                loading="lazy"
                                style={{
                                    border: 0,
                                    position: "absolute",
                                    top: 0,
                                    height: "100%",
                                    width: "100%",
                                }}
                                allow="accelerometer;gyroscope;autoplay;encrypted-media;picture-in-picture;"
                                allowFullScreen={true}
                            ></iframe>
                        </div>
                        <div className="py-4 text-base font-bold text-white">
                            Free Sample Videos:
                        </div>
                    </div>
                    {/* <div className=""> */}
                    <div className="max-h-[250px] overflow-y-scroll bg-primary-black">
                        {/* <div> */}
                        {videosPreviewID.map((data, i) => (
                            <button
                                onClick={() =>
                                    handleChangeVideoPreviewID(data.asset.url)
                                }
                                key={i}
                                className={`flex w-full items-center  p-4 ${
                                    videoPreviewID === data.asset.url
                                        ? "bg-[#3e4143]"
                                        : ""
                                }`}
                            >
                                <div className="mr-4 w-16">
                                    <img
                                        className="h-9 w-full object-cover"
                                        loading="lazy"
                                        src={`https://vz-eb495bf1-353.b-cdn.net/${data.asset.url}/thumbnail.jpg`}
                                    />
                                </div>
                                <div className="relative mr-2 h-5 w-5 rounded-full bg-white">
                                    <TiMediaPlay className="absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2" />
                                </div>
                                <div className="mr-4 flex-1 text-left text-sm font-bold text-white">
                                    {data.title}
                                </div>
                                <div className="text-xs font-bold text-white">
                                    {convertSecondsToHoursMinutes(
                                        data.asset.duration,
                                    )}
                                </div>
                            </button>
                        ))}
                        {/* {Array.from({ length: 2 }).map((_, index) => (
                            
                        ))} */}
                        {/* </div> */}
                    </div>
                </Modal.Body>
                {/* <Modal.Footer>
                  <Button onClick={() => handleSetPreviewVideoModal(false)}>
                      I accept
                  </Button>
                  <Button
                      color="gray"
                      onClick={() => handleSetPreviewVideoModal(false)}
                  >
                      Decline
                  </Button>
              </Modal.Footer> */}
            </Modal>
        </Flowbite>
    );
}
