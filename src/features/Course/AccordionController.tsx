import * as React from "react";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";
import { IAccordionItemProps } from "./AccordionItem";
export interface IAccordionControllerProps {
    children:
        | React.ReactElement<IAccordionItemProps>
        | React.ReactElement<IAccordionItemProps>[];
    //  icon:
    arrowIcon?: React.FC<React.ComponentProps<"svg">>;
}

const AccordionController: React.FC<IAccordionControllerProps> = (
    props: IAccordionControllerProps,
) => {
    const { children, arrowIcon } = props;

    const childrenWithProps = React.Children.map(children, (child) => {
        // Check if the child is a valid React element
        if (React.isValidElement(child)) {
            // Clone the child and pass the additional prop
            return React.cloneElement(child, {
                arrowIcon,
            });
        }
        return child;
    });
    return <div>{childrenWithProps}</div>;
};
AccordionController.defaultProps = {
    arrowIcon: IoIosArrowUp,
};
export default AccordionController;
