import BestSeller from "../../components/ui/core/BestSeller";
import ReviewStar from "../../components/ui/core/ReviewStar";
import { Course, CourseShow } from "../../models";
import { formatNumberWithCommas } from "../../utils";



export interface IHeaderCourseProps {
    course: CourseShow
}

export default function HeaderCourse (props: IHeaderCourseProps) {
    const { course } = props
  return (
      <div className="bg-primary-black px-4 py-2 text-white">
          <div className="mb-1overflow-hidden overflow-ellipsis whitespace-nowrap text-base font-bold">
              {course.title}
          </div>
          <div className="flex items-center">
              {/* <BestSeller className="mr-2" /> */}
              <span className="mr-2 text-sm font-bold text-[#f69c08]">
                  {course.averageReview}
              </span>
              <ReviewStar color="#f69c08" number={course.averageReview} />
              <span className="mr-2 text-sm text-[#8dfdff] underline">
                  ({formatNumberWithCommas(course.countReview)} ratings)
              </span>
              <span className="text-sm text-white">{course.countStudent} students</span>
          </div>
      </div>
  );
}
