import * as React from 'react';
import { BsCurrencyDollar, BsStarFill } from 'react-icons/bs';
import { FaHeart, FaRegHeart } from 'react-icons/fa';
import { MdGroup } from 'react-icons/md';
import { Link } from 'react-router-dom';
import BestSeller from '../../components/ui/core/BestSeller';
import ReviewStar from '../../components/ui/core/ReviewStar';

export interface IStudentBoughtCourseProps {
}

export default function StudentBoughtCourse (props: IStudentBoughtCourseProps) {
  return (
      <div className='relative'>
          <div className="flex py-4 border-b border-primary-hover-gray">
              <div>
                  <img
                      className="min-w-16 h-16 w-16 border border-[#d1d7dc]"
                      loading="lazy"
                      width={64}
                      height={64}
                      src="https://img-c.udemycdn.com/course/50x50/1672410_9ff1_5.jpg"
                  />
              </div>
              <div className="ml-2">
                  <Link
                      className="after:content-[''] after:absolute after:top-0 after:left-0 after:w-full after:h-full line-clamp-2 text-base font-bold text-primary-black"
                      to="/"
                  >
                      Node.js, Express, MongoDB & More: The Complete Bootcamp
                      2024
                  </Link>
                  <div className="mt-2 flex">
                      {/* <BestSeller className="mr-2" /> */}
                      <div>
                          <span className="text-sm font-bold text-[#1e6055]">
                              42 total hours
                          </span>
                      </div>
                  </div>
              </div>
              <div className="ml-6 flex">
                  <div className="flex">
                      <div className="flex">
                          <span className="mr-1 text-sm font-bold text-[#4d3105]">
                              4.7
                          </span>
                          <BsStarFill
                              className={`mr-[2px] h-4 w-4 text-[#f69c08]`}
                          />
                      </div>
                      <div className="ml-6 mr-8 flex">
                          <MdGroup />
                          <span className="ml-1 text-sm text-primary-black">
                              125,723
                          </span>
                      </div>
                      <div className="flex  h-fit text-base font-bold">
                          <BsCurrencyDollar />
                          <div className="-mt-1">100</div>
                      </div>
                  </div>
              </div>
              <div className='flex items-center'>
                  <div className="flex h-10 w-10 cursor-pointer items-center justify-center rounded-full border border-black px-3 hover:bg-primary-hover-gray">
                      <FaRegHeart className="h-6 w-6" />
                  </div>
              </div>
          </div>
      </div>
  );
}
