import * as React from "react";
import { AiOutlineFile } from "react-icons/ai";
import { CiYoutube } from "react-icons/ci";
import { GoLightBulb } from "react-icons/go";
import { IoIosArrowUp } from "react-icons/io";
import { LECTURE_WATCH_ASSET_TYPE } from "../../constants";
import { Lecture } from "../../models";
import { convertSecondsToHoursMinutes } from "../../utils";
export interface ILectureItemProps {
    lecture: Lecture;
    type: "video" | "article" | "quiz";
    handleShowModalPreviewWithVideoID: (videoID: string) => void;
}

export default function LectureItem(props: ILectureItemProps) {
    const [isOpen, setIsOpen] = React.useState(false);

    const { type, lecture, handleShowModalPreviewWithVideoID } = props;

    const handleToggleOpen = () => {
        setIsOpen((isOpen) => !isOpen);
    };
    const getTotalTimeLecture = (): string => {
        if (type === "video") {
            const assetVideo = lecture.assets.find(
                (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
            );

            if (assetVideo) {
                return convertSecondsToHoursMinutes(assetVideo.duration);
            } else {
                return "";
            }
        } else {
            return "";
        }
    };
    return (
        <li
            className={`${lecture.isPromotional && "cursor-pointer"} text-none`}
            onClick={() => {
                if (lecture.isPromotional) {
                    const assetVideo = lecture.assets.find(
                        (asset) => asset.type === LECTURE_WATCH_ASSET_TYPE,
                    );

                    if (assetVideo) {
                        handleShowModalPreviewWithVideoID(assetVideo.url);
                    }
                }
            }}
        >
            <div className="flex justify-start py-2">
                {type === "video" && <CiYoutube />}
                {type === "article" && <AiOutlineFile />}
                {type === "quiz" && <GoLightBulb />}

                <div className="-mt-1 ml-4 flex flex-1">
                    <div>
                        <div className="flex">
                            <button
                                className={`max-w-[400px] cursor-default overflow-hidden text-ellipsis whitespace-nowrap text-sm ${
                                    lecture.isPromotional &&
                                    "cursor-pointer text-primary-blue underline"
                                } `}
                            >
                                <span>{lecture.title}</span>
                            </button>
                            {lecture.description && (
                                <button
                                    onClick={handleToggleOpen}
                                    className={`${
                                        isOpen ? "" : "-rotate-180"
                                    } ml-2  flex h-6 w-6 items-center justify-center rounded-full transition-transform delay-150 ease-linear`}
                                >
                                    <IoIosArrowUp />
                                </button>
                            )}
                        </div>
                        {isOpen && lecture.description && (
                            <div className="max-w-[400px] pt-2 text-sm text-[#6a6f73]">
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: `${lecture.description}`,
                                    }}
                                ></div>
                            </div>
                        )}
                    </div>
                    <div className="flex-1"></div>
                    <span className="ml-8 text-sm text-primary-blue underline">
                        {lecture.isPromotional && "Preview"}
                    </span>
                    <span className="ml-8 text-sm text-primary-gray">
                        {getTotalTimeLecture()}
                    </span>
                    <span></span>
                </div>
            </div>
        </li>
    );
}
