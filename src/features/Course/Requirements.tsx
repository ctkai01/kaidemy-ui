import * as React from 'react';
import { GoDotFill } from 'react-icons/go';
import { CourseShow } from '../../models';

export interface IRequirementsProps {
    course: CourseShow
}

export default function Requirements (props: IRequirementsProps) {
    const { course } = props
  return (
      <div>
          <h2 className="mb-4 text-2xl font-bold text-primary-black">
              Requirements
          </h2>
          <ul>
              {course.requirements?.map((item, index) => (
                  <li key={index}>
                      <div className="flex items-center py-1">
                          <GoDotFill />
                          <div className="ml-4 text-sm text-primary-black">
                              {item}
                          </div>
                      </div>
                  </li>
              ))}
          </ul>
      </div>
  );
}
