import { Accordion, Button } from "flowbite-react";
import { useEffect, useMemo, useRef, useState } from "react";
import AccordionItem from "./AccordionItem";
import AccordionController from "./AccordionController";
import CoursePanel from "./CoursePanel";
import LectureItem from "./LectureItem";
import { GoDotFill } from "react-icons/go";
import Requirements from "./Requirements";
import DescriptionToggle from "./DescriptionToggle";
import StudentAlsoBought from "./StudentAlsoBought";
import IntroInstructor from "./IntroInstructor";
import { CourseShow, Lecture } from "../../models";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../../constants";
import { convertSecondsToHoursMinutes } from "../../utils";

export interface ICourseContentProps {
    course: CourseShow;
    totalDuration: number;
    totalLecture: number;
    totalSection: number;
    isInitAccordion: boolean;
    isCollapseAll: boolean;
    handleShowModalPreviewWithVideoID: (videoID: string) => void;
    toggleAccordion: () => void;
}

export default function CourseContent(props: ICourseContentProps) {
    const {
        course,
        totalDuration,
        totalLecture,
        totalSection,
        isCollapseAll,
        isInitAccordion,
        toggleAccordion,
        handleShowModalPreviewWithVideoID,
    } = props;

    const totalTimeCurriculum = (lectures: Lecture[]): string => {
        let totalDuration = 0;
        lectures.forEach((lecture) => {
            if (lecture.type === LECTURE_TYPE) {
                lecture.assets.forEach((asset) => {
                    if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
                        totalDuration += asset.duration;
                    }
                });
            }
        });

        return convertSecondsToHoursMinutes(totalDuration);
    };
    return (
        <div>
            <h2 className="mb-2 text-2xl font-bold text-primary-black">
                Course content
            </h2>
            <div className="mb-8">
                <div className="flex items-center justify-between text-sm text-primary-black">
                    <span className="my-2">
                        {totalSection} sections • {totalLecture} lectures •{" "}
                        <span>
                            <span>
                                {convertSecondsToHoursMinutes(totalDuration)}
                            </span>{" "}
                            total length
                        </span>
                    </span>
                    <button
                        onClick={toggleAccordion}
                        className="h-[40px] bg-transparent font-bold text-primary-blue"
                    >
                        <span>
                            {`${isCollapseAll ? "Collapse" : "Expand"}`} all
                            sections
                        </span>
                    </button>
                </div>
                <AccordionController>
                    {course.curriculums.map((curriculum, index) => (
                        <AccordionItem
                            key={index}
                            isOpen={
                                index === 0 && !isInitAccordion
                                    ? true
                                    : isCollapseAll
                            }
                            title={
                                <CoursePanel
                                    numberLecture={curriculum.lectures.length}
                                    time={totalTimeCurriculum(
                                        curriculum.lectures,
                                    )}
                                    title={curriculum.title}
                                />
                            }
                        >
                            <ul className="border-t border-primary-hover-gray px-6 py-4">
                                {curriculum.lectures.map((lecture) => {
                                    if (lecture.type == LECTURE_TYPE) {
                                        if (lecture.article) {
                                            return (
                                                <LectureItem
                                                    key={lecture.id}
                                                    lecture={lecture}
                                                    type="article"
                                                    handleShowModalPreviewWithVideoID={
                                                        handleShowModalPreviewWithVideoID
                                                    }
                                                />
                                            );
                                        } else {
                                            return (
                                                <LectureItem
                                                    key={lecture.id}
                                                    lecture={lecture}
                                                    type="video"
                                                    handleShowModalPreviewWithVideoID={
                                                        handleShowModalPreviewWithVideoID
                                                    }
                                                />
                                            );
                                        }
                                    } else {
                                        return (
                                            <LectureItem
                                                key={lecture.id}
                                                lecture={lecture}
                                                type="quiz"
                                                handleShowModalPreviewWithVideoID={
                                                    handleShowModalPreviewWithVideoID
                                                }
                                            />
                                        );
                                    }
                                })}
                            </ul>
                        </AccordionItem>
                    ))}
                    {/* {Array.from({ length: 4 }).map((_, i) => (
                        <AccordionItem
                            key={i}
                            isOpen={
                                i === 0 && !isInitAccordion
                                    ? true
                                    : isCollapseAll
                            }
                            title={
                                <CoursePanel
                                    numberLecture={2}
                                    time="4min"
                                    title="Introduction"
                                />
                            }
                        >
                            <ul className="border-t border-primary-hover-gray px-6 py-4">
                                <LectureItem type="article" />
                                <LectureItem type="quiz" />
                                <LectureItem type="video" />
                                <LectureItem type="video" />
                            </ul>
                        </AccordionItem> */}
                    {/* ))} */}
                </AccordionController>
            </div>
            <div className="mb-8">
                <Requirements course={course} />
            </div>
            <div className="mb-8">
                <h2 className="mb-4 text-2xl font-bold text-primary-black">
                    Description
                </h2>
                <DescriptionToggle
                    leastHeight="224"
                    text={`${course.description}`}
                />
            </div>
        </div>
    );
}
