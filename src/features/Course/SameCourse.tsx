import React from "react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Link } from "react-router-dom";
import BestSeller from "../../components/ui/core/BestSeller";
import ReviewStar from "../../components/ui/core/ReviewStar";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../../constants";
import { CourseShow, User } from "../../models";
import {
    convertSecondsToHoursMinutes,
    formatNumberWithCommas,
} from "../../utils";

interface ISameCourse {
    course: CourseShow;
    user: User
}

const SameCourse = (props: ISameCourse) => {
    const { course, user } = props;
    const { totalDuration, totalLecture } = React.useMemo(() => {
        let totalDuration = 0;
        let totalLecture = 0;

        course.curriculums?.forEach((curriculum) => {
            if (curriculum.lectures) {
                totalLecture += curriculum.lectures.length;
                curriculum.lectures.forEach((lecture) => {
                    if (lecture.type === LECTURE_TYPE) {
                        lecture.assets.forEach((asset) => {
                            if (asset.type === LECTURE_WATCH_ASSET_TYPE) {
                                totalDuration += asset.duration;
                            }
                        });
                    }
                });
            }
        });

        return { totalDuration, totalLecture };
    }, []);
    return (
        <div className="group relative px-2">
            <div className="relative mb-1">
                <img
                    className="w-full"
                    src={
                        course.image
                            ? course.image
                            : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                    }
                />
            </div>
            <div>
                <Link
                    to={`/course/${course.id}`}
                    className="mb-1 line-clamp-2 text-base  font-bold text-primary-black after:absolute after:left-0 after:top-0 after:h-full after:w-full after:content-['']"
                >
                    {course.title}
                </Link>
                <div className="mb-1 text-xs text-primary-gray">
                    {user.name}
                </div>
                <div className="mb-1 flex items-center">
                    <span className="mr-2 text-sm font-bold text-[#4d3105]">
                        4.7
                    </span>
                    <ReviewStar number={4.7} />
                    <span className="text-xs text-[#6a6f73]">
                        ({formatNumberWithCommas(1500)})
                    </span>
                </div>
                <div className="mb-1">
                    <div className="flex text-xs text-primary-gray">
                        <span>
                            {convertSecondsToHoursMinutes(totalDuration)}
                        </span>
                        <span className="flex before:mx-1 before:text-[6px] before:content-['\25CF']">
                            {totalLecture} lectures
                        </span>
                        <span className="flex before:mx-1 before:text-[6px] before:content-['\25CF']">
                            {course.level?.name}
                        </span>
                    </div>
                </div>
                <div className="mb-1 flex items-center text-base font-bold text-primary-black">
                    <BsCurrencyDollar />
                    <span>{course.price?.value}</span>
                </div>
                {/* <BestSeller /> */}
            </div>
        </div>
    );
};

export default SameCourse;
