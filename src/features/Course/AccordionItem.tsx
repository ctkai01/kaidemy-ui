import * as React from "react";

export interface IAccordionItemProps {
    children: React.ReactNode;
    title: React.ReactNode;
    isOpen?: boolean;
    positionIcon?: "start" | "end"
    paddingPanel?: string
    arrowIcon?: React.FC<React.ComponentProps<"svg">>;
}

const AccordionItem: React.FC<IAccordionItemProps> = (
    props: IAccordionItemProps,
) => {
    const {
        children,
        title,
        isOpen,
        positionIcon,
        paddingPanel,
        arrowIcon: ArrowIcon,
    } = props;
    const [isOpenContent, setIsOpenContent] = React.useState(isOpen);
    const handleToggle = () => {
        setIsOpenContent((isOpen) => !isOpen);
    };

    React.useEffect(() => {
        setIsOpenContent(isOpen);
    }, [isOpen]);
    return (
        <div className="border border-b-0 border-primary-hover-gray last:border-b">
            <div
                onClick={handleToggle}
                className={`flex ${
                    positionIcon === "end" ? "flex-row-reverse" : "items-center"
                }  cursor-pointer  bg-[#f7f9fa] ${paddingPanel}`}
            >
                {ArrowIcon && (
                    <ArrowIcon
                        className={`${isOpenContent ? "" : "-rotate-180"}  ${
                            positionIcon === "end" ? "ml-3" : "mr-3"
                        } transition-transform delay-150 ease-linear `}
                    />
                )}
                <div className="flex-1">{title}</div>
            </div>

            <div className={`${isOpenContent ? "block" : "hidden"}`}>
                {children}
            </div>
        </div>
    );
};

AccordionItem.defaultProps = {
    isOpen: false,
    positionIcon: "start",
    paddingPanel: "px-6 py-4",
};

export default AccordionItem;
