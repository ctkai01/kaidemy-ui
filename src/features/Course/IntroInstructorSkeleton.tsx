import { IoIosStar } from "react-icons/io";
import { PiCertificateFill } from "react-icons/pi";
import { FaUserGroup } from "react-icons/fa6";
import { Link } from "react-router-dom";
import { AiFillPlayCircle } from "react-icons/ai";
import DescriptionToggle from "./DescriptionToggle";

export interface IIntroInstructorSkeletonProps {}

export default function IntroInstructorSkeleton(
    props: IIntroInstructorSkeletonProps,
) {
    return (
        <div className="animate-pulse">
            <h2 className="mb-4 bg-slate-300 text-2xl font-bold text-primary-black">
                Instructor
            </h2>
            <div>
                <div className="mb-1 bg-slate-300">
                    <Link
                        className="t text-[19px] font-bold text-primary-blue underline"
                        to={""}
                    >
                        Academind by Maximilian Schwarzmüller
                    </Link>
                </div>
                <div className="mt-2 flex bg-slate-300">
                    <Link className="mr-4" to="">
                        <img
                            className="h-[112px] w-[112px] rounded-[50%] object-cover"
                            loading="lazy"
                            src="https://img-c.udemycdn.com/user/200_H/31926668_94e7_6.jpg"
                        />
                    </Link>
                    <ul>
                        <li>
                            <div className="flex items-center py-1">
                                <IoIosStar />
                                <div className="ml-4 text-sm text-primary-black">
                                    4.6 Instructor Ratings
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="flex items-center py-1">
                                <PiCertificateFill />
                                <div className="ml-4 text-sm text-primary-black">
                                    640,937 Ratings
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="flex items-center py-1">
                                <FaUserGroup />
                                <div className="ml-4 text-sm text-primary-black">
                                    2,686,473 Students
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="flex items-center py-1">
                                <AiFillPlayCircle />
                                <div className="ml-4 text-sm text-primary-black">
                                    46 Courses
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="mt-4">
                <DescriptionToggle
                    leastHeight="96"
                    text={`<p><strong"><em>What is Node.js</em>?</strong><span"> That's the most important question in a Node course I'd argue and in this lecture, we'll explore what exactly NodeJS is and why it's amazing.</span></p><p><strong"><em>What is Node.js</em>?</strong><span"> That's the most important question in a Node course I'd argue and in this lecture, we'll explore what exactly NodeJS is and why it's amazing.</span></p><p><strong"><em>What is Node.js</em>?</strong><span"> That's the most important question in a Node course I'd argue and in this lecture, we'll explore what exactly NodeJS is and why it's amazing Lorem ipsum dolor, sit amet consectetur adipisicing elit. Necessitatibus ipsam tenetur ducimus aut quas cumque, quisquam commodi dolores quod? Provident minus nisi omnis ex officiis vitae dolor, libero mollitia maxime, voluptatem pariatur suscipit aspernatur tempore quam cum, magni autem? Earum dolores recusandae suscipit, fuga veritatis nesciunt illum neque cum aliquam dolorum iusto deserunt aspernatur delectus in aut odio magni velit explicabo nihil dignissimos obcaecati, necessitatibus quis itaque alias. Consequatur totam asperiores corrupti mollitia quas, veniam repellat saepe iure labore qui!.</span></p>`}
                />
            </div>
        </div>
    );
}
