import { Button } from "flowbite-react";
import { AiOutlineHeart, AiOutlineYoutube } from "react-icons/ai";
import { BsCurrencyDollar } from "react-icons/bs";
import { FaHeart } from "react-icons/fa";
import { TbPlayerPlayFilled } from "react-icons/tb";
import { IoTimeOutline } from "react-icons/io5";
import { CourseShow } from "../../models";
import {
    convertSecondsToHoursMinutes,
    formatDayMonthYear,
    getCoursePurchaseByID,
} from "../../utils";
import useAddCart from "../../hooks/useAddCart";
import { useAppDispatch, useAppSelector } from "../../hooks/redux-hook";
import { selectUserAuth } from "../../services/state/redux/authSlide";
import { Link, useNavigate } from "react-router-dom";
import { selectWishList } from "../../services/state/redux/wishListSlide";
import { selectCart } from "../../services/state/redux/cartSlide";
import useAddWishList from "../../hooks/useAddWishList";
import useRemoveWishList from "../../hooks/useRemoveWishList";
import { selectCoursePurchase } from "../../services/state/redux/coursePurchaseSlide";
import { RiErrorWarningFill } from "react-icons/ri";
export interface ISidebarCourseProps {
    videoPreviewID: string;
    course: CourseShow;
    totalDuration: number;
    handleSetPreviewVideoModal: (command: boolean, init: boolean) => void;
}

export default function SidebarCourse(props: ISidebarCourseProps) {
    const {
        videoPreviewID,
        totalDuration,
        course,
        handleSetPreviewVideoModal,
    } = props;
    const user = useAppSelector(selectUserAuth);
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const wishList = useAppSelector(selectWishList);
    const cart = useAppSelector(selectCart);

    const [addCart] = useAddCart(course.id, user, navigate, dispatch, wishList);
    const [handleRemoveWishList] = useRemoveWishList(
        course.id,
        user,
        navigate,
        dispatch,
        wishList,
    );
    const [handleAddWishList] = useAddWishList(
        course.id,
        user,
        navigate,
        dispatch,
        cart,
    );
    let coursePurchase = useAppSelector(selectCoursePurchase);
    const checkWishList = (): boolean => {
        const isExist = wishList.find((item) => item.courseID === course.id);
        if (isExist) {
            return true;
        } else {
            return false;
        }
    };

    const checkCart = (): boolean => {
        if (cart) {
            const isExist = cart.cartItems.find(
                (item) => item.course.id === course.id,
            );
            if (isExist) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    };
    return (
        <div className="w-[340px] rounded-md border-primary-gray bg-white shadow-lg">
            <div>
                <div
                    onClick={() => {
                        if (videoPreviewID) {
                            handleSetPreviewVideoModal(true, true);
                        }
                    }}
                    className="relative cursor-pointer rounded-md border border-b-0 border-white bg-white"
                >
                    <span className="relative pt-[56%]">
                        <img
                            className="w-full rounded-md"
                            loading="lazy"
                            src={
                                course.image
                                    ? course.image
                                    : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                            }
                        />

                        <div className="bg-overlay absolute left-0 top-0 h-full w-full"></div>
                        {videoPreviewID && (
                            <div className="absolute left-0 top-0 flex h-full w-full items-end justify-center pb-4 text-base font-bold text-white">
                                Preview this course
                            </div>
                        )}
                    </span>
                    {videoPreviewID && (
                        <div className="absolute left-1/2 top-1/2 z-30 h-14 w-14 -translate-x-1/2 -translate-y-1/2">
                            <div className="flex h-full items-center justify-center rounded-full bg-white">
                                <TbPlayerPlayFilled className="h-8 w-8" />
                            </div>
                        </div>
                    )}
                </div>
                <div className="p-6">
                    {getCoursePurchaseByID(coursePurchase, course.id) ? (
                        <>
                            <div className="flex">
                                <RiErrorWarningFill className="h-6 w-6 text-cyan-500" />
                                <div className=" font-base ml-4 font-bold text-primary-black">
                                    You purchased this course on{" "}
                                    {formatDayMonthYear(
                                        getCoursePurchaseByID(
                                            coursePurchase,
                                            course.id,
                                        )
                                            ? getCoursePurchaseByID(
                                                  coursePurchase,
                                                  course.id,
                                              ).createdAt
                                            : "",
                                    )}
                                </div>
                            </div>
                            <Link to={`/course/${course.id}/learn`}>
                                <Button
                                    color="dark"
                                    className="mt-2 w-full font-bold"
                                >
                                    Go to course
                                </Button>
                            </Link>
                        </>
                    ) : (
                        <>
                            <div className="flex items-center justify-center py-1 text-[32px] font-bold text-primary-black">
                                <BsCurrencyDollar />
                                {course.price?.value}
                            </div>
                            <div className="mb-2 mt-2 flex">
                                <div
                                    className="mr-2 flex-1"
                                    onClick={() => {
                                        if (checkCart()) {
                                            navigate("/cart");
                                        } else {
                                            addCart();
                                        }
                                    }}
                                >
                                    <Button className="h-full w-full text-base font-bold">
                                        {checkCart()
                                            ? "Đi tời giỏ hàng"
                                            : "Add to cart"}
                                    </Button>
                                </div>
                                <div
                                    onClick={() => {
                                        if (checkWishList()) {
                                            handleRemoveWishList();
                                        } else {
                                            handleAddWishList();
                                        }
                                    }}
                                    className="flex h-12 cursor-pointer items-center justify-center rounded border border-black px-3 hover:bg-primary-hover-gray"
                                >
                                    {checkWishList() ? (
                                        <FaHeart className="h-6 w-6" />
                                    ) : (
                                        <AiOutlineHeart className="h-6 w-6" />
                                    )}
                                </div>
                            </div>
                        </>
                    )}

                    {/* <Button
                        className="w-full text-base font-bold text-white"
                        color="blue"
                        outline
                    >
                        Buy now
                    </Button> */}
                    <div className="pt-6">
                        <h2 className="mb-2 text-base font-bold text-primary-black">
                            This course includes:
                        </h2>
                        <ul>
                            <li>
                                <div className="flex items-center py-1 text-sm">
                                    <div>
                                        <AiOutlineYoutube />
                                    </div>
                                    <div className="ml-4">
                                        {convertSecondsToHoursMinutes(
                                            totalDuration,
                                        )}{" "}
                                        on-demand video
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="flex items-center py-1 text-sm">
                                    <div>
                                        <IoTimeOutline />
                                    </div>
                                    <div className="ml-4">
                                        Full timelife access
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
}
