import { IoIosStar } from "react-icons/io";
import { PiCertificateFill, PiEyedropperSampleThin } from "react-icons/pi";
import { FaUserGroup } from "react-icons/fa6";
import { Link } from "react-router-dom";
import { AiFillPlayCircle } from "react-icons/ai";
import DescriptionToggle from "./DescriptionToggle";
import { StatAuthor } from "../../models";
import { Avatar, Flowbite } from "flowbite-react";
import { akaName, formatNumber } from "../../utils";
import type { CustomFlowbiteTheme } from "flowbite-react";
export interface IIntroInstructorProps {
    statAuthor: StatAuthor;
}

const customAvatarTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            size: {
                xl: "h-[112px] w-[112px]",
            },
        },
    },
};
export default function IntroInstructor(props: IIntroInstructorProps) {
    const { statAuthor } = props;
    return (
        <div>
            <h2 className="mb-4 text-2xl font-bold text-primary-black">
                Instructor
            </h2>
            <div>
                <div className="mb-1">
                    <Link
                        className="t text-[19px] font-bold text-primary-blue underline"
                        to={`/user/${statAuthor.user.id}`}
                    >
                        {statAuthor.user.name}
                    </Link>
                </div>
                <div className="mt-2 flex">
                    <Link className="mr-4" to="">
                        {statAuthor.user.avatar ? (
                            <Flowbite theme={{ theme: customAvatarTheme }}>
                                <Avatar
                                    img={statAuthor.user.avatar}
                                    alt="avatar"
                                    rounded
                                    size="xl"
                                />
                            </Flowbite>
                        ) : (
                            <Flowbite theme={{ theme: customAvatarTheme }}>
                                <Avatar
                                    placeholderInitials={akaName(
                                        statAuthor.user.name
                                            ? statAuthor.user.name
                                            : "",
                                    )}
                                    rounded
                                    size="xl"
                                />
                            </Flowbite>
                        )}
                    </Link>
                    <ul>
                        <li>
                            <div className="flex items-center py-1">
                                <IoIosStar />
                                <div className="ml-4 text-sm text-primary-black">
                                    {statAuthor.averageReview} Instructor Rating
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="flex items-center py-1">
                                <PiCertificateFill />
                                <div className="ml-4 text-sm text-primary-black">
                                    {formatNumber(statAuthor.countReview)}
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="flex items-center py-1">
                                <FaUserGroup />
                                <div className="ml-4 text-sm text-primary-black">
                                    {formatNumber(statAuthor.countStudent)}{" "}
                                    Students
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="flex items-center py-1">
                                <AiFillPlayCircle />
                                <div className="ml-4 text-sm text-primary-black">
                                    {statAuthor.totalCourse} Courses
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="mt-4">
                <DescriptionToggle
                    leastHeight="96"
                    text={
                        statAuthor.user.biography
                            ? statAuthor.user.biography
                            : ""
                    }
                />
            </div>
        </div>
    );
}
