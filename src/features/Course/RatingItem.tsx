import { Avatar, CustomFlowbiteTheme, Flowbite } from "flowbite-react";
import * as React from "react";
import ReviewStar from "../../components/ui/core/ReviewStar";
import { LearningReview } from "../../models";
import { akaName, formatDistanceToNowTime } from "../../utils";

const customTheme: CustomFlowbiteTheme = {
    avatar: {
        root: {
            initials: {
                base: "bg-primary-black flex justify-center items-center",
                text: "text-white font-bold",
            },
        },
    },
};

export interface IRatingItemProps {
    review: LearningReview;
    handleSetReviewListModal: (command: boolean) => void;
}

export default function RatingItem(props: IRatingItemProps) {
    const { review, handleSetReviewListModal } = props;
    const [isShowButtonMore, setIsShowButtonMore] = React.useState(false);
    const moreBtnContainer = React.useRef<HTMLDivElement>(null);

    React.useEffect(() => {
        if (moreBtnContainer.current) {
            if (moreBtnContainer.current.clientHeight >= 120) {
                setIsShowButtonMore(true);
            }
        }
    }, []);

    return (
        <div className="w-[47%] border-t border-primary-hover-gray pb-8 pt-6 odd:mr-4 even:ml-4">
            <div className="mb-4">
                <div className="mb-4 flex">
                    <Flowbite theme={{ theme: customTheme }}>
                        {review.user?.avatar ? (
                            <Avatar
                                img={review.user.avatar}
                                className="mr-4"
                                rounded
                            />
                        ) : (
                            <Avatar
                                placeholderInitials={akaName(review.user.name)}
                                className="mr-4"
                                rounded
                            />
                        )}
                    </Flowbite>
                    <div className="">
                        <p className="text-base font-bold text-primary-black">
                            {review.user.name}
                        </p>
                        <div className="mt-1 flex items-center">
                            <ReviewStar
                                color="#f69c08"
                                number={review.starCount ? review.starCount : 0}
                                size={3}
                            />
                            <div className="ml-1 text-xs font-bold text-primary-gray">
                                {formatDistanceToNowTime(review.createdAt)}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="font-base max-h-[120px] overflow-hidden overflow-ellipsis">
                    <div ref={moreBtnContainer} className="">
                        {review.comment}
                    </div>
                </div>
                {isShowButtonMore && (
                    <button
                        onClick={() => handleSetReviewListModal(true)}
                        className="h-10"
                    >
                        <span className="text-sm font-bold text-primary-black underline">
                            Show more
                        </span>
                    </button>
                )}
            </div>
        </div>
    );
}
