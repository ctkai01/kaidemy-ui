import { useEffect, useRef, useState } from "react";
import { IoIosArrowDown, IoIosArrowUp } from "react-icons/io";

export interface IDescriptionToggleProps {
    leastHeight: "96" | "176" | "224";
    text: string;
}

export default function DescriptionToggle(props: IDescriptionToggleProps) {
    const { leastHeight, text } = props;
    const [isShowButtonBiography, setIsShowButtonBiography] = useState(false);
    const biographyContainer = useRef<HTMLDivElement>(null);
    const [isToggleShowMore, setIsToggleShowMore] = useState(false);
    const [maxHeight, setMaxHeight] = useState("");

    useEffect(() => {
        let maxH;
        if (leastHeight === "96") {
            maxH = 24;
        }

        if (leastHeight === "176") {
            maxH = 44;
        }

        if (leastHeight === "224") {
            maxH = 56;
        }
        const mh = `max-h-${maxH}`;
        setMaxHeight(mh);
        if (biographyContainer.current) {
            if (biographyContainer.current.clientHeight > +leastHeight) {
                setIsShowButtonBiography(true);
            }
        }
    });

    const handleToggleMoreButton = () => {
        setIsToggleShowMore((isToggleShowMore) => !isToggleShowMore);
    };


    return (
        <div>
            <div
                className={`${
                    isShowButtonBiography && !isToggleShowMore
                        ? "custom-mask"
                        : ""
                } ${
                    isToggleShowMore ? "" : `${maxHeight}  overflow-hidden`
                }  text-base text-primary-black`}
            >
                <div
                    ref={biographyContainer}
                    className="text-sm text-primary-black"
                >
                    <div
                        dangerouslySetInnerHTML={{
                            __html: text,
                        }}
                    ></div>
                </div>
            </div>
            {isShowButtonBiography && (
                <button
                    onClick={handleToggleMoreButton}
                    className="flex h-[40px] items-center text-sm font-bold text-primary-blue"
                >
                    <span className="mr-2">
                        {isToggleShowMore ? "Show less" : "Show more"}
                    </span>
                    {isToggleShowMore ? <IoIosArrowUp /> : <IoIosArrowDown />}
                </button>
            )}
        </div>
    );
}
