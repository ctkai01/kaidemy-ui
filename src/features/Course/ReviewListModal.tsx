import {
    Button,
    CustomFlowbiteTheme,
    Flowbite,
    Modal,
    Rating,
} from "flowbite-react";
import { BsStarFill } from "react-icons/bs";
import { GoDotFill } from "react-icons/go";
import {
    LearningReview,
    OverallReviewCourse,
    PaginationInfo,
} from "../../models";
import RatingItemModal from "./RatingItemModal";

export interface IReviewListModalProps {
    openReviewListModal: boolean;
    currentPage: number;
    infoPagination: PaginationInfo;
    learningReviews: LearningReview[];
    overallReview: OverallReviewCourse;
    handleNextPage: () => void;
    handleSetReviewListModal: (command: boolean) => void;
}

const customTheme: CustomFlowbiteTheme = {
    modal: {
        // content: {
        //     inner: "bg-[#2d2f31]",
        // },
        header: {
            base: "flex items-start justify-between rounded-t dark:border-gray-600 p-5  shadow-md",
        },
        // body: {
        //     base: " flex-1 overflow-auto",
        // },
        // footer: {
        //     popup: "",
        // },
        // root: {
        //     sizes: {
        //         "2xl": "max-w-[600px]",
        //     },
        // },
    },
};

export default function ReviewListModal(props: IReviewListModalProps) {
    const {
        overallReview,
        learningReviews,
        infoPagination,
        currentPage,
        handleSetReviewListModal,
        handleNextPage,
        openReviewListModal,
    } = props;
    return (
        <Flowbite theme={{ theme: customTheme }}>
            <Modal
                dismissible
                size={"4xl"}
                show={openReviewListModal}
                onClose={() => handleSetReviewListModal(false)}
            >
                <Modal.Header>
                    <div className="flex items-center">
                        <BsStarFill
                            className={`mr-[4px] h-4 w-4 text-[#f69c08]`}
                        />
                        <span className="text-2xl font-bold text-primary-black">
                            {overallReview.averageReview} course rating
                        </span>
                        <GoDotFill className="mx-1 text-[#6a6f73]" />
                        <span className="text-2xl font-bold text-primary-black">
                            {overallReview.totalReview} ratings
                        </span>
                    </div>
                </Modal.Header>
                <Modal.Body>
                    <div className="flex">
                        <div className="mr-12">
                            <div className="mb-6 w-[220px]">
                                <Rating.Advanced
                                    percentFilled={
                                        overallReview.overall.fiveStar
                                    }
                                    className="mb-2"
                                >
                                    5 star
                                </Rating.Advanced>
                                <Rating.Advanced
                                    percentFilled={
                                        overallReview.overall.fourStar
                                    }
                                    className="mb-2"
                                >
                                    4 star
                                </Rating.Advanced>
                                <Rating.Advanced
                                    percentFilled={
                                        overallReview.overall.threeStar
                                    }
                                    className="mb-2"
                                >
                                    3 star
                                </Rating.Advanced>
                                <Rating.Advanced
                                    percentFilled={
                                        overallReview.overall.twoStar
                                    }
                                    className="mb-2"
                                >
                                    2 star
                                </Rating.Advanced>
                                <Rating.Advanced
                                    percentFilled={
                                        overallReview.overall.oneStar
                                    }
                                >
                                    1 star
                                </Rating.Advanced>
                            </div>
                        </div>

                        <div>
                            {learningReviews &&
                                learningReviews.map((review) => (
                                    <>
                                        <RatingItemModal review={review} />
                                    </>
                                ))}
                            {infoPagination.totalPage > 1 &&
                                currentPage < infoPagination.totalPage && (
                                    <Button
                                        onClick={() => handleNextPage()}
                                        className="w-full"
                                        outline
                                    >
                                        Show more reviews
                                    </Button>
                                )}
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </Flowbite>
    );
}
