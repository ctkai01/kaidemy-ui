import { Button } from "flowbite-react";
import * as React from "react";
import { BsStarFill } from "react-icons/bs";
import { GoDotFill } from "react-icons/go";
import {
    LearningReview,
    OverallReviewCourse,
    PaginationInfo,
    StatAuthor,
} from "../../models";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import RatingItem from "./RatingItem";
import ReviewListModal from "./ReviewListModal";

export interface IRatingSectionProps {
    statAuthor: StatAuthor;
    courseID: number;
    openReviewListModal: boolean;
    handleSetReviewListModal: (command: boolean) => void;
}

const SIZE_PAGE = 4;

export default function RatingSection(props: IRatingSectionProps) {
    const {
        statAuthor,
        courseID,
        openReviewListModal,
        handleSetReviewListModal,
    } = props;
    const [infoPagination, setInfoPagination] = React.useState<PaginationInfo>({
        totalItem: 0,
        totalPage: 0,
        size: 0,
    });
    const [learningReviews, getLearningReviews] = React.useState<
        LearningReview[]
    >([]);

    const [overallReview, setOverallReview] =
        React.useState<OverallReviewCourse>({
            averageReview: 0,
            overall: {
                fiveStar: 0,
                fourStar: 0,
                threeStar: 0,
                twoStar: 0,
                oneStar: 0,
            },
            totalReview: 0,
        });

    const [currentPage, setCurrentPage] = React.useState(1);

    React.useEffect(() => {
        const fetchReviews = async () => {
            try {
                const data = await CourseServiceApi.getReviews(
                    courseID,
                    currentPage,
                    SIZE_PAGE,
                    null,
                    null,
                );
                setInfoPagination({
                    totalPage: data.meta.pageCount,
                    totalItem: data.meta.itemCount,
                    size: data.meta.size,
                });
                if (!data.item) {
                    getLearningReviews([]);
                } else {
                    getLearningReviews((learningReview) => [
                        ...learningReview,
                        ...data.item,
                    ]);
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchReviews();
    }, [currentPage]);

    React.useEffect(() => {
        const fetchOverallOverview = async () => {
            try {
                const data =
                    await CourseServiceApi.getOverallReviewByCourseID(courseID);
                setOverallReview({
                    averageReview: data.averageReview,
                    totalReview: data.totalReview,
                    overall: data.overall,
                });
            } catch (e) {
                console.log(e);
            }
        };
        fetchOverallOverview();
    }, []);
    const handleNextPage = () => {
        setCurrentPage((currentPage) => currentPage + 1);
    };

    return (
        <div>
            <ReviewListModal
                currentPage={currentPage}
                infoPagination={infoPagination}
                learningReviews={learningReviews}
                overallReview={overallReview}
                openReviewListModal={openReviewListModal}
                handleNextPage={handleNextPage}
                handleSetReviewListModal={handleSetReviewListModal}
            />
            <div className="mb-6 flex items-center">
                <BsStarFill className={`mr-[4px] h-4 w-4 text-[#f69c08]`} />
                <span className="text-2xl font-bold text-primary-black">
                    {overallReview.averageReview} course ratings
                </span>
                <GoDotFill className="mx-1 text-[#6a6f73]" />
                <span className="text-2xl font-bold text-primary-black">
                    {overallReview.totalReview} ratings
                </span>
            </div>
            <div className="flex flex-wrap">
                {learningReviews &&
                    learningReviews.map((review) => (
                        <RatingItem
                            review={review}
                            handleSetReviewListModal={handleSetReviewListModal}
                        />
                    ))}

                {/* <RatingItem
                    handleSetReviewListModal={handleSetReviewListModal}
                />
                <RatingItem
                    handleSetReviewListModal={handleSetReviewListModal}
                />
                <RatingItem
                    handleSetReviewListModal={handleSetReviewListModal}
                /> */}
            </div>
            <Button onClick={() => handleSetReviewListModal(true)}>
                Show all reviews
            </Button>
        </div>
    );
}
