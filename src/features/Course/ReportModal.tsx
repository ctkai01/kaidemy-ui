import { Button, Label, Modal, Select, Textarea } from "flowbite-react";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Course, IssueType } from "../../models";
import { CategoryServiceApi } from "../../services/api/categoryServiceApi";
import { CourseServiceApi } from "../../services/api/courseServiceApi";

export interface IReportModalProps {
    openReportModal: boolean;
    idCourse: number;
    handleSetReportModal: (command: boolean) => void;
}

export default function ReportModal(props: IReportModalProps) {
    const { openReportModal, idCourse, handleSetReportModal } = props;
    const [issueTypes, setIssueTypes] = useState<IssueType[]>([]);
    const [issueDetail, setIssueDetail] = useState("");
    const [issueTypeSelect, setIssueTypeSelect] = useState("");
    useEffect(() => {
        const fetchIssueType = async () => {
            try {
                const data = await CategoryServiceApi.getIssueTypes();
                setIssueTypes(data.item);
                if (data.item.length) {
                    setIssueTypeSelect(data.item[0].id);
                }
            } catch (e) {
                console.log(e);
            }
        };
        fetchIssueType();
    }, []);

    const handleSubmitReport = async () => {

        try {
            const body = {
                courseID: idCourse,
                issueTypeID: issueTypeSelect,
                description: issueDetail,
            };
           
            await CourseServiceApi.reportCourse(body);
            toast(
                <div className="font-bold">Reported course successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
            setIssueDetail("");
            setIssueTypeSelect("");
            handleSetReportModal(false);
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Reported course failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <Modal
            dismissible
            show={openReportModal}
            onClose={() => handleSetReportModal(false)}
        >
            <Modal.Header>Report</Modal.Header>
            <Modal.Body>
                <div className="space-y-6">
                    <div className="max-w-md">
                        <div className="mb-2 block">
                            <Label
                                htmlFor="countries"
                                value="Issue types"
                            />
                        </div>
                        <Select
                            onChange={(e) => setIssueTypeSelect(e.target.value)}
                            id="countries"
                            required
                        >
                            {issueTypes.map((item) => (
                                <option key={item.id} value={item.id}>
                                    {item.name}
                                </option>
                            ))}
                        </Select>
                    </div>
                </div>
                <div className="mt-4 max-w-md">
                    <div className="mb-2 block">
                        <Label htmlFor="comment" value="Comment" />
                    </div>
                    <Textarea
                        id="comment"
                        value={issueDetail}
                        onChange={(e) => setIssueDetail(e.target.value)}
                        //   placeholder="Leave a comment..."
                        required
                        rows={4}
                    />
                </div>
            </Modal.Body>
            <Modal.Footer>
                <div className="flex w-full justify-end gap-2">
                    <Button
                        color="gray"
                        onClick={() => handleSetReportModal(false)}
                    >
                        Cancel
                    </Button>
                    <Button
                        disabled={issueDetail ? false : true}
                        onClick={() => handleSubmitReport()}
                    >
                        Submit
                    </Button>
                </div>
            </Modal.Footer>
        </Modal>
    );
}
