import { Button } from "flowbite-react";
import { useState } from "react";
import { TiTick } from "react-icons/ti";
import { Link } from "react-router-dom";
import { CourseShow, StatAuthor } from "../../models";
import CourseContent from "./CourseContent";
import IntroInstructor from "./IntroInstructor";
import RatingSection from "./RatingSection";
import SameCourse from "./SameCourse";
import StudentAlsoBought from "./StudentAlsoBought";

export interface ICourseDetailBodyProps {
    course: CourseShow;
    totalDuration: number;
    totalLecture: number;
    totalSection: number;
    statAuthor: StatAuthor | null;
    openReviewListModal: boolean;
    handleSetReviewListModal: (command: boolean) => void;
    handleShowModalPreviewWithVideoID: (videoID: string) => void;
    handleSetReportModal: (command: boolean) => void;
}

export default function CourseDetailBody(props: ICourseDetailBodyProps) {
    const {
        course,
        totalDuration,
        totalLecture,
        statAuthor,
        totalSection,
        openReviewListModal,
        handleShowModalPreviewWithVideoID,
        handleSetReviewListModal,
        handleSetReportModal,
    } = props;
    const [isCollapseAll, setIsCollapseAll] = useState(false);
    const [isInitAccordion, setIsInitAccordion] = useState(false);

    const toggleAccordion = () => {
        setIsCollapseAll((isCollapseAll) => !isCollapseAll);
        setIsInitAccordion(true);
    };
    return (
        <div>
            <div className="mb-8 border border-primary-hover-gray pb-4 pt-6">
                <h2 className="tex-white mx-6 mb-4 text-2xl font-bold">
                    What you'll learn
                </h2>
                <div className="mx-6">
                    <ul className="flex flex-wrap justify-between">
                        {course.outComes?.map((item, index) => (
                            <li key={index} className="mb-5 w-[47%]">
                                <div className="flex items-start py-1">
                                    <TiTick />
                                    <div className="ml-4 flex-1">
                                        <span className="line-clamp-4 block text-sm text-primary-black">
                                            {item}
                                        </span>
                                    </div>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
            <CourseContent
                totalDuration={totalDuration}
                totalLecture={totalLecture}
                totalSection={totalSection}
                course={course}
                handleShowModalPreviewWithVideoID={
                    handleShowModalPreviewWithVideoID
                }
                toggleAccordion={toggleAccordion}
                isCollapseAll={isCollapseAll}
                isInitAccordion={isInitAccordion}
            />
            {/* <div className="mb-8">
                <StudentAlsoBought />
            </div> */}
            <div className="mb-8">
                {statAuthor && <IntroInstructor statAuthor={statAuthor} />}
            </div>
            <div className="mb-8">
                {statAuthor && (
                    <RatingSection
                        openReviewListModal={openReviewListModal}
                        statAuthor={statAuthor}
                        courseID={course.id}
                        handleSetReviewListModal={handleSetReviewListModal}
                    />
                )}
            </div>
            <div className="mb-8">
                {/* SameCourse */}
                {/* <h2 className="mb-4 text-2xl font-bold text-primary-black">
                    Các khóa học khác của
                    <Link className="text-primary-blue" to="">
                        {" "}
                        Academind by Maximilian Schwarzmüller
                    </Link>
                </h2> */}
                <div className="flex">
                    {/* <div className="w-1/3">
                        <SameCourse />
                    </div>
                    <div className="w-1/3">
                        <SameCourse />
                    </div>
                    <div className="w-1/3">
                        <SameCourse />
                    </div> */}
                </div>
            </div>
            <Button
                onClick={() => handleSetReportModal(true)}
                color="dark"
                className="w-full"
            >
                Report
            </Button>
        </div>
    );
}
