export interface ICoursePanelProps {
    title: string;
    numberLecture: number;
    time: string;
}

export default function CoursePanel(props: ICoursePanelProps) {
    const { numberLecture, time, title } = props;
    return (
        <div className="flex">
            <span className="flex-1 text-base font-bold text-primary-black">
                {title}
            </span>
            <span className="ml-6 text-sm text-primary-black">
                {numberLecture} lectures • <span>{time}</span>
            </span>
        </div>
    );
}
