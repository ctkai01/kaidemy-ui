import StudentBoughtCourse from "./StudentBoughtCourse";

export interface IStudentAlsoBoughtProps {}

export default function StudentAlsoBought(props: IStudentAlsoBoughtProps) {
    return (
        <div>
            <h2 className="text-2xl font-bold text-primary-black">
                Student also bought
            </h2>
            <div>
                {Array.from({ length: 2 }).map((_, i) => (
                    <StudentBoughtCourse key={i} />
                ))}
            </div>
        </div>
    );
}
