import { Button } from "flowbite-react";
import { Link } from "react-router-dom";
import { Cart } from "../../models/cart";
import CartItemShow from "./CartItem.tsx";

export interface ICartListProps {
    cart: Cart | null;
}

export default function CartList(props: ICartListProps) {
    const { cart } = props;
    return (
        <div>
            <h3 className="mb-2 text-base font-bold text-primary-black">
                {cart?.cartItems.length} Courses in Cart
            </h3>
            {cart && cart.cartItems.length === 0 && (
                <div className="mb-[30px] flex flex-col items-center justify-center border border-[#d1d7dc] p-6">
                    <img
                        src="https://s.udemycdn.com/browse_components/flyout/empty-shopping-cart-v2.jpg"
                        className="mb-6"
                    />
                    <p className="mb-6 text-base text-primary-black">
                        Your cart is empty. Keep shopping to find a course!
                    </p>
                    <Link to="/">
                        <Button>Keep shopping</Button>
                    </Link>
                </div>
            )}
            {cart &&
                cart.cartItems.map((cartItem) => (
                    <CartItemShow
                        key={cartItem.course.id}
                        cartItem={cartItem}
                    />
                ))}
        </div>
    );
}
