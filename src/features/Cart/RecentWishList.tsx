import { Learning } from "../../models";
import CartWishListItem from "./CartWishListItem";

export interface IRecentWishListProps {
    wishList: Learning[];
}

export default function RecentWishList(props: IRecentWishListProps) {
    const { wishList } = props;
    return (
        <div>
            <h3 className="mb-2 text-base font-bold text-primary-black">
                Recently Wishlist
            </h3>
            {wishList.map((item) => (
                <CartWishListItem key={item.id} item={item} />
            ))}
        </div>
    );
}
