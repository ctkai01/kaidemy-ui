import { Link } from "react-router-dom";
import BestSeller from "../../components/ui/core/BestSeller";
import ReviewStar from "../../components/ui/core/ReviewStar";
import {
    formatNumberWithCommas,
    convertSecondsToHoursMinutes,
} from "../../utils";
import { GoDotFill } from "react-icons/go";
import { BsCurrencyDollar } from "react-icons/bs";
import { Learning } from "../../models";
import React from "react";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../../constants";
import { PaymentServiceApi } from "../../services/api/paymentServiceApi";
import { addCartItem } from "../../services/state/redux/cartSlide";
import { toast } from "react-toastify";
import { removeWishList } from "../../services/state/redux/wishListSlide";
import { useAppDispatch } from "../../hooks/redux-hook";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
export interface ICartWishListItemProps {
    item: Learning;
}

export default function CartWishListItem(props: ICartWishListItemProps) {
    const { item } = props;
    const dispatch = useAppDispatch();

    // const { totalDuration, totalLecture } = React.useMemo(() => {
    //     let totalDuration = 0;
    //     let totalLecture = 0;
    //     if (item.course.curriculums) {
    //         item.course.curriculums.forEach((curriculum) => {
    //             if (curriculum.lectures) {
    //                 totalLecture += curriculum.lectures.length;
    //                 curriculum.lectures.forEach((lecture) => {
    //                     if (lecture.type === LECTURE_TYPE) {
    //                         if (lecture.assets) {
    //                             lecture.assets.forEach((asset) => {
    //                                 if (
    //                                     asset.type === LECTURE_WATCH_ASSET_TYPE
    //                                 ) {
    //                                     totalDuration += asset.duration;
    //                                 }
    //                             });
    //                         }
    //                     }
    //                 });
    //             }
    //         });
    //     }
    //     return { totalDuration, totalLecture };
    // }, []);

    const handleRemoveWishList = async () => {
        try {
            await CourseServiceApi.removeWishList(item.id);
            dispatch(removeWishList(item.id));
            toast(
                <div className="font-bold">
                    Removed from Wishlist successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(
                <div className="font-bold">Removed from Wishlist failed!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "error",
                    theme: "colored",
                },
            );
        }
    };

    const handleMoveCart = async () => {
        try {
            const dataAddCartItem = await PaymentServiceApi.addCartItem(
                item.course.id,
            );
            dispatch(addCartItem(dataAddCartItem));

            //Remove
            await CourseServiceApi.removeWishList(item.id);
            dispatch(removeWishList(item.id));

            toast(
                <div className="font-bold">Added to cart successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            console.log(e);
            toast(<div className="font-bold">Added to cart failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="border-t border-primary-gray py-4">
            <div className="relative flex">
                <div>
                    <div className="mr-4 border border-primary-gray">
                        <img
                            className="w-[120px]"
                            // className="h-[68px] w-[120px]"
                            src={
                                item.course.image
                                    ? item.course.image
                                    : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                            }
                        />
                    </div>
                </div>

                <div>
                    <h3 className="-mt-1 line-clamp-2 text-base font-bold text-primary-black">
                        <Link
                            className="after:absolute after:left-0 after:top-0 after:block after:h-full after:w-full after:content-['']"
                            to={`/course/${item.courseID}`}
                        >
                            {item.course.title}
                        </Link>
                    </h3>
                    <span className="mt-1 text-xs text-primary-gray">
                        {item.course.author.name}
                    </span>
                    <div className="mt-2 flex">
                        {/* <div className="mr-2">
                            <BestSeller />
                        </div> */}
                        <div className="flex items-center">
                            <span className="mr-2 text-sm font-bold text-[#4d3105]">
                                {item.averageReview ? item.averageReview : 0}
                            </span>
                            <ReviewStar
                                number={
                                    item.averageReview ? item.averageReview : 0
                                }
                            />
                            <span className="text-xs text-primary-gray">
                                (
                                {formatNumberWithCommas(
                                    item.countReview ? item.countReview : 0,
                                )}{" "}
                                reviews)
                            </span>
                        </div>
                    </div>
                    <div className="mt-2 flex items-center">
                        <span className="text-xs text-primary-gray">
                            {convertSecondsToHoursMinutes(item.course.duration)}
                        </span>
                        <GoDotFill className="ml-1 mr-1 h-3 w-3 text-primary-gray" />
                        <span className="text-xs text-primary-gray">
                            {item.lectureCount} lectures
                            {/* {totalLecture} bài học */}
                        </span>
                        <GoDotFill className="ml-1 mr-1 h-3 w-3 text-primary-gray" />
                        <span className="text-xs text-primary-gray">
                            {item.course.level.name}
                        </span>
                    </div>
                </div>
                <div className="flex flex-1 flex-col items-end pl-6">
                    <button
                        className="relative w-fit"
                        onClick={() => handleRemoveWishList()}
                    >
                        <span className="text-sm text-primary-blue">
                            Remove
                        </span>
                    </button>
                    <button
                        className="relative w-fit"
                        onClick={() => handleMoveCart()}
                    >
                        <span className="text-sm text-primary-blue">
                            Move to cart
                        </span>
                    </button>
                    <button></button>
                </div>
                <div className="pl-12">
                    <div className="flex items-center text-base font-bold text-primary-blue">
                        <BsCurrencyDollar />
                        {item.course.price.value}
                    </div>
                </div>
            </div>
        </div>
    );
}
