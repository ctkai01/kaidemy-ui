import { Link } from "react-router-dom";
import BestSeller from "../../components/ui/core/BestSeller";
import ReviewStar from "../../components/ui/core/ReviewStar";
import {
    convertSecondsToHoursMinutes,
    formatNumberWithCommas,
} from "../../utils";
import { GoDotFill } from "react-icons/go";
import { BsCurrencyDollar, BsDisc } from "react-icons/bs";
import { CartItem } from "../../models/cart";
import React from "react";
import { LECTURE_TYPE, LECTURE_WATCH_ASSET_TYPE } from "../../constants";
import { toast } from "react-toastify";
import { PaymentServiceApi } from "../../services/api/paymentServiceApi";
import { useAppDispatch } from "../../hooks/redux-hook";
import { removeCartItem } from "../../services/state/redux/cartSlide";
import { CourseServiceApi } from "../../services/api/courseServiceApi";
import { addWishList } from "../../services/state/redux/wishListSlide";
export interface ICartItemShowProps {
    cartItem: CartItem;
}

export default function CartItemShow(props: ICartItemShowProps) {
    const { cartItem } = props;
    const dispatch = useAppDispatch();
    // const { totalDuration, totalLecture } = React.useMemo(() => {
    //     let totalDuration = 0;
    //     let totalLecture = 0;
    //     if (cartItem.course.Curriculums) {
    //         cartItem.course.Curriculums.forEach((curriculum) => {
    //             if (curriculum.Lectures) {
    //                 totalLecture += curriculum.Lectures.length;
    //                 curriculum.Lectures.forEach((lecture) => {
    //                     if (lecture.Type === LECTURE_TYPE) {
    //                         if (lecture.Assets) {
    //                             lecture.Assets.forEach((asset) => {
    //                                 if (
    //                                     asset.Type === LECTURE_WATCH_ASSET_TYPE
    //                                 ) {
    //                                     console.log("What: ", asset.Duration);
    //                                     console.log("lecture: ", lecture.ID);
    //                                     console.log(
    //                                         "curriculum: ",
    //                                         curriculum.ID,
    //                                     );
    //                                     totalDuration += asset.Duration;
    //                                 }
    //                             });
    //                         }
    //                     }
    //                 });
    //             }
    //         });
    //     }
    //     return { totalDuration, totalLecture };
    // }, []);

    // console.log("totalDuration: ", totalDuration);
    const handleRemoveCartItem = async () => {
        try {
            await PaymentServiceApi.removeCartItem(cartItem.course.id);
            dispatch(removeCartItem(cartItem.course.id));
            toast(
                <div className="font-bold">Remove cart item successfully!</div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            toast(<div className="font-bold">Remove cart item failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };

    const handleMoveWishList = async () => {
        try {
            //Add wishlist
            const dataCreate = await CourseServiceApi.addWishList(
                cartItem.course.id,
            );
            dispatch(addWishList(dataCreate));

            //Remove cart
            await PaymentServiceApi.removeCartItem(cartItem.course.id);
            dispatch(removeCartItem(cartItem.course.id));

            toast(
                <div className="font-bold">
                    Moved to Wishlist successfully!
                </div>,
                {
                    draggable: false,
                    position: "top-right",
                    type: "success",
                    theme: "colored",
                },
            );
        } catch (e) {
            toast(<div className="font-bold">Moved to Wishlist failed!</div>, {
                draggable: false,
                position: "top-right",
                type: "error",
                theme: "colored",
            });
        }
    };
    return (
        <div className="border-t border-primary-gray py-4">
            <div className="relative flex">
                <div>
                    <div className="mr-4 border border-primary-gray">
                        <img
                            className="w-[120px]"
                            // className="h-[68px] w-[120px]"
                            src={
                                cartItem.course.image
                                    ? cartItem.course.image
                                    : "https://s.udemycdn.com/course/200_H/placeholder.jpg"
                            }
                        />
                    </div>
                </div>

                <div className="flex-1">
                    <h3 className="-mt-1 line-clamp-2 text-base font-bold text-primary-black">
                        <Link
                            className="after:absolute after:left-0 after:top-0 after:block after:h-full after:w-full after:content-['']"
                            to={`/course/${cartItem.course.id}`}
                        >
                            {cartItem.course.title}
                        </Link>
                    </h3>
                    <span className="mt-1 text-xs text-primary-gray">
                        {cartItem.course.user.name}
                    </span>
                    <div className="mt-2 flex">
                        {/* <div className="mr-2">
                            <BestSeller />
                        </div> */}
                        <div className="flex items-center">
                            <span className="mr-2 text-sm font-bold text-[#4d3105]">
                                {cartItem.course.averageReview
                                    ? cartItem.course.averageReview
                                    : 0}
                            </span>
                            <ReviewStar
                                number={
                                    cartItem.course.averageReview
                                        ? cartItem.course.averageReview
                                        : 0
                                }
                            />
                            <span className="text-xs text-primary-gray">
                                (
                                {formatNumberWithCommas(
                                    cartItem.course.countReview
                                        ? cartItem.course.countReview
                                        : 0,
                                )}{" "}
                                reviews)
                            </span>
                        </div>
                    </div>
                    <div className="mt-2 flex items-center">
                        <span className="text-xs text-primary-gray">
                            {convertSecondsToHoursMinutes(
                                cartItem.course.duration,
                            )}
                        </span>
                        <GoDotFill className="ml-1 mr-1 h-3 w-3 text-primary-gray" />
                        <span className="text-xs text-primary-gray">
                            {cartItem.course.lectureCount} lectures
                        </span>
                        <GoDotFill className="ml-1 mr-1 h-3 w-3 text-primary-gray" />
                        <span className="text-xs text-primary-gray">
                            {cartItem.course.level
                                ? cartItem.course.level.name
                                : "None"}
                        </span>
                    </div>
                </div>
                <div className="flex flex-1 flex-col items-end pl-6">
                    <button
                        onClick={() => handleRemoveCartItem()}
                        className="relative w-fit"
                    >
                        <span className="text-sm text-primary-blue">
                            Remove
                        </span>
                    </button>
                    <button
                        onClick={() => handleMoveWishList()}
                        className="relative w-fit"
                    >
                        <span className="text-sm text-primary-blue">
                            Move to Wishlist
                        </span>
                    </button>
                    <button></button>
                </div>
                <div className="pl-12">
                    <div className="flex items-center text-base font-bold text-primary-blue">
                        <BsCurrencyDollar />
                        {cartItem.course.price
                            ? cartItem.course.price.value
                            : 0}
                    </div>
                </div>
            </div>
        </div>
    );
}
