import { Button, Spinner } from "flowbite-react";
import * as React from "react";
import { BsCurrencyDollar } from "react-icons/bs";
import { Cart } from "../../models/cart";
import { PaymentServiceApi } from "../../services/api/paymentServiceApi";

export interface ICheckoutProps {
    cart: Cart;
}

export default function Checkout(props: ICheckoutProps) {
    const { cart } = props;
    const [loading, setLoading] = React.useState(false);
    const totalMoney = React.useMemo(() => {
        const total = cart.cartItems.reduce((total, cartItem) => {
            const price = cartItem.course.price
                ? cartItem.course.price.value
                : 0;
            return price + total;
        }, 0);
        return total;
    }, [cart]);

    const handleCheckout = async () => {
        try {
            setLoading(true);
            const dataRequestPayment = await PaymentServiceApi.requestPayment();
            window.open(dataRequestPayment, "_blank");
            setLoading(false);
        } catch (e) {
            console.log(e);
        }
    };
    return (
        <div className="ml-16 min-w-[300px]">
            <div>
                <div className="mb-2 text-base font-bold text-primary-gray">
                    Total:{" "}
                </div>
                <div className="flex items-center py-1 text-3xl font-bold text-primary-black">
                    <BsCurrencyDollar />
                    {totalMoney}
                </div>
                <div className="mt-2" onClick={() => handleCheckout()}>
                    <Button className="w-full font-bold">
                        {loading ? <Spinner /> : "Checkout"}
                    </Button>
                </div>
                <a target="_blank"></a>
            </div>
        </div>
    );
}
