export interface Cart {
    userID: number;
    cartItems: CartItem[];
}

// export interface AddCartItem {
//     user_id: number;
//     courseID: number;
// }

export interface CartItem {
    cartID: number;
    course: CartCourse;
}

export interface CartCourse {
    id: number;
    countReview?: number;
    averageReview?: number;
    image: string | null;
    level: CartLevel | null;
    price: CartPrice | null;
    title: string;
    subtitle: string;
    createdAt: Date;
    user: CartUser;
    description: string;
    duration: number;
    lectureCount: number;
}

export interface CartUser {
    id: number;
    name: string;
}

export interface CartLevel {
    id: number;
    name: string;
}

export interface CartPrice {
    id: number;
    tier: string;
    value: number;
}

export interface CartCurriculum {
    id: number;
    lectures: CartLecture[] | null;
}

export interface CartLecture {
    id: number;
    type: number;
    assets: CartAsset[] | null;
}

export interface CartAsset {
    id: number;
    type: number;
    duration: number;
}
